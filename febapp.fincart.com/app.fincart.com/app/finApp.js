﻿(function () {
    'use strict';
    angular.module('fincartApp',
        [
            //Third Party Modules
            'ngStorage',
            'ui.router',
            'ng.deviceDetector',
            'ngAnimate',
            'ngSanitize',
            'ngIdle',
            'ui.bootstrap',
            'ui.bootstrap.popover',
            '720kb.datepicker',
            'rzModule',
            'ui.mask',
            'ngMaterial',
            'oitozero.ngSweetAlert',
            'nvd3',

            //Fincart Modules
            'envModule',
            //'counter',
            'angucomplete-alt',
            'ngFileUpload',
            //'ui.toggle',
            //'questionModule',
        ]);
})();