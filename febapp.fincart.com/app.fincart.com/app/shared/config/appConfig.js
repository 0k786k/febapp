﻿(function (finApp) {
    'use strict';
    finApp.config(['KeepaliveProvider', 'IdleProvider', 'TitleProvider', '$mdDateLocaleProvider', function (KeepaliveProvider, IdleProvider, TitleProvider, $mdDateLocaleProvider) {
        TitleProvider.enabled(false);
        IdleProvider.idle(3 * 60);
        IdleProvider.timeout(3 * 60);
        KeepaliveProvider.interval(3 * 60);
        $mdDateLocaleProvider.formatDate = function (date) {
            return moment(date).format('YYYY-MM-DD');
        };
    }]);

    finApp.run(['$rootScope', '$state', 'Idle', 'Keepalive', '$mdDialog', 'oauthTokenFactory', 'userFactory', 'envService', 'oauthService', 'userService', 'constTrackEvents', 'constTrackDevice', function ($rootScope, $state, Idle, Keepalive, $mdDialog, oauthTokenFactory, userFactory, envService, oauthService, userService, constTrackEvents, constTrackDevice) {

        $rootScope.profilepercent = { startval: 0, endval: 0 };

        var run = function () {
            var req = new XMLHttpRequest();
            req.timeout = 5000;
            req.open('GET', envService.read('apiServerDateUrl'), true);
            req.send();
        }

        setInterval(run, 10000);

        $rootScope.$on('$stateChangeStart', function (evt, to, params) {
            if (to.redirectTo) {
                evt.preventDefault();
                $state.go(to.redirectTo, params, { location: 'replace' })
            }
        });

        $rootScope.$on('$stateChangeSuccess', function () { });

        $rootScope.navigate = function (dataslide) {
            $('html, body').animate({ scrollTop: $("#" + dataslide).offset().top }, 'fast');
        }

        $rootScope.startUserSession = function () {
            Idle.watch();
        };

        $rootScope.endUserSession = function () {
            $rootScope.userlogout();
            Idle.unwatch();
        };

        $rootScope.userlogout = function () {
            oauthService.logout().then(function () {
                Idle.unwatch();
                $rootScope.ClearAllLocalStorage();
                $state.go('login', { type: 'login' });
            });
        };

        $rootScope.resetUserSession = function () {
            Idle.unwatch();
            Idle.watch();
        };

        $rootScope.ClearAllLocalStorage = function () {

            //new
            userFactory.cleartotalcartDetails();
            userFactory.clearsipNotifyDetails();
            //
        };

        $rootScope.$on('IdleStart', function () {
            $mdDialog.show(
                 {
                     clickOutsideToClose: false,
                     template: '<md-dialog aria-label="Idle Start">' +
                                '<md-toolbar>' +
                                    '<div class="md-toolbar-tools">' +
                                        '<h2>You are Idle. Do Something!</h2>' +
                                    '</div>' +
                                '</md-toolbar>' +
                                '<md-dialog-content>' +
                                    '<div layout="row" idle-countdown="countdown" ng-init="countdown=180">' +
                                        '<div class="md-dialog-content">' +
                                        '<p>You will be logged out in {{countdown}} seconds.</p>' +
                                        '</div>' +
                                    '</div>' +
                                '</md-dialog-content>' +
                                '<md-dialog-actions layout="row">' +
                                    '<md-button class="btn btn-success btn-sm" ng-click="staylogin();">' +
                                    'Stay Log In' +
                                    '</md-button>' +
                                    '<span flex></span>' +
                                    '<md-button class="btn btn-danger btn-sm" ng-click="logout();">' +
                                    'Log Out' +
                                    '</md-button>' +
                                '</md-dialog-actions>' +
                            '</md-dialog>',
                     ariaLabel: 'loader',
                     controller: idlestartController
                 }).then(function () {
                     $rootScope.resetUserSession();
                 }, function () {
                     $rootScope.endUserSession();
                 });
            function idlestartController($scope, $mdDialog) {
                $scope.staylogin = function () {
                    $mdDialog.hide();
                }
                $scope.logout = function () {
                    $mdDialog.cancel();
                }
            }
        });

        $rootScope.$on('IdleEnd', function () { });

        $rootScope.$on('IdleTimeout', function () {
            $rootScope.endUserSession();
            $mdDialog.show(
                 {
                     clickOutsideToClose: true,
                     template: '<md-dialog aria-label="Idle Timeout">' +
                                '<md-toolbar>' +
                                    '<div class="md-toolbar-tools">' +
                                        '<h2>You have Timed Out!</h2>' +
                                    '</div>' +
                                '</md-toolbar>' +
                                '<md-dialog-content>' +
                                    '<div layout="row">' +
                                        '<div class="md-dialog-content">' +
                                        '<p>You were idle too long. You are logged out.' +
                                        '</div>' +
                                    '</div>' +
                                '</md-dialog-content>' +
                                '<md-dialog-actions layout="row" layout-align="center center">' +
                                    '<md-button class="btn btn-success btn-sm" ng-click="ok();">' +
                                    'OK' +
                                    '</md-button>' +
                                '</md-dialog-actions>' +
                            '</md-dialog>',
                     ariaLabel: 'loader',
                     controller: idleendController
                 }).then(function () { }, function () { });
            function idleendController($scope, $mdDialog) {
                $scope.ok = function () {
                    $mdDialog.cancel();
                }
            }
        });

        $rootScope.downloadAndroid = function () {
            userService.eventTracker('', constTrackEvents.trackEvents['ANDROID_DOWNLOAD_CLICK'], 'E', constTrackDevice.trackDevice['WEB_PLATFORM']).then(function () { });
            window.open('https://play.google.com/store/apps/details?id=com.nws.fincart', '_blank');
        };

        $rootScope.alerts = [];
        $rootScope.allGoals = [];
        $rootScope.isDisabledModalBtn = false;
        $rootScope.btnModel = {};
        $rootScope.disableNameModel = {};

        $rootScope.addAlert = function (msgtype, message, style) {
            $rootScope.alerts.push({ type: msgtype, msg: message, style: style });
        };

        $rootScope.closeAlert = function (index) {
            $rootScope.alerts.splice(index, 1);
        };

        $rootScope.showModelProgress = function () {

            $rootScope.processingSpinnerClass = "processingSpinnerModal";
            $rootScope.spinnerClass = "modal-spinner";
        }

        $rootScope.hideModelProgress = function () {

            $rootScope.processingSpinnerClass = "";
            $rootScope.spinnerClass = "";
        }


        $rootScope.showLoader = function () {
            $mdDialog.show(
                {
                    clickOutsideToClose: false,
                    template: '<md-dialog>' +
                              '  <md-dialog-content layout="row" layout-align="center center">' +
                              '    <img src="../../../../assets/img/finloader.gif"/>' +
                              '  </md-dialog-content>' +
                              '</md-dialog>',
                    ariaLabel: 'loader'

                }
            //  $mdDialog.alert()
            //    .parent(angular.element(document.querySelector('#popupContainer')))
            //    .clickOutsideToClose(false)
            //    //.title('This is an alert title')
            //    .htmlContent('<img src="../../../../assets/img/finloader.gif"/>')
            //    //.ariaLabel('Alert Dialog Demo')
            //    .ok('Got it!')
            //    .openFrom('#left')
            //    .closeTo(angular.element(document.querySelector('#right')))
            );
        };

        $rootScope.hideLoader = function () {
            $mdDialog.hide();
        };

        if (oauthTokenFactory.getUserToken().refresh_token && oauthTokenFactory.getisloggedin()) {
            oauthService.refreshUser(true);
        }
        else {
            oauthTokenFactory.destroyUserToken();
            oauthTokenFactory.setisloggedin(false);
        }

    }]);
})(angular.module('fincartApp'));