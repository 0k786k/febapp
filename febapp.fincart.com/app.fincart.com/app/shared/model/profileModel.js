﻿(function (finApp) {
    'use strict';
    finApp.factory('profileModel', [function () {
        var factory = {};

        factory.Profile = {
            profileID: null,
            InvestProfile: null,
            basicid: null
        }

        factory.parseProfile = function (data) {

            this.Profile.profileID = data.profileID;
            this.Profile.InvestProfile = data.InvestProfile;
            this.Profile.basicid = data.basicid;

            return this.Profile;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));