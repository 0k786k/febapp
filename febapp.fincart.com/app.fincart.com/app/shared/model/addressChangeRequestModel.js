﻿(function (finApp) {
    'use strict';
    finApp.factory('addressChangeRequestModel', [function () {
        var factory = {};

        factory.AddressChangeRequest = {
            basicid: null,
            address: null,
            city: null,
            state: null,
            pincode: null,
            country: null,
            devicetype: null,
            reason: null,
            panFile: null,
            aadhaarFile: null
        }

        factory.parseAddressChangeRequest = function (data) {

            this.AddressChangeRequest.basicid = data.basicid;
            this.AddressChangeRequest.address = data.address;
            this.AddressChangeRequest.city = data.city;
            this.AddressChangeRequest.state = data.state;
            this.AddressChangeRequest.pincode = data.pincode;
            this.AddressChangeRequest.country = data.country;
            this.AddressChangeRequest.devicetype = data.devicetype;
            this.AddressChangeRequest.reason = data.reason;
            this.AddressChangeRequest.panFile = data.panFile;
            this.AddressChangeRequest.aadhaarFile = data.aadhaarFile;

            return this.AddressChangeRequest;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));