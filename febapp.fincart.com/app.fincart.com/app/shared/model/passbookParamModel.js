﻿(function (finApp) {
    'use strict';
    finApp.factory('passbookParamModel', [function () {
        var factory = {};

        factory.PassbookParam = {
            basicid: null,
            trxntypeid: null,
            schemeid: null,
            goalcode: null,
            trxnstatus: null,
            openDate: null,
            closeDate: null,
            foliono: null
        }

        factory.parsePassbookParam = function (data) {

            this.PassbookParam.basicid = data.basicid;
            this.PassbookParam.trxntypeid = data.trxntypeid;
            this.PassbookParam.schemeid = data.schemeid;
            this.PassbookParam.goalcode = data.goalcode;
            this.PassbookParam.trxnstatus = data.trxnstatus;
            this.PassbookParam.openDate = data.openDate;
            this.PassbookParam.closeDate = data.closeDate;
            this.PassbookParam.foliono = data.foliono;

            return this.PassbookParam;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));