﻿(function (finApp) {
    'use strict';
    finApp.factory('pincodeDetailsModel', [function () {
        var factory = {};

        factory.PincodeDetails = {
            city: null,
            state: null
        }

        factory.parsePincodeDetails = function (data) {
            this.PincodeDetails.city = data.city;
            this.PincodeDetails.state = data.state;
            return this.PincodeDetails;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));