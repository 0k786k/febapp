﻿(function (finApp) {
    'use strict';
    finApp.factory('goalresultModel', [function () {
        var factory = {};

        factory.Goalresult = {
            time:null,
            getLumpsum: null,
            getSip: null,
            investLumpsum: null,
            investSip: null,
            PMT: null,
            ROR: null,
            RORL: null,
            Inflation: null
        }

        factory.parseGoalresult = function (data) {

            this.Goalresult.time = data.time;
            this.Goalresult.getLumpsum= data.getLumpsum;
            this.Goalresult.getSip= data.getSip;
            this.Goalresult.investLumpsum= data.investLumpsum;
            this.Goalresult.investSip= data.investSip;
            this.Goalresult.PMT= data.PMT;
            this.Goalresult.ROR = data.ROR;
            this.Goalresult.RORL = data.RORL;
            this.Goalresult.Inflation= data.Inflation;

            return this.Goalresult;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));