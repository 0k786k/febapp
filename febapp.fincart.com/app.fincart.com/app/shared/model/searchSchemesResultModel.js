﻿(function (finApp) {
    'use strict';
    finApp.factory('searchSchemesResultModel', [function () {
        var factory = {};

        factory.searchSchemesResult = {
            schemeId: null,
            schemeName: null,
            orgSchemeName: null,
            obj: null,
            subObj: null,
            status: null,
            aum: null,
            vendorName: null,
            rating: null,
            _1M: null,
            _3M: null,
            _1Y: null,
            _3Y: null,
            _5Y: null,
            minAmt: null,
            maxAmt: null,
            addminAmt: null,
            addMaxAmt: null,
            sipminAmt: null,
            sipmaxAmt: null,
            fundName: null,
            fundImg: null,
            fundId: null,
            goalName: null,
            goalCode: null,
            goalImg: null
        }

        factory.parseSearchSchemesResult = function (data) {

            this.searchSchemesResult.schemeId = data.schemeId;
            this.searchSchemesResult.schemeName = data.schemeName;
            this.searchSchemesResult.orgSchemeName = data.orgSchemeName;
            this.searchSchemesResult.obj = data.obj;
            this.searchSchemesResult.subObj = data.subObj;
            this.searchSchemesResult.status = data.status;
            this.searchSchemesResult.aum = data.aum;
            this.searchSchemesResult.vendorName = data.vendorName;
            this.searchSchemesResult.rating = data.rating;
            this.searchSchemesResult._1M = data._1M;
            this.searchSchemesResult._3M = data._3M;
            this.searchSchemesResult._1Y = data._1Y;
            this.searchSchemesResult._3Y = data._3Y;
            this.searchSchemesResult._5Y = data._5Y;
            this.searchSchemesResult.minAmt = data.minAmt;
            this.searchSchemesResult.maxAmt = data.maxAmt;
            this.searchSchemesResult.addminAmt = data.addminAmt;
            this.searchSchemesResult.addMaxAmt = data.addMaxAmt;
            this.searchSchemesResult.sipminAmt = data.sipminAmt;
            this.searchSchemesResult.sipmaxAmt = data.sipmaxAmt;
            this.searchSchemesResult.fundName= data.fundName;
            this.searchSchemesResult.fundImg = data.fundImg;
            this.searchSchemesResult.fundId = data.fundId;
            this.searchSchemesResult.goalName= data.goalName;
            this.searchSchemesResult.goalCode= data.goalCode;
            this.searchSchemesResult.goalImg = data.goalImg;

            return this.searchSchemesResult;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));