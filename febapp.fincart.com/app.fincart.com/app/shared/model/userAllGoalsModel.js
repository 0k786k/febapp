﻿(function (finApp) {
    'use strict';
    finApp.factory('UserAllGoalsModel', [function () {
        var factory = {};

        factory.UserAllGoals = {
            userGoalId: null,
            goalName: null,
            goalImg: null,
            investedAmt: null,
            currentAmt: null,
            goalPerc: null,
            getAmount: null,
            goalCode: null,
            duration: null,
            isDelete: null,
            typeCode: null,
            investAmount: null,
            trxntype: null
        }

        factory.parseUserAllGoals = function (data) {

            this.UserAllGoals.userGoalId = data.userGoalId;
            this.UserAllGoals.goalName = data.goalName;
            this.UserAllGoals.goalImg = data.goalImg;
            this.UserAllGoals.investedAmt = data.investedAmt;
            this.UserAllGoals.currentAmt = data.currentAmt;
            this.UserAllGoals.goalPerc = data.goalPerc;
            this.UserAllGoals.getAmount = data.getAmount;
            this.UserAllGoals.goalCode = data.goalCode;
            this.UserAllGoals.duration = data.duration;
            this.UserAllGoals.isDelete = data.isDelete;
            this.UserAllGoals.typeCode = data.typeCode;
            this.UserAllGoals.investAmount = data.investAmount;
            this.UserAllGoals.trxntype = data.trxntype;

            return this.UserAllGoals;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));