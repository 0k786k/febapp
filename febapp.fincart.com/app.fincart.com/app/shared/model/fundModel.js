﻿(function (finApp) {
    'use strict';
    finApp.factory('fundModel', [function () {
        var factory = {};

        factory.Fund = {
            fundid: null,
            fundName: null,
            fundImg: null
        }

        factory.parseFund = function (data) {
            this.Fund.fundid = data.fundid;
            this.Fund.fundName = data.fundName;
            this.Fund.fundImg = data.fundImg;
            return this.Fund;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));