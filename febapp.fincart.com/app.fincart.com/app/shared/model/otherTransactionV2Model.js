﻿(function (finApp) {
    'use strict';
    finApp.factory('otherTransactionV2Model', [function () {
        var factory = {};

        factory.OtherTransactionV2 = {
            basicID: null,
            ProfileID: null,
            purSchemeId: null,
            sellSchemeId: null,
            tranType: null,// SIP, STP, SWP, SWITCH, R
            PurFolioNo: null,
            SellFolioNo: null,
            Amount: null,
            No_of_Installment: null,
            MDate: null,
            userGoalId: null,
            Units: null,
            bankId: null,
            mandateId: null,
            startDate: null
        }

        factory.parseOtherTransactionV2 = function (data) {

            this.OtherTransactionV2.basicID = data.basicID;
            this.OtherTransactionV2.ProfileID = data.ProfileID;
            this.OtherTransactionV2.purSchemeId = data.purSchemeId;
            this.OtherTransactionV2.sellSchemeId = data.sellSchemeId;
            this.OtherTransactionV2.tranType = data.tranType;
            this.OtherTransactionV2.PurFolioNo = data.PurFolioNo;
            this.OtherTransactionV2.SellFolioNo = data.SellFolioNo;
            this.OtherTransactionV2.Amount = data.Amount;
            this.OtherTransactionV2.No_of_Installment = data.No_of_Installment;
            this.OtherTransactionV2.MDate = data.MDate;
            this.OtherTransactionV2.userGoalId = data.userGoalId;
            this.OtherTransactionV2.Units = data.Units;
            this.OtherTransactionV2.bankId = data.bankId;
            this.OtherTransactionV2.mandateId = data.mandateId;
            this.OtherTransactionV2.startDate = data.startDate;

            return this.OtherTransactionV2;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));