﻿(function (finApp) {
    'use strict';
    finApp.factory('investFullSummaryListModel', ['investFullSummaryModel', function (investFullSummaryModel) {
        var factory = {};

        factory.InvestFullSummaryList = {
            fullSummaryList: []
        }

        factory.parseInvestFullSummaryList = function (data) {

            this.InvestFullSummaryList.fullSummaryList = [];

            angular.forEach(data, function (value, key) {
                investFullSummaryModel.InvestFullSummary = value;
                this.InvestFullSummaryList.fullSummaryList.push(investFullSummaryModel.InvestFullSummary);
            }, this);

            return this.InvestFullSummaryList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));