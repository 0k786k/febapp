﻿(function (finApp) {
    'use strict';
    finApp.factory('investmentAccountsListModel', ['investmentAccountsModel', function (investmentAccountsModel) {
        var factory = {};

        factory.InvestmentAccountsList = {
            InvestmentAccounts: []
        }

        factory.parseInvestmentAccountsList = function (data) {

            this.InvestmentAccountsList.InvestmentAccounts = [];

            angular.forEach(data, function (value, key) {
                investmentAccountsModel.InvestmentAccounts = value;
                this.InvestmentAccountsList.InvestmentAccounts.push(investmentAccountsModel.InvestmentAccounts);
            }, this);

            return this.InvestmentAccountsList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));