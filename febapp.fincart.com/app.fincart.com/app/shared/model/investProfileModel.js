﻿(function (finApp) {
    'use strict';
    finApp.factory('investProfileModel', [function () {
        var factory = {};

        factory.InvestProfile = {
            FirstApplBasicId: null,
            SecondApplBasicId: null,
            ThirdApplBasicId: null,
            holdingMode: null
        }

        factory.parseInvestProfile = function (data) {

            this.InvestProfile.FirstApplBasicId = data.FirstApplBasicId;
            this.InvestProfile.SecondApplBasicId = data.SecondApplBasicId;
            this.InvestProfile.ThirdApplBasicId = data.ThirdApplBasicId;
            this.InvestProfile.holdingMode = data.holdingMode;

            return this.InvestProfile;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));