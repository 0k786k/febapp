﻿(function (finApp) {
    'use strict';
    finApp.factory('fincartPOISaveModel', [function () {
        var factory = {};

        factory.FincartPOISave = {
            basicid: null
        }

        factory.parseFincartPOISave = function (data) {

            this.FincartPOISave.basicid = data.basicid;

            return this.FincartPOISave;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));