﻿(function (finApp) {
    'use strict'; 
    finApp.factory('cafViewProfileResultModel', ['cafViewProfileModel', function (cafViewProfileModel) {
        var factory = {};

        factory.CafViewProfileResult = {
            basic_id: null,
            memberId: null,
            groupLeader: null,
            clientname: null,
            profilepic: null,
            pannumber: null,
            profilepercent: 0.0,
            totalInvstAccounts: 0,
            cafFailReason: null,
            kycFailReason: null,
            mandateFailReason: null,
            cafStatus: null,
            kycStatus: null,
            isCafComplete: null,
            isMemberBasic: null,
            isMemberAddrs: null,
            isMemberBank: null,
            isMemberNominee: null,
            isNomineeReq: null,
            isMemberFatca: null,
            isMemberMandate: null,
            mandateStatus: null,
            CafViewProfile: []
        }

        factory.parseCafViewProfileResult = function (data) {

            this.CafViewProfileResult.basic_id = data.basic_id;
            this.CafViewProfileResult.memberId = data.memberId;
            this.CafViewProfileResult.groupLeader = data.groupLeader;
            this.CafViewProfileResult.clientname = data.clientname;
            this.CafViewProfileResult.profilepic = data.profilepic;
            this.CafViewProfileResult.pannumber = data.pannumber;
            this.CafViewProfileResult.profilepercent = parseFloat(data.profilepercent);
            this.CafViewProfileResult.totalInvstAccounts = parseInt(data.totalInvstAccounts);
            this.CafViewProfileResult.cafFailReason = data.cafFailReason;
            this.CafViewProfileResult.kycFailReason = data.kycFailReason;
            this.CafViewProfileResult.mandateFailReason = data.mandateFailReason;
            this.CafViewProfileResult.cafStatus = data.cafStatus;
            this.CafViewProfileResult.kycStatus = data.kycStatus;
            this.CafViewProfileResult.isCafComplete = data.isCafComplete;
            this.CafViewProfileResult.isMemberBasic = data.isMemberBasic;
            this.CafViewProfileResult.isMemberAddrs = data.isMemberAddrs;
            this.CafViewProfileResult.isMemberBank = data.isMemberBank;
            this.CafViewProfileResult.isMemberNominee = data.isMemberNominee;
            this.CafViewProfileResult.isNomineeReq = data.isNomineeReq;
            this.CafViewProfileResult.isMemberFatca = data.isMemberFatca;
            this.CafViewProfileResult.isMemberMandate = data.isMemberMandate;
            this.CafViewProfileResult.mandateStatus = data.mandateStatus;

            this.CafViewProfileResult.CafViewProfile = []

            angular.forEach(data.CafViewProfile, function (value, key) {
                cafViewProfileModel.CafViewProfile = value;
                this.CafViewProfileResult.CafViewProfile.push(cafViewProfileModel.CafViewProfile);
            }, this);

            return this.CafViewProfileResult;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));