﻿(function (finApp) {
    'use strict';
    finApp.factory('searchSchemesModel', [function () {
        var factory = {};

        factory.SearchSchemes = {
            fundid: null,
            obj: null,
            subObj: null,
            schemeName: null,
            userGoalId: null
        }

        factory.parseSearchSchemes = function (data) {

            this.SearchSchemes.fundid = data.fundid;
            this.SearchSchemes.obj = data.obj;
            this.SearchSchemes.subObj = data.subObj;
            this.SearchSchemes.schemeName = data.schemeName;
            this.SearchSchemes.userGoalId = data.userGoalId;

            return this.SearchSchemes;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));