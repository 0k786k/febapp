﻿(function (finApp) {
    'use strict';
    finApp.factory('fundsWithCategoryModel', ['fundModel', 'objectiveModel', 'subobjectiveModel', function (fundModel,objectiveModel, subobjectiveModel) {
        var factory = {};

        factory.FundsWithCategory = {
            funds: [],
            obj: [],
            subObj: [],
            goalName: null,
            goalCode: null,
            typeCode: null,
            userGoalId: null,
            goalImg: null
        }

        factory.parseFundsWithCategory = function (data) {
            this.FundsWithCategory.funds = [];
            angular.forEach(data.funds, function (value, key) {
                fundModel.Fund=value;
                this.FundsWithCategory.funds.push(fundModel.Fund);
            }, this);

            this.FundsWithCategory.obj = [];
            angular.forEach(data.obj, function (value, key) {
                objectiveModel.Objective=value;
                this.FundsWithCategory.obj.push(objectiveModel.Objective);
            }, this);

            this.FundsWithCategory.subObj = [];
            angular.forEach(data.subObj, function (value, key) {
                subobjectiveModel.SubObjective=value
                this.FundsWithCategory.subObj.push(subobjectiveModel.SubObjective);
            }, this);


            this.FundsWithCategory.goalName=data.goalName;
            this.FundsWithCategory.goalCode = data.goalCode;
            this.FundsWithCategory.typeCode = data.typeCode;
            this.FundsWithCategory.userGoalId=data.userGoalId;
            this.FundsWithCategory.goalImg = data.goalImg;

            return this.FundsWithCategory;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));