﻿(function (finApp) {
    'use strict';
    finApp.factory('userCartCountModel', [function () {
        var factory = {};

        factory.UserCartCount = {
            lumpsumInCart: null,
            sipInCart: null,
            instasipInCart: null
        }

        factory.parseUserCartCount = function (data) {

            this.UserCartCount.lumpsumInCart = data.lumpsumInCart;
            this.UserCartCount.sipInCart = data.sipInCart;
            this.UserCartCount.instasipInCart = data.instasipInCart;

            return this.UserCartCount;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));