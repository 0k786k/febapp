﻿(function (finApp) {
    'use strict';
    finApp.factory('addToCartModel', [function () {
        var factory = {};

        factory.AddToCart = {
            basicID: null,
            ProfileID: null,
            purSchemeId: null,
            tranType: null,
            Amount: null,
            No_of_Installment: null,
            MDate: null,
            PurFolioNo: null,
            bankId: null,
            userGoalId: null,
            startDate: null
        }

        factory.parseAddToCart = function (data) {

            this.AddToCart.basicID = data.basicID;
            this.AddToCart.ProfileID = data.ProfileID;
            this.AddToCart.purSchemeId = data.purSchemeId;
            this.AddToCart.tranType = data.tranType;
            this.AddToCart.Amount = data.Amount;
            this.AddToCart.No_of_Installment = data.No_of_Installment;
            this.AddToCart.MDate = data.MDate;
            this.AddToCart.PurFolioNo = data.PurFolioNo;
            this.AddToCart.bankId = data.bankId;
            this.AddToCart.userGoalId = data.userGoalId;
            this.AddToCart.startDate = data.startDate;

            return this.AddToCart;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));