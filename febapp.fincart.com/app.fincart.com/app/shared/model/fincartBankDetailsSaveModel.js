﻿(function (finApp) {
    'use strict';
    finApp.factory('fincartBankDetailsSaveModel', [function () {
        var factory = {};

        factory.FincartBankDetailsSave = {
            basicid: null,
            accountnumber: null,
            bankaddress: null,
            bankname: null,
            branch: null,
            ifsc: null,
            micr: null,
            bankcity: null,
            nameasperbank: null,
            AccountType: null
        }

        factory.parseFincartBankDetailsSave = function (data) {

            this.FincartBankDetailsSave.basicid= data.basicid;
            this.FincartBankDetailsSave.accountnumber= data.accountnumber;
            this.FincartBankDetailsSave.bankaddress= data.bankaddress;
            this.FincartBankDetailsSave.bankname= data.bankname;
            this.FincartBankDetailsSave.branch= data.branch;
            this.FincartBankDetailsSave.ifsc = data.ifsc;
            this.FincartBankDetailsSave.micr= data.micr;
            this.FincartBankDetailsSave.bankcity= data.bankcity;
            this.FincartBankDetailsSave.nameasperbank= data.nameasperbank;
            this.FincartBankDetailsSave.AccountType= data.AccountType;

            return this.FincartBankDetailsSave;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));