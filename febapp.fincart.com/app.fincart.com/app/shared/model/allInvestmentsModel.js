﻿(function (finApp) {
    'use strict';
    finApp.factory('allInvestmentsModel', ['UserAllGoalsModel', function (UserAllGoalsModel) {
        var factory = {};

        factory.AllInvestments = {
            schemeName: null,
            schemeId: null,
            folioNo: null,
            balUnits: null,
            gain: null,
            currValue: null,
            investValue: null,
            CAGR: null,
            fundid: null,
            fundName: null,
            fundImg: null,
            basicID: null,
            ProfileID: null,
            bankId: null,
            bankName: null,
            bankAccNo: null,
            mandateId: null,
            obj: null,
            subObj: null,
            trxnSource: null,
            addminAmt: null,
            addMaxAmt: null,
            sipminAmt: null,
            sipmaxAmt: null,
            memberName: null,
            InvestProfile: null,
            goalList: []
        }

        factory.parseAllInvestments = function (data) {

            this.AllInvestments.schemeName = data.schemeName;
            this.AllInvestments.schemeId = data.schemeId;
            this.AllInvestments.folioNo = data.folioNo;
            this.AllInvestments.balUnits = data.balUnits;
            this.AllInvestments.gain = data.gain;
            this.AllInvestments.currValue = data.currValue;
            this.AllInvestments.investValue = data.investValue;
            this.AllInvestments.CAGR = data.CAGR;
            this.AllInvestments.fundid = data.fundid;
            this.AllInvestments.fundName = data.fundName;
            this.AllInvestments.fundImg = data.fundImg;
            this.AllInvestments.basicID = data.basicID;
            this.AllInvestments.ProfileID = data.ProfileID;
            this.AllInvestments.bankId = data.bankId;
            this.AllInvestments.bankName = data.bankName;
            this.AllInvestments.bankAccNo = data.bankAccNo;
            this.AllInvestments.mandateId = data.mandateId;
            this.AllInvestments.obj = data.obj;
            this.AllInvestments.subObj = data.subObj;
            this.AllInvestments.trxnSource = data.trxnSource;
            this.AllInvestments.addminAmt = data.addminAmt;
            this.AllInvestments.addMaxAmt = data.addMaxAmt;
            this.AllInvestments.sipminAmt = data.sipminAmt;
            this.AllInvestments.sipmaxAmt = data.sipmaxAmt;
            this.AllInvestments.memberName = data.memberName;
            this.AllInvestments.InvestProfile = data.InvestProfile;

            this.AllInvestments.goalList = []
            angular.forEach(data.goalList, function (value, key) {
                UserAllGoalsModel.UserAllGoals = value;
                this.AllInvestments.goalList.push(UserAllGoalsModel.UserAllGoals);
            }, this);

            return this.AllInvestments;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));