﻿(function (finApp) {
    'use strict';
    finApp.factory('serverResponseModel', [function () {
        var factory = {};

        factory.ServerResponse = {
            status: null,
            errorCode: null,
            msg: null,
            data: null
        }

        factory.parseServerResponse = function (data) {
            this.ServerResponse.status = data.status;
            this.ServerResponse.errorCode = data.errorCode;
            this.ServerResponse.msg = data.msg;
            this.ServerResponse.data = data.data;

            return this.ServerResponse;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));


//status : "SUCCESS",
//    msg : "",
//errorcode:"100 - USer friendly Error | 200 - DB ERROR | 300 - API ERROR",