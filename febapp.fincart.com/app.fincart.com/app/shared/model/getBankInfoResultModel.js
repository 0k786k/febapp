﻿(function (finApp) {
    'use strict';
    finApp.factory('getBankInfoResultModel', [function () {
        var factory = {};

        factory.GetBankInfoResult = {
            BankName: null,
            IFSC: null,
            MICR: null,
            Branch: null,
            BankAddress: null,
            Contact: null,
            City: null,
            Distric: null,
            State: null,
        }

        factory.parseGetBankInfoResult = function (data) {

            this.GetBankInfoResult.BankName= data.BankName;
            this.GetBankInfoResult.IFSC= data.IFSC;
            this.GetBankInfoResult.MICR= data.MICR;
            this.GetBankInfoResult.Branch= data.Branch;
            this.GetBankInfoResult.BankAddress= data.BankAddress;
            this.GetBankInfoResult.Contact= data.Contact;
            this.GetBankInfoResult.City= data.City;
            this.GetBankInfoResult.Distric= data.Distric;
            this.GetBankInfoResult.State= data.State;

            return this.GetBankInfoResult;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));