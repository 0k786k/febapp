﻿(function (finApp) {
    'use strict';
    finApp.factory('userTokenModel', [function () {
        var factory = {};

        factory.UserToken = {
            access_token: null,
            token_type: null,
            expires_in: null,
            refresh_token: null,
            error: null,
            error_description: null
        }

        factory.parseUserToken = function (data) {
            if (data.error) {
                this.UserToken.error = data.error;
                this.UserToken.error_description = data.error_description;
            }
            else {
                this.UserToken.access_token = data.access_token;
                this.UserToken.token_type = data.token_type;
                this.UserToken.expires_in = data.expires_in;
                this.UserToken.refresh_token = data.refresh_token;
            }
            return this.UserToken;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));