﻿(function (finApp) {
    'use strict';
    finApp.factory('sysmTrxnlistModel', [function () {
        var factory = {};

        factory.SysmTrxnlist = {
            schemeName: null,
            orgSchemeName: null,
            amount: null,
            bankAcc: null,
            memberName: null,
            basicId: null,
            trxnType: null,
            startDate: null,
            endDate: null,
            folioNo: null,
            sysmCount: null,
            sysmDate: null
        }

        factory.parseSysmTrxnlist = function (data) {

            this.SysmTrxnlist.schemeName = data.schemeName;
            this.SysmTrxnlist.orgSchemeName = data.orgSchemeName;
            this.SysmTrxnlist.amount = data.amount;
            this.SysmTrxnlist.bankAcc = data.bankAcc;
            this.SysmTrxnlist.memberName = data.memberName;
            this.SysmTrxnlist.basicId = data.basicId;
            this.SysmTrxnlist.trxnType = data.trxnType;
            this.SysmTrxnlist.startDate = data.startDate;
            this.SysmTrxnlist.endDate = data.endDate;
            this.SysmTrxnlist.folioNo = data.folioNo;
            this.SysmTrxnlist.sysmCount= data.sysmCount;
            this.SysmTrxnlist.sysmDate = data.sysmDate;

            return this.SysmTrxnlist;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));