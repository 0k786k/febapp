﻿(function (finApp) {
    'use strict';
    finApp.factory('registerReqModel', [function () {
        var factory = {};

        factory.RegisterReq = {
            Name: null,
            Userid: null,
            password: null,
            ClientStatus: null,
            Mobile: null,
            trackerCode: null
        }

        factory.parseRegisterReq = function (data) {

            this.RegisterReq.Name = data.Name;
            this.RegisterReq.Userid = data.Userid;
            this.RegisterReq.password = data.password;
            this.RegisterReq.ClientStatus = data.ClientStatus;
            this.RegisterReq.Mobile = data.Mobile;
            this.RegisterReq.trackerCode = data.trackerCode;

            return this.RegisterReq;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));