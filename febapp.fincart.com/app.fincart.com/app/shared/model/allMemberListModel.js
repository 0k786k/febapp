﻿(function (finApp) {
    'use strict';
    finApp.factory('allMemberListModel', ['memberModel', 'profileModel', 'mandateModel', 'folioModel', 'bankdetailsModel', function (memberModel, profileModel, mandateModel, folioModel, bankdetailsModel) {
        var factory = {};

        factory.allMemberListModel = {
            memberList: [],
            profileList: [],
            mandateList: [],
            folioList: [],
            bankList: []
        }

        factory.parseAllMemberListModel = function (data) {
            
            this.allMemberListModel.memberList = [];
            angular.forEach(data.memberList, function (value, key) {
                memberModel.Member = value;
                this.allMemberListModel.memberList.push(memberModel.Member);
            }, this);


            this.allMemberListModel.profileList = [];
            angular.forEach(data.profileList, function (value, key) {
                profileModel.Profile = value;
                this.allMemberListModel.profileList.push(profileModel.Profile);
            }, this);


            this.allMemberListModel.mandateList = [];
            angular.forEach(data.mandateList, function (value, key) {
                mandateModel.Mandate = value;
                this.allMemberListModel.mandateList.push(mandateModel.Mandate);
            }, this);


            this.allMemberListModel.folioList = [];
            angular.forEach(data.folioList, function (value, key) {
                folioModel.Folio = value;
                this.allMemberListModel.folioList.push(folioModel.Folio);
            }, this);


            this.allMemberListModel.bankList = [];
            angular.forEach(data.bankList, function (value, key) {
                bankdetailsModel.BankDetails = value;
                this.allMemberListModel.bankList.push(bankdetailsModel.BankDetails);
            }, this);

            return this.allMemberListModel;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));