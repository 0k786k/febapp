﻿(function (finApp) {
    'use strict';
    finApp.factory('userCAFNomineeModel', [function () {
        var factory = {};

        factory.UserCAFNominee = {
            basicId: null,
            nomId: null,
            nomName: null,
            nomDOB: null,
            nomRealtion: null,
            nomShare: null,
            nomGuardName: null,
            nomGuardPan: null
        }

        factory.parseUserCAFNominee = function (data) {
            this.UserCAFNominee.basicId = data.basicId;
            this.UserCAFNominee.nomId = data.nomId;
            this.UserCAFNominee.nomName = data.nomName;
            this.UserCAFNominee.nomDOB = data.nomDOB;
            this.UserCAFNominee.nomRealtion = data.nomRealtion;
            this.UserCAFNominee.nomShare = data.nomShare;
            this.UserCAFNominee.nomGuardName = data.nomGuardName;
            this.UserCAFNominee.nomGuardPan = data.nomGuardPan;
            return this.UserCAFNominee;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));