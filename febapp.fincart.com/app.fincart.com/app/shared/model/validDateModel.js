﻿(function (finApp) {
    'use strict';
    finApp.factory('validDateModel', [function () {
        var factory = {};

        factory.ValidDate = {
            Day: null,
            Date: null
        }

        factory.parseValidDate = function (data) {
            this.ValidDate.Day = data.Day;
            this.ValidDate.Date = data.Date;
            return this.ValidDate;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));