﻿(function (finApp) {
    'use strict';
    finApp.factory('fincartStartVedioResponseModel', [function () {
        var factory = {};

        factory.FincartStartVedioResponse = {
            transactionid: null,
            number: null,
            status: null,
            message: null
        }

        factory.parseFincartStartVedioResponse = function (data) {

            this.FincartStartVedioResponse.transactionid = data.transactionid;
            this.FincartStartVedioResponse.number = data.number;
            this.FincartStartVedioResponse.status = data.status;
            this.FincartStartVedioResponse.message = data.message;

            return this.FincartStartVedioResponse;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));