﻿(function (finApp) {
    'use strict';
    finApp.factory('investShortSummaryModel', [function () {
        var factory = {};

        factory.investShortSummary = {
            totalInvestedAmt: null,
            netInvestedAmt: null,
            currentAmount: null,
            gain: null,
            switchIn: null,
            switchOut: null,
            sell: null,
            divPayout: null,
            divReinvest: null,
            xirr: null,
            debtPerc: null,
            equityPerc: null,
            hybridPerc: null,
            debtCurrValue: null,
            equityCurrValue: null,
            hybridCurrValue: null,
            debtInvestValue: null,
            equityInvestValue: null,
            hybridInvestValue: null
        }

        factory.parseInvestShortSummary = function (data) {

            this.investShortSummary.totalInvestedAmt = data.totalInvestedAmt;
            this.investShortSummary.netInvestedAmt = data.netInvestedAmt;
            this.investShortSummary.currentAmount = data.currentAmount;
            this.investShortSummary.gain = data.gain;
            this.investShortSummary.switchIn = data.switchIn;
            this.investShortSummary.switchOut = data.switchOut;
            this.investShortSummary.sell = data.sell;
            this.investShortSummary.divPayout = data.divPayout;
            this.investShortSummary.divReinvest = data.divReinvest;
            this.investShortSummary.xirr = data.xirr;
            this.investShortSummary.debtPerc = data.debtPerc;
            this.investShortSummary.equityPerc = data.equityPerc;
            this.investShortSummary.hybridPerc = data.hybridPerc;
            this.investShortSummary.debtCurrValue= data.debtCurrValue;
            this.investShortSummary.equityCurrValue= data.equityCurrValue;
            this.investShortSummary.hybridCurrValue = data.hybridCurrValue;
            this.investShortSummary.debtInvestValue= data.debtInvestValue;
            this.investShortSummary.equityInvestValue= data.equityInvestValue;
            this.investShortSummary.hybridInvestValue = data.hybridInvestValue;

            return this.investShortSummary;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));