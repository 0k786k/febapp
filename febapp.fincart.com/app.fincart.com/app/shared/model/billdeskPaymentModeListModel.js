﻿(function (finApp) {
    'use strict';
    finApp.factory('billdeskPaymentModeListModel', ['billdeskPaymentModeModel', function (billdeskPaymentModeModel) {
        var factory = {};

        factory.BilldeskPaymentModeList = {
            PaymentModeList: []
        }

        factory.parseBilldeskPaymentModeList = function (data) {

            this.BilldeskPaymentModeList.PaymentModeList = [];

            angular.forEach(data, function (value, key) {
                billdeskPaymentModeModel.BilldeskPaymentMode = value;
                this.BilldeskPaymentModeList.PaymentModeList.push(billdeskPaymentModeModel.BilldeskPaymentMode);
            }, this);

            return this.BilldeskPaymentModeList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));