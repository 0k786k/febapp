﻿(function (finApp) {
    'use strict';
    finApp.factory('objectiveModel', [function () {
        var factory = {};

        factory.Objective = {
            objName: null
        }

        factory.parseObjective = function (data) {
            this.Objective.objName = data.objName;
            return this.Objective;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));