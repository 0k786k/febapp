﻿(function (finApp) {
    'use strict';
    finApp.factory('passbookItemsModel', ['passbookModel', function (passbookModel) {
        var factory = {};

        factory.PassbookItems = {
            passbookItms: []
        }

        factory.parsePassbookItems = function (data) {

            this.PassbookItems.passbookItms = [];

            angular.forEach(data.passbookItms, function (value, key) {
                passbookModel.Passbook = value;
                this.PassbookItems.passbookItms.push(passbookModel.Passbook);
            }, this);

            return this.PassbookItems;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));