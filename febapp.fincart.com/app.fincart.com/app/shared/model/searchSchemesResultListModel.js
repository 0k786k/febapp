﻿(function (finApp) {
    'use strict';
    finApp.factory('searchSchemesResultListModel', ['searchSchemesResultModel', function (searchSchemesResultModel) {
        var factory = {};

        factory.searchSchemesResultList = {
            searchSchemesResult: []
        }

        factory.parseSearchSchemesResultList = function (data) {
            
            this.searchSchemesResultList.searchSchemesResult = [];

            angular.forEach(data, function (value, key) {
                searchSchemesResultModel.searchSchemesResult = value;
                this.searchSchemesResultList.searchSchemesResult.push(searchSchemesResultModel.searchSchemesResult);
            }, this);

            return this.searchSchemesResultList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));