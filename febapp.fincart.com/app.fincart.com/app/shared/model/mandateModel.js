﻿(function (finApp) {
    'use strict';
    finApp.factory('mandateModel', [function () {
        var factory = {};

        factory.Mandate = {
            mandateNo: null,
            mandateLimit: null,
            basicid: null
        }

        factory.parseMandate = function (data) {
            this.Mandate.mandateNo = data.mandateNo;
            this.Mandate.mandateLimit = data.mandateLimit;
            this.Mandate.basicid = data.basicid;
            return this.Mandate;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));