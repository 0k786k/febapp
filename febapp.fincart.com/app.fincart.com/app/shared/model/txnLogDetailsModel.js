﻿(function (finApp) {
    'use strict';
    finApp.factory('txnLogDetailsModel', [function () {
        var factory = {};

        factory.TxnLogDetails = {
            schemeName: null,
            amount: null,
            folioNo: null
        }

        factory.parseTxnLogDetails = function (data) {

            this.TxnLogDetails.schemeName = data.schemeName;
            this.TxnLogDetails.amount = data.amount;
            this.TxnLogDetails.folioNo = data.folioNo;

            return this.TxnLogDetails;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));