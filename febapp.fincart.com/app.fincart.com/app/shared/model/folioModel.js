﻿(function (finApp) {
    'use strict';
    finApp.factory('folioModel', [function () {
        var factory = {};

        factory.Folio = {
            folioNo: null,
            basicid: null
        }

        factory.parseFolio = function (data) {
            this.Folio.folioNo = data.folioNo;
            this.Folio.basicid = data.basicid;
            return this.Folio;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));