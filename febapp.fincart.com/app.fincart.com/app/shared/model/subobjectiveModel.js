﻿(function (finApp) {
    'use strict';
    finApp.factory('subobjectiveModel', [function () {
        var factory = {};

        factory.SubObjective = {
            objName: null
        }

        factory.parseSubObjective = function (data) {
            this.SubObjective.objName = data.objName;
            this.SubObjective.subObjName = data.subObjName;
            return this.SubObjective;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));