﻿(function (finApp) {
    'use strict';
    finApp.factory('kycModel', [function () {
        var factory = {};

        factory.KYC = {
            KYC_STATUS: null,
            PAN_NUMBER: null,
            PAN_NAME: null,
            KYC_DATE: null,
            KYC_FROM: null
        }

        factory.parseKYC = function (data) {

            this.KYC.KYC_STATUS = data.KYC_STATUS;
            this.KYC.PAN_NUMBER = data.PAN_NUMBER;
            this.KYC.PAN_NAME = data.PAN_NAME;
            this.KYC.KYC_DATE = data.KYC_DATE;
            this.KYC.KYC_FROM = data.KYC_FROM;

            return this.KYC;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));