﻿(function (finApp) {
    'use strict';
    finApp.factory('signzyUserProfileStageModel', [function () {
        var factory = {};

        factory.SignzyUserProfileStage = {
            isKYC: null,
            isAccount: null,
            isPOIPOA: null,
            isBasic: null,
            isBank: null,
            isSign: null,
            isVedio: null,
            isQuickSip: null,
            isGoal: null,
            isEdit: null,
            isCart: null
        }

        factory.parseSignzyUserProfileStage = function (data) {

            this.SignzyUserProfileStage.isKYC = data.isKYC;
            this.SignzyUserProfileStage.isAccount = data.isAccount;
            this.SignzyUserProfileStage.isPOIPOA = data.isPOIPOA;
            this.SignzyUserProfileStage.isBasic = data.isBasic;
            this.SignzyUserProfileStage.isBank = data.isBank;
            this.SignzyUserProfileStage.isSign = data.isSign;
            this.SignzyUserProfileStage.isVedio = data.isVedio;
            this.SignzyUserProfileStage.isQuickSip = data.isQuickSip;
            this.SignzyUserProfileStage.isGoal = data.isGoal;
            this.SignzyUserProfileStage.isEdit = data.isEdit;
            this.SignzyUserProfileStage.isCart = data.isCart;

            return this.SignzyUserProfileStage;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));