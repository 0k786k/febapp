﻿(function (finApp) {
    'use strict';
    finApp.factory('systematicRemindersModel', ['sysmTrxnlistModel', function (sysmTrxnlistModel) {
        var factory = {};

        factory.SystematicReminders = {
            sysmtDate: null,
            sysmTrxnlist: []
        }

        factory.parseSystematicReminders = function (data) {

            this.SystematicReminders.sysmtDate = data.sysmtDate;

            this.SystematicReminders.sysmTrxnlist = [];

            angular.forEach(data.sysmTrxnlist, function (value, key) {
                sysmTrxnlistModel.SysmTrxnlist = value;
                this.SystematicReminders.sysmTrxnlist.push(sysmTrxnlistModel.SysmTrxnlist);
            }, this);

            return this.SystematicReminders;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));