﻿(function (finApp) {
    'use strict';
    finApp.factory('mandateDetailsModel', [function () {
        var factory = {};

        factory.MandateDetails = {
            clientname: null,
            profilename: null,
            isactive: null,
            uploadstatus: null,
            untilcancel: null,
            moh: null,
            MandateID: null,
            ProfileID: null,
            Bank: null,
            Branch: null,
            AccountNo: null,
            IFSC: null,
            MICR: null,
            FromMonth: null,
            FromYear: null,
            ToMonth: null,
            ToYear: null,
            PerDayLimit: null,
            UMRN_NO: null,
            reason: null
        }

        factory.parseMandateDetails = function (data) {

            this.MandateDetails.clientname = data.clientname;
            this.MandateDetails.profilename = data.profilename;
            this.MandateDetails.isactive = data.isactive;
            this.MandateDetails.uploadstatus = data.uploadstatus;
            this.MandateDetails.untilcancel = data.untilcancel;
            this.MandateDetails.moh = data.moh;
            this.MandateDetails.MandateID= data.MandateID;
            this.MandateDetails.ProfileID= data.ProfileID;
            this.MandateDetails.Bank= data.Bank;
            this.MandateDetails.Branch= data.Branch;
            this.MandateDetails.AccountNo= data.AccountNo;
            this.MandateDetails.IFSC= data.IFSC;
            this.MandateDetails.MICR= data.MICR;
            this.MandateDetails.FromMonth= data.FromMonth;
            this.MandateDetails.FromYear= data.FromYear;
            this.MandateDetails.ToMonth= data.ToMonth;
            this.MandateDetails.ToYear= data.ToYear;
            this.MandateDetails.PerDayLimit= data.PerDayLimit;
            this.MandateDetails.UMRN_NO= data.UMRN_NO;
            this.MandateDetails.reason= data.reason;

            return this.MandateDetails;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));