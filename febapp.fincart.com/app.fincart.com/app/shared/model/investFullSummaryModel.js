﻿(function (finApp) {
    'use strict';
    finApp.factory('investFullSummaryModel', [function () {
        var factory = {};

        factory.InvestFullSummary = {
            purchase: null,
            currentAmt: null,
            gain: null,
            switchIn: null,
            switchOut: null,
            sell: null,
            divPayout: null,
            divReinvest: null,
            xirr: null,
            memberType: null,
            applicant: null,
            debtPerc: null,
            equityPerc: null,
            hybridPerc: null,
            debtCurrValue: null,
            equityCurrValue: null,
            hybridCurrValue: null,
            debtInvestValue: null,
            equityInvestValue: null,
            hybridInvestValue: null
        }

        factory.parseInvestFullSummary = function (data) {

            this.InvestFullSummary.purchase = data.purchase;
            this.InvestFullSummary.currentAmt = data.currentAmt;
            this.InvestFullSummary.gain = data.gain;
            this.InvestFullSummary.switchIn = data.switchIn;
            this.InvestFullSummary.switchOut = data.switchOut;
            this.InvestFullSummary.sell = data.sell;
            this.InvestFullSummary.divPayout = data.divPayout;
            this.InvestFullSummary.divReinvest = data.divReinvest;
            this.InvestFullSummary.xirr = data.xirr;
            this.InvestFullSummary.memberType = data.memberType;
            this.InvestFullSummary.applicant = data.applicant;
            this.InvestFullSummary.debtPerc = data.debtPerc;
            this.InvestFullSummary.equityPerc = data.equityPerc;
            this.InvestFullSummary.hybridPerc = data.hybridPerc;
            this.InvestFullSummary.debtCurrValue= data.debtCurrValue;
            this.InvestFullSummary.equityCurrValue = data.equityCurrValue;
            this.InvestFullSummary.hybridCurrValue = data.hybridCurrValue;
            this.InvestFullSummary.debtInvestValue = data.debtInvestValue;
            this.InvestFullSummary.equityInvestValue = data.equityInvestValue;
            this.InvestFullSummary.hybridInvestValue = data.hybridInvestValue;

            return this.InvestFullSummary;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));