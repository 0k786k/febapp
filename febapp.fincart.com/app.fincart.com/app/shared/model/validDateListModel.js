﻿(function (finApp) {
    'use strict';
    finApp.factory('validDateListModel', ['validDateModel',function (validDateModel) {
        var factory = {};

        factory.ValidDateList = {
            validdates: []
        }

        factory.parseValidDateList = function (data) {

            this.ValidDateList.validdates = [];

            angular.forEach(data, function (value, key) {
                validDateModel.ValidDate = value;
                this.ValidDateList.validdates.push(validDateModel.ValidDate);
            }, this);

            return this.ValidDateList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));