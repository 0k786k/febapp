﻿(function (finApp) {
    'use strict';
    finApp.factory('addGoalModel', [function () {
        var factory = {};

        factory.AddGoal = {
            userGoalId: null,
            basicid: null,
            Relation: null,
            childName: null,
            age: null,
            gender: null,
            annualIncome: null,
            goalCode: null,
            otherGoalName: null,
            typeCode: null,
            monthlyAmount: null,
            presentValue: null,
            risk: null,
            goal_StartDate: null,
            goal_EndDate: null,
            inflationRate: null,
            ror: null,
            PMT: null,
            downPaymentRate: null,
            trnx_Type: null,
            getAmount: null,
            retirementAge: null,
            investAmount: null,
            people: null
        }

        factory.parseAddGoal = function (data) {

            this.AddGoal.userGoalId = data.userGoalId;
            this.AddGoal.basicid = data.basicid;
            this.AddGoal.Relation = data.Relation;
            this.AddGoal.childName = data.childName;
            this.AddGoal.age = data.age;
            this.AddGoal.gender = data.gender;
            this.AddGoal.annualIncome = data.annualIncome;
            this.AddGoal.goalCode = data.goalCode;
            this.AddGoal.otherGoalName = data.otherGoalName;
            this.AddGoal.typeCode = data.typeCode;
            this.AddGoal.monthlyAmount = data.monthlyAmount;
            this.AddGoal.presentValue = data.presentValue;
            this.AddGoal.risk = data.risk;
            this.AddGoal.goal_StartDate = data.goal_StartDate;
            this.AddGoal.goal_EndDate = data.goal_EndDate;
            this.AddGoal.inflationRate = data.inflationRate;
            this.AddGoal.ror = data.ror;
            this.AddGoal.PMT = data.PMT;
            this.AddGoal.downPaymentRate = data.downPaymentRate;
            this.AddGoal.trnx_Type = data.trnx_Type;
            this.AddGoal.getAmount = data.getAmount;
            this.AddGoal.retirementAge = data.retirementAge;
            this.AddGoal.investAmount = data.investAmount;
            this.AddGoal.people = data.people;

            return this.AddGoal;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));