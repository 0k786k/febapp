﻿(function (finApp) {
    'use strict';
    finApp.factory('allInvestmentsListModel', ['allInvestmentsModel', function (allInvestmentsModel) {
        var factory = {};

        factory.AllInvestmentsList = {
            allInvestments: []
        }

        factory.parseAllInvestmentsList = function (data) {
            this.AllInvestmentsList.allInvestments = []

            angular.forEach(data, function (value, key) {
                allInvestmentsModel.AllInvestments = value;
                this.AllInvestmentsList.allInvestments.push(allInvestmentsModel.AllInvestments);
            }, this);

            return this.AllInvestmentsList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));