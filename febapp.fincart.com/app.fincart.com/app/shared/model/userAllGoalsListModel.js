﻿(function (finApp) {
    'use strict';
    finApp.factory('UserAllGoalsListModel', ['UserAllGoalsModel', function (UserAllGoalsModel) {
        var factory = {};

        factory.UserAllGoalsList = {
            userAllGoals: []
        }

        factory.parseUserAllGoalsList = function (data) {
            this.UserAllGoalsList.userAllGoals = []
            angular.forEach(data, function (value, key) {
                UserAllGoalsModel.UserAllGoals = value;
                this.UserAllGoalsList.userAllGoals.push(UserAllGoalsModel.UserAllGoals);
            }, this);

            return this.UserAllGoalsList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));
