﻿(function (finApp) {
    'use strict'; 
    finApp.factory('billdskPaymentLogModel', ['txnLogDetailsModel', function (txnLogDetailsModel) {
        var factory = {};

        factory.BilldskPaymentLog = {
            txnStatus:null,
            txnStatusImg:null,
            txnId:null,
            txnTotalAmount:null,
            txnFailReason: null,
            txnLogDetails: []
        }

        factory.parseBilldskPaymentLog = function (data) {

            this.BilldskPaymentLog.txnStatus = data.txnStatus;
            this.BilldskPaymentLog.txnStatusImg = data.txnStatusImg;
            this.BilldskPaymentLog.txnId = data.txnId;
            this.BilldskPaymentLog.txnTotalAmount = data.txnTotalAmount;
            this.BilldskPaymentLog.txnFailReason = data.txnFailReason;

            this.BilldskPaymentLog.txnLogDetails = []
            angular.forEach(data.txnLogDetails, function (value, key) {
                txnLogDetailsModel.TxnLogDetails = value;
                this.BilldskPaymentLog.txnLogDetails.push(txnLogDetailsModel.TxnLogDetails);
            }, this);

            return this.BilldskPaymentLog;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));