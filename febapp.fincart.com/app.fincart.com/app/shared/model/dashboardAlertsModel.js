﻿(function (finApp) {
    'use strict';
    finApp.factory('dashboardAlertsModel', ['sysmSipShortReminderModel', 'sysmTrxnlistModel', 'userCartCountModel', 'birthdayAlertModel', 'eventAlertModel',function (sysmSipShortReminderModel, sysmTrxnlistModel, userCartCountModel, birthdayAlertModel, eventAlertModel) {
        var factory = {};

        factory.DashboardAlerts = {
            sysmSipTopReminders: [],
            sipShortReminder: null,
            cartCount: null,
            birthdayAlert: [],
            eventAlert: []
        }

        factory.parseDashboardAlerts = function (data) {

            this.DashboardAlerts.sipShortReminder = data.sipShortReminder;
            this.DashboardAlerts.cartCount = data.cartCount;

            this.DashboardAlerts.sysmSipTopReminders = [];
            angular.forEach(data.sysmSipTopReminders, function (value, key) {
                sysmTrxnlistModel.SysmTrxnlist = value;
                this.DashboardAlerts.sysmSipTopReminders.push(sysmTrxnlistModel.SysmTrxnlist);
            }, this);

            this.DashboardAlerts.birthdayAlert = [];
            angular.forEach(data.birthdayAlert, function (value, key) {
                birthdayAlertModel.BirthdayAlert = value;
                this.DashboardAlerts.birthdayAlert.push(birthdayAlertModel.BirthdayAlert);
            }, this);

            this.DashboardAlerts.eventAlert = [];
            angular.forEach(data.eventAlert, function (value, key) {
                eventAlertModel.EventAlert = value;
                this.DashboardAlerts.eventAlert.push(eventAlertModel.EventAlert);
            }, this);

            return this.DashboardAlerts;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));