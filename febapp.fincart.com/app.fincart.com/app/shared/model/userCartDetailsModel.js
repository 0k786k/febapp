﻿(function (finApp) {
    'use strict';
    finApp.factory('userCartDetailsModel', [function () {
        var factory = {};

        factory.UserCartDetails = {
            profileId: null,
            basicId: null,
            amount: null,
            folioNo: null,
            cartId: null,
            schemeId: null,
            schemeName: null,
            tranType: null,
            goalName: null,
            goalImg: null,
            userGoalId: null,
            fundRating: null,
            sipStartDate: null,
            sipEndDate: null,
            ratingVendor: null,
            fundId: null,
            fundName: null,
            fundImg: null
        }

        factory.parseUserCartDetails = function (data) {

            this.UserCartDetails.profileId = data.profileId;
            this.UserCartDetails.basicId = data.basicId;
            this.UserCartDetails.amount = data.amount;
            this.UserCartDetails.folioNo = data.folioNo;
            this.UserCartDetails.cartId = data.cartId;
            this.UserCartDetails.schemeId = data.schemeId;
            this.UserCartDetails.schemeName = data.schemeName;
            this.UserCartDetails.tranType = data.tranType;
            this.UserCartDetails.goalName = data.goalName;
            this.UserCartDetails.goalImg = data.goalImg;
            this.UserCartDetails.userGoalId = data.userGoalId;
            this.UserCartDetails.fundRating = data.fundRating;
            this.UserCartDetails.sipStartDate = data.sipStartDate;
            this.UserCartDetails.sipEndDate = data.sipEndDate;
            this.UserCartDetails.ratingVendor = data.ratingVendor;
            this.UserCartDetails.fundId = data.fundId;
            this.UserCartDetails.fundName = data.fundName;
            this.UserCartDetails.fundImg = data.fundImg;

            return this.UserCartDetails;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));