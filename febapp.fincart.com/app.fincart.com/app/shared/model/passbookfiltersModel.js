﻿(function (finApp) {
    'use strict';
    finApp.factory('passbookfiltersModel', [function () {
        var factory = {};

        factory.Passbookfilters = {
            trantypes: [],
            schemes: [],
            goals: [],
            memberNames: [],
            txnstatus: [],
            foliono: []
        }

        factory.parsePassbookfilters = function (data) {

            this.Passbookfilters.trantypes = [];
            this.Passbookfilters.schemes = [];
            this.Passbookfilters.goals = [];
            this.Passbookfilters.memberNames = [];
            this.Passbookfilters.txnstatus = [];
            this.Passbookfilters.foliono = [];

            this.Passbookfilters.trantypes = data.trantypes;
            this.Passbookfilters.schemes = data.schemes;
            this.Passbookfilters.goals = data.goals;
            this.Passbookfilters.memberNames = data.memberNames;
            this.Passbookfilters.txnstatus = data.txnstatus;
            this.Passbookfilters.foliono = data.foliono;

            return this.Passbookfilters;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));