﻿(function (finApp) {
    'use strict';
    finApp.factory('birthdayAlertModel', [function () {
        var factory = {};

        factory.BirthdayAlert = {
            birthDate: null,
            userName: null,
            wishes: null
        }

        factory.parseBirthdayAlert = function (data) {

            this.BirthdayAlert.birthDate = data.birthDate;
            this.BirthdayAlert.userName = data.userName;
            this.BirthdayAlert.wishes = data.wishes;

            return this.BirthdayAlert;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));