﻿(function (finApp) {
    'use strict';
    finApp.factory('finCalcModel', [function () {
        var factory = {};

        factory.FinCalcModel = {
            type: null,
            currentAmount: null,
            startDate: null,
            endDate: null,
            goalCode: null,
            otherGoalName: null,
            childName: null,
            travelPeople: null,
            locationCode: null,
            budgetType: null,
            businessStartupCost: null,
            monthlyexpence: null
        }

        factory.parseFinCalcModel = function (data) {

            this.FinCalcModel.type= data.type;
            this.FinCalcModel.currentAmount= data.currentAmount;
            this.FinCalcModel.startDate = data.startDate;
            this.FinCalcModel.endDate = data.endDate;
            this.FinCalcModel.goalCode= data.goalCode;
            this.FinCalcModel.otherGoalName = data.otherGoalName;
            this.FinCalcModel.childName = data.childName;
            this.FinCalcModel.travelPeople = data.travelPeople;
            this.FinCalcModel.locationCode = data.locationCode;
            this.FinCalcModel.budgetType = data.budgetType;
            this.FinCalcModel.businessStartupCost = data.businessStartupCost;
            this.FinCalcModel.monthlyexpence = data.monthlyexpence;

            return this.FinCalcModel;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));