﻿(function (finApp) {
    'use strict';
    finApp.factory('bankdetailsModel', [function () {
        var factory = {};

        factory.BankDetails = {
            accountNo: null,
            bankName: null,
            bankid: null,
            basicid: null
        }

        factory.parseBankDetails = function (data) {

            this.BankDetails.accountNo = data.accountNo;
            this.BankDetails.bankName = data.bankName;
            this.BankDetails.bankid = data.bankid;
            this.BankDetails.basicid = data.basicid;

            return this.BankDetails;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));