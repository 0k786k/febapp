﻿(function (finApp) {
    'use strict';
    finApp.factory('loggedInDetailsModel', [function () {
        var factory = {};

        factory.LoggedInDetails = {
            basicid: null,
            userid: null,
            memberid: null,
            groupleader: null,
            name: null,
            mobile: null,
            phone: null,
            kycstatus: null,
            cafstatus: null,
            profilepic: null,
            isGoal: null,
            isQuickSip: null,
            isTaxSaving: null,
            isPrime: null,
            isEmailVerify: null,
            isMobileVerfiy: null,
            isAccountDone: null,
            isPOIPOADone: null,
            isBasicDone: null,
            isVideoDone: null,
            isBankDone: null,
            isSignDone: null,
            RmName: null,
            RmMobile: null,
            RmEmail: null,
            kycFailReason: null,
            CafFailReason: null,
            mandateFailReason: null
        }

        factory.parseLoggedInDetails = function (data) {

            this.LoggedInDetails.basicid = data.basicid;
            this.LoggedInDetails.userid = data.userid;
            this.LoggedInDetails.memberid = data.memberid;
            this.LoggedInDetails.groupleader = data.groupleader;
            this.LoggedInDetails.name = data.name;
            this.LoggedInDetails.mobile = data.mobile;
            this.LoggedInDetails.phone = data.phone;
            this.LoggedInDetails.kycstatus = data.kycstatus;
            this.LoggedInDetails.cafstatus = data.cafstatus;
            this.LoggedInDetails.profilepic = data.profilepic;
            this.LoggedInDetails.isGoal = data.isGoal;
            this.LoggedInDetails.isQuickSip = data.isQuickSip;
            this.LoggedInDetails.isTaxSaving = data.isTaxSaving;
            this.LoggedInDetails.isPrime = data.isPrime;
            this.LoggedInDetails.isEmailVerify = data.isEmailVerify;
            this.LoggedInDetails.isMobileVerfiy = data.isMobileVerfiy;
            this.LoggedInDetails.isAccountDone = data.isAccountDone;
            this.LoggedInDetails.isPOIPOADone = data.isPOIPOADone;
            this.LoggedInDetails.isBasicDone = data.isBasicDone;
            this.LoggedInDetails.isVideoDone = data.isVideoDone;
            this.LoggedInDetails.isBankDone = data.isBankDone;
            this.LoggedInDetails.isSignDone = data.isSignDone;
            this.LoggedInDetails.RmName= data.RmName;
            this.LoggedInDetails.RmMobile= data.RmMobile;
            this.LoggedInDetails.RmEmail= data.RmEmail;
            this.LoggedInDetails.kycFailReason= data.kycFailReason;
            this.LoggedInDetails.CafFailReason= data.CafFailReason;
            this.LoggedInDetails.mandateFailReason = data.mandateFailReason;

            return this.LoggedInDetails;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));