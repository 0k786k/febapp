﻿(function (finApp) {
    'use strict';
    finApp.factory('userCAFDataModel', ['userCAFNomineeModel', 'userCAFFatcaTaxModel', function (userCAFNomineeModel, userCAFFatcaTaxModel) {
        var factory = {};

        factory.UserCAFData = {
            basicId: null,
            memberid: null,
            groupleaderId: null,
            profilepic: null,
            clientName: null,
            mobile: null,
            pan: null,
            AnnualIncome: null,
            aadharNo: null,
            dob: null,
            email: null,
            occupation: null,
            prmAddress: null,
            prmPincode: null,
            prmState: null,
            prmCity: null,
            country: null,
            comAddress: null,
            comPincode: null,
            comState: null,
            comCity: null,
            bankName: null,
            bankAddress: null,
            ifsc: null,
            micr: null,
            accNo: null,
            nameAsPerBank: null,
            branchName: null,
            branchCity: null,
            isSameAddress: null,
            isNominee: null,
            isTaxOthIndia: null,
            ftcaBirthPlace: null,
            ftcaBirthCoutry: null,
            ftcaWealthSource: null,
            ftcaPoliticalExpo: null,
            cafNominee: [],
            cafFatcatax: []
        }

        factory.parseUserCAFData = function (data) {
            this.UserCAFData.basicId= data.basicId;
            this.UserCAFData.memberid= data.memberid;
            this.UserCAFData.groupleaderId= data.groupleaderId;
            this.UserCAFData.profilepic= data.profilepic;
            this.UserCAFData.clientName= data.clientName;
            this.UserCAFData.mobile= data.mobile;
            this.UserCAFData.pan= data.pan;
            this.UserCAFData.AnnualIncome= data.AnnualIncome;
            this.UserCAFData.aadharNo= data.aadharNo;
            this.UserCAFData.dob= data.dob;
            this.UserCAFData.email= data.email;
            this.UserCAFData.occupation= data.occupation;
            this.UserCAFData.prmAddress= data.prmAddress;
            this.UserCAFData.prmPincode= data.prmPincode;
            this.UserCAFData.prmState= data.prmState;
            this.UserCAFData.prmCity= data.prmCity;
            this.UserCAFData.country= data.country;
            this.UserCAFData.comAddress= data.comAddress;
            this.UserCAFData.comPincode= data.comPincode;
            this.UserCAFData.comState= data.comState;
            this.UserCAFData.comCity= data.comCity;
            this.UserCAFData.bankName= data.bankName;
            this.UserCAFData.bankAddress= data.bankAddress;
            this.UserCAFData.ifsc= data.ifsc;
            this.UserCAFData.micr= data.micr;
            this.UserCAFData.accNo= data.accNo;
            this.UserCAFData.nameAsPerBank= data.nameAsPerBank;
            this.UserCAFData.branchName= data.branchName;
            this.UserCAFData.branchCity= data.branchCity;
            this.UserCAFData.isSameAddress= data.isSameAddress;
            this.UserCAFData.isNominee= data.isNominee;
            this.UserCAFData.isTaxOthIndia = data.isTaxOthIndia;
            this.UserCAFData.ftcaBirthPlace = data.ftcaBirthPlace;
            this.UserCAFData.ftcaBirthCoutry= data.ftcaBirthCoutry;
            this.UserCAFData.ftcaWealthSource= data.ftcaWealthSource;
            this.UserCAFData.ftcaPoliticalExpo = data.ftcaPoliticalExpo;

            this.UserCAFData.cafNominee = [];
            angular.forEach(data.cafNominee, function (value, key) {
                userCAFNomineeModel.UserCAFNominee = value;
                this.UserCAFData.cafNominee.push(userCAFNomineeModel.UserCAFNominee);
            }, this);

            this.UserCAFData.cafFatcatax = [];
            angular.forEach(data.cafFatcatax, function (value, key) {
                userCAFFatcaTaxModel.UserCAFFatcaTax = value;
                this.UserCAFData.cafFatcatax.push(userCAFFatcaTaxModel.UserCAFFatcaTax);
            }, this);


            return this.UserCAFData;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));