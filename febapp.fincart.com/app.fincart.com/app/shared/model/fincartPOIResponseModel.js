﻿(function (finApp) {
    'use strict';
    finApp.factory('fincartPOIResponseModel', [function () {
        var factory = {};

        factory.FincartPOIResponse = {
            name: null,
            fathername: null,
            dob: null,
            panno: null,
            address: null,
            city: null,
            state: null,
            pincode: null,
            status: null,
            message: null
        }

        factory.parseFincartPOIResponse = function (data) {

            this.FincartPOIResponse.name= data.name;
            this.FincartPOIResponse.fathername= data.fathername;
            this.FincartPOIResponse.dob= data.dob;
            this.FincartPOIResponse.panno= data.panno;
            this.FincartPOIResponse.address= data.address;
            this.FincartPOIResponse.city= data.city;
            this.FincartPOIResponse.state= data.state;
            this.FincartPOIResponse.pincode= data.pincode;
            this.FincartPOIResponse.status= data.status;
            this.FincartPOIResponse.message= data.message;

            return this.FincartPOIResponse;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));