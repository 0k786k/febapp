﻿(function (finApp) {
    'use strict';
    finApp.factory('fincartDeleteStepModel', [function () {
        var factory = {};

        factory.FincartDeleteStep = {
            basicid: null,
            step: null
        }

        factory.parseFincartDeleteStep = function (data) {

            this.FincartDeleteStep.basicid = data.basicid;
            this.FincartDeleteStep.step = data.step;

            return this.FincartDeleteStep;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));