﻿(function (finApp) {
    'use strict';
    finApp.factory('fincartKYcDataModel', [function () {
        var factory = {};

        factory.FincartKYcData = {
            basicid: null,
            gender: null,
            martialStatus: null,
            mothersName: null,
            occupation: null,
            addressType: null,
            annualIncome: null,
            sourceofIncome: null,
            Nominee_Name1: null,
            Relation1: null,
            Nominee_Dob1: null,
            Nom1_percent: null,
            Nominee_Name2: null,
            Relation2: null,
            Nominee_Dob2: null,
            Nom2_percent: null,
            Nominee_Name3: null,
            Relation3: null,
            Nominee_Dob3: null,
            Nom3_percent: null,
        }

        factory.parseFincartKYcData = function (data) {

            this.FincartKYcData.basicid= data.basicid;
            this.FincartKYcData.gender= data.gender;
            this.FincartKYcData.martialStatus= data.martialStatus;
            this.FincartKYcData.mothersName= data.mothersName;
            this.FincartKYcData.occupation= data.occupation;
            this.FincartKYcData.addressType= data.addressType;
            this.FincartKYcData.annualIncome= data.annualIncome;
            this.FincartKYcData.sourceofIncome= data.sourceofIncome;
            this.FincartKYcData.Nominee_Name1= data.Nominee_Name1;
            this.FincartKYcData.Relation1= data.Relation1;
            this.FincartKYcData.Nominee_Dob1= data.Nominee_Dob1;
            this.FincartKYcData.Nom1_percent= data.Nom1_percent;
            this.FincartKYcData.Nominee_Name2= data.Nominee_Name2;
            this.FincartKYcData.Relation2= data.Relation2;
            this.FincartKYcData.Nominee_Dob2= data.Nominee_Dob2;
            this.FincartKYcData.Nom2_percent= data.Nom2_percent;
            this.FincartKYcData.Nominee_Name3= data.Nominee_Name3;
            this.FincartKYcData.Relation3= data.Relation3;
            this.FincartKYcData.Nominee_Dob3= data.Nominee_Dob3;
            this.FincartKYcData.Nom3_percent = data.Nom3_percent;

            return this.FincartKYcData;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));