﻿(function (finApp) {
    'use strict';
    finApp.factory('memberModel', [function () {
        var factory = {};

        factory.Member = {
            memberName: null,
            basicid: null
        }

        factory.parseMember = function (data) {
            this.Member.memberName = data.memberName;
            this.Member.basicid = data.basicid;
            return this.Member;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));