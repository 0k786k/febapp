﻿(function (finApp) {
    'use strict';
    finApp.factory('passbookModel', ['UserAllGoalsModel', function (UserAllGoalsModel) {
        var factory = {};

        factory.Passbook = {
            tranType: null,
            subTranType: null,
            txnDate: null,
            schemeShortName: null,
            schemefullName: null,
            investStatus: null,
            statusSummary: null,
            amount: null,
            schemeId: null,
            units: null,
            nav: null,
            trxnId: null,
            failReason: null,
            foliono: null,
            passbookGoals: []
        }

        factory.parsePassbook = function (data) {

            this.Passbook.tranType = data.tranType;
            this.Passbook.subTranType = data.subTranType;
            this.Passbook.txnDate = data.txnDate;
            this.Passbook.schemeShortName = data.schemeShortName;
            this.Passbook.schemefullName = data.schemefullName;
            this.Passbook.investStatus = data.investStatus;
            this.Passbook.statusSummary = data.statusSummary;
            this.Passbook.amount = data.amount;
            this.Passbook.schemeId = data.schemeId;
            this.Passbook.units = data.units;
            this.Passbook.nav = data.nav;
            this.Passbook.trxnId = data.trxnId;
            this.Passbook.failReason = data.failReason;
            this.Passbook.foliono = data.foliono;

            this.Passbook.passbookGoals = []
            angular.forEach(data, function (value, key) {
                UserAllGoalsModel.UserAllGoals = value;
                this.Passbook.passbookGoals.push(UserAllGoalsModel.UserAllGoals);
            }, this);

            return this.Passbook;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));