﻿(function (finApp) {
    'use strict';
    finApp.factory('userCAFFatcaTaxModel', [function () {
        var factory = {};

        factory.UserCAFFatcaTax = {
            ftcaTaxCountry: null,
            CountryPayerIdno: null,
            idenifType: null
        }

        factory.parseUserCAFFatcaTax = function (data) {

            this.UserCAFFatcaTax.ftcaTaxCountry = data.ftcaTaxCountry;
            this.UserCAFFatcaTax.CountryPayerIdno = data.CountryPayerIdno;
            this.UserCAFFatcaTax.idenifType = data.idenifType;

            return this.UserCAFFatcaTax;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));