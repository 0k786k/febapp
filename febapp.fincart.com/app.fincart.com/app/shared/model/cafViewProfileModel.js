﻿(function (finApp) {
    'use strict';
    finApp.factory('cafViewProfileModel', [function () {
        var factory = {};

        factory.CafViewProfile = {
            basic_id: null,
            memberId: null,
            groupLeader: null,
            clientname: null,
            profilepic: null,
            pannumber: null,
            profilepercent: 0.0,
            totalInvstAccounts: 0,
            cafFailReason: null,
            kycFailReason: null,
            mandateFailReason: null,
            cafStatus: null,
            kycStatus: null,
            isCafComplete: null,
            isMemberBasic: null,
            isMemberAddrs: null,
            isMemberBank: null,
            isMemberNominee: null,
            isNomineeReq: null,
            isMemberFatca: null,
            isMemberMandate: null,
            mandateStatus: null
        }


        factory.parseCafViewProfile = function (data) {

            this.CafViewProfile.basic_id = data.basic_id;
            this.CafViewProfile.memberId = data.memberId;
            this.CafViewProfile.groupLeader = data.groupLeader;
            this.CafViewProfile.clientname = data.clientname;
            this.CafViewProfile.profilepic = data.profilepic;
            this.CafViewProfile.pannumber = data.pannumber;
            this.CafViewProfile.profilepercent = parseFloat(data.profilepercent);
            this.CafViewProfile.totalInvstAccounts = parseInt(data.totalInvstAccounts);
            this.CafViewProfile.cafFailReason = data.cafFailReason;
            this.CafViewProfile.kycFailReason = data.kycFailReason;
            this.CafViewProfile.mandateFailReason = data.mandateFailReason;
            this.CafViewProfile.cafStatus = data.cafStatus;
            this.CafViewProfile.kycStatus = data.kycStatus;
            this.CafViewProfile.isCafComplete = data.isCafComplete;
            this.CafViewProfile.isMemberBasic = data.isMemberBasic;
            this.CafViewProfile.isMemberAddrs = data.isMemberAddrs;
            this.CafViewProfile.isMemberBank = data.isMemberBank;
            this.CafViewProfile.isMemberNominee = data.isMemberNominee;
            this.CafViewProfile.isNomineeReq = data.isNomineeReq;
            this.CafViewProfile.isMemberFatca = data.isMemberFatca;
            this.CafViewProfile.isMemberMandate = data.isMemberMandate;
            this.CafViewProfile.mandateStatus = data.mandateStatus;

            return this.CafViewProfile;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));