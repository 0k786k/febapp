﻿(function (finApp) {
    'use strict';
    finApp.factory('systematicRemindersListModel', ['systematicRemindersModel', function (systematicRemindersModel) {
        var factory = {};

        factory.SystematicRemindersList = {
            remindersList: []
        }

        factory.parseSystematicRemindersList = function (data) {

            this.SystematicRemindersList.remindersList = [];

            angular.forEach(data, function (value, key) {
                //systematicRemindersModel.sysmtDate = value.sysmtDate;
                //systematicRemindersModel.sysmTrxnlist = value.sysmTrxnlist;
                this.SystematicRemindersList.remindersList.push(value);
            }, this);

            return this.SystematicRemindersList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));