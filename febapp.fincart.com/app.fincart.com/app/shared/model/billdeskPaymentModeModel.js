﻿(function (finApp) {
    'use strict';
    finApp.factory('billdeskPaymentModeModel', [function () {
        var factory = {};

        factory.BilldeskPaymentMode = {
            payType: null,
            payModeId: null,
            status: null,
            productId: null,
            modeImg: null
        }

        factory.parseBilldeskPaymentMode = function (data) {

            this.BilldeskPaymentMode.payType = data.payType;
            this.BilldeskPaymentMode.payModeId = data.payModeId;
            this.BilldeskPaymentMode.status = data.status;
            this.BilldeskPaymentMode.productId = data.productId;
            this.BilldeskPaymentMode.modeImg = data.modeImg;

            return this.BilldeskPaymentMode;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));