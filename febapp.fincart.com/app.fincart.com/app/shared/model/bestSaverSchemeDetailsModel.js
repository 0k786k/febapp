﻿(function (finApp) {
    'use strict';
    finApp.factory('bestSaverSchemeDetailsModel', [function () {
        var factory = {};

        factory.BestSaverSchemeDetails = {
            ARNCode: null,
            AgentName: null,
            BankName: null,
            EUIN: null,
            EasyInvestFlag: null,
            Folio: null,
            FreeAmt: null,
            FreeSwpAmt: null,
            FreeUnits: null,
            InProcessAmt: null,
            InstaRedeemEligible: null,
            Insta_Amount: null,
            Insta_Units: null,
            InvestedAmt: null,
            LockinAmt: null,
            LockinSWPAmt: null,
            LockinUnits: null,
            MinAmt: null,
            MinUnits: null,
            Msg1: null,
            Msg2: null,
            NAV: null,
            NAV_Date: null,
            Option_Code: null,
            Plan_Code: null,
            PledgeUnits: null,
            RestrictedAmt: null,
            RestrictedUnits: null,
            Return_Msg: null,
            Return_Code: null,
            SubARNCode: null,
            SchemeCategory: null,
            SchemeDescription: null,
            Scheme_Code: null,
            TotalAmt: null,
            Totalunits: null,
            GoalCode: null,
            TypeCode: null,
            Usergoalid: null,
            Scheme_Id: null,
            Invst_FCode: null,
            Invst_SCode: null,
            Org_FundCode: null,
            Org_SchemeCode: null,
            BillDesk_AMC_Code: null,
            BillDesk_Code: null,
            ExlCode: null,
            DivOption: null,
            folioNo: null,
            memberid: null,
            profileid: null,
            BankCode: null,
            Acc_no: null,
            basicid: null,
            bankid: null,
            mandateid: null
        }

        factory.parseBestSaverSchemeDetails = function (data) {

            this.BestSaverSchemeDetails.ARNCode = data.ARNCode;
            this.BestSaverSchemeDetails.AgentName = data.AgentName;
            this.BestSaverSchemeDetails.BankName = data.BankName;
            this.BestSaverSchemeDetails.EUIN = data.EUIN;
            this.BestSaverSchemeDetails.EasyInvestFlag = data.EasyInvestFlag;
            this.BestSaverSchemeDetails.Folio = data.folioNo;
            this.BestSaverSchemeDetails.FreeAmt = data.FreeAmt;
            this.BestSaverSchemeDetails.FreeSwpAmt = data.FreeSwpAmt;
            this.BestSaverSchemeDetails.FreeUnits = data.FreeUnits;
            this.BestSaverSchemeDetails.InProcessAmt = data.InProcessAmt;
            this.BestSaverSchemeDetails.InstaRedeemEligible = data.InstaRedeemEligible;
            this.BestSaverSchemeDetails.Insta_Amount = data.Insta_Amount;
            this.BestSaverSchemeDetails.Insta_Units = data.Insta_Units;
            this.BestSaverSchemeDetails.InvestedAmt = data.InvestedAmt;
            this.BestSaverSchemeDetails.LockinAmt = data.LockinAmt;
            this.BestSaverSchemeDetails.LockinSWPAmt = data.LockinSWPAmt;
            this.BestSaverSchemeDetails.LockinUnits = data.LockinUnits;
            this.BestSaverSchemeDetails.MinAmt = data.MinAmt;
            this.BestSaverSchemeDetails.MinUnits = data.MinUnits;
            this.BestSaverSchemeDetails.Msg1 = data.Msg1;
            this.BestSaverSchemeDetails.Msg2 = data.Msg2;
            this.BestSaverSchemeDetails.NAV = data.NAV;
            this.BestSaverSchemeDetails.NAV_Date = data.NAV_Date;
            this.BestSaverSchemeDetails.Option_Code = data.Option_Code;
            this.BestSaverSchemeDetails.Plan_Code = data.Plan_Code;
            this.BestSaverSchemeDetails.PledgeUnits = data.PledgeUnits;
            this.BestSaverSchemeDetails.RestrictedAmt = data.RestrictedAmt;
            this.BestSaverSchemeDetails.RestrictedUnits = data.RestrictedUnits;
            this.BestSaverSchemeDetails.Return_Msg = data.Return_Msg;
            this.BestSaverSchemeDetails.Return_Code = data.Return_Code;
            this.BestSaverSchemeDetails.SubARNCode = data.SubARNCode;
            this.BestSaverSchemeDetails.SchemeCategory = data.SchemeCategory;
            this.BestSaverSchemeDetails.SchemeDescription = data.SchemeDescription;
            this.BestSaverSchemeDetails.Scheme_Code = data.Scheme_Code;
            this.BestSaverSchemeDetails.TotalAmt = data.TotalAmt;
            this.BestSaverSchemeDetails.Totalunits = data.Totalunits;
            this.BestSaverSchemeDetails.GoalCode = data.GoalCode;
            this.BestSaverSchemeDetails.TypeCode = data.TypeCode;
            this.BestSaverSchemeDetails.Usergoalid = data.Usergoalid;
            this.BestSaverSchemeDetails.Scheme_Id = data.Scheme_Id;
            this.BestSaverSchemeDetails.Invst_FCode = data.Invst_FCode;
            this.BestSaverSchemeDetails.Invst_SCode = data.Invst_SCode;
            this.BestSaverSchemeDetails.Org_FundCode = data.Org_FundCode;
            this.BestSaverSchemeDetails.Org_SchemeCode = data.Org_SchemeCode;
            this.BestSaverSchemeDetails.BillDesk_AMC_Code = data.BillDesk_AMC_Code;
            this.BestSaverSchemeDetails.BillDesk_Code = data.BillDesk_Code;
            this.BestSaverSchemeDetails.ExlCode = data.ExlCode;
            this.BestSaverSchemeDetails.DivOption = data.DivOption;
            this.BestSaverSchemeDetails.folioNo = data.folioNo;
            this.BestSaverSchemeDetails.memberid = data.memberid;
            this.BestSaverSchemeDetails.profileid = data.profileid;
            this.BestSaverSchemeDetails.BankCode = data.BankCode;
            this.BestSaverSchemeDetails.Acc_no = data.Acc_no;
            this.BestSaverSchemeDetails.basicid = data.basicid;
            this.BestSaverSchemeDetails.bankid = data.bankid;
            this.BestSaverSchemeDetails.mandateid = data.mandateid;

            return this.BestSaverSchemeDetails;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));