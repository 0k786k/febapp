﻿(function (finApp) {
    'use strict';
    finApp.factory('bankChangeRequestModel', [function () {
        var factory = {};

        factory.BankChangeRequest = {
            BasicId: null,
            ifsc: null, 
            branchadd: null, 
            bankname: null, 
            branchname: null, 
            branchcity: null, 
            micr: null, 
            accountnumber: null, 
            accountname: null, 
            bankAccType: null, 
            devicetype: null, 
            reason: null, 
            chequeFile: null
        }

        factory.parseBankChangeRequest = function (data) {

            this.BankChangeRequest.BasicId= data.BasicId;
            this.BankChangeRequest.ifsc= data.ifsc; 
            this.BankChangeRequest.branchadd= data.branchadd; 
            this.BankChangeRequest.bankname= data.bankname; 
            this.BankChangeRequest.branchname= data.branchname; 
            this.BankChangeRequest.branchcity= data.branchcity; 
            this.BankChangeRequest.micr= data.micr; 
            this.BankChangeRequest.accountnumber= data.accountnumber; 
            this.BankChangeRequest.accountname= data.accountname; 
            this.BankChangeRequest.bankAccType= data.bankAccType; 
            this.BankChangeRequest.devicetype= data.devicetype; 
            this.BankChangeRequest.reason= data.reason; 
            this.BankChangeRequest.chequeFile = data.chequeFile;

            return this.BankChangeRequest;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));