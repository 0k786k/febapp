﻿(function (finApp) {
    'use strict';
    finApp.factory('eventAlertModel', [function () {
        var factory = {};

        factory.EventAlert = {
            eventName: null,
            eventDate: null,
            eventMsg: null
        }

        factory.parseEventAlert = function (data) {

            this.EventAlert.eventName = data.eventName;
            this.EventAlert.eventDate = data.eventDate;
            this.EventAlert.eventMsg = data.eventMsg;

            return this.EventAlert;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));