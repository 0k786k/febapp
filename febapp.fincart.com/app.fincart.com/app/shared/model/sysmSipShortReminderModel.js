﻿(function (finApp) {
    'use strict';
    finApp.factory('sysmSipShortReminderModel', [function () {
        var factory = {};

        factory.SysmSipShortReminder = {
            totalAmount: null,
            upcomingSipCount: null
        }

        factory.parseSysmSipShortReminder = function (data) {

            this.SysmSipShortReminder.totalAmount = data.totalAmount;
            this.SysmSipShortReminder.upcomingSipCount = data.upcomingSipCount;

            return this.SysmSipShortReminder;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));