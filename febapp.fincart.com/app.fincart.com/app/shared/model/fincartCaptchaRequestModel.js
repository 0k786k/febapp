﻿(function (finApp) {
    'use strict';
    finApp.factory('fincartCaptchaRequestModel', [function () {
        var factory = {};

        factory.FincartCaptchaRequest = {
            pannumber: null,
            email: null,
            mobile: null,
            name: null,
            basicid: null,
            GroupLeader: null,
            IsKYC: null
        }

        factory.parseFincartCaptchaRequest = function (data) {

            this.FincartCaptchaRequest.pannumber = data.pannumber;
            this.FincartCaptchaRequest.email = data.email;
            this.FincartCaptchaRequest.mobile = data.mobile;
            this.FincartCaptchaRequest.name = data.name;
            this.FincartCaptchaRequest.basicid = data.basicid;
            this.FincartCaptchaRequest.GroupLeader = data.GroupLeader;
            this.FincartCaptchaRequest.IsKYC = data.IsKYC;

            return this.FincartCaptchaRequest;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));