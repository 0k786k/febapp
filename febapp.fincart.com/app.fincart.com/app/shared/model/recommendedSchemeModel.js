﻿(function (finApp) {
    'use strict';
    finApp.factory('recommendedSchemeModel', [function () {
        var factory = {};

        factory.RecommendedScheme = {
            risk: null,
            amount: null,
            duration: null,
            usergoalId: null
        }

        factory.parseRecommendedScheme = function (data) {

            this.RecommendedScheme.risk = data.risk;
            this.RecommendedScheme.amount = data.amount;
            this.RecommendedScheme.duration = data.duration;
            this.RecommendedScheme.usergoalId = data.usergoalId;

            return this.RecommendedScheme;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));