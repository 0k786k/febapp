﻿(function (finApp) {
    'use strict';
    finApp.factory('POIPOAFileUploadModel', [function () {
        var factory = {};

        factory.POIPOAFileUpload = {
            basicid: null,
            type: null,
            panfile1: null,
            frontfile2: null,
            backfile3: null
        }

        factory.parsePOIPOAFileUpload = function (data) {

            this.POIPOAFileUpload.basicid = data.basicid;
            this.POIPOAFileUpload.type = data.type;
            this.POIPOAFileUpload.panfile1 = data.panfile1;
            this.POIPOAFileUpload.frontfile2 = data.frontfile2;
            this.POIPOAFileUpload.backfile3 = data.backfile3;

            return this.POIPOAFileUpload;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));