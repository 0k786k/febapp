﻿(function (finApp) {
    'use strict';
    finApp.factory('userCartDetailsListModel', ['userCartDetailsModel', function (userCartDetailsModel) {
        var factory = {};

        factory.UserCartDetailsList = {
            userCartDetailsList: []
        }

        factory.parseUserCartDetailsList = function (data) {

            this.UserCartDetailsList.userCartDetailsList = [];

            angular.forEach(data, function (value, key) {
                userCartDetailsModel.UserCartDetails = value;
                this.UserCartDetailsList.userCartDetailsList.push(userCartDetailsModel.UserCartDetails);
            }, this);

            return this.UserCartDetailsList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));