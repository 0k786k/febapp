﻿(function (finApp) {
    'use strict';
    finApp.factory('investmentAccountsModel', [function () {
        var factory = {};

        factory.InvestmentAccounts = {
            ProfileId: null,
            FirstApplicant: null,
            SecondApplicant: null,
            ThirdApplicant: null,
            FirstPAN: null,
            SecondPAN: null,
            ThirdPAN: null,
            HoldingMode: null,
            firstApplicantBasicID: null,
            secondApplicantBasicID: null,
            thirdApplicantBasicID: null,
            groupLeaderBasicID: null
        }

        factory.parseInvestmentAccount = function (data) {

            this.InvestmentAccount.ProfileId= data.ProfileId;
            this.InvestmentAccount.FirstApplicant= data.FirstApplicant;
            this.InvestmentAccount.SecondApplicant= data.SecondApplicant;
            this.InvestmentAccount.ThirdApplicant= data.ThirdApplicant;
            this.InvestmentAccount.FirstPAN= data.FirstPAN;
            this.InvestmentAccount.SecondPAN= data.SecondPAN;
            this.InvestmentAccount.ThirdPAN= data.ThirdPAN;
            this.InvestmentAccount.HoldingMode= data.HoldingMode;
            this.InvestmentAccount.firstApplicantBasicID= data.firstApplicantBasicID;
            this.InvestmentAccount.secondApplicantBasicID= data.secondApplicantBasicID;
            this.InvestmentAccount.thirdApplicantBasicID= data.thirdApplicantBasicID;
            this.InvestmentAccount.groupLeaderBasicID= data.groupLeaderBasicID;

            return this.InvestmentAccount;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));