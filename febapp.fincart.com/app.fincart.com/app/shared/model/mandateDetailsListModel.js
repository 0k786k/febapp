﻿(function (finApp) {
    'use strict';
    finApp.factory('mandateDetailsListModel', ['mandateDetailsModel', function (mandateDetailsModel) {
        var factory = {};

        factory.MandateDetailsList = {
            mandateList: []
        }

        factory.parseMandateDetailsList = function (data) {

            this.MandateDetailsList.mandateList = [];

            angular.forEach(data, function (value, key) {
                mandateDetailsModel.MandateDetails = value;
                this.MandateDetailsList.mandateList.push(mandateDetailsModel.MandateDetails);
            }, this);

            return this.MandateDetailsList;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));