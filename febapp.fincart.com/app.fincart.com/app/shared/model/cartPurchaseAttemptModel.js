﻿(function (finApp) {
    'use strict';
    finApp.factory('cartPurchaseAttemptModel', [function () {
        var factory = {};

        factory.CartPurchaseAttempt = {
            basicID: null,
            ProfileID: null,
            trxntblId: null,
            cartIdBunch: null,
            paymodeId: null,
            returnUrl: null,
            productId: null
        }

        factory.parseCartPurchaseAttempt = function (data) {

            this.CartPurchaseAttempt.basicID = data.basicID;
            this.CartPurchaseAttempt.ProfileID = data.ProfileID;
            this.CartPurchaseAttempt.trxntblId = data.trxntblId;
            this.CartPurchaseAttempt.cartIdBunch = data.cartIdBunch;
            this.CartPurchaseAttempt.paymodeId = data.paymodeId;
            this.CartPurchaseAttempt.returnUrl = data.returnUrl;
            this.CartPurchaseAttempt.productId = data.productId;

            return this.CartPurchaseAttempt;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));