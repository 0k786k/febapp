﻿(function (finApp) {
    'use strict';
    finApp.factory('cafeditRequestsModel', [function () {
        var factory = {};

        factory.CafeditRequests = {
            CafRequestId: null,
            CafRequestType: null,
            CafRequestStatus: null,
            BirthCertificateFileName: null,
            BirthCertificate: null,
            Pan: null,
            Passport: null,
            Itin: null,
            Aadhar: null,
            hufpan: null,
            hufbank: null,
            cancelcheque: null,
            Client_Name: null,
            Logged_UserId: null,
            BasicId: null,
            GroupLeaderName: null
        }

        factory.parseCafeditRequests = function (data) {

            this.CafeditRequests.CafRequestId= data.CafRequestId;
            this.CafeditRequests.CafRequestType= data.CafRequestType;
            this.CafeditRequests.CafRequestStatus= data.CafRequestStatus;
            this.CafeditRequests.BirthCertificateFileName= data.BirthCertificateFileName;
            this.CafeditRequests.BirthCertificate= data.BirthCertificate;
            this.CafeditRequests.Pan= data.Pan;
            this.CafeditRequests.Passport= data.Passport;
            this.CafeditRequests.Itin= data.Itin;
            this.CafeditRequests.Aadhar= data.Aadhar;
            this.CafeditRequests.hufpan= data.hufpan;
            this.CafeditRequests.hufbank= data.hufbank;
            this.CafeditRequests.cancelcheque= data.cancelcheque;
            this.CafeditRequests.Client_Name= data.Client_Name;
            this.CafeditRequests.Logged_UserId= data.Logged_UserId;
            this.CafeditRequests.BasicId= data.BasicId;
            this.CafeditRequests.GroupLeaderName= data.GroupLeaderName;

            return this.CafeditRequests;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));