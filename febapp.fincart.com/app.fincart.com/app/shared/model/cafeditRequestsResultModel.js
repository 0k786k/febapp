﻿(function (finApp) {
    'use strict';
    finApp.factory('cafeditRequestsResultModel', ['cafeditRequestsModel', function (cafeditRequestsModel) {
        var factory = {};

        factory.CafeditRequestsResult = {
            cafeditRequests: []
        }

        factory.parseCafeditRequestsResult = function (data) {

            this.CafeditRequestsResult.cafeditRequests = [];

            angular.forEach(data.cafeditRequests, function (value, key) {
                cafeditRequestsModel.CafeditRequests = value;
                this.CafeditRequestsResult.cafeditRequests.push(cafeditRequestsModel.CafeditRequests);
            }, this);

            return this.CafeditRequestsResult;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));