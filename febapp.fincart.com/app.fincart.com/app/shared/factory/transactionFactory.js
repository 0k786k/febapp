﻿(function (finApp) {
    'use strict';
    finApp.factory('transactionFactory', ['$localStorage', function ($localStorage) {
        var factory = {};

        //#region (User Current Payment Gateway Redirect Bunch Transaction Id) 
        factory.setCurrentBunchTxnId = function (data) {
            $localStorage.CurrentBunchTxnId = data;
        }

        factory.destroyCurrentBunchTxnId = function () {
            delete $localStorage.CurrentBunchTxnId;
        }

        factory.getCurrentBunchTxnId = function () {
            if ($localStorage.CurrentBunchTxnId) {
                return $localStorage.CurrentBunchTxnId;
            }
            return 0;
        }
        //#endregion (User Current Payment Gateway Redirect Bunch Transaction Id)

        return factory;
    }]);
})(angular.module('fincartApp'));