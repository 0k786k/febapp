﻿(function (userMod) {
    'use strict';
    userMod.factory('userprofileFactory', ['$http', '$q', '$localStorage', '$rootScope', function ($http, $q, $localStorage, $rootScope) {
        var factory = {};

        //#region UUID
        //set user unique id in local storage
        factory.setUUID = function (UUID) {
            if (!$localStorage.UUID) {
                $localStorage.UUID = UUID;
            }
        }

        // get user unique id from local storage
        factory.getUUID = function () {
            var UUID = false;
            if ($localStorage.UUID) {
                UUID = $localStorage.UUID;
            }
            return UUID;
        }

        // remove user unique id from local storage
        factory.clearUUID = function () {
            if ($localStorage.UUID) {
                delete $localStorage.UUID;
            }
        }
        // #endregion UUID

        //#region Login Status
        //set user login status local storage
        factory.setisloggedin = function (status) {
            $localStorage.isloggedin = status;
        }

        // get user login status from local storage
        factory.getisloggedin = function () {
            var status = false;
            if ($localStorage.isloggedin) {
                status = $localStorage.isloggedin;
            }
            return status;
        }

        // remove user login status from local storage
        factory.clearUUID = function () {
            if ($localStorage.isloggedin) {
                delete $localStorage.isloggedin;
            }
        }
        //#endregion Login Status

        //#region Manage Profile Details
        //profile details variables
        factory.userProfileDetails = {
            code: null,
            firstname: null,
            lastname: null,
            email: null,
            userid: null,
            password: null,
            mobile: null,
            status: null,
            role: null

        }

        //set user profile details by user token
        factory.setUserProfileDetails = function (data) {
            this.userProfileDetails.code = data.Code;

            var userfullname = data.Name.split(/(\s+)/);

            this.userProfileDetails.firstname = userfullname[0] ? userfullname[0] : "";
            this.userProfileDetails.lastname = userfullname[2] ? userfullname[2] : "";
            this.userProfileDetails.email = data.Userid;
            this.userProfileDetails.userid = data.Userid;
            this.userProfileDetails.password = data.password;
            this.userProfileDetails.mobile = data.Mobile;
            this.userProfileDetails.status = data.ClientStatus;
            //this.userProfileDetails.role = data.role;

            // store userdetails in local storage to keep user logged in between page refreshes
            $localStorage.currentUser = this.userProfileDetails;
        }

        // remove user from local storage
        factory.clearUserProfileDetails = function () {
            delete $localStorage.currentUser;
        }

        // get user profile details
        factory.getUserProfileDetails = function () {

            return $localStorage.currentUser;



        }
        //#endregion Manage Profile Details


        //#region Fintoken Access
        //user token details variables
        factory.userToken = {
            access_token: null,
            token_type: null,
            expires_in: null,
            refresh_token: null,
            error: null,
            error_description: null
        }

        //set user token sucess details by userid and password
        factory.setUserTokenSucess = function (data) {

            this.userToken.access_token = data.access_token;
            this.userToken.token_type = data.token_type;
            this.userToken.expires_in = data.expires_in;
            this.userToken.refresh_token = data.refresh_token;
            // store token in local storage to keep user logged in between page refreshes
            $localStorage.currentToken = this.userToken;
            // add jwt token to auth header for all requests made by the $http service
            $http.defaults.headers.common.Authorization = 'Bearer ' + data.access_token;
        }

        //set user token error details by userid and password
        factory.setUserTokenError = function (data) {
            
            this.userToken.error = data.error;
            this.userToken.error_description = data.error_description;
            $localStorage.currentToken = this.userToken;
        }

        // remove token from local storage and clear http auth header
        factory.destroyUserToken = function () {
            delete $localStorage.currentToken;
            $http.defaults.headers.common.Authorization = '';
        }

        //get user token details
        factory.getUserToken = function () {
            var usertoken;
            if ($localStorage.currentToken) {
                usertoken = $localStorage.currentToken;
            }
            else {
                usertoken = this.userToken;
            }
            return usertoken;
        }
        //#endregion Fintoken Access


        //#region Groupleader Pan Details
        //pan details variables
        factory.userPanDetails = {
            status: false,
            number: null,
            name: null,
            date: null,
            registrar: null
        }

        //set user pan details
        factory.setuserPanDetails = function (data) {
            this.userPanDetails.status = data.KYC_STATUS === "Y" ? true : false;
            this.userPanDetails.number = data.PAN_NUMBER;
            this.userPanDetails.name = data.PAN_NAME;
            this.userPanDetails.date = data.KYC_DATE;
            this.userPanDetails.registrar = data.KYC_FROM;
            // store userPandetails in local storage to keep user logged in between page refreshes
            $localStorage.currentUserPan = this.userPanDetails;
        }

        // remove user pan from local storage
        factory.clearuserPanDetails = function () {
            delete $localStorage.currentUserPan;
        }

        // get user pan details
        factory.getuserPanDetails = function () {
            var userpan;
            if ($localStorage.currentUserPan) {
                userpan = $localStorage.currentUserPan;
            }
            else {
                userpan = this.userPanDetails;
            }
            return userpan;
        }
        //#endregion Groupleader Pan Details


        //#region Member Pan Details
        //set NewMember pan details
        factory.setNewMemberPanDetails = function (data) {
            this.userPanDetails.status = data.KYC_STATUS === "Y" ? true : false;
            this.userPanDetails.number = data.PAN_NUMBER;
            this.userPanDetails.name = data.PAN_NAME;
            this.userPanDetails.date = data.KYC_DATE;
            this.userPanDetails.registrar = data.KYC_FROM;
            // store NewMember Pan details in local storage to keep user logged in between page refreshes
            $localStorage.newMemberPan = this.userPanDetails;
        }

        // remove newMemberPan from local storage
        factory.clearNewMemberPanDetails = function () {
            delete $localStorage.newMemberPan;
        }

        // get newMemberPan details
        factory.getNewMemberPanDetails = function () {
            var userpan;
            if ($localStorage.newMemberPan) {
                userpan = $localStorage.newMemberPan;
            }
            else {
                userpan = this.userPanDetails;
            }
            return userpan;
        }
        //#endregion Member Pan Details


        //#region Minor Pan Details
        //minor pan details variables
        factory.minorPanDetails = {
            status: false,
            number: null,
            name: null,
            date: null,
            registrar: null
        }

        //set minor pan details
        factory.setminorPanDetails = function (data) {
            this.minorPanDetails.status = data.KYC_STATUS === "Y" ? true : false;
            this.minorPanDetails.number = data.PAN_NUMBER;
            this.minorPanDetails.name = data.PAN_NAME;
            this.minorPanDetails.date = data.KYC_DATE;
            this.minorPanDetails.registrar = data.KYC_FROM;
            // store minorPandetails in local storage to keep user logged in between page refreshes
            $localStorage.currentminorPan = this.minorPanDetails;
        }

        // remove minor pan from local storage
        factory.clearminorPanDetails = function () {
            delete $localStorage.currentminorPan;
        }

        // get minor pan details
        factory.getminorPanDetails = function () {
            var minorpan;
            if ($localStorage.currentminorPan) {
                minorpan = $localStorage.currentminorPan;
            }
            else {
                minorpan = this.minorPanDetails;
            }
            return minorpan;
        }
        //#endregion Minor Pan Details


        //#region Gaurdian Pan Details
        //gaurdian pan details variables
        factory.gaurdianPanDetails = {
            status: false,
            number: null,
            name: null,
            date: null,
            registrar: null
        }

        //set gaurdian pan details
        factory.setgaurdianPanDetails = function (data) {
            this.gaurdianPanDetails.status = data.KYC_STATUS === "Y" ? true : false;
            this.gaurdianPanDetails.number = data.PAN_NUMBER;
            this.gaurdianPanDetails.name = data.PAN_NAME;
            this.gaurdianPanDetails.date = data.KYC_DATE;
            this.gaurdianPanDetails.registrar = data.KYC_FROM;
            // store gaurdianPandetails in local storage to keep user logged in between page refreshes
            $localStorage.currentgaurdianPan = this.gaurdianPanDetails;
        }

        // remove gaurdian pan from local storage
        factory.cleargaurdianPanDetails = function () {
            delete $localStorage.currentgaurdianPan;
        }

        // get gaurdian pan details
        factory.getgaurdianPanDetails = function () {
            var gaurdianpan;
            if ($localStorage.currentgaurdianPan) {
                gaurdianpan = $localStorage.currentgaurdianPan;
            }
            else {
                gaurdianpan = this.gaurdianPanDetails;
            }
            return gaurdianpan;
        }
        //#endregion Gaurdian Pan Details


        //#region Bank Details
        //bank details variables
        factory.bankDetails = {
            status: false,
            ifsc: null,
            micr: null,
            address: null,
            bankname: null,
            branchname: null,
            branchcity: null
        }
        

        //set bank details
        factory.setbankDetails = function (data) {
            
            this.bankDetails.status = data.BankAddress? true : false;
            if (data.BankAddress) {
                this.bankDetails.ifsc = data.IFSC;
                this.bankDetails.micr = data.MICR;
                this.bankDetails.address = data.BankAddress;
                this.bankDetails.bankname = data.BankName;
                this.bankDetails.branchname = data.Branch;
                this.bankDetails.branchcity = data.City;
            }
            else {
                this.bankDetails.ifsc = null;
                this.bankDetails.micr = null;
                this.bankDetails.address = null;
                this.bankDetails.bankname = null;
                this.bankDetails.branchname = null;
                this.bankDetails.branchcity = null;
            }

            // store banketails in local storage to keep user logged in between page refreshes
            $localStorage.currentbank = this.bankDetails;
        }

        // remove bank from local storage
        factory.clearbankDetails = function () {
            delete $localStorage.currentbank;
        }

        // get bank details
        factory.getbankDetails = function () {
            var bank;
            if ($localStorage.currentbank) {
                bank = $localStorage.currentbank;
            }
            else {
                bank = this.bankDetails;
            }
            return bank;
        }
        //#endregion Bank Details


        //#region OLD CODE -- (Caf Details)
        //Caf details variables
        factory.userCafDetails = {
            status: false,
            category: null,
            haskyc: null,
            pannumber: null,
            panname: null,
            dob: null,
            mpannumber: null,
            mpanname: null,
            mdob: null,
            gpannumber: null,
            gpanname: null,
            gdob: null,
            occupation: null,
            income: null,
            mobile: null,
            address: null,
            city: null,
            state: null,
            country: null,
            pin: null,
            nricity: null,
            nristate: null,
            nricountry: null,
            ifsc: null,
            micr: null,
            branchaddress: null,
            bankname: null,
            bankfullname: null,
            branchname: null,
            branchcity: null,
            accounttype: null,
            accountnumber: null,
            bankaccountname: null,
            hasnominee: null,
            nominee1name: null,
            nominee1relation: null,
            nominee1dob: null,
            nominee1share: null,
            nominee2name: null,
            nominee2relation: null,
            nominee2dob: null,
            nominee2share: null,
            nominee3name: null,
            nominee3relation: null,
            nominee3dob: null,
            nominee3share: null,
            birthplace: null,
            birthcountry: null,
            wealthsource: null,
            ispep: null,
            istaxother: null,
            taxcountry1: null,
            taxidnumber1: null,
            taxidtype1: null,
            taxcountry2: null,
            taxidnumber2: null,
            taxidtype2: null,
            taxcountry3: null,
            taxidnumber3: null,
            taxidtype3: null

        }

        //set user caf status
        //factory.setuserCafStatus = function (status) {
        //    var usercaf;
        //    if ($localStorage.currentUserCaf) {
        //        usercaf = $localStorage.currentUserCaf;
        //    }
        //    else {
        //        usercaf = this.USERCAF;
        //    }
        //    usercaf.status = status;
        //    usercaf.category = null;
        //    usercaf.haskyc = null;
        //    usercaf.pannumber = null;
        //    usercaf.panname = null;
        //    usercaf.dob = null;
        //    usercaf.mpannumber = null;
        //    usercaf.mpanname = null;
        //    usercaf.mdob = null;
        //    usercaf.gpannumber = null;
        //    usercaf.gpanname = null;
        //    usercaf.gdob = null;
        //    usercaf.occupation = null;
        //    usercaf.income = null;
        //    usercaf.mobile = null;
        //    usercaf.address = null;
        //    usercaf.city = null;
        //    usercaf.state = null;
        //    usercaf.country = null;
        //    usercaf.pin = null;
        //    usercaf.nricity = null;
        //    usercaf.nristate = null;
        //    usercaf.nricountry = null;
        //    usercaf.ifsc = null;
        //    usercaf.micr = null;
        //    usercaf.branchaddress = null;
        //    usercaf.bankname = null;
        //    usercaf.bankfullname = null;
        //    usercaf.branchname = null;
        //    usercaf.branchcity = null;
        //    usercaf.accounttype = null;
        //    usercaf.accountnumber = null;
        //    usercaf.bankaccountname = null;
        //    usercaf.hasnominee = null;
        //    usercaf.nominee1name = null;
        //    usercaf.nominee1relation = null;
        //    usercaf.nominee1dob = null;
        //    usercaf.nominee1share = null;
        //    usercaf.nominee2name = null;
        //    usercaf.nominee2relation = null;
        //    usercaf.nominee2dob = null;
        //    usercaf.nominee2share = null;
        //    usercaf.nominee3name = null;
        //    usercaf.nominee3relation = null;
        //    usercaf.nominee3dob = null;
        //    usercaf.nominee3share = null;
        //    usercaf.birthplace = null;
        //    usercaf.birthcountry = null;
        //    usercaf.wealthsource = null;
        //    usercaf.ispep = null;
        //    usercaf.istaxother = null;
        //    usercaf.taxcountry1 = null;
        //    usercaf.taxidnumber1 = null;
        //    usercaf.taxidtype1 = null;
        //    usercaf.taxcountry2 = null;
        //    usercaf.taxidnumber2 = null;
        //    usercaf.taxidtype2 = null;
        //    usercaf.taxcountry3 = null;
        //    usercaf.taxidnumber3 = null;
        //    usercaf.taxidtype3 = null
        //    // store userCafdetails in local storage
        //    $localStorage.currentUserCaf = usercaf;
        //}

        //set user caf details
        factory.setuserCafDetails = function (data) {

            // store usercafdetails in local storage
            this.USERCAF = data;
            $localStorage.currentUserCaf = this.USERCAF;
        }

        // remove user caf from local storage
        factory.clearuserCafDetails = function () {

            delete $localStorage.currentUserCaf;
        }

        // get user caf details
        factory.getuserCafDetails = function () {
            
            var usercaf;
            if ($localStorage.currentUserCaf) {
                usercaf = $localStorage.currentUserCaf;
            }
            else {
                usercaf = this.USERCAF;
            }
            return usercaf;
        }

        // get user current age from caf dob
        factory.getuserCurrentAge = function () {

            var currge = 0;
            if ($localStorage.currentUserCaf) {
                var today = new Date();
                var birthDate = new Date($localStorage.currentUserCaf.dob);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                currge = age;
            }
            return currge;
        }

        //--------------------MemberCaf Methods-------------------


        //set Membercaf details
        factory.setMemberCafDetails = function (data) {

            // store usercafdetails in local storage
            this.USERCAF = data;
            $localStorage.memberCaf = this.USERCAF;
        }

        // remove Membercaf from local storage
        factory.clearMemberCafDetails = function () {

            delete $localStorage.memberCaf;
        }

        // get Membercaf details
        factory.getMemberCafDetails = function () {
            var usercaf;
            if ($localStorage.memberCaf) {
                usercaf = $localStorage.memberCaf;
            }
            else {
                usercaf = this.USERCAF;
            }
            return usercaf;
        }

        //get CAF Details of member from API Investwell
        factory.setMemberCafDetailsFromApi = function (data, kycstatus) {


            // store usercafdetails in local storage
            this.USERCAF.category = data.Status;
            this.USERCAF.panname = data.ClientName;
            this.USERCAF.category = data.Status;
            this.USERCAF.address = data.Address1;
            this.USERCAF.city = data.City;
            this.USERCAF.state = data.State;
            this.USERCAF.country = data.Country;
            this.USERCAF.pin = data.Pin;
            this.USERCAF.mobile = data.Mobile;
            this.USERCAF.occupation = data.Occupation;
            this.USERCAF.dob = data.Date_of_Birth;
            this.USERCAF.gpanname = data.Guardname;
            this.USERCAF.gpannumber = data.Guardpan;
            this.USERCAF.gdob = data.Guardian_DOB;
            this.USERCAF.nricity = data.CityName;
            this.USERCAF.nristate = data.StateName;
            this.USERCAF.nricountry = data.CountryName;
            this.USERCAF.income = data.AnnIncome;
            this.USERCAF.pannumber = data.First_pan;
            this.USERCAF.haskyc = kycstatus === "Y" ? true : false;
            this.USERCAF.bankname = data.Bank_name;
            this.USERCAF.accounttype = data.Acc_Type;
            this.USERCAF.accountnumber = data.Acc_No;
            this.USERCAF.ifsc = data.IFSC;
            this.USERCAF.bankaccountname = data.NameAsPerBank;
            this.USERCAF.micr = data.Ecsno;
            this.USERCAF.branchname = data.Branch;
            this.USERCAF.branchcity = data.Bankcity;
            this.USERCAF.branchaddress = data.Bank_Address;
            this.USERCAF.hasnominee = data.ReqNominee;
            this.USERCAF.nominee1name = data.Nominee_Name;
            this.USERCAF.nominee1relation = data.Relation;
            this.USERCAF.nominee1dob = data.Nominee_Dob;
            this.USERCAF.nominee1share = data.Nom1_percent;
            this.USERCAF.nominee2name = data.Nom2_name;
            this.USERCAF.nominee2relation = data.Nom2_relation;
            this.USERCAF.nominee2dob = data.Nom2_dob;
            this.USERCAF.nominee2share = data.Nom2_percent;
            this.USERCAF.nominee3name = data.Nom3_Name;
            this.USERCAF.nominee3relation = data.Nom3_Relation;
            this.USERCAF.nominee3dob = data.Nom3_Dob;
            this.USERCAF.nominee3share = data.Nom3_Percent;
            this.USERCAF.birthplace = data.Place_of_Birth;
            this.USERCAF.birthcountry = data.Country_of_Birth;
            this.USERCAF.wealthsource = data.Source_Wealth;
            this.USERCAF.ispep = data.Pol_Exp_Person;
            this.USERCAF.taxcountry1 = data.Country_Tax_Res1;
            this.USERCAF.taxidnumber1 = data.CountryPayer_Id_no;
            this.USERCAF.taxidtype1 = data.Idenif_Type;
            this.USERCAF.taxcountry2 = data.Country_Tax_Res2;
            this.USERCAF.taxidnumber2 = data.CountryPayer_Id_no2;
            this.USERCAF.taxidtype2 = data.Idenif_Type2;
            this.USERCAF.taxcountry3 = data.Country_Tax_Res3;
            this.USERCAF.taxidnumber3 = data.CountryPayer_Id_no3;
            this.USERCAF.taxidtype3 = data.Idenif_Type3
            $localStorage.memberCaf = this.USERCAF;

        }

        //get CAF Details of LoggedIn User/Current from API Investwell
        factory.setUserCafDetailsFromApi = function (data, kycstatus) {


            // store usercafdetails in local storage
            this.USERCAF.category = data.Status;
            this.USERCAF.panname = data.ClientName;
            this.USERCAF.category = data.Status;
            this.USERCAF.address = data.Address1;
            this.USERCAF.city = data.City;
            this.USERCAF.state = data.State;
            this.USERCAF.country = data.Country;
            this.USERCAF.pin = data.Pin;
            this.USERCAF.mobile = data.Mobile;
            this.USERCAF.occupation = data.Occupation;
            this.USERCAF.dob = data.Date_of_Birth;
            this.USERCAF.gpanname = data.Guardname;
            this.USERCAF.gpannumber = data.Guardpan;
            this.USERCAF.gdob = data.Guardian_DOB;
            this.USERCAF.nricity = data.CityName;
            this.USERCAF.nristate = data.StateName;
            this.USERCAF.nricountry = data.CountryName;
            this.USERCAF.income = data.AnnIncome;
            this.USERCAF.pannumber = data.First_pan;
            this.USERCAF.haskyc = kycstatus === "Y" ? true : false;
            this.USERCAF.bankname = data.Bank_name;
            this.USERCAF.accounttype = data.Acc_Type;
            this.USERCAF.accountnumber = data.Acc_No;
            this.USERCAF.ifsc = data.IFSC;
            this.USERCAF.bankaccountname = data.NameAsPerBank;
            this.USERCAF.micr = data.Ecsno;
            this.USERCAF.branchname = data.Branch;
            this.USERCAF.branchcity = data.Bankcity;
            this.USERCAF.branchaddress = data.Bank_Address;
            this.USERCAF.hasnominee = data.ReqNominee;
            this.USERCAF.nominee1name = data.Nominee_Name;
            this.USERCAF.nominee1relation = data.Relation;
            this.USERCAF.nominee1dob = data.Nominee_Dob;
            this.USERCAF.nominee1share = data.Nom1_percent;
            this.USERCAF.nominee2name = data.Nom2_name;
            this.USERCAF.nominee2relation = data.Nom2_relation;
            this.USERCAF.nominee2dob = data.Nom2_dob;
            this.USERCAF.nominee2share = data.Nom2_percent;
            this.USERCAF.nominee3name = data.Nom3_Name;
            this.USERCAF.nominee3relation = data.Nom3_Relation;
            this.USERCAF.nominee3dob = data.Nom3_Dob;
            this.USERCAF.nominee3share = data.Nom3_Percent;
            this.USERCAF.birthplace = data.Place_of_Birth;
            this.USERCAF.birthcountry = data.Country_of_Birth;
            this.USERCAF.wealthsource = data.Source_Wealth;
            this.USERCAF.ispep = data.Pol_Exp_Person;
            this.USERCAF.taxcountry1 = data.Country_Tax_Res1;
            this.USERCAF.taxidnumber1 = data.CountryPayer_Id_no;
            this.USERCAF.taxidtype1 = data.Idenif_Type;
            this.USERCAF.taxcountry2 = data.Country_Tax_Res2;
            this.USERCAF.taxidnumber2 = data.CountryPayer_Id_no2;
            this.USERCAF.taxidtype2 = data.Idenif_Type2;
            this.USERCAF.taxcountry3 = data.Country_Tax_Res3;
            this.USERCAF.taxidnumber3 = data.CountryPayer_Id_no3;
            this.USERCAF.taxidtype3 = data.Idenif_Type3
            $localStorage.currentUserCaf = this.USERCAF;


        }

        //get CAF Details of LoggedIn User/Current from Fincart Database
        factory.setUserCafFromDatabase = function (data, kycstatus) {


            // store usercafdetails in local storage
            this.USERCAF.category = data.Status;
            this.USERCAF.panname = data.ClientName;
            this.USERCAF.category = data.Status;
            this.USERCAF.address = data.Address1;
            this.USERCAF.city = data.City;
            this.USERCAF.state = data.State;
            this.USERCAF.country = data.Country;
            this.USERCAF.pin = data.Pin;
            this.USERCAF.mobile = data.Mobile;
            this.USERCAF.occupation = data.Occupation;
            this.USERCAF.dob = data.Date_of_Birth;
            this.USERCAF.gpanname = data.Guardname;
            this.USERCAF.gpannumber = data.Guardpan;
            this.USERCAF.gdob = data.Guardian_DOB;
            this.USERCAF.nricity = data.CityName;
            this.USERCAF.nristate = data.StateName;
            this.USERCAF.nricountry = data.CountryName;
            this.USERCAF.income = data.AnnIncome;
            this.USERCAF.pannumber = data.First_pan;
            this.USERCAF.haskyc = kycstatus === "Y" ? true : false;
            this.USERCAF.bankname = data.Bank_name;
            this.USERCAF.accounttype = data.Acc_Type;
            this.USERCAF.accountnumber = data.Acc_No;
            this.USERCAF.ifsc = data.IFSC;
            this.USERCAF.bankaccountname = data.NameAsPerBank;
            this.USERCAF.micr = data.Ecsno;
            this.USERCAF.branchname = data.Branch;
            this.USERCAF.branchcity = data.Bankcity;
            this.USERCAF.branchaddress = data.Bank_Address;
            this.USERCAF.hasnominee = data.ReqNominee;
            this.USERCAF.nominee1name = data.Nominee_Name;
            this.USERCAF.nominee1relation = data.Relation;
            this.USERCAF.nominee1dob = data.Nominee_Dob;
            this.USERCAF.nominee1share = data.Nom1_percent;
            this.USERCAF.nominee2name = data.Nom2_name;
            this.USERCAF.nominee2relation = data.Nom2_relation;
            this.USERCAF.nominee2dob = data.Nom2_dob;
            this.USERCAF.nominee2share = data.Nom2_percent;
            this.USERCAF.nominee3name = data.Nom3_Name;
            this.USERCAF.nominee3relation = data.Nom3_Relation;
            this.USERCAF.nominee3dob = data.Nom3_Dob;
            this.USERCAF.nominee3share = data.Nom3_Percent;
            this.USERCAF.birthplace = data.Place_of_Birth;
            this.USERCAF.birthcountry = data.Country_of_Birth;
            this.USERCAF.wealthsource = data.Source_Wealth;
            this.USERCAF.ispep = data.Pol_Exp_Person;
            this.USERCAF.taxcountry1 = data.Country_Tax_Res1;
            this.USERCAF.taxidnumber1 = data.CountryPayer_Id_no;
            this.USERCAF.taxidtype1 = data.Idenif_Type;
            this.USERCAF.taxcountry2 = data.Country_Tax_Res2;
            this.USERCAF.taxidnumber2 = data.CountryPayer_Id_no2;
            this.USERCAF.taxidtype2 = data.Idenif_Type2;
            this.USERCAF.taxcountry3 = data.Country_Tax_Res3;
            this.USERCAF.taxidnumber3 = data.CountryPayer_Id_no3;
            this.USERCAF.taxidtype3 = data.Idenif_Type3
            $localStorage.currentUserCaf = this.USERCAF;


        }
        //#endregion OLD CODE -- (Caf Details)


        //#region Cart Transaction
        //CART TRANSACTTION MODEL FOR LOCAL STORAGE

        factory.cartTransaction = {


            Scode: null,
            Fcode: null,
            Amount: null,
            NAVDate: null,
            DivOption: null,
            FolioNo: null,
            BankName: null,
            BankBranch: null,
            Account_No: null,
            ProfileID: null,

            cartId: null,

            type: null,

            startDate: null,
            endDate: null,
            noOfInstallment: null,
            mandateId: null,
            firstSipDate: null,
            mDate: null,
            frequency: null,
            billDeskCode: null,
            billAmcCode: null,
            schemeName: null,
            investProfile: null,
            bankCode: null

        }
        //--------------------START CART TRANSACTION LOCALSTORAGE FUNCTION-------------------
        //SET STORAGE
        factory.setCartTransDetails = function (data) {

            this.cartTransaction = data;
            $localStorage.cartTransact = this.cartTransaction;
        }

        // REMOVE STORAGE
        factory.clearCartTransDetails = function () {
            delete $localStorage.cartTransact;
        }

        // GET STORAGE
        factory.getCartTransDetails = function () {

            var cartTrans;
            if ($localStorage.cartTransact) {
                cartTrans = $localStorage.cartTransact;
            }
            else {
                cartTrans = this.cartTransaction;
            }
            return cartTrans;
        }
        //--------------------END CART TRANSACTION LOCALSTORAGE FUNCTION-------------------
        //#endregion Cart Transaction



        //#region -------NEW CODE (Manage CAF)#################################################################################################
        

        //GroupLeader CAF
        //NEW GroupLeader CAF variables
        factory.USERCAF = {
            status: false,
            category: null,
            haskyc: null,
            pannumber: null,
            panname: null,
            dob: null,
            mpannumber: null,
            mpanname: null,
            mdob: null,
            gpannumber: null,
            gpanname: null,
            gdob: null,
            occupation: null,
            income: null,
            mobile: null,
            address: null,
            city: null,
            state: null,
            country: null,
            pin: null,
            commcity: null,
            commstate: null,
            commcountry: null,
            commpin: null,
            nricity: null,
            nristate: null,
            nricountry: null,
            ifsc: null,
            micr: null,
            branchaddress: null,
            bankname: null,
            bankfullname: null,
            branchname: null,
            branchcity: null,
            accounttype: null,
            accountnumber: null,
            bankaccountname: null,
            hasnominee: null,
            nominee1name: null,
            nominee1relation: null,
            nominee1dob: null,
            nominee1share: null,
            nominee2name: null,
            nominee2relation: null,
            nominee2dob: null,
            nominee2share: null,
            nominee3name: null,
            nominee3relation: null,
            nominee3dob: null,
            nominee3share: null,
            birthplace: null,
            birthcountry: null,
            wealthsource: null,
            ispep: null,
            istaxother: null,
            taxcountry1: null,
            taxidnumber1: null,
            taxidtype1: null,
            taxcountry2: null,
            taxidnumber2: null,
            taxidtype2: null,
            taxcountry3: null,
            taxidnumber3: null,
            taxidtype3: null,

            memberid: null,
            basicid: null,
            groupleader: null,
            profilepic: null,
            aadhar: null,
            cafstatus: null,
            cafbasicstatus: null,
            cafaddressstatus: null,
            cafbankstatus: null,
            cafnomineestatus: null,
            caffatcastatus: null,
            Nom1Id: null,
            Nom2Id: null,
            Nom3Id: null,
            email: null,
            corpaddress: null,
            sameAddress: null
            
        }

        //Member CAF
        //NEW Member CAF variables
        factory.MEMBERCAF = {
            status: false,
            category: null,
            haskyc: null,
            pannumber: null,
            panname: null,
            dob: null,
            mpannumber: null,
            mpanname: null,
            mdob: null,
            gpannumber: null,
            gpanname: null,
            gdob: null,
            occupation: null,
            income: null,
            mobile: null,
            address: null,
            city: null,
            state: null,
            country: null,
            pin: null,
            commcity: null,
            commstate: null,
            commcountry: null,
            commpin: null,
            nricity: null,
            nristate: null,
            nricountry: null,
            ifsc: null,
            micr: null,
            branchaddress: null,
            bankname: null,
            bankfullname: null,
            branchname: null,
            branchcity: null,
            accounttype: null,
            accountnumber: null,
            bankaccountname: null,
            hasnominee: null,
            nominee1name: null,
            nominee1relation: null,
            nominee1dob: null,
            nominee1share: null,
            nominee2name: null,
            nominee2relation: null,
            nominee2dob: null,
            nominee2share: null,
            nominee3name: null,
            nominee3relation: null,
            nominee3dob: null,
            nominee3share: null,
            birthplace: null,
            birthcountry: null,
            wealthsource: null,
            ispep: null,
            istaxother: null,
            taxcountry1: null,
            taxidnumber1: null,
            taxidtype1: null,
            taxcountry2: null,
            taxidnumber2: null,
            taxidtype2: null,
            taxcountry3: null,
            taxidnumber3: null,
            taxidtype3: null,

            memberid: null,
            basicid: null,
            groupleader: null,
            profilepic: null,
            aadhar: null,
            cafstatus: null,
            cafbasicstatus: null,
            cafaddressstatus: null,
            cafbankstatus: null,
            cafnomineestatus: null,
            caffatcastatus: null,
            Nom1Id: null,
            Nom2Id: null,
            Nom3Id: null,
            email: null,
            corpaddress: null,
            sameAddress: null
        }


        //#region EKYC
        //EKYC
        //EKYC variables
        factory.EKYC = {
           
            invsttype: null,
            basicid: null,
            sessid: null,
            pan: null,
            email: null,
            errcode: null,
            errdesc: null,
            kycstatuscode: null,
            itpanname: null,
            aadhaarname: null,
            fathername: null,
            gender: null,
            maritalstatus: null,
            dob: null,
            nationality: null,
            aadhaarno: null,
            address1: null,
            address2: null,
            address3: null,
            city: null,
            state: null,
            pincode: null,
            mobileno: null,
            cafInvestwellStatus:null

        }

        // SET EKYC
        //SET EKYC variables
        factory.setEKYC = function (data) {
            
            var deferred = $q.defer();
            if (data) {

                this.EKYC.invsttype = data.invsttype;

                this.EKYC.basicid = data.basicid;
                this.EKYC.sessid = data.sessid;
                this.EKYC.pan = data.pan;
                this.EKYC.email = data.email;
                this.EKYC.errcode = data.errcode;
                this.EKYC.errdesc = data.errdesc;
                this.EKYC.kycstatuscode = data.kycstatuscode;
                this.EKYC.itpanname = data.itpanname;
                this.EKYC.aadhaarname = data.aadhaarname;
                this.EKYC.fathername = data.fathername;
                this.EKYC.gender = data.gender;
                this.EKYC.maritalstatus = data.maritalstatus;
                this.EKYC.dob = data.dob;
                this.EKYC.nationality = data.nationality;
                this.EKYC.aadhaarno = data.aadhaarno;
                this.EKYC.address1 = data.address1;
                this.EKYC.address2 = data.address2;
                this.EKYC.address3 = data.address3;
                this.EKYC.city = data.city;
                this.EKYC.state = data.state;
                this.EKYC.pincode = data.pincode;
                this.EKYC.mobileno = data.mobileno;
                this.EKYC.cafInvestwellStatus = data.cafInvestwellStatus;

                $localStorage.ekyc = this.EKYC;
                deferred.resolve();
            }
            else { deferred.resolve(); }

            return deferred.promise;
        }


        // REMOVE EKYC from local storage
        factory.clearEKYC = function () {
            
            this.resetEKYC();
            delete $localStorage.ekyc;
        }

        // GET EKYC from localstorage
        factory.getKYC = function () {
            
            var ekycdata;
            if ($localStorage.ekyc) {
                ekycdata = $localStorage.ekyc;
            }
            else {
                ekycdata = this.EKYC;
            }
            return ekycdata;
        }


        //#region VIEWPROFILE
        //DEFINE VIEWPROFILE variables
        factory.VIEWPROFILE = {
            
            basicid: null,
            clientname: null,
            clientprofilepicname: null,
            groupleader: null,
            memberid: null,
            profilepic: null,
            pannumber: null,
            profilepercent: null,
            totalInvstAccounts: null,
            ismandate: null,
            iscafcomplete: null,
            iskyccomplete: null,
            ismemberbasic: null,
            ismemberaddress: null,
            ismemberbank: null,
            ismembernominee: null,
            ismemberfatca: null,
            cafviewprofilelist: []
        }

        // SET VIEWPROFILE
        factory.setVIEWPROFILE = function (data) {
            
            
            if (data) {
                // store VIEWPROFILE in local storage
                var self = this;

                var nameArray = "";
                if (data.clientname.split(/(\s+)/)) {
                    nameArray = data.clientname.split(/(\s+)/).filter(function (e) { return e.trim().length > 0; });
                }
                else {
                    nameArray = data.clientname.filter(function (e) { return e.trim().length > 0; });
                }

                var proname = "";

                if (nameArray.length > 1) {
                    proname = proname + nameArray[0].charAt(0).toUpperCase() + nameArray[1].charAt(0).toUpperCase();
                }
                else if (nameArray.length > 0) {
                    if (nameArray[0].length > 1) {
                        proname = proname + nameArray[0].charAt(0).toUpperCase() + nameArray[0].charAt(1).toUpperCase();
                    }
                    else {
                    proname = proname + nameArray[0].charAt(0).toUpperCase();
                }
                }

                this.VIEWPROFILE.basicid = data.basic_id;
                this.VIEWPROFILE.clientname = data.clientname;
                this.VIEWPROFILE.clientprofilepicname = proname;
                this.VIEWPROFILE.groupleader = data.groupLeader;
                this.VIEWPROFILE.memberid = data.memberId;
                this.VIEWPROFILE.profilepic = data.profilepic;
                this.VIEWPROFILE.pannumber = data.pannumber;
                this.VIEWPROFILE.profilepercent = data.profilepercent;
                this.VIEWPROFILE.totalInvstAccounts = data.totalInvstAccounts;
                this.VIEWPROFILE.ismandate = data.ismandate;
                this.VIEWPROFILE.iscafcomplete = data.iscafcomplete;
                this.VIEWPROFILE.iskyccomplete = data.iskyccomplete;
                this.VIEWPROFILE.ismemberbasic = data.ismemberbasic;
                this.VIEWPROFILE.ismemberaddress = data.ismemberaddress;
                this.VIEWPROFILE.ismemberbank = data.ismemberbank;
                this.VIEWPROFILE.ismembernominee = data.ismembernominee;
                this.VIEWPROFILE.ismemberfatca = data.ismemberfatca;
                this.VIEWPROFILE.cafviewprofilelist.length = 0;
                angular.forEach(data.CafViewProfile, function (value, key) {

                    var nameArrayMember = "";
                    if (value.clientname.split(/(\s+)/)) {
                        nameArrayMember = value.clientname.split(/(\s+)/).filter(function (e) { return e.trim().length > 0; });
                    }
                    else {
                        nameArrayMember = value.clientname.filter(function (e) { return e.trim().length > 0; });
                    }

                    var pronameMember = "";

                    if (nameArrayMember.length > 1) {
                        pronameMember = pronameMember + nameArrayMember[0].charAt(0).toUpperCase() + nameArrayMember[1].charAt(0).toUpperCase();
                    }
                    else if (nameArrayMember.length > 0) {
                        if (nameArrayMember[0].length > 1) {
                            pronameMember = pronameMember + nameArrayMember[0].charAt(0).toUpperCase() + nameArrayMember[0].charAt(1).toUpperCase();
                        }
                        else {
                        pronameMember = pronameMember + nameArrayMember[0].charAt(0).toUpperCase();
                    }
                    }

                    self.VIEWPROFILE.cafviewprofilelist.push({
                        basicid: value.basic_id,
                        memberid: value.memberId,
                        groupleader: value.groupLeader,
                        clientname: value.clientname,
                        clientprofilepicname: pronameMember,
                        profilepic: value.profilepic,
                        pannumber: value.pannumber,
                        profilepercent: value.profilepercent,
                        ismandate: value.ismandate,
                        iskyccomplete: value.iskyccomplete,
                        iscafcomplete: value.iscafcomplete,
                        ismemberbasic: value.ismemberbasic,
                        ismemberaddress: value.ismemberaddress,
                        ismemberbank: value.ismemberbank,
                        ismembernominee: value.ismembernominee,
                        ismemberfatca: value.ismemberfatca

                    });
                });
           

                $localStorage.viewProfiles = this.VIEWPROFILE;
            }

        }

        // remove VIEWPROFILE from local storage
        factory.clearVIEWPROFILE = function () {
            this.resetVIEWPROFILE();
            delete $localStorage.viewProfiles;
        }

        // get VIEWPROFILE
        factory.getVIEWPROFILE = function () {

            var viewprofiles;
            if ($localStorage.viewProfiles) {
                viewprofiles = $localStorage.viewProfiles;
            }
            else {
                viewprofiles = this.VIEWPROFILE;
            }
            return viewprofiles;
        }

        // SET Karvy Kyc Status to localstorage & VIEWPROFILE variables
        factory.setKarvyKycStatusInViewProfile = function (basicid, status) {
            
            var viewprofiles;
            
            if (status && status === "Y") {

                if ($localStorage.viewProfiles) {
                    viewprofiles = $localStorage.viewProfiles;
                    if (viewprofiles.basicid === basicid) {
                        viewprofiles.iskyccomplete = "True";
                        this.VIEWPROFILE = viewprofiles;
                        $localStorage.viewProfiles = this.VIEWPROFILE;
                    }
                    else {
                        if (viewprofiles.cafviewprofilelist.filter(function (obj) { return obj.basicid === basicid; })[0].basicid) {

                            var found = viewprofiles.cafviewprofilelist.filter(function (obj) { return obj.basicid === basicid; })[0];
                            if (found) { 
                                
                                viewprofiles.cafviewprofilelist[viewprofiles.cafviewprofilelist.indexOf(found)].iskyccomplete = "True";
                                this.VIEWPROFILE = viewprofiles;
                                $localStorage.viewProfiles = this.VIEWPROFILE;
                            }

                            
                        }
                    }
                }
                
            }
        }
        //#endregion VIEWPROFILE


        // SET GroupLeader CAF
        factory.setGroupLeaderCaf = function (data) {
            
            if (data) {
                $rootScope.CurrentUserCafStatus = data.CAFStatus === "True" ? true : false;
                $rootScope.prmBasicId = data.basicId;
                //FIND PROFILE CAF PERCENT
                var percentCount = 0;
                if (data.CAFBasicStatus === "True") {
                    percentCount += 20;
                }
                if (data.CAFAddressStatus === "True") {
                    percentCount += 20;
                }
                if (data.CAFBankStatus === "True") {
                    percentCount += 20;
                }
                if (data.CAFNomineeStatus === "True" || data.ReqNominee) {
                    percentCount += 20;
                }
                if (data.CAFFatcaStatus === "True") {
                    percentCount += 20;
                }

                $rootScope.profilepercent.endval = parseInt(percentCount);

                // store USERCAF in local storage
                this.USERCAF.category = data.Status;
                this.USERCAF.panname = data.ClientName;
                this.USERCAF.category = data.Status;
                this.USERCAF.address = data.Address1;
                this.USERCAF.city = data.City;
                this.USERCAF.state = data.State;
                this.USERCAF.country = data.Country;
                this.USERCAF.pin = data.Pin;
                this.USERCAF.commcity = data.Commcity,
                this.USERCAF.commstate = data.Commstate,
                this.USERCAF.commcountry = data.Commcountry,
                this.USERCAF.commpin = data.Commpin,
                this.USERCAF.mobile = data.Mobile;
                this.USERCAF.occupation = data.Occupation;
                this.USERCAF.dob = data.Date_of_Birth;
                this.USERCAF.gpanname = data.Guardname;
                this.USERCAF.gpannumber = data.Guardpan;
                this.USERCAF.gdob = data.Guardian_DOB;
                this.USERCAF.nricity = data.CityName;
                this.USERCAF.nristate = data.StateName;
                this.USERCAF.nricountry = data.CountryName;
                this.USERCAF.income = data.AnnIncome;
                this.USERCAF.pannumber = data.First_pan;
                this.USERCAF.haskyc = data.kycStatus === "Y" ? true : false;
                this.USERCAF.bankname = data.Bank_name;
                this.USERCAF.accounttype = data.Acc_Type;
                this.USERCAF.accountnumber = data.Acc_No;
                this.USERCAF.ifsc = data.IFSC;
                this.USERCAF.bankaccountname = data.NameAsPerBank;
                this.USERCAF.micr = data.Ecsno;
                this.USERCAF.branchname = data.Branch;
                this.USERCAF.branchcity = data.Bankcity;
                this.USERCAF.branchaddress = data.Bank_Address;
                this.USERCAF.hasnominee = data.ReqNominee;
                this.USERCAF.nominee1name = data.Nominee_Name;
                this.USERCAF.nominee1relation = data.Relation;
                this.USERCAF.nominee1dob = data.Nominee_Dob;
                this.USERCAF.nominee1share = data.Nom1_percent;
                this.USERCAF.nominee2name = data.Nom2_name;
                this.USERCAF.nominee2relation = data.Nom2_relation;
                this.USERCAF.nominee2dob = data.Nom2_dob;
                this.USERCAF.nominee2share = data.Nom2_percent;
                this.USERCAF.nominee3name = data.Nom3_Name;
                this.USERCAF.nominee3relation = data.Nom3_Relation;
                this.USERCAF.nominee3dob = data.Nom3_Dob;
                this.USERCAF.nominee3share = data.Nom3_Percent;
                this.USERCAF.birthplace = data.Place_of_Birth;
                this.USERCAF.birthcountry = data.Country_of_Birth;
                this.USERCAF.wealthsource = data.Source_Wealth;
                this.USERCAF.ispep = data.Pol_Exp_Person;
                this.USERCAF.taxcountry1 = data.Country_Tax_Res1;
                this.USERCAF.taxidnumber1 = data.CountryPayer_Id_no;
                this.USERCAF.taxidtype1 = data.Idenif_Type;
                this.USERCAF.taxcountry2 = data.Country_Tax_Res2;
                this.USERCAF.taxidnumber2 = data.CountryPayer_Id_no2;
                this.USERCAF.taxidtype2 = data.Idenif_Type2;
                this.USERCAF.taxcountry3 = data.Country_Tax_Res3;
                this.USERCAF.taxidnumber3 = data.CountryPayer_Id_no3;
                this.USERCAF.taxidtype3 = data.Idenif_Type3;
                this.USERCAF.status = data.CAFStatus === "True" ? true : false;
                this.USERCAF.memberid = data.MemberId;
                this.USERCAF.basicid = data.basicId;
                this.USERCAF.groupleader = data.groupleader;
                this.USERCAF.profilepic = data.profilePic;
                this.USERCAF.aadhar = data.aadhar;
                this.USERCAF.cafstatus = data.CAFStatus;
                this.USERCAF.cafbasicstatus = data.CAFBasicStatus;
                this.USERCAF.cafaddressstatus = data.CAFAddressStatus;
                this.USERCAF.cafbankstatus = data.CAFBankStatus;
                this.USERCAF.cafnomineestatus = data.CAFNomineeStatus;
                this.USERCAF.caffatcastatus = data.CAFFatcaStatus;
                this.USERCAF.Nom1Id = data.Nom1Id,
                this.USERCAF.Nom2Id = data.Nom2Id,
                this.USERCAF.Nom3Id = data.Nom3Id,
                this.USERCAF.istaxother = data.isTaxOther,
                this.USERCAF.email = data.Email,
                this.USERCAF.corpaddress = data.Commaddress1;
                this.USERCAF.sameAddress = data.sameAddress;

                $localStorage.currentUserCaf = this.USERCAF;
            }

        }

        // remove GroupLeader caf from local storage
        factory.clearGroupLeaderCAF = function () {
            this.resetUSERCAF();
            delete $localStorage.currentUserCaf;
        }

        // get caf GroupLeader
        factory.getGroupLeaderCAF = function () {
            
            var usercaf;
            if ($localStorage.currentUserCaf) {
                usercaf = $localStorage.currentUserCaf;
            }
            else {
                usercaf = this.USERCAF;
            }
            return usercaf;
        }

        // SET Karvy Kyc Status to localstorage & GroupLeader variables
        factory.setKarvyKycStatusInGroupLeaderCAF = function (status) {
            
            var usercaf;
            if (status && status === "Y") {

                if ($localStorage.currentUserCaf) {
                    usercaf = $localStorage.currentUserCaf;
                    usercaf.haskyc = true;
                    this.USERCAF = usercaf;
                    $localStorage.currentUserCaf = this.USERCAF;
                    
                }
                
            }
        }

        // SET MEMBER CAF FROM DATATBASE
        factory.setMemberCafFromDatabase = function (data) {
            
            // store USERCAF in local storage
            this.MEMBERCAF.category = data.Status;
            this.MEMBERCAF.panname = data.ClientName;
            this.MEMBERCAF.category = data.Status;
            this.MEMBERCAF.address = data.Address1;
            this.MEMBERCAF.city = data.City;
            this.MEMBERCAF.state = data.State;
            this.MEMBERCAF.country = data.Country;
            this.MEMBERCAF.pin = data.Pin;
            this.MEMBERCAF.commcity = data.Commcity;
            this.MEMBERCAF.commstate = data.Commstate;
            this.MEMBERCAF.commcountry = data.Commcountry;
            this.MEMBERCAF.commpin = data.Commpin;
            this.MEMBERCAF.mobile = data.Mobile;
            this.MEMBERCAF.occupation = data.Occupation;
            this.MEMBERCAF.dob = data.Date_of_Birth;
            this.MEMBERCAF.gpanname = data.Guardname;
            this.MEMBERCAF.gpannumber = data.Guardpan;
            this.MEMBERCAF.gdob = data.Guardian_DOB;
            this.MEMBERCAF.nricity = data.CityName;
            this.MEMBERCAF.nristate = data.StateName;
            this.MEMBERCAF.nricountry = data.CountryName;
            this.MEMBERCAF.income = data.AnnIncome;
            this.MEMBERCAF.pannumber = data.First_pan;
            this.MEMBERCAF.haskyc = data.kycStatus === "Y" ? true : false;
            this.MEMBERCAF.bankname = data.Bank_name;
            this.MEMBERCAF.accounttype = data.Acc_Type;
            this.MEMBERCAF.accountnumber = data.Acc_No;
            this.MEMBERCAF.ifsc = data.IFSC;
            this.MEMBERCAF.bankaccountname = data.NameAsPerBank;
            this.MEMBERCAF.micr = data.Ecsno;
            this.MEMBERCAF.branchname = data.Branch;
            this.MEMBERCAF.branchcity = data.Bankcity;
            this.MEMBERCAF.branchaddress = data.Bank_Address;
            this.MEMBERCAF.hasnominee = data.ReqNominee;
            this.MEMBERCAF.nominee1name = data.Nominee_Name;
            this.MEMBERCAF.nominee1relation = data.Relation;
            this.MEMBERCAF.nominee1dob = data.Nominee_Dob;
            this.MEMBERCAF.nominee1share = data.Nom1_percent;
            this.MEMBERCAF.nominee2name = data.Nom2_name;
            this.MEMBERCAF.nominee2relation = data.Nom2_relation;
            this.MEMBERCAF.nominee2dob = data.Nom2_dob;
            this.MEMBERCAF.nominee2share = data.Nom2_percent;
            this.MEMBERCAF.nominee3name = data.Nom3_Name;
            this.MEMBERCAF.nominee3relation = data.Nom3_Relation;
            this.MEMBERCAF.nominee3dob = data.Nom3_Dob;
            this.MEMBERCAF.nominee3share = data.Nom3_Percent;
            this.MEMBERCAF.birthplace = data.Place_of_Birth;
            this.MEMBERCAF.birthcountry = data.Country_of_Birth;
            this.MEMBERCAF.wealthsource = data.Source_Wealth;
            this.MEMBERCAF.ispep = data.Pol_Exp_Person;
            this.MEMBERCAF.taxcountry1 = data.Country_Tax_Res1;
            this.MEMBERCAF.taxidnumber1 = data.CountryPayer_Id_no;
            this.MEMBERCAF.taxidtype1 = data.Idenif_Type;
            this.MEMBERCAF.taxcountry2 = data.Country_Tax_Res2;
            this.MEMBERCAF.taxidnumber2 = data.CountryPayer_Id_no2;
            this.MEMBERCAF.taxidtype2 = data.Idenif_Type2;
            this.MEMBERCAF.taxcountry3 = data.Country_Tax_Res3;
            this.MEMBERCAF.taxidnumber3 = data.CountryPayer_Id_no3;
            this.MEMBERCAF.taxidtype3 = data.Idenif_Type3;
            this.MEMBERCAF.status = data.CAFStatus === "True" ? true : false;

            this.MEMBERCAF.memberid = data.MemberId;
            this.MEMBERCAF.basicid = data.basicId;
            this.MEMBERCAF.groupleader = data.groupleader;
            this.MEMBERCAF.profilepic = data.profilePic;
            this.MEMBERCAF.aadhar = data.aadhar;
            this.MEMBERCAF.cafstatus = data.CAFStatus;
            this.MEMBERCAF.cafbasicstatus = data.CAFBasicStatus;
            this.MEMBERCAF.cafaddressstatus = data.CAFAddressStatus;
            this.MEMBERCAF.cafbankstatus = data.CAFBankStatus;
            this.MEMBERCAF.cafnomineestatus = data.CAFNomineeStatus;
            this.MEMBERCAF.caffatcastatus = data.CAFFatcaStatus;
            this.MEMBERCAF.Nom1Id = data.Nom1Id,
            this.MEMBERCAF.Nom2Id = data.Nom2Id,
            this.MEMBERCAF.Nom3Id = data.Nom3Id,
            this.MEMBERCAF.istaxother = data.isTaxOther,
            this.MEMBERCAF.email = data.Email,
            this.MEMBERCAF.corpaddress = data.Commaddress1;
            this.MEMBERCAF.sameAddress = data.sameAddress;
            $localStorage.memberCaf = this.MEMBERCAF;


        }

        // SET MEMBER CAF FROM APP
        factory.setMemberCafFromApp = function (data) {

            // store MEMBERCAF in local storage
            // store membercaf details in local storage
            this.MEMBERCAF = data;
            $localStorage.memberCaf = this.MEMBERCAF;

        }


        // REMOVE MEMBER CAF
        factory.clearMemberCAF = function () {

            // CLEAR CAF VARIABLES AND LOCAL STORAGE
            
            this.resetMEMBERCAF();
            delete $localStorage.memberCaf;


        }

        // GET MEMBER CAF
        factory.getMemberCAF = function () {
            
            var membercaf;
            if ($localStorage.memberCaf) {
                membercaf = $localStorage.memberCaf;
            }
            else {
                membercaf = this.MEMBERCAF;
            }
            return membercaf;
        }

        // SET Karvy Kyc Status to localstorage & MemberCAF variables
        factory.setKarvyKycStatusInMemberCAF = function (status) {
            
            var memberCaf;
            if (status && status === "Y") {

                var membercaf;
                if ($localStorage.memberCaf) {
                    membercaf = $localStorage.memberCaf;
                    membercaf.haskyc = true;
                    this.MEMBERCAF = membercaf;
                    $localStorage.memberCaf = this.MEMBERCAF;

                }

            }
        }




        // RESET ALL GROUPLEADER CAF VARIABLE INSTANCE
        factory.resetUSERCAF = function () {
            this.USERCAF.category = null;
            this.USERCAF.panname = null;
            this.USERCAF.category = null;
            this.USERCAF.address = null;
            this.USERCAF.city = null;
            this.USERCAF.state = null;
            this.USERCAF.country = null;
            this.USERCAF.pin = null;
            this.USERCAF.commcity = null,
            this.USERCAF.commstate = null,
            this.USERCAF.commcountry = null,
            this.USERCAF.commpin = null,
            this.USERCAF.mobile = null;
            this.USERCAF.occupation = null;
            this.USERCAF.dob = null;
            this.USERCAF.gpanname = null;
            this.USERCAF.gpannumber = null;
            this.USERCAF.gdob = null;
            this.USERCAF.nricity = null;
            this.USERCAF.nristate = null;
            this.USERCAF.nricountry = null;
            this.USERCAF.income = null;
            this.USERCAF.pannumber = null;
            this.USERCAF.haskyc = null;
            this.USERCAF.bankname = null;
            this.USERCAF.accounttype = null;
            this.USERCAF.accountnumber = null;
            this.USERCAF.ifsc = null;
            this.USERCAF.bankaccountname = null;
            this.USERCAF.micr = null;
            this.USERCAF.branchname = null;
            this.USERCAF.branchcity = null;
            this.USERCAF.branchaddress = null;
            this.USERCAF.hasnominee = null;
            this.USERCAF.nominee1name = null;
            this.USERCAF.nominee1relation = null;
            this.USERCAF.nominee1dob = null;
            this.USERCAF.nominee1share = null;
            this.USERCAF.nominee2name = null;
            this.USERCAF.nominee2relation = null;
            this.USERCAF.nominee2dob = null;
            this.USERCAF.nominee2share = null;
            this.USERCAF.nominee3name = null;
            this.USERCAF.nominee3relation = null;
            this.USERCAF.nominee3dob = null;
            this.USERCAF.nominee3share = null;
            this.USERCAF.birthplace = null;
            this.USERCAF.birthcountry = null;
            this.USERCAF.wealthsource = null;
            this.USERCAF.ispep = null;
            this.USERCAF.taxcountry1 = null;
            this.USERCAF.taxidnumber1 = null;
            this.USERCAF.taxidtype1 = null;
            this.USERCAF.taxcountry2 = null;
            this.USERCAF.taxidnumber2 = null;
            this.USERCAF.taxidtype2 = null;
            this.USERCAF.taxcountry3 = null;
            this.USERCAF.taxidnumber3 = null;
            this.USERCAF.taxidtype3 = null;
            this.USERCAF.status = false;

            this.USERCAF.memberid = null;
            this.USERCAF.basicid = null;
            this.USERCAF.groupleader = null;
            this.USERCAF.profilepic = null;
            this.USERCAF.aadhar = null;
            this.USERCAF.cafstatus = null;
            this.USERCAF.cafbasicstatus = null;
            this.USERCAF.cafaddressstatus = null;
            this.USERCAF.cafbankstatus = null;
            this.USERCAF.cafnomineestatus = null;
            this.USERCAF.caffatcastatus = null;
            this.USERCAF.Nom1Id = null;
            this.USERCAF.Nom2Id = null;
            this.USERCAF.Nom3Id = null;
            this.USERCAF.istaxother = null;
            this.USERCAF.email = null;
            this.USERCAF.corpaddress = null;
            this.USERCAF.sameAddress = null;
        }

        // RESET ALL MEMBER CAF VARIABLE INSTANCE
        factory.resetMEMBERCAF = function () {
            this.MEMBERCAF.category = null;
            this.MEMBERCAF.panname = null;
            this.MEMBERCAF.category = null;
            this.MEMBERCAF.address = null;
            this.MEMBERCAF.city = null;
            this.MEMBERCAF.state = null;
            this.MEMBERCAF.country = null;
            this.MEMBERCAF.pin = null;
            this.MEMBERCAF.commcity = null,
            this.MEMBERCAF.commstate = null,
            this.MEMBERCAF.commcountry = null,
            this.MEMBERCAF.commpin = null,
            this.MEMBERCAF.mobile = null;
            this.MEMBERCAF.occupation = null;
            this.MEMBERCAF.dob = null;
            this.MEMBERCAF.gpanname = null;
            this.MEMBERCAF.gpannumber = null;
            this.MEMBERCAF.gdob = null;
            this.MEMBERCAF.nricity = null;
            this.MEMBERCAF.nristate = null;
            this.MEMBERCAF.nricountry = null;
            this.MEMBERCAF.income = null;
            this.MEMBERCAF.pannumber = null;
            this.MEMBERCAF.haskyc = null;
            this.MEMBERCAF.bankname = null;
            this.MEMBERCAF.accounttype = null;
            this.MEMBERCAF.accountnumber = null;
            this.MEMBERCAF.ifsc = null;
            this.MEMBERCAF.bankaccountname = null;
            this.MEMBERCAF.micr = null;
            this.MEMBERCAF.branchname = null;
            this.MEMBERCAF.branchcity = null;
            this.MEMBERCAF.branchaddress = null;
            this.MEMBERCAF.hasnominee = null;
            this.MEMBERCAF.nominee1name = null;
            this.MEMBERCAF.nominee1relation = null;
            this.MEMBERCAF.nominee1dob = null;
            this.MEMBERCAF.nominee1share = null;
            this.MEMBERCAF.nominee2name = null;
            this.MEMBERCAF.nominee2relation = null;
            this.MEMBERCAF.nominee2dob = null;
            this.MEMBERCAF.nominee2share = null;
            this.MEMBERCAF.nominee3name = null;
            this.MEMBERCAF.nominee3relation = null;
            this.MEMBERCAF.nominee3dob = null;
            this.MEMBERCAF.nominee3share = null;
            this.MEMBERCAF.birthplace = null;
            this.MEMBERCAF.birthcountry = null;
            this.MEMBERCAF.wealthsource = null;
            this.MEMBERCAF.ispep = null;
            this.MEMBERCAF.taxcountry1 = null;
            this.MEMBERCAF.taxidnumber1 = null;
            this.MEMBERCAF.taxidtype1 = null;
            this.MEMBERCAF.taxcountry2 = null;
            this.MEMBERCAF.taxidnumber2 = null;
            this.MEMBERCAF.taxidtype2 = null;
            this.MEMBERCAF.taxcountry3 = null;
            this.MEMBERCAF.taxidnumber3 = null;
            this.MEMBERCAF.taxidtype3 = null;
            this.MEMBERCAF.status = false;

            this.MEMBERCAF.memberid = null;
            this.MEMBERCAF.basicid = null;
            this.MEMBERCAF.groupleader = null;
            this.MEMBERCAF.profilepic = null;
            this.MEMBERCAF.aadhar = null;
            this.MEMBERCAF.cafstatus = null;
            this.MEMBERCAF.cafbasicstatus = null;
            this.MEMBERCAF.cafaddressstatus = null;
            this.MEMBERCAF.cafbankstatus = null;
            this.MEMBERCAF.cafnomineestatus = null;
            this.MEMBERCAF.caffatcastatus = null;
            this.MEMBERCAF.Nom1Id = null;
            this.MEMBERCAF.Nom2Id = null;
            this.MEMBERCAF.Nom3Id = null;
            this.MEMBERCAF.istaxother = null;
            this.MEMBERCAF.email = null;
            this.MEMBERCAF.corpaddress = null;
            this.MEMBERCAF.sameAddress = null;
        }

        // RESET VIEWPROFILE VARIABLE INSTANCE
        factory.resetVIEWPROFILE = function () {
            this.VIEWPROFILE.basicid = null;
            this.VIEWPROFILE.clientname = null;
            this.VIEWPROFILE.clientprofilepicname = null;
            this.VIEWPROFILE.groupleader = null;
            this.VIEWPROFILE.memberid = null;
            this.VIEWPROFILE.profilepic = null;
            this.VIEWPROFILE.pannumber = null;
            this.VIEWPROFILE.profilepercent = null;
            this.VIEWPROFILE.totalInvstAccounts = null;
            this.VIEWPROFILE.ismandate = null;
            this.VIEWPROFILE.iscafcomplete = null;
            this.VIEWPROFILE.iskyccomplete = null;
            this.VIEWPROFILE.ismemberbasic = null;
            this.VIEWPROFILE.ismemberaddress = null;
            this.VIEWPROFILE.ismemberbank = null;
            this.VIEWPROFILE.ismembernominee = null;
            this.VIEWPROFILE.ismemberfatca = null;
            this.VIEWPROFILE.cafviewprofilelist = [];
        }

        // RESET EKYC INSTANCE
        factory.resetEKYC = function ()
        {

            this.EKYC.invsttype = null;
            this.EKYC.basicid = null;
            this.EKYC.sessid = null;
            this.EKYC.pan = null;
            this.EKYC.email = null;
            this.EKYC.errcode = null;
            this.EKYC.errdesc = null;
            this.EKYC.kycstatuscode = null;
            this.EKYC.itpanname = null;
            this.EKYC.aadhaarname = null;
            this.EKYC.fathername = null;
            this.EKYC.gender = null;
            this.EKYC.maritalstatus = null;
            this.EKYC.dob = null;
            this.EKYC.nationality = null;
            this.EKYC.aadhaarno = null;
            this.EKYC.address1 = null;
            this.EKYC.address2 = null;
            this.EKYC.address3 = null;
            this.EKYC.city = null;
            this.EKYC.state = null;
            this.EKYC.pincode = null;
            this.EKYC.mobileno = null;
        }

        //#endregion -------NEW CODE (Manage CAF)############################################################################################################-


        //#region Lumpsum Cart Details
        //lumpSum cart details variables
        factory.lumpsumcartDetails = {
            cartid: null,
            foliono: null,
            fundcode: null,
            schemecode: null,
            schemename: null,
            schemetype: null,
            dividend: null,
            objective: null,
            fscode: null,
            exlcode: null,
            amount: null,
            minpurchaseamount: null,
            minadditonalpurchaseamount: null,
            billschemecode: null,
            billamccode: null,
            nav: null,
            navdate: null
        }

        //lumpSum cart details
        factory.setlumpsumcartDetails = function (data) {
            if (data.length > 0) {
                var cartproducts = [];
                cartproducts.length = 0;
                angular.forEach(data, function (value, key) {
                    cartproducts.push({
                        cartid: value.Cartid,
                        foliono: value.FolioNo === "-" ? "NEW" : value.FolioNo,
                        fundcode: value.Fcode,
                        schemecode: value.Scode,
                        schemename: value.Org_Scheme,
                        schemetype: value.type,
                        dividend: value.DivOpt === "Z" || value.DivOpt === "" ? "N/A" : value.DivOpt,
                        objective: value.objective,
                        fscode: value.fscode,
                        exlcode: value.exlcode,
                        amount: parseInt(value.amount),
                        minpurchaseamount: parseInt(value.MinPurAmount),
                        minadditonalpurchaseamount: value.MinAdPurAmount,
                        billschemecode: value.BillScCode,
                        billamccode: value.BillAmcCode,
                        nav: value.nav,
                        navdate: value.NAVDATE
                    });
                });

                // store lumpSum cart in local storage to keep user logged in between page refreshes
                $localStorage.lumpsumcart = cartproducts;
            }
            else {
                // store lumpSum cart in local storage to keep user logged in between page refreshes
                $localStorage.lumpsumcart = [];
            }
        }

        // remove lumpsum cart from local storage
        factory.clearlumpsumcartDetails = function () {
            delete $localStorage.lumpsumcart;
        }

        // get lumpsum cart details
        factory.getlumpsumcartDetails = function () {
            var lcart;
            if ($localStorage.lumpsumcart) {
                lcart = $localStorage.lumpsumcart;
            }
            else {
                lcart = [];
            }
            return lcart;
        }
        //#endregion Lumpsum Cart Details


        //#region Sip Cart Details
        //sip cart details variables
        factory.sipcartDetails = {
            cartid: null,
            foliono: null,
            fundcode: null,
            schemecode: null,
            schemename: null,
            schemetype: null,
            dividend: null,
            objective: null,
            fscode: null,
            exlcode: null,
            amount: null,
            minpurchaseamount: null,
            minadditonalpurchaseamount: null,
            billschemecode: null,
            billamccode: null,
            nav: null,
            navdate: null,
            startdate: null,
            enddate: null,
            firstsipdate: null,
            noOfInstallment: null,
            mDate: null,

        }

        //sip cart details
        factory.setsipcartDetails = function (data) {
            var cartproducts = [];
            cartproducts.length = 0;

            if ($localStorage.sipcart) {
                cartproducts = $localStorage.sipcart;
            }

            if (data.length > 0) {
                angular.forEach(data, function (value, key) {
                    cartproducts.push({
                        cartid: value.CartID,
                        foliono: value.FolioNo === "-" ? "NEW" : value.FolioNo,
                        fundcode: value.Fcode,
                        schemecode: value.Scode,
                        schemename: value.Org_Scheme,
                        schemetype: value.type,
                        dividend: value.DivOption === "Z" || value.DivOption === "" ? "N/A" : value.DivOption,
                        objective: value.objective,
                        fscode: value.fscode,
                        exlcode: value.exlcode,
                        amount: parseInt(value.Amount),
                        minpurchaseamount: parseInt(value.MinPurAmount),
                        minadditonalpurchaseamount: parseInt(value.MinAdPurAmount),
                        billschemecode: value.BillScCode,
                        billamccode: value.BillAmcCode,
                        nav: value.nav,
                        navdate: value.NAVDATE,
                        startdate: value.StartDate,
                        enddate: value.EndDate,
                        firstsipdate: value.FirstSIPDate,
                        noOfInstallment: value.no_of_installment,
                        mDate: value.ManDate
                    });
                });
            }
            // store sip cart in local storage to keep user logged in between page refreshes
            $localStorage.sipcart = cartproducts;

        }

        // remove sip cart from local storage
        factory.clearsipcartDetails = function () {
            delete $localStorage.sipcart;
        }

        // get sip cart details
        factory.getsipcartDetails = function () {
            var scart;
            if ($localStorage.sipcart) {
                scart = $localStorage.sipcart;
            }
            else {
                scart = [];
            }
            return scart;
        }
        //#endregion Sip Cart Details

        //#region Total Cart Details
        //total cart details variables
        factory.totalcartDetails = {
            TotalLumpsum: 0,
            TotalInstaSip: 0,
            TotalSip: 0
        }

        //total cart details
        factory.settotalcartDetails = function (data) {
            
            this.totalcartDetails.TotalLumpsum = parseInt(data.TotalLumpsum);
            this.totalcartDetails.TotalInstaSip = parseInt(data.TotalInstaSip);
            this.totalcartDetails.TotalSip = parseInt(data.TotalSip);

            // store total cart in local storage to keep user logged in between page refreshes
            $localStorage.totalcart = this.totalcartDetails;
        }

        //increment total cart details
        factory.incrementcart = function (lumpsum, instantsip, sip) {
            
            if ($localStorage.totalcart) {
                this.totalcartDetails = $localStorage.totalcart;
                this.totalcartDetails.TotalLumpsum = this.totalcartDetails.TotalLumpsum + lumpsum;
                this.totalcartDetails.TotalInstaSip = this.totalcartDetails.TotalInstaSip + instantsip;
                this.totalcartDetails.TotalSip = this.totalcartDetails.TotalSip + sip;
            }
            // store total cart in local storage to keep user logged in between page refreshes
            $localStorage.totalcart = this.totalcartDetails;
        }

        //decrement total cart details
        factory.decrementcart = function (lumpsum, instantsip, sip) {

            if ($localStorage.totalcart) {
                this.totalcartDetails = $localStorage.totalcart;
                if (this.totalcartDetails.TotalLumpsum > 0) {
                    this.totalcartDetails.TotalLumpsum = this.totalcartDetails.TotalLumpsum - lumpsum;
                }
                if (this.totalcartDetails.TotalInstaSip > 0) {
                    this.totalcartDetails.TotalInstaSip = this.totalcartDetails.TotalInstaSip - instantsip;
                }
                if (this.totalcartDetails.TotalSip > 0) {
                    this.totalcartDetails.TotalSip = this.totalcartDetails.TotalSip - sip;
                }
            }

            // store total cart in local storage to keep user logged in between page refreshes
            $localStorage.totalcart = this.totalcartDetails;
        }

        // remove total cart from local storage
        factory.cleartotalcartDetails = function () {
            delete $localStorage.totalcart;
        }

        // get total cart details
        factory.gettotalcartDetails = function () {
            var tcart;
            if ($localStorage.totalcart) {
                tcart = $localStorage.totalcart;
            }
            else {
                tcart = this.totalcartDetails;
            }
            return tcart;
        }
        //#endregion Total Cart Details


        return factory;
    }]);
})(angular.module('fincartApp'));