﻿(function (finApp) {
    'use strict';
    finApp.factory('oauthTokenFactory', ['$http', '$localStorage', 'userTokenModel', function ($http, $localStorage, userTokenModel) {
        var factory = {};

        factory.setUserToken = function (data) {
            $localStorage.currentToken = userTokenModel.parseUserToken(data);
        }

        factory.destroyUserToken = function () {
            delete $localStorage.currentToken;
            $http.defaults.headers.common.Authorization = '';
        }

        factory.getUserToken = function () {
            if ($localStorage.currentToken) {
                return $localStorage.currentToken;
            }
            return userTokenModel.UserToken;
        }

        factory.setisloggedin = function (status) {
            $localStorage.isloggedin = status;
        }

        factory.getisloggedin = function () {
            if ($localStorage.isloggedin) {
                return $localStorage.isloggedin;
            }
            return false;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));