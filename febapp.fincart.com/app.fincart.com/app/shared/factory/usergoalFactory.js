﻿(function (userMod) {
    'use strict';
    userMod.factory('usergoalFactory', ['$http', '$localStorage', function ($http, $localStorage) {
        var factory = {};

        //#region TaxSaving Goal
        //TaxSaving Goal variables
        factory.taxSavings = {
            provFund:null,
            lifeFund: null,
            smallSaveFund: null,
            loanFund: null,
            totalInvest: null,
            totalRequired: null
            
        }

        //user goals variables
        factory.userGoals = {
            fin_name: '',
            fin_age: 0,
            fin_cliententerpoint: '',
            fin_gender: { code: '', name: '' },//M MALE-001/ F FEMALE-002
            fin_maritialstatus: { code: '', name: '' },//SINGLE-001/ MARRIED-002/ WITH KIDS-003
            fin_annualincome: 0,
            fin_monthlyexpense: 0,
            fin_spouse: { status: false, isworking: false, name: '', income: 0, mobile: '', age: 0, rid: null },
            fin_child: { status: false, count: 0, detail: [] },
            fin_emi: { status: false, amount: 0, loans: [] },//loans HOME-001, CAR-002, EDUCATION-003,PERSONAL-004,CREDIT CARD-005,OTHERS-006/ put loans as{code:001,name:'HOME LOAN' }
            fin_familyplanning: { id: 0, status: false, goalcode: 'FG11', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type BUDGET-001, COMFY-002, LUXURY-003
            fin_study: { id: 0, status: false, goalcode: 'FG5', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, location: { code: '', name: '', place: '' }, amount: 0, time: 1 },//location USA OR CANADA-001, EUROPE-002,SOUTH EAST ASIA-003,INDIA-004,OTHER-005 (note:for other change fill place frpm textbox)   
            fin_wedding: { id: 0, status: false, goalcode: 'FG7', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type SIMPLE-001, MODERATE-002, LAVISH-003
            fin_house: { id: 0, status: false, goalcode: 'FG4', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, time: 1 },
            fin_car: { id: 0, status: false, goalcode: 'FG3', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type SMALL-001, HATCHBACK-002, SEDAN-003, SUV-004, LUXURY-005
            fin_travel: { status: false, goalcode: 'FG6', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, location: { code: '', name: '', place: '' }, people: { count: 1, name: '' }, type: { code: '', name: '' }, amount: 0, time: 1 },//location USA OR CANADA-001, ,AUSTRALIA OR NZ-002, EUROPE-003,SOUTH EAST ASIA-004,INDIA-005//people SOLO-1, '+1'-2, FAMILY-2(married-2/ kids-4)//type BUDGET-001, COMFY-002, LUXURY-003
            fin_bike: { id: 0, status: false, goalcode: 'FG1', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type 250 CC-250, 500 CC-500, 800 CC-800, 1000 CC-1000, 1200 CC-1200
            fin_business: { id: 0, status: false, goalcode: 'FG2', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, monthly_amount: 0, time: 1 },
            fin_sabbatical: { id: 0, status: false, goalcode: 'FG10', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, starttime: 1, endtime: 1 },
            fin_wealth: { id: 0, status: false, goalcode: 'FG8', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 5 },//type SIMPLE-001, MODERATE-002, AWESOME-003
            fin_retirement: { id: 0, status: false, goalcode: 'FG9', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, retirementage: 60, lifeexpectency: 80 },
            fin_other: [],//type if goal comes from dropdown the code added else code of new goal return from add goal api used
            fin_emergency: { id: 0, status: false, goalcode: 'FG17', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, time: 1.5 },
            fin_terminsurance: { id: 0, status: false, goalcode: 'FG23', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, time: 0 },
            fin_healthinsurance: { id: 0, status: false, goalcode: 'FG24', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, time: 0 }
        }

        //set goal data for current user
        factory.addGoalData = function (data) {
            //store userGoals in local storage to get userGoals back between page refreshes
            
            $localStorage.currentUserData = data;
        }

        //set user goal data for current user from database
        factory.setGoalData = function (data) {
            
            //set userGoals in local storage to get userGoals back between page refreshes
            var isUserGoalArr = false;
            var Goals = this.userGoals;
            var UserDetail = data.UserDetail;
            var UserRelation = data.UserRelation;
            var UserLoans = data.UserLoans;
            var UserGoal = data.UserGoal;

            if (data)
            {
                if (!data.UserGoal) {
                    UserGoal = data;
                    isUserGoalArr = true;
                }
               
            }

            if (UserDetail) {
                Goals.fin_name = UserDetail[0].Name;
                Goals.fin_age = UserDetail[0].Age;

                if (UserDetail[0].Gender === "001") {
                    Goals.fin_gender.code = "001";
                    Goals.fin_gender.name = "MALE";
                }
                else {
                    Goals.fin_gender.code = "002";
                    Goals.fin_gender.name = "FEMALE";
                }

                if (UserDetail[0].MaritialStatus === "001") {
                    Goals.fin_maritialstatus.code = "001";
                    Goals.fin_maritialstatus.name = "SINGLE";
                }
                else if (UserDetail[0].MaritialStatus === "002") {
                    Goals.fin_maritialstatus.code = "002";
                    Goals.fin_maritialstatus.name = "MARRIED";
                }
                else {
                    Goals.fin_maritialstatus.code = "003";
                    Goals.fin_maritialstatus.name = "WITH KIDS";
                }

                Goals.fin_annualincome = UserDetail[0].AnnualIncome;
                Goals.fin_monthlyexpense = UserDetail[0].MonthlyExpence;
                Goals.fin_spouse.status = UserDetail[0].Spouse;

                if (UserDetail[0].ChildCount > 0) {
                    Goals.fin_child.status = true;
                    Goals.fin_child.count = UserDetail[0].ChildCount;
                }

                if (UserRelation) {
                    Goals.fin_child.detail.length = 0;
                    for (var irelation = 0; irelation < UserRelation.length; irelation++) {
                        if (UserRelation[irelation].Relation === "001") {
                            Goals.fin_spouse.name = UserRelation[irelation].Name;
                            Goals.fin_spouse.mobile = UserRelation[irelation].Mobile;
                            Goals.fin_spouse.age = UserRelation[irelation].Age;
                            if (UserRelation[irelation].AnnualIncome > 0) {
                                Goals.fin_spouse.isworking = true;
                                Goals.fin_spouse.income = UserRelation[irelation].AnnualIncome;
                            }
                            Goals.fin_spouse.rid = UserRelation[irelation].RelationID;
                        }
                        else if (UserRelation[irelation].Relation === "002") {
                            var child = {
                                rid: UserRelation[irelation].RelationID,
                                name: UserRelation[irelation].Name,
                                age: UserRelation[irelation].Age,
                                mobile: UserRelation[irelation].Mobile,
                                gender: { code: '', name: '' },
                                study: { id: 0, status: false, goalcode: 'FG12', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, location: { code: '001', name: 'USA OR CANADA', place: 'USA OR CANADA' }, amount: 0, time: 1 },
                                wedding: { id: 0, status: false, goalcode: 'FG13', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '001', name: 'SIMPLE' }, amount: 0, time: 1 }
                            }

                            if (UserRelation[irelation].Gender === "001") {
                                child.gender.code = "001";
                                child.gender.name = "MALE";
                            }




                            else {
                                child.gender.code = "002";
                                child.gender.name = "FEMALE";
                            }

                            if (UserRelation[irelation].UserGoal) {
                                for (var irelationgoals = 0; irelationgoals < UserRelation[irelation].UserGoal.length; irelationgoals++) {

                                    if (UserRelation[irelation].UserGoal[irelationgoals].GoalCode === "FG12") {
                                        child.study.id = UserRelation[irelation].UserGoal[irelationgoals].ID;
                                        child.study.status = true;
                                        child.study.risk = UserRelation[irelation].UserGoal[irelationgoals].Risk;
                                        child.study.productlist = UserRelation[irelation].UserGoal[irelationgoals].ProductList;
                                        child.study.investmenttype = UserRelation[irelation].UserGoal[irelationgoals].InvestmentType;
                                        child.study.getamount = UserRelation[irelation].UserGoal[irelationgoals].GetAmount;
                                        child.study.investamount = UserRelation[irelation].UserGoal[irelationgoals].InvestAmount;
                                        child.study.goalachieved = UserRelation[irelation].UserGoal[irelationgoals].GoalAchieved;
                                        child.study.investedamount = UserRelation[irelation].UserGoal[irelationgoals].InvestedAmount;
                                        child.study.currentamount = UserRelation[irelation].UserGoal[irelationgoals].CurrentAmount;
                                        child.study.pendingamount = UserRelation[irelation].UserGoal[irelationgoals].PendingAmount;

                                        if (UserRelation[irelation].UserGoal[irelationgoals].Location === "001") {
                                            child.study.location.code = "001";
                                            child.study.location.name = "USA OR CANADA";
                                        }
                                        else if (UserRelation[irelation].UserGoal[irelationgoals].Location === "002") {
                                            child.study.location.code = "002";
                                            child.study.location.name = "EUROPE";
                                        }
                                        else if (UserRelation[irelation].UserGoal[irelationgoals].Location === "003") {
                                            child.study.location.code = "003";
                                            child.study.location.name = "SOUTH EAST ASIA";
                                        }
                                        else if (UserRelation[irelation].UserGoal[irelationgoals].Location === "004") {
                                            child.study.location.code = "004";
                                            child.study.location.name = "INDIA";
                                        }
                                        else {
                                            child.study.location.code = "005";
                                            child.study.location.name = "OTHER";
                                        }

                                        child.study.location.place = UserRelation[irelation].UserGoal[irelationgoals].Place;
                                        child.study.amount = UserRelation[irelation].UserGoal[irelationgoals].Amount;
                                        child.study.time = UserRelation[irelation].UserGoal[irelationgoals].Duration;
                                    }
                                    else if (UserRelation[irelation].UserGoal[irelationgoals].GoalCode === "FG13") {
                                        child.wedding.id = UserRelation[irelation].UserGoal[irelationgoals].ID;
                                        child.wedding.status = true;
                                        child.wedding.risk = UserRelation[irelation].UserGoal[irelationgoals].Risk;
                                        child.wedding.productlist = UserRelation[irelation].UserGoal[irelationgoals].ProductList;
                                        child.wedding.investmenttype = UserRelation[irelation].UserGoal[irelationgoals].InvestmentType;
                                        child.wedding.getamount = UserRelation[irelation].UserGoal[irelationgoals].GetAmount;
                                        child.wedding.investamount = UserRelation[irelation].UserGoal[irelationgoals].InvestAmount;
                                        child.wedding.goalachieved = UserRelation[irelation].UserGoal[irelationgoals].GoalAchieved;
                                        child.wedding.investedamount = UserRelation[irelation].UserGoal[irelationgoals].InvestedAmount;
                                        child.wedding.currentamount = UserRelation[irelation].UserGoal[irelationgoals].CurrentAmount;
                                        child.wedding.pendingamount = UserRelation[irelation].UserGoal[irelationgoals].PendingAmount;

                                        if (UserRelation[irelation].UserGoal[irelationgoals].TypeCode === "001") {
                                            child.wedding.type.code = "001";
                                            child.wedding.type.name = "SIMPLE";
                                        }
                                        else if (UserRelation[irelation].UserGoal[irelationgoals].TypeCode === "002") {
                                            child.wedding.type.code = "002";
                                            child.wedding.type.name = "MODERATE";
                                        }
                                        else {
                                            child.wedding.type.code = "003";
                                            child.wedding.type.name = "LAVISH";
                                        }

                                        child.wedding.amount = UserRelation[irelation].UserGoal[irelationgoals].Amount;
                                        child.wedding.time = UserRelation[irelation].UserGoal[irelationgoals].Duration;
                                    }
                                }
                            }
                            Goals.fin_child.detail.push(child);
                        }
                    }
                }



                Goals.fin_emi.status = UserDetail[0].Emi;
                Goals.fin_emi.amount = UserDetail[0].EmiAmount;

                if (UserDetail[0].Loans) {
                    if (UserLoans) {
                        for (var iloans = 0; iloans < UserLoans.length; iloans++) {

                            if (UserLoans[iloans].LoanID === "001") {
                                Goals.fin_emi.loans.push({ code: '001', name: 'HOME LOANS' });
                            }
                            else if (UserLoans[iloans].LoanID === "002") {
                                Goals.fin_emi.loans.push({ code: '002', name: 'CAR LOANS' });
                            }
                            else if (UserLoans[iloans].LoanID === "003") {
                                Goals.fin_emi.loans.push({ code: '003', name: 'EDUCATION LOANS' });
                            }
                            else if (UserLoans[iloans].LoanID === "004") {
                                Goals.fin_emi.loans.push({ code: '004', name: 'PERSONAL LOANS' });
                            }
                            else if (UserLoans[iloans].LoanID === "005") {
                                Goals.fin_emi.loans.push({ code: '005', name: 'CREDIT CARD PAYMENTS' });
                            }
                            else if (UserLoans[iloans].LoanID === "006") {
                                Goals.fin_emi.loans.push({ code: '006', name: 'OTHERS' });
                            }
                        }
                    }
                }

            }

            if (UserGoal) {
                for (var igoals = 0; igoals < UserGoal.length; igoals++) {
                    if (UserGoal[igoals].GoalCode === "FG11") {
                        Goals.fin_familyplanning.id = UserGoal[igoals].ID;
                        Goals.fin_familyplanning.status = true;
                        Goals.fin_familyplanning.risk = UserGoal[igoals].Risk;
                        Goals.fin_familyplanning.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_familyplanning.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_familyplanning.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_familyplanning.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_familyplanning.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_familyplanning.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_familyplanning.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_familyplanning.pendingamount = UserGoal[igoals].PendingAmount;

                        if (UserGoal[igoals].TypeCode === "001") {
                            Goals.fin_familyplanning.type.code = "001";
                            Goals.fin_familyplanning.type.name = "BUDGET";
                        }
                        else if (UserGoal[igoals].TypeCode === "002") {
                            Goals.fin_familyplanning.type.code = "002";
                            Goals.fin_familyplanning.type.name = "COMFY";
                        }
                        else {
                            Goals.fin_familyplanning.type.code = "003";
                            Goals.fin_familyplanning.type.name = "LUXURY";
                        }

                        Goals.fin_familyplanning.amount = UserGoal[igoals].Amount;
                        Goals.fin_familyplanning.time = UserGoal[igoals].Duration;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG5") {
                        Goals.fin_study.id = UserGoal[igoals].ID;
                        Goals.fin_study.status = true;
                        Goals.fin_study.risk = UserGoal[igoals].Risk;
                        Goals.fin_study.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_study.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_study.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_study.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_study.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_study.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_study.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_study.pendingamount = UserGoal[igoals].PendingAmount;

                        if (UserGoal[igoals].Location === "001") {
                            Goals.fin_study.location.code = "001";
                            Goals.fin_study.location.name = "USA OR CANADA";
                        }
                        else if (UserGoal[igoals].Location === "002") {
                            Goals.fin_study.location.code = "002";
                            Goals.fin_study.location.name = "EUROPE";
                        }
                        else if (UserGoal[igoals].Location === "003") {
                            Goals.fin_study.location.code = "003";
                            Goals.fin_study.location.name = "SOUTH EAST ASIA";
                        }
                        else if (UserGoal[igoals].Location === "004") {
                            Goals.fin_study.location.code = "004";
                            Goals.fin_study.location.name = "INDIA";
                        }
                        else {
                            Goals.fin_study.location.code = "005";
                            Goals.fin_study.location.name = "OTHER";
                        }

                        Goals.fin_study.location.place = UserGoal[igoals].Place;
                        Goals.fin_study.amount = UserGoal[igoals].Amount;
                        Goals.fin_study.time = UserGoal[igoals].Duration;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG7") {
                        Goals.fin_wedding.id = UserGoal[igoals].ID;
                        Goals.fin_wedding.status = true;
                        Goals.fin_wedding.risk = UserGoal[igoals].Risk;
                        Goals.fin_wedding.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_wedding.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_wedding.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_wedding.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_wedding.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_wedding.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_wedding.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_wedding.pendingamount = UserGoal[igoals].PendingAmount;

                        if (UserGoal[igoals].TypeCode === "001") {
                            Goals.fin_wedding.type.code = "001";
                            Goals.fin_wedding.type.name = "SIMPLE";
                        }
                        else if (UserGoal[igoals].TypeCode === "002") {
                            Goals.fin_wedding.type.code = "002";
                            Goals.fin_wedding.type.name = "MODERATE";
                        }
                        else {
                            Goals.fin_wedding.type.code = "003";
                            Goals.fin_wedding.type.name = "LAVISH";
                        }

                        Goals.fin_wedding.amount = UserGoal[igoals].Amount;
                        Goals.fin_wedding.time = UserGoal[igoals].Duration;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG4") {
                        Goals.fin_house.id = UserGoal[igoals].ID;
                        Goals.fin_house.status = true;
                        Goals.fin_house.risk = UserGoal[igoals].Risk;
                        Goals.fin_house.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_house.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_house.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_house.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_house.amount = UserGoal[igoals].Amount;
                        Goals.fin_house.time = UserGoal[igoals].Duration;
                        Goals.fin_house.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_house.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_house.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_house.pendingamount = UserGoal[igoals].PendingAmount;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG3") {
                        Goals.fin_car.id = UserGoal[igoals].ID;
                        Goals.fin_car.status = true;
                        Goals.fin_car.risk = UserGoal[igoals].Risk;
                        Goals.fin_car.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_car.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_car.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_car.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_car.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_car.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_car.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_car.pendingamount = UserGoal[igoals].PendingAmount;

                        if (UserGoal[igoals].TypeCode === "001") {
                            Goals.fin_car.type.code = "001";
                            Goals.fin_car.type.name = "SMALL";
                        }
                        else if (UserGoal[igoals].TypeCode === "002") {
                            Goals.fin_car.type.code = "002";
                            Goals.fin_car.type.name = "HATCHBACK";
                        }
                        else if (UserGoal[igoals].TypeCode === "003") {
                            Goals.fin_car.type.code = "003";
                            Goals.fin_car.type.name = "SEDAN";
                        }
                        else if (UserGoal[igoals].TypeCode === "004") {
                            Goals.fin_car.type.code = "004";
                            Goals.fin_car.type.name = "SUV";
                        }
                        else {
                            Goals.fin_car.type.code = "005";
                            Goals.fin_car.type.name = "LUXURY";
                        }

                        Goals.fin_car.amount = UserGoal[igoals].Amount;
                        Goals.fin_car.time = UserGoal[igoals].Duration;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG6") {
                        Goals.fin_travel.id = UserGoal[igoals].ID;
                        Goals.fin_travel.status = true;
                        Goals.fin_travel.risk = UserGoal[igoals].Risk;
                        Goals.fin_travel.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_travel.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_travel.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_travel.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_travel.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_travel.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_travel.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_travel.pendingamount = UserGoal[igoals].PendingAmount;

                        if (UserGoal[igoals].Location === "001") {
                            Goals.fin_travel.location.code = "001";
                            Goals.fin_travel.location.name = "USA OR CANADA";
                        }
                        else if (UserGoal[igoals].Location === "002") {
                            Goals.fin_travel.location.code = "002";
                            Goals.fin_travel.location.name = "AUSTRALIA OR NZ";
                        }
                        else if (UserGoal[igoals].Location === "003") {
                            Goals.fin_travel.location.code = "003";
                            Goals.fin_travel.location.name = "EUROPE";
                        }
                        else if (UserGoal[igoals].Location === "004") {
                            Goals.fin_travel.location.code = "004";
                            Goals.fin_travel.location.name = "SOUTH EAST ASIA";
                        }
                        else {
                            Goals.fin_travel.location.code = "005";
                            Goals.fin_travel.location.name = "INDIA";
                        }

                        Goals.fin_travel.location.place = UserGoal[igoals].Place;

                        if (UserGoal[igoals].People === "1") {
                            Goals.fin_travel.people.count = 1;
                            Goals.fin_travel.people.name = "SOLO";
                        }
                        else if (UserGoal[igoals].People === "2") {
                            Goals.fin_travel.people.count = 2;
                            if (UserDetail[0].MaritialStatus === "002") {
                                Goals.fin_travel.people.name = "FAMILY";
                            }
                            else {
                                Goals.fin_travel.people.name = "+1";
                            }
                        }
                        else {
                            Goals.fin_travel.people.count = 4;
                            Goals.fin_travel.people.name = "FAMILY";
                        }

                        if (UserGoal[igoals].TypeCode === "001") {
                            Goals.fin_travel.type.code = "001";
                            Goals.fin_travel.type.name = "BUDGET";
                        }
                        else if (UserGoal[igoals].TypeCode === "002") {
                            Goals.fin_travel.type.code = "002";
                            Goals.fin_travel.type.name = "COMFY";
                        }
                        else {
                            Goals.fin_travel.type.code = "003";
                            Goals.fin_travel.type.name = "LUXURY";
                        }

                        Goals.fin_travel.amount = UserGoal[igoals].Amount;
                        Goals.fin_travel.time = UserGoal[igoals].Duration;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG1") {
                        Goals.fin_bike.id = UserGoal[igoals].ID;
                        Goals.fin_bike.status = true;
                        Goals.fin_bike.risk = UserGoal[igoals].Risk;
                        Goals.fin_bike.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_bike.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_bike.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_bike.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_bike.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_bike.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_bike.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_bike.pendingamount = UserGoal[igoals].PendingAmount;

                        if (UserGoal[igoals].TypeCode === "250") {
                            Goals.fin_bike.type.code = "250";
                            Goals.fin_bike.type.name = "250 CC";
                        }
                        else if (UserGoal[igoals].TypeCode === "500") {
                            Goals.fin_bike.type.code = "500";
                            Goals.fin_bike.type.name = "500 CC";
                        }
                        else if (UserGoal[igoals].TypeCode === "800") {
                            Goals.fin_bike.type.code = "800";
                            Goals.fin_bike.type.name = "800 CC";
                        }
                        else if (UserGoal[igoals].TypeCode === "1000") {
                            Goals.fin_bike.type.code = "1000";
                            Goals.fin_bike.type.name = "1000 CC";
                        }
                        else {
                            Goals.fin_bike.type.code = "1200";
                            Goals.fin_bike.type.name = "1200 CC";
                        }

                        Goals.fin_bike.amount = UserGoal[igoals].Amount;
                        Goals.fin_bike.time = UserGoal[igoals].Duration;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG2") {
                        Goals.fin_business.id = UserGoal[igoals].ID;
                        Goals.fin_business.status = true;
                        Goals.fin_business.risk = UserGoal[igoals].Risk;
                        Goals.fin_business.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_business.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_business.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_business.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_business.amount = UserGoal[igoals].Amount;
                        Goals.fin_business.monthly_amount = UserGoal[igoals].MonthlyAmount;
                        Goals.fin_business.time = UserGoal[igoals].Duration;
                        Goals.fin_business.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_business.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_business.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_business.pendingamount = UserGoal[igoals].PendingAmount;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG10") {
                        Goals.fin_sabbatical.id = UserGoal[igoals].ID;
                        Goals.fin_sabbatical.status = true;
                        Goals.fin_sabbatical.risk = UserGoal[igoals].Risk;
                        Goals.fin_sabbatical.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_sabbatical.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_sabbatical.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_sabbatical.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_sabbatical.starttime = UserGoal[igoals].Start;
                        Goals.fin_sabbatical.endtime = UserGoal[igoals].End;
                        Goals.fin_sabbatical.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_sabbatical.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_sabbatical.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_sabbatical.pendingamount = UserGoal[igoals].PendingAmount;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG8") {
                        Goals.fin_wealth.id = UserGoal[igoals].ID;
                        Goals.fin_wealth.status = true;
                        Goals.fin_wealth.risk = UserGoal[igoals].Risk;
                        Goals.fin_wealth.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_wealth.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_wealth.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_wealth.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_wealth.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_wealth.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_wealth.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_wealth.pendingamount = UserGoal[igoals].PendingAmount;

                        if (UserGoal[igoals].TypeCode === "001") {
                            Goals.fin_wealth.type.code = "001";
                            Goals.fin_wealth.type.name = "SIMPLE";
                        }
                        else if (UserGoal[igoals].TypeCode === "002") {
                            Goals.fin_wealth.type.code = "002";
                            Goals.fin_wealth.type.name = "MODERATE";
                        }
                        else {
                            Goals.fin_wealth.type.code = "003";
                            Goals.fin_wealth.type.name = "AWESOME";
                        }

                        Goals.fin_wealth.amount = UserGoal[igoals].Amount;
                        Goals.fin_wealth.time = UserGoal[igoals].Duration;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG9") {
                        Goals.fin_retirement.id = UserGoal[igoals].ID;
                        Goals.fin_retirement.status = true;
                        Goals.fin_retirement.risk = UserGoal[igoals].Risk;
                        Goals.fin_retirement.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_retirement.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_retirement.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_retirement.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_retirement.retirementage = UserDetail[0].RetirementAge;
                        Goals.fin_retirement.lifeexpectency = UserDetail[0].LifeExpentency;
                        Goals.fin_retirement.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_retirement.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_retirement.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_retirement.pendingamount = UserGoal[igoals].PendingAmount;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG14") {
                        var othergoal = {
                            id: isUserGoalArr ? UserGoal[igoals].GoalID : UserGoal[igoals].ID,
                            status: true,
                            goalcode: 'FG14',
                            risk: UserGoal[igoals].Risk,
                            productlist: UserGoal[igoals].ProductList,
                            investmenttype: UserGoal[igoals].InvestmentType,
                            getamount: UserGoal[igoals].GetAmount,
                            investamount: UserGoal[igoals].InvestAmount,
                            goalachieved: UserGoal[igoals].GoalAchieved,
                            investedamount: UserGoal[igoals].InvestedAmount,
                            currentamount: UserGoal[igoals].CurrentAmount,
                            pendingamount: UserGoal[igoals].PendingAmount,
                            type: { code: UserGoal[igoals].TypeCode, name: UserGoal[igoals].Place },
                            amount: UserGoal[igoals].Amount,
                            time: UserGoal[igoals].Goaltime
                        }
                        Goals.fin_other.push(othergoal);
                    }
                    else if (UserGoal[igoals].GoalCode === "FG17") {
                        Goals.fin_emergency.id = UserGoal[igoals].ID;
                        Goals.fin_emergency.status = true;
                        Goals.fin_emergency.risk = UserGoal[igoals].Risk;
                        Goals.fin_emergency.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_emergency.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_emergency.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_emergency.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_emergency.amount = UserGoal[igoals].Amount;
                        Goals.fin_emergency.time = UserGoal[igoals].Duration;
                        Goals.fin_emergency.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_emergency.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_emergency.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_emergency.pendingamount = UserGoal[igoals].PendingAmount;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG23") {
                        Goals.fin_terminsurance.id = UserGoal[igoals].ID;
                        Goals.fin_terminsurance.status = true;
                        Goals.fin_terminsurance.risk = UserGoal[igoals].Risk;
                        Goals.fin_terminsurance.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_terminsurance.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_terminsurance.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_terminsurance.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_terminsurance.amount = UserGoal[igoals].Amount;
                        Goals.fin_terminsurance.time = UserGoal[igoals].Duration;
                        Goals.fin_terminsurance.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_terminsurance.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_terminsurance.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_terminsurance.pendingamount = UserGoal[igoals].PendingAmount;
                    }
                    else if (UserGoal[igoals].GoalCode === "FG24") {
                        Goals.fin_healthinsurance.id = UserGoal[igoals].ID;
                        Goals.fin_healthinsurance.status = true;
                        Goals.fin_healthinsurance.risk = UserGoal[igoals].Risk;
                        Goals.fin_healthinsurance.productlist = UserGoal[igoals].ProductList;
                        Goals.fin_healthinsurance.investmenttype = UserGoal[igoals].InvestmentType;
                        Goals.fin_healthinsurance.getamount = UserGoal[igoals].GetAmount;
                        Goals.fin_healthinsurance.investamount = UserGoal[igoals].InvestAmount;
                        Goals.fin_healthinsurance.amount = UserGoal[igoals].Amount;
                        Goals.fin_healthinsurance.time = UserGoal[igoals].Duration;
                        Goals.fin_healthinsurance.goalachieved = UserGoal[igoals].GoalAchieved;
                        Goals.fin_healthinsurance.investedamount = UserGoal[igoals].InvestedAmount;
                        Goals.fin_healthinsurance.currentamount = UserGoal[igoals].CurrentAmount;
                        Goals.fin_healthinsurance.pendingamount = UserGoal[igoals].PendingAmount;
                    }
                }
            }

            $localStorage.currentUserData = Goals;
        }

        //set user taxSaving in localStorage
        factory.setTaxSaving = function (data) {

            //set taxSaving in localStorage
            
            if (data)
            {
                this.taxSavings.provFund = data.provFund;
                this.taxSavings.lifeFund = data.lifeFund;
                this.taxSavings.smallSaveFund = data.smallSaveFund;
                this.taxSavings.loanFund = data.loanFund;
                this.taxSavings.totalInvest = data.totalInvest;
                this.taxSavings.totalRequired = data.totalRequired;
            }

            $localStorage.currentTaxSaving = this.taxSavings;
        }

        //get user taxsaving details
        factory.getTaxSaving = function () {
            var taxsaving;
            if ($localStorage.currentTaxSaving) {
                taxsaving = $localStorage.currentTaxSaving;
            }
            else {
                taxsaving = this.taxSavings;
            }
            return taxsaving;
        }

        // remove TaxSaving from local storage
        factory.resetTaxSaving = function () {

            this.taxSavings.provFund = null;
            this.taxSavings.lifeFund = null;
            this.taxSavings.smallSaveFund = null;
            this.taxSavings.loanFund = null;
            this.taxSavings.totalInvest = null;
            this.taxSavings.totalRequired = null;
           
            delete $localStorage.currentTaxSaving;
        }

        // remove goal data from local storage
        factory.resetGoalData = function () {

            this.userGoals.fin_name = '',
            this.userGoals.fin_age = 0,
            this.userGoals.fin_cliententerpoint = '',
            this.userGoals.fin_gender = { code: '', name: '' },//M MALE-001/ F FEMALE-002
            this.userGoals.fin_maritialstatus = { code: '', name: '' },//SINGLE-001/ MARRIED-002/ WITH KIDS-003
            this.userGoals.fin_annualincome = 0,
            this.userGoals.fin_monthlyexpense = 0,
            this.userGoals.fin_spouse = { status: false, isworking: false, name: '', income: 0, mobile: '', age: 0, rid: null },
            this.userGoals.fin_child = { status: false, count: 0, detail: [] },
            this.userGoals.fin_emi = { status: false, amount: 0, loans: [] },//loans HOME-001, CAR-002, EDUCATION-003,PERSONAL-004,CREDIT CARD-005,OTHERS-006/ put loans as{code:001,name:'HOME LOAN' }
            this.userGoals.fin_familyplanning = { id: 0, status: false, goalcode: 'FG11', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type BUDGET-001, COMFY-002, LUXURY-003
            this.userGoals.fin_study = { id: 0, status: false, goalcode: 'FG5', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, location: { code: '', name: '', place: '' }, amount: 0, time: 1 },//location USA OR CANADA-001, EUROPE-002,SOUTH EAST ASIA-003,INDIA-004,OTHER-005 (note:for other change fill place frpm textbox)   
            this.userGoals.fin_wedding = { id: 0, status: false, goalcode: 'FG7', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type SIMPLE-001, MODERATE-002, LAVISH-003
            this.userGoals.fin_house = { id: 0, status: false, goalcode: 'FG4', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, time: 1 },
            this.userGoals.fin_car = { id: 0, status: false, goalcode: 'FG3', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type SMALL-001, HATCHBACK-002, SEDAN-003, SUV-004, LUXURY-005
            this.userGoals.fin_travel = { status: false, goalcode: 'FG6', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, location: { code: '', name: '', place: '' }, people: { count: 1, name: '' }, type: { code: '', name: '' }, amount: 0, time: 1 },//location USA OR CANADA-001, ,AUSTRALIA OR NZ-002, EUROPE-003,SOUTH EAST ASIA-004,INDIA-005//people SOLO-1, '+1'-2, FAMILY-2(married-2/ kids-4)//type BUDGET-001, COMFY-002, LUXURY-003
            this.userGoals.fin_bike = { id: 0, status: false, goalcode: 'FG1', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type 250 CC-250, 500 CC-500, 800 CC-800, 1000 CC-1000, 1200 CC-1200
            this.userGoals.fin_business = { id: 0, status: false, goalcode: 'FG2', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, monthly_amount: 0, time: 1 },
            this.userGoals.fin_sabbatical = { id: 0, status: false, goalcode: 'FG10', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, starttime: 1, endtime: 1 },
            this.userGoals.fin_wealth = { id: 0, status: false, goalcode: 'FG8', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 5 },//type SIMPLE-001, MODERATE-002, AWESOME-003
            this.userGoals.fin_retirement = { id: 0, status: false, goalcode: 'FG9', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, retirementage: 60, lifeexpectency: 80 },
            this.userGoals.fin_other = [],//{ id: 0, status: false, goalcode: 'FG14', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, type: { code: '', name: '' }, amount: 0, time: 1 },//type if goal comes from dropdown the code added else code of new goal return from add goal api used
            this.userGoals.fin_emergency = { id: 0, status: false, goalcode: 'FG17', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, time: 1.5 },
            this.userGoals.fin_terminsurance = { id: 0, status: false, goalcode: 'FG23', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, time: 0 },
            this.userGoals.fin_healthinsurance = { id: 0, status: false, goalcode: 'FG24', risk: 'M', productlist: '', investmenttype: 'S', getamount: 0, investamount: 0, goalachieved: 0, investedamount: 0, currentamount: 0, pendingamount: 0, amount: 0, time: 0 }

            delete $localStorage.currentUserData;
        }

        // get goal data of current user
        factory.getGoalData = function () {

            if ($localStorage.currentUserData) {
                return $localStorage.currentUserData;
            }
            else {
                this.resetGoalData();
                return this.userGoals;
            }
        }

        // check if 3 goals are check by user in 20 questions
        factory.is3goals = function () {

            if ($localStorage.currentUserData) {

                var goals = $localStorage.currentUserData
                var selectedgoals = 0;

                if (goals.fin_familyplanning.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_study.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_wedding.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_house.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_car.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_travel.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_bike.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_business.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_sabbatical.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_wealth.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_retirement.status) {
                    selectedgoals = selectedgoals + 1;
                }

                if (goals.fin_other.length > 0) {
                    selectedgoals = selectedgoals + 1;
                }


                if (goals.fin_child.status) {
                    var keepGoingStudy = true;
                    var keepGoingWedding = true;
                    angular.forEach(goals.fin_child.detail, function (value, key) {
                        if (keepGoingStudy) {
                            if (value.study.status) {
                                selectedgoals = selectedgoals + 1;
                                keepGoingStudy = false;
                            }
                        }
                        if (keepGoingWedding) {
                            if (value.wedding.status) {
                                selectedgoals = selectedgoals + 1;
                                keepGoingWedding = false;
                            }
                        }
                    });
                }



                if (selectedgoals >= 3) {
                    return true;
                }
                else {
                    return false;
                }

            }
            else {
                return false;
            }
        }


        // get current user loan data
        factory.getUserLoanData = function () {
            var goalsdata = factory.getGoalData();
            var userloan = [];
            angular.forEach(goalsdata.fin_emi.loans, function (value, key) {
                userloan.push({ "LoanID": "" + value.code + "" });
            });

            return userloan;
        }

        // get current user relation data
        factory.getUserRelationData = function () {
            var goalsdata = factory.getGoalData();

            var relations = [];

            var spouseRelation = {

                "RelationID": goalsdata.fin_spouse.rid ? "" + goalsdata.fin_spouse.rid + "" : "",

                "Relation": "001",

                "Name": "" + goalsdata.fin_spouse.name + "",

                "Mobile": "" + goalsdata.fin_spouse.mobile + "",

                "Age": goalsdata.fin_spouse.age,

                "Gender": "002",

                "AnnualIncome": "" + goalsdata.fin_spouse.income + "",

                "UserGoal": []
            }

            if (goalsdata.fin_spouse.status) {
                relations.push(spouseRelation);
            }

            angular.forEach(goalsdata.fin_child.detail, function (value, key) {


                var childSelectedGoals = [];

                var studyGoal = {

                    "RelationID": value.rid ? "" + value.rid + "" : "",

                    "GoalCode": "" + value.study.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "" + value.study.location.code + "",

                    "Place": "" + value.study.location.place + "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + value.study.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + value.study.time + "",

                    "Risk": "" + value.study.risk + "",

                    "InvestmentType": "" + value.study.investmenttype + "",

                    "GetAmount": "" + value.study.getamount + "",

                    "InvestAmount": "" + value.study.investamount + "",

                    "ProductList": "" + value.study.productlist + ""

                }

                var weddingGoal = {

                    "RelationID": value.rid ? "" + value.rid + "" : "",

                    "GoalCode": "" + value.wedding.goalcode + "",

                    "TypeCode": "" + value.wedding.type.code + "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + value.wedding.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + value.wedding.time + "",

                    "Risk": "" + value.wedding.risk + "",

                    "InvestmentType": "" + value.wedding.investmenttype + "",

                    "GetAmount": "" + value.wedding.getamount + "",

                    "InvestAmount": "" + value.wedding.investamount + "",

                    "ProductList": "" + value.wedding.productlist + ""

                }


                if (value.study.status) {
                    childSelectedGoals.push(studyGoal);
                }

                if (value.wedding.status) {
                    childSelectedGoals.push(weddingGoal);
                }

                var chlidRelation = {

                    "RelationID": value.rid ? "" + value.rid + "" : "",

                    "Relation": "002",

                    "Name": "" + value.name + "",

                    "Mobile": "" + value.mobile + "",

                    "Age": value.age,

                    "Gender": "" + value.gender.code + "",

                    "AnnualIncome": "" + 0 + "",

                    "UserGoal": childSelectedGoals

                };

                relations.push(chlidRelation);

            });

            return relations;
        }

        // get current user selected goal data 
        factory.getUserGoalData = function () {
            
            var goalsdata = factory.getGoalData();

            var userSelectedGoals = []

            if (goalsdata.fin_familyplanning.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_familyplanning.goalcode + "",

                    "TypeCode": "" + goalsdata.fin_familyplanning.type.code + "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_familyplanning.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_familyplanning.time + "",

                    "Risk": "" + goalsdata.fin_familyplanning.risk + "",

                    "InvestmentType": "" + goalsdata.fin_familyplanning.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_familyplanning.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_familyplanning.investamount + "",

                    "ProductList": "" + goalsdata.fin_familyplanning.productlist + ""

                });
            }

            if (goalsdata.fin_study.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_study.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "" + goalsdata.fin_study.location.code + "",

                    "Place": "" + goalsdata.fin_study.location.place + "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_study.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_study.time + "",

                    "Risk": "" + goalsdata.fin_study.risk + "",

                    "InvestmentType": "" + goalsdata.fin_study.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_study.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_study.investamount + "",

                    "ProductList": "" + goalsdata.fin_study.productlist + ""

                });
            }

            if (goalsdata.fin_wedding.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_wedding.goalcode + "",

                    "TypeCode": "" + goalsdata.fin_wedding.type.code + "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_wedding.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_wedding.time + "",

                    "Risk": "" + goalsdata.fin_wedding.risk + "",

                    "InvestmentType": "" + goalsdata.fin_wedding.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_wedding.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_wedding.investamount + "",

                    "ProductList": "" + goalsdata.fin_wedding.productlist + ""

                });
            }

            if (goalsdata.fin_house.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_house.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_house.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_house.time + "",

                    "Risk": "" + goalsdata.fin_house.risk + "",

                    "InvestmentType": "" + goalsdata.fin_house.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_house.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_house.investamount + "",

                    "ProductList": "" + goalsdata.fin_house.productlist + ""

                });
            }

            if (goalsdata.fin_car.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_car.goalcode + "",

                    "TypeCode": "" + goalsdata.fin_car.type.code + "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_car.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_car.time + "",

                    "Risk": "" + goalsdata.fin_car.risk + "",

                    "InvestmentType": "" + goalsdata.fin_car.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_car.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_car.investamount + "",

                    "ProductList": "" + goalsdata.fin_car.productlist + ""

                });
            }

            if (goalsdata.fin_travel.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_travel.goalcode + "",

                    "TypeCode": "" + goalsdata.fin_travel.type.code + "",

                    "People": "" + goalsdata.fin_travel.people.count + "",

                    "Location": "" + goalsdata.fin_travel.location.code + "",

                    "Place": "" + goalsdata.fin_travel.location.place + "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_travel.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_travel.time + "",

                    "Risk": "" + goalsdata.fin_travel.risk + "",

                    "InvestmentType": "" + goalsdata.fin_travel.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_travel.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_travel.investamount + "",

                    "ProductList": "" + goalsdata.fin_travel.productlist + ""

                });
            }

            if (goalsdata.fin_bike.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_bike.goalcode + "",

                    "TypeCode": "" + goalsdata.fin_bike.type.code + "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_bike.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_bike.time + "",

                    "Risk": "" + goalsdata.fin_bike.risk + "",

                    "InvestmentType": "" + goalsdata.fin_bike.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_bike.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_bike.investamount + "",

                    "ProductList": "" + goalsdata.fin_bike.productlist + ""

                });
            }

            if (goalsdata.fin_business.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_business.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + goalsdata.fin_business.monthly_amount + "",

                    "Amount": "" + goalsdata.fin_business.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_business.time + "",

                    "Risk": "" + goalsdata.fin_business.risk + "",

                    "InvestmentType": "" + goalsdata.fin_business.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_business.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_business.investamount + "",

                    "ProductList": "" + goalsdata.fin_business.productlist + ""

                });
            }

            if (goalsdata.fin_sabbatical.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_sabbatical.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + 0 + "",

                    "Start": "" + goalsdata.fin_sabbatical.starttime + "",

                    "End": "" + goalsdata.fin_sabbatical.endtime + "",

                    "Duration": "" + 0 + "",

                    "Risk": "" + goalsdata.fin_sabbatical.risk + "",

                    "InvestmentType": "" + goalsdata.fin_sabbatical.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_sabbatical.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_sabbatical.investamount + "",

                    "ProductList": "" + goalsdata.fin_sabbatical.productlist + ""

                });
            }

            if (goalsdata.fin_wealth.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_wealth.goalcode + "",

                    "TypeCode": "" + goalsdata.fin_wealth.type.code + "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_wealth.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_wealth.time + "",

                    "Risk": "" + goalsdata.fin_wealth.risk + "",

                    "InvestmentType": "" + goalsdata.fin_wealth.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_wealth.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_wealth.investamount + "",

                    "ProductList": "" + goalsdata.fin_wealth.productlist + ""

                });
            }

            if (goalsdata.fin_retirement.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_retirement.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + 0 + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_retirement.retirementage - goalsdata.fin_age + "",

                    "Risk": "" + goalsdata.fin_retirement.risk + "",

                    "InvestmentType": "" + goalsdata.fin_retirement.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_retirement.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_retirement.investamount + "",

                    "ProductList": "" + goalsdata.fin_retirement.productlist + ""

                });
            }

            if (goalsdata.fin_other.length > 0) {
                
                for (var i = 0; i < goalsdata.fin_other.length; i++) {
                    userSelectedGoals.push({

                        "GoalCode": "" + goalsdata.fin_other[i].goalcode + "",

                        "TypeCode": "" + goalsdata.fin_other[i].type.code + "",

                        "People": "" + 0 + "",

                        "Location": "",

                        "Place": "" + goalsdata.fin_other[i].type.name + "",

                        "MonthlyAmount": "" + 0 + "",

                        "Amount": "" + goalsdata.fin_other[i].amount + "",

                        "Start": "",

                        "End": "",

                        "Duration": "" + goalsdata.fin_other[i].time + "",

                        "Risk": "" + goalsdata.fin_other[i].risk + "",

                        "InvestmentType": "" + goalsdata.fin_other[i].investmenttype + "",

                        "GetAmount": "" + goalsdata.fin_other[i].getamount + "",

                        "InvestAmount": "" + goalsdata.fin_other[i].investamount + "",

                        "ProductList": "" + goalsdata.fin_other[i].productlist + "",

                        "RelationID": "" + goalsdata.fin_other[i].id + ""

                    });
                }
            }

            if (goalsdata.fin_emergency.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_emergency.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_emergency.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_emergency.time + "",

                    "Risk": "" + goalsdata.fin_emergency.risk + "",

                    "InvestmentType": "" + goalsdata.fin_emergency.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_emergency.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_emergency.investamount + "",

                    "ProductList": "" + goalsdata.fin_emergency.productlist + ""

                });
            }

            if (goalsdata.fin_terminsurance.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_terminsurance.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_terminsurance.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_terminsurance.time + "",

                    "Risk": "" + goalsdata.fin_terminsurance.risk + "",

                    "InvestmentType": "" + goalsdata.fin_terminsurance.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_terminsurance.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_terminsurance.investamount + "",

                    "ProductList": "" + goalsdata.fin_terminsurance.productlist + ""

                });
            }

            if (goalsdata.fin_healthinsurance.status) {
                userSelectedGoals.push({

                    "GoalCode": "" + goalsdata.fin_healthinsurance.goalcode + "",

                    "TypeCode": "",

                    "People": "" + 0 + "",

                    "Location": "",

                    "Place": "",

                    "MonthlyAmount": "" + 0 + "",

                    "Amount": "" + goalsdata.fin_healthinsurance.amount + "",

                    "Start": "",

                    "End": "",

                    "Duration": "" + goalsdata.fin_healthinsurance.time + "",

                    "Risk": "" + goalsdata.fin_healthinsurance.risk + "",

                    "InvestmentType": "" + goalsdata.fin_healthinsurance.investmenttype + "",

                    "GetAmount": "" + goalsdata.fin_healthinsurance.getamount + "",

                    "InvestAmount": "" + goalsdata.fin_healthinsurance.investamount + "",

                    "ProductList": "" + goalsdata.fin_healthinsurance.productlist + ""

                });
            }

            return userSelectedGoals;
        }

        return factory;
    }]);
})(angular.module('fincartApp'));