﻿(function (finApp) {
    'use strict';
    finApp.factory('userFactory', ['$localStorage', 'loggedInDetailsModel', function ($localStorage, loggedInDetailsModel) {
        var factory = {};

        //#region (User LoggedIn Details) 
        factory.setUserLoggedInDetails = function (data) {
            $localStorage.UserLoggedInDetails = loggedInDetailsModel.parseLoggedInDetails(data);
        }

        factory.destroyUserLoggedInDetails = function () {
            delete $localStorage.UserLoggedInDetails;
        }

        factory.getUserLoggedInDetails = function () {
            if ($localStorage.UserLoggedInDetails) {
                return $localStorage.UserLoggedInDetails;
            }
            return loggedInDetailsModel.LoggedInDetails;
        }
        //#endregion (User LoggedIn Details)

        //#region Total Cart Details
        factory.totalcartDetails = {
            cartTotalCount: 0
        }

        factory.settotalcartDetails = function (data) {
            this.totalcartDetails.cartTotalCount = parseInt(data.lumpsumInCart) + parseInt(data.instasipInCart);
            $localStorage.totalcart = this.totalcartDetails;
        }

        factory.incrementcart = function (count) {
            if ($localStorage.totalcart) {
                this.totalcartDetails = $localStorage.totalcart;
                this.totalcartDetails.cartTotalCount = this.totalcartDetails.cartTotalCount + count;
            }
            $localStorage.totalcart = this.totalcartDetails;
        }

        factory.decrementcart = function (count) {

            if ($localStorage.totalcart) {
                this.totalcartDetails = $localStorage.totalcart;
                if (this.totalcartDetails.cartTotalCount > 0) {
                    this.totalcartDetails.cartTotalCount = this.totalcartDetails.cartTotalCount - count;
                }
            }
            $localStorage.totalcart = this.totalcartDetails;
        }

        factory.cleartotalcartDetails = function () {
            this.totalcartDetails.cartTotalCount = 0;
            delete $localStorage.totalcart;
        }

        factory.gettotalcartDetails = function () {
            var tcart;
            if ($localStorage.totalcart) {
                tcart = $localStorage.totalcart;
            }
            else {
                tcart = this.totalcartDetails;
            }
            return tcart;
        }
        //#endregion Total Cart Details

        //#region SIP Notification Data
        factory.sipNotifyDetails = {
            sipNotifyData: []
         }

        factory.setsipNotifyDetails = function (data) {
            this.sipNotifyDetails.sipNotifyData = data;
            $localStorage.sipNotify = this.sipNotifyDetails;
        }

        factory.clearsipNotifyDetails = function () {
            this.sipNotifyDetails.sipNotifyData = [];
            delete $localStorage.sipNotify;
        }

        factory.getsipNotifyDetails = function () {
            if ($localStorage.sipNotify) {
                return $localStorage.sipNotify;
            }
            else {
                return this.sipNotifyDetails;
            }
        }
        //#endregion Total Cart Details
        return factory;
    }]);
})(angular.module('fincartApp'));