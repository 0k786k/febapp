﻿(function (finApp) {
    'use strict';
    // define a service to hold the setup logic
    finApp.factory('dotLoaderService', function () {
        var s = {};

        s.setup = function ($scope, element, attrs) {
            // parse directive attributes
            if (attrs.count) $scope.count = parseInt(attrs.count);
            if (attrs.size) $scope.size = parseFloat(attrs.size);
            if (attrs.color) $scope.color = attrs.color;
            if (attrs.radius) $scope.radius = parseFloat(attrs.radius);

            // if no dotlist was provided, setup a single color loader
            if (typeof $scope.dotlist === "undefined") {
                s.setupSingleColorLoader($scope);
            }
            s.applyStyles($scope, element);
        };

        s.setupSingleColorLoader = function ($scope) {
            if (typeof $scope.color === "undefined") $scope.color = $scope.defaults.color;
            if (typeof $scope.count === "undefined") $scope.count = $scope.defaults.count;
            $scope.dotlist = [];
            for (var i = 0; i < $scope.count; i++) {
                $scope.dotlist.push({ color: $scope.color });
            }
        };

        s.applyStyles = function ($scope, element) {
            angular.element(element[0]).css('font-size', [$scope.size, 'em'].join(''));
        };

        s.applyDotStyles = function ($scope, element) {
            if (typeof $scope.radius === "undefined") $scope.radius = $scope.defaults.radius;
            var dots = element.querySelectorAll('.dot');
            angular.element(dots).css('border-radius', [$scope.radius, '%'].join(''));
        };

        return s;
    });

    // define the dot loader directive and inject the dot loader service
    finApp.directive('dotLoader',['dotLoaderService', '$timeout', function (dotLoaderService, $timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: { count: '@', color: '@', radius: '@', dotlist: '=?', txt: '=?' },
            controller: ['$scope',function ($scope) {
                $scope.defaults = { color: '#fff', count: 5, radius: 50 };
            }],
            template: [
                '<div class="easy-dot-loader">',
                '<div style="font-size: 24px;">{{txt}}</div>',
                    '<div class="loader">',
                        '<div class="dot" ng-repeat="item in dotlist track by $index" ',
                            'style="background-color: {{ item.color }};"><span>{{ item.text }}</span></div>',
                    '</div>',
                '</div>'
            ].join(''),
            link: function (scope, element, attrs) {
                dotLoaderService.setup(scope, element, attrs);
                $timeout(function () {
                    dotLoaderService.applyDotStyles(scope, element[0]);
                });
            }
        };
    }]);
})(angular.module('fincartApp'));


