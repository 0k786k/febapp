﻿(function (finApp) {
    'use strict';
    finApp.factory('validationFactory',['$timeout', function ($timeout) {

        var fac = {};

        fac.validEmail = function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
       
        fac.validMobile = function (mobile, countryCode) {
            
            if (countryCode != '+91') {
                
                return true;
            }
            else {
                var IndNum = /^[0]?[789]\d{9}$/;
                return IndNum.test(mobile);
            }


            
        }
      
        fac.validPan = function (pan,accType) {
            
            var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
            var code_chk = pan.substring(3, 4);
            if (regpan.test(pan))
            {
                if (accType === "03" && code_chk.toUpperCase() === "H")
                {
                    return true;
                }
                else if (accType != "00" && code_chk.toUpperCase() === "P") {
                    return true;
                }
                else if (accType != "00" && code_chk.toUpperCase() === "P") {
                    return true;
                }
                else { return false; }
            }
            else { return false;}
            
        }

        fac.validIFSC = function (ifsc) {
            var regifsc = /^[a-zA-Z0-9]{11}$/;
           var ret = regifsc.test(ifsc);
           return ret;
        }
        fac.validAccNumber = function (acc) {
            var regaccno = /^\d*$/;
            return regaccno.test(acc);
        }
        fac.validAadhar = function (aadhar) {
            var regaadhar = /^\d{4}\s\d{4}\s\d{4}$/;
            return regaadhar.test(aadhar);
        }
        
        return fac;

    }]);
})(angular.module('fincartApp'));