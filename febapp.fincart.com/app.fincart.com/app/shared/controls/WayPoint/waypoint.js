﻿(function (finApp) {
    'use strict';
    finApp.directive('wayPoint',['$document', '$rootScope', function ($document, $rootScope) {
        return {
            restrict: 'A',
            link: function () {
                $('.gfield').each(function () {
                    $(this).waypoint(function () {
                        $('.gfield').removeClass('lf-active');
                        $(this.element).addClass('lf-active');
                    });
                }, { offset: '50%' });
            }
        }
    }]);

})(angular.module('fincartApp'));