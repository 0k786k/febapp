﻿(function (finApp) {
    'use strict';
    finApp.directive('pdfSaveButton',['$rootScope', function ($rootScope) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    var activePdfSaveId = attrs.pdfSaveButton;
                    var activePdfSaveName = attrs.pdfName;
                    var activePdfContent = attrs.pdfContent;
                    var activePdfStyle = attrs.pdfStyle;
                    $rootScope.$broadcast('savePdfEvent', { activePdfSaveId: activePdfSaveId, activePdfSaveName: activePdfSaveName, activePdfContent: activePdfContent, activePdfStyle: activePdfStyle });
                })
                var myListener = scope.$on('savePdfEvent', function (event, args) {
                    var broadcastedPdfSaveId = args.activePdfSaveId;
                    var broadcastedPdfSaveName = args.activePdfSaveName || 'default.pdf';
                    var broadcastedPdfContent = JSON.parse(args.activePdfContent);
                    var broadcastedPdfStyle = JSON.parse(args.activePdfStyle);

                    html2canvas(document.getElementById(broadcastedPdfSaveId), {
                        onrendered: function () {
                            var docDefinition = {
                                content: broadcastedPdfContent,
                                styles: broadcastedPdfStyle
                            };
                            pdfMake.createPdf(docDefinition).download(broadcastedPdfSaveName);
                        }
                    })
                });
                scope.$on('$destroy', myListener);
            }
        }
    }]);
})(angular.module('fincartApp'));