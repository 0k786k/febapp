﻿(function (finApp) {
    'use strict';
    finApp.directive('formSubmitter', ['$timeout', '$window', '$rootScope', function ($timeout, $window, $rootScope) {
        return {
            restrict: 'E',
            replace: true,
            template: '<form id="ThePopUpForm" action="{{ formData.url }}" method="{{ formData.method }}" target="ThePopUpWindow">' +
                '   <div ng-repeat="(key,val) in formData.params">' +
                '       <input type="hidden" name="{{ key }}" value="{{ val }}" />' +
                '   </div>' +
                '</form>',
            link: function ($scope, $element, $attrs) {
                $scope.$on('form.submit', function (event, data) {

                    $scope.formData = data;
                   
                    console.log('auto submitting form...');
                    var left = screen.width / 2 - 200, top = screen.height / 2 - 250
                   
                    $scope.trustSrc = function (src) {
                        return $sce.trustAsResourceUrl(src);
                    }

                    $timeout(function () {
                        $rootScope.popupWND = window.open('', 'ThePopUpWindow', "top=" + top + ",left=" + left + ",status=0,title=0,height=700,width=400,scrollbars=1");
                        //var frmSubmit = angular.element('#ThePopUpForm');
                        //frmSubmit.submit(); 
                        //document.getElementById('ThePopUpForm').submit();
                        jQuery($element).submit();

                        if ($rootScope.popupWND) {
                            var timer = setInterval(function () {
                                if ($rootScope.popupWND.closed) {
                                    
                                    //var resultHND = angular.element('#popwinRes');
                                    //$rootScope.hideModelProgress();
                                    //if (resultHND) {
                                    //    if (resultHND[0].value) {
                                            
                                    //        var objMsg = $rootScope.aadhaarLinkMsg.filter(function (obj) { return obj.value.toString() === resultHND[0].value; })[0];
                                    //        if (objMsg) {
                                    //            $rootScope.adhrMsg = objMsg.msg;
                                    //            if (objMsg.value === 'AL000' || objMsg.value === 'AL001') {
                                    //                $rootScope.onAadhaarLinkSuccess("1");
                                    //            }
                                    //            else { $rootScope.onAadhaarWinowFailed(); }
                                                
                                    //        } else { $rootScope.adhrMsg = "Something went wrong..please try again after some time..!!"; }
                                            
                                    //    }
                                    //    else {

                                    //    }

                                    //}

                                    $rootScope.$apply();

                                    clearInterval(timer);
                                    var jsonClose = { data: "{ \"errcode\": 5, \"errdesc\": \"Closed\" }" };
                                    $rootScope.$broadcast('ekyc.windowmsg', jsonClose);
                                }
                            }, 100);
                        }
                        else {
                            $rootScope.adhrMsg = "Popup window is disabled on your browser..";
                            $rootScope.onAadhaarWinowFailed();
                        }
                       
                        
                    })
                })
            }
        }
    }]);
})(angular.module('fincartApp'));
