﻿(function (finApp) {
    'use strict';
    finApp.factory('blinkFactory', ['$timeout', function ($timeout) {

        var fac = {};

        fac.blinkValid = function (ctrl) {
            var count = 0;
            function promise() {
                if (count < 8) {
                    if (count % 2 === 0) {
                        ctrl.css("border", "3px solid lightgreen");
                        ++count;
                        $timeout(promise, 300);
                    }
                    else {
                        ctrl.css("border", "3px solid #fff");
                        ++count;
                        $timeout(promise, 300);
                    }
                }
            };
            promise();
        }

        fac.blinkValidProp = function (ctrl, prop, size) {
            var count = 0;
            function promise() {
                if (count < 8) {
                    if (count % 2 === 0) {
                        ctrl.css(prop, size + " solid lightgreen");
                        ++count;
                        $timeout(promise, 300);
                    }
                    else {
                        ctrl.css(prop, size + " solid #fff");
                        ++count;
                        $timeout(promise, 300);
                    }
                }
            };
            promise();
        }

        fac.blinkInvalid = function (ctrl) {
            var count = 0;
            function promise() {
                if (count < 7) {
                    if (count % 2 === 0) {
                        ctrl.css("border", "3px solid red");
                        ++count;
                        $timeout(promise, 300);
                    }
                    else {
                        ctrl.css("border", "3px solid #fff");
                        ++count;
                        $timeout(promise, 300);
                    }
                }
            };
            promise();
        }

        fac.blinkInvalidProp = function (ctrl, prop, size) {
            var count = 0;
            function promise() {
                if (count < 7) {
                    if (count % 2 === 0) {
                        ctrl.css(prop, size + " solid red");
                        ++count;
                        $timeout(promise, 300);
                    }
                    else {
                        ctrl.css(prop, size + " solid #fff");
                        ++count;
                        $timeout(promise, 300);
                    }
                }
            };
            promise();
        }
        return fac;

    }]);
})(angular.module('fincartApp'));