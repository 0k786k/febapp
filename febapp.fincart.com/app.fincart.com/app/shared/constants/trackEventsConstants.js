﻿(function (finApp) {
    'use strict';
    finApp.constant('constTrackEvents', {
        trackEvents: {
            REGISTER_USER_FROM_LANDPAGE: 'REG_USR',
            REGISTER_OUTSIDE_AFTER_GOAL_NOLP: 'REG_USR_OUTSIDE_GOAL_NO_LANDP',
            REGISTER_NOLP: 'REG_USR_NO_LANDP',
            ANDROID_DOWNLOAD_CLICK: 'AAPP_DWLD_CLICK',
            FULL_FINPLAN: 'FIN_FULL_PLANNING_CLICK',
            QUICK_SIP_HOME: 'QUICK_SIP_HOME_CLICK',
            
            SINGLEGOAL_OWN_STUDY: 'SGLE_GOAL_OWNSTUDY_CLICK',
            SINGLEGOAL_OWN_WEDD: 'SGLE_GOAL_OWNWEDD_CLICK',
            SINGLEGOAL_CAR: 'SGLE_GOAL_CAR_CLICK',
            SINGLEGOAL_BIKE: 'SGLE_GOAL_BIKE_CLICK',
            SINGLEGOAL_CHILD_PLAN: 'SGLE_GOAL_CHLDPLN_CLICK',
            SINGLEGOAL_HOME_PLAN: 'SGLE_GOAL_HOMEPLN_CLICK',
            SINGLEGOAL_CHILD_EDU: 'SGLE_GOAL_CHLDEDU_CLICK',
            SINGLEGOAL_TRAVEL: 'SGLE_GOAL_TRVL_CLICK',
            SINGLEGOAL_STARTUP: 'SGLE_GOAL_STRTUP_CLICK',
            SINGLEGOAL_CHILD_WEDD: 'SGLE_GOAL_CHLDWEDD_CLICK',
            SINGLEGOAL_SABBATICAL: 'SGLE_GOAL_SBBTL_CLICK',
            SINGLEGOAL_WEALTH_CREATE: 'SGLE_GOAL_WLTHCRT_CLICK',
            SINGLEGOAL_RETIREMENT: 'SGLE_GOAL_RTRMNT_CLICK',
            SINGLEGOAL_OTHER_GOAL: 'SGLE_GOAL_OTHRGOL_CLICK',
            
            DASHBOARD_OWN_STUDY: 'DASH_GOAL_OWNSTUDY_CLICK',
            DASHBOARD_OWN_WEDD: 'DASH_GOAL_OWNWEDD_CLICK',
            DASHBOARD_GOAL_CAR: 'DASH_GOAL_CAR_CLICK',
            DASHBOARD_GOAL_BIKE: 'DASH_GOAL_BIKE_CLICK',
            DASHBOARD_CHILD_PLAN: 'DASH_GOAL_CHLDPLN_CLICK',
            DASHBOARD_HOME_PLAN: 'DASH_GOAL_HOMEPLN_CLICK',
            DASHBOARD_CHILD_EDU: 'DASH_GOAL_CHLDEDU_CLICK',
            DASHBOARD_TRAVEL: 'DASH_GOAL_TRVL_CLICK',
            DASHBOARD_STARTUP: 'DASH_GOAL_STRTUP_CLICK',
            DASHBOARD_CHILD_WEDD: 'DASH_GOAL_CHLDWEDD_CLICK',
            DASHBOARD_SABBATICAL: 'DASH_GOAL_SBBTL_CLICK',
            DASHBOARD_WEALTH_CREATE: 'DASH_GOAL_WLTHCRT_CLICK',
            DASHBOARD_RETIREMENT: 'DASH_GOAL_RTRMNT_CLICK',
            DASHBOARD_OTHER_GOAL: 'DASH_GOAL_OTHRGOL_CLICK',
            
            CAF_BASIC: 'CAF_BASIC',
            CAF_ADDRESS: 'CAF_ADDRS',
            CAF_BANK: 'CAF_BNK',
            CAF_NOMINEE: 'CAF_NOM',
            CAF_FATCA: 'CAF_FATC',
            ADD_TOCART: 'AAD_CRT',
            CONTINUE_TRANSACTION_CLICK: 'CONT_TRANS_CLK',
            TRANSACTION_COMPLETE: 'TRANS_COMPL',
            PROCEED_EKYC_CLICK: 'PROCD_EKYC_CLICK',
            EKYC_COMPLETED: 'EKYC_COMPL',
            ADD_NEW_MEMBR_CLICK: 'ADD_NW_MEMBER',
            MEMBER_ADDED: 'NW_MEMBER_ADDED',

            DASHBOARD_QUICK_SIP_CLICK: 'DASH_QUICK_SIP_CLICK',
            DASHBOARD_QUICK_SIP_ADDED: 'DASH_QUICK_SIP_ADDED',
            DASHBOARD_GOAL_EDIT_CLICK: 'DASH_GOAL_EDIT_CLICK',
            DASHBOARD_GOAL_INVEST_MORE_CLICK: 'DASH_GOAL_INVEST_MORE_CLICK',
            DASHBOARD_GOAL_ACTIVATE_CLICK: 'DASH_GOAL_ACTIVATE_CLICK'
            
        }
    });
})(angular.module('fincartApp'));