﻿(function (finApp) {
    'use strict';
    finApp.constant('goalConstants', {
        goals: [
                {
                    name: "Bike",
                    value: "FG1"
                },
                {
                    name: "Business",
                    value: "FG2"
                },
                {
                    name: "Car",
                    value: "FG3"
                },
                {
                    name: "House",
                    value: "FG4"
                },
                {
                    name: "Study",
                    value: "FG5"
                },
                {
                    name: "Travel",
                    value: "FG6"
                },
                {
                    name: "Wedding",
                    value: "FG7"
                },
                {
                    name: "Wealth",
                    value: "FG8"
                },
                {
                    name: "Retirement",
                    value: "FG9"
                },
                {
                    name: "Sabbatical",
                    value: "FG10"
                },
                {
                    name: "Family Planning",
                    value: "FG11"
                },
                {
                    name: "Chlid's Study",
                    value: "FG12"
                },
                {
                    name: "Chlid's Wedding",
                    value: "FG13"
                },
                {
                    name: "Other",
                    value: "FG14"
                },
                {
                    name: "Emergency",
                    value: "FG17"
                },
                {
                    name: "Term Insurance",
                    value: "FG23"
                },
                {
                    name: "Life Insurance",
                    value: "FG24"
                },
                {
                    name: "Quick Sip",
                    value: "FG220"
                },
                {
                    name: "Tax Saving",
                    value: "FG104"
                },
                {
                    name: "Best Saver Account",
                    value: "FG309"
                }
        ],
        travelLocations: [
                {
                    name: "USA OR CANADA",
                    value: "001"
                },
                {
                    name: "AUSTRALIA OR NZ",
                    value: "002"
                },
                {
                    name: "EUROPE",
                    value: "003"
                },
                {
                    name: "SOUTH EAST ASIA",
                    value: "004"
                },
                {
                    name: "INDIA",
                    value: "005"
                }
        ],
        travelbudget: [
                {
                    name: "BUDGET",
                    value: "001"
                },
                {
                    name: "COMFY",
                    value: "002"
                },
                {
                    name: "LUXURY",
                    value: "003"
                }
        ],
        studyLocations: [
                {
                    name: "USA OR CANADA",
                    value: "001"
                },
                {
                    name: "EUROPE",
                    value: "002"
                },
                {
                    name: "SOUTH EAST ASIA",
                    value: "003"
                },
                {
                    name: "INDIA",
                    value: "004"
                },
                {
                    name: "OTHER",
                    value: "005"
                }
        ],
        childStudyLocations: [
                {
                    name: "USA OR CANADA",
                    value: "001"
                },
                {
                    name: "EUROPE",
                    value: "002"
                },
                {
                    name: "SOUTH EAST ASIA",
                    value: "003"
                },
                {
                    name: "INDIA",
                    value: "004"
                }
        ]
    });
})(angular.module('fincartApp'));