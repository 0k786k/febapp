﻿(function (finApp) {
    'use strict';
    finApp.constant('dashboardConstants', {
        dashboardGoalSlider: [
                {
                    heading: "Quick Sip",
                    subheading: "Start Early + Invest Regularly = Create Wealth",
                    code: "FG220",
                    id: 1
                },
				{
				    heading: "Plan for Chlid's Study",
				    subheading: "Plan for chlid's study without Loan",
				    code: "FG12",
				    id: 2
				},
				{
				    heading: "Tax Saving",
				    subheading: "Save Tax + Build Wealth = ELSS",
				    code: "FG104",
				    id: 3
				},
				{
				    heading: "Plan for Retirement",
				    subheading: "Plan for a good Retirement",
				    code: "FG9",
				    id: 4
				},
				{
				    heading: "Plan for Travel",
				    subheading: "Plan your travel without EMI",
				    code: "FG6",
				    id: 5
				},
                {
                    heading: "Buy a Car",
                    subheading: "Buy a car without EMI and get profit around 20%",
                    code: "FG3",
                    id: 6
                },
				{
				    heading: "Plan for Chlid's Wedding",
				    subheading: "Plan for chlid's wedding without Loan",
				    code: "FG13",
				    id: 7
				},
				{
				    heading: "Create Wealth",
				    subheading: "Become a crorepati",
				    code: "FG8",
				    id: 8
				},
                {
                    heading: "Buy a House",
                    subheading: "Plan your own house Down-Payment",
                    code: "FG4",
                    id: 9
                },
				{
				    heading: "Thinking of Family Planning?",
				    subheading: "Plan for family planning",
				    code: "FG11",
				    id: 10
				},
				{
				    heading: "Plan your Wedding",
				    subheading: "Plan your wedding without Loan",
				    code: "FG7",
				    id: 11
				},
                {
                    heading: "Start your Study",
                    subheading: "Start your study without Loan",
                    code: "FG5",
                    id: 12
                },
				{
				    heading: "Start your Business",
				    subheading: "Start your business without Loan",
				    code: "FG2",
				    id: 13
				},
				{
				    heading: "Buy a Bike",
				    subheading: "Buy a bike without EMI and get profit around 20%",
				    code: "FG1",
				    id: 14
				},
				{
				    heading: "Best Saver Account",
				    subheading: "Boond Boond se account bharta hai",
				    code: "FG309",
				    id: 15
				},
                {
                    heading: "Want to take Sabbatical?",
                    subheading: "Plan for sabbatical",
                    code: "FG10",
                    id: 16
                },
                {
                    heading: "Any Other goal in mind?",
                    subheading: "Plan for any goal",
                    code: "FG14",
                    id: 17
                }
        ],
        impactStorySlider: [
                {
                    name: "Sanjay Harika",
                    title: "Group Sales Manager",
                    url: "https://www.youtube.com/embed/xfRguEZqG2M",
                    id: 1
                },
                {
                    name: "Sanjay Harika",
                    title: "Group Sales Manager",
                    url: "https://www.youtube.com/embed/xfRguEZqG2M",
                    id: 2
                },
                {
                    name: "Sanjay Harika",
                    title: "Group Sales Manager",
                    url: "https://www.youtube.com/embed/xfRguEZqG2M",
                    id: 3
                },
                {
                    name: "Sanjay Harika",
                    title: "Group Sales Manager",
                    url: "https://www.youtube.com/embed/xfRguEZqG2M",
                    id: 4
                },
                {
                    name: "Sanjay Harika",
                    title: "Group Sales Manager",
                    url: "https://www.youtube.com/embed/xfRguEZqG2M",
                    id: 5
                }
        ]
    });
})(angular.module('fincartApp'));