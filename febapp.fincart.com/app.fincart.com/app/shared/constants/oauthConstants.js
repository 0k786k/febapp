﻿(function (finApp) {
    'use strict';
    finApp.constant('oauthConstants', {
        oauth: {
            grant_type_password: 'password',
            grant_type_refresh_token: 'refresh_token',
            client_id: 'fincartApp',
            client_secret: '264e3285b0934defa5067bfb4bc9d682'
        }
    });
})(angular.module('fincartApp'));