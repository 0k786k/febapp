﻿(function (finApp) {
    'use strict';
    finApp.constant('errorConstants', {
        errorcode: {
            111: 'Angular Error',
            100: 'User Friendly Error',
            200: 'DB Error',
            300: 'API Error'
        }
    });
})(angular.module('fincartApp'));