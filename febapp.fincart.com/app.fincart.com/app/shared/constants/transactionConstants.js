﻿(function (finApp) {
    'use strict';
    finApp.constant('transactionConstants', {
        installments: [
                {
                    name: "1 Year",
                    value: 12
                },
                {
                    name: "1.5 Years",
                    value: 18
                },
                {
                    name: "2 Years",
                    value: 24
                },
                {
                    name: "2.5 Years",
                    value: 30
                },
                {
                    name: "3 Years",
                    value: 36
                }, {
                    name: "3.5 Year",
                    value: 42
                },
                {
                    name: "4 Years",
                    value: 48
                },
                {
                    name: "4.5 Years",
                    value: 54
                },
                {
                    name: "5 Years",
                    value: 60
                },
                {
                    name: "5.5 Years",
                    value: 66
                }, {
                    name: "6 Year",
                    value: 72
                },
                {
                    name: "6.5 Years",
                    value: 78
                },
                {
                    name: "7 Years",
                    value: 84
                },
                {
                    name: "7.5 Years",
                    value: 90
                },
                {
                    name: "8 Years",
                    value: 96
                }, {
                    name: "8.5 Year",
                    value: 102
                },
                {
                    name: "9 Years",
                    value: 108
                },
                {
                    name: "9.5 Years",
                    value: 114
                },
                {
                    name: "10 Years",
                    value: 120
                },
                {
                    name: "10.5 Years",
                    value: 126
                },
                {
                    name: "11 Year",
                    value: 132
                },
                {
                    name: "11.5 Years",
                    value: 138
                },
                {
                    name: "12 Years",
                    value: 144
                },
                {
                    name: "12.5 Years",
                    value: 150
                },
                {
                    name: "13 Years",
                    value: 156
                }, {
                    name: "13.5 Year",
                    value: 162
                },
                {
                    name: "14 Years",
                    value: 168
                },
                {
                    name: "14.5 Years",
                    value: 174
                },
                {
                    name: "15 Years",
                    value: 180
                },
                {
                    name: "15.5 Years",
                    value: 186
                }, {
                    name: "16 Year",
                    value: 192
                },
                {
                    name: "16.5 Years",
                    value: 198
                },
                {
                    name: "17 Years",
                    value: 204
                },
                {
                    name: "17.5 Years",
                    value: 210
                },
                {
                    name: "18 Years",
                    value: 216
                }, {
                    name: "18.5 Year",
                    value: 222
                },
                {
                    name: "19 Years",
                    value: 228
                },
                {
                    name: "19.5 Years",
                    value: 234
                },
                {
                    name: "20 Years",
                    value: 240
                },
                {
                    name: "20.5 Years",
                    value: 246
                },
                {
                    name: "21 Year",
                    value: 252
                },
                {
                    name: "21.5 Years",
                    value: 258
                },
                {
                    name: "22 Years",
                    value: 264
                },
                {
                    name: "22.5 Years",
                    value: 270
                },
                {
                    name: "23 Years",
                    value: 276
                }, {
                    name: "23.5 Year",
                    value: 282
                },
                {
                    name: "24 Years",
                    value: 288
                },
                {
                    name: "24.5 Years",
                    value: 294
                },
                {
                    name: "25 Years",
                    value: 300
                },
                {
                    name: "25.5 Years",
                    value: 306
                }, {
                    name: "26 Year",
                    value: 312
                },
                {
                    name: "26.5 Years",
                    value: 318
                },
                {
                    name: "27 Years",
                    value: 324
                },
                {
                    name: "27.5 Years",
                    value: 330
                },
                {
                    name: "28Years",
                    value: 336
                }, {
                    name: "28.5 Year",
                    value: 342
                },
                {
                    name: "29 Years",
                    value: 348
                },
                {
                    name: "29.5 Years",
                    value: 354
                },
                {
                    name: "30 Years",
                    value: 360
                },
                {
                    name: "30.5 Years",
                    value: 366
                },
                {
                    name: "31 Years",
                    value: 372
                },
                {
                    name: "31.5 Years",
                    value: 378
                },
                {
                    name: "32 Years",
                    value: 384
                },
                {
                    name: "32.5 Years",
                    value: 390
                },
                {
                    name: "33 Years",
                    value: 396
                },
                {
                    name: "33.5 Years",
                    value: 402
                },
                {
                    name: "34 Years",
                    value: 408
                },
                {
                    name: "34.5 Years",
                    value: 414
                },
                {
                    name: "35 Years",
                    value: 420
                },
                {
                    name: "35.5 Years",
                    value: 426
                },
                {
                    name: "36 Years",
                    value: 432
                },
                {
                    name: "36.5 Years",
                    value: 438
                },
                {
                    name: "37 Years",
                    value: 444
                },
                {
                    name: "37.5 Years",
                    value: 450
                },
                {
                    name: "38 Years",
                    value: 456
                },
                {
                    name: "38.5 Years",
                    value: 462
                },
                {
                    name: "39 Years",
                    value: 468
                },
                {
                    name: "39.5 Years",
                    value: 474
                },
                {
                    name: "40 Years",
                    value: 480
                },
                {
                    name: "40.5 Years",
                    value: 486
                },
                {
                    name: "41 Years",
                    value: 492
                },
                {
                    name: "41.5 Years",
                    value: 498
                },
                {
                    name: "42 Years",
                    value: 504
                },
                {
                    name: "42.5 Years",
                    value: 510
                },
                {
                    name: "43 Years",
                    value: 516
                },
                {
                    name: "43.5 Years",
                    value: 522
                },
                {
                    name: "44 Years",
                    value: 528
                },
                {
                    name: "44.5 Years",
                    value: 534
                },
                {
                    name: "45 Years",
                    value: 540
                },
                {
                    name: "45.5 Years",
                    value: 546
                },
                {
                    name: "46 Years",
                    value: 552
                },
                {
                    name: "46.5 Years",
                    value: 558
                },
                {
                    name: "47 Years",
                    value: 564
                },
                {
                    name: "47.5 Years",
                    value: 570
                },
                {
                    name: "48 Years",
                    value: 576
                },
                {
                    name: "48.5 Years",
                    value: 582
                },
                {
                    name: "49 Years",
                    value: 588
                },
                {
                    name: "49.5 Years",
                    value: 594
                },
                {
                    name: "50 Years",
                    value: 600
                }
        ],
        swp_Installments: [
                {
                    name: "1 Month",
                    value: 1
                },
                {
                    name: "2 Months",
                    value: 2
                },
                {
                    name: "3 Months",
                    value: 3
                },
                {
                    name: "4 Months",
                    value: 4
                },
                {
                    name: "5 Months",
                    value: 5
                },
                {
                    name: "6 Months",
                    value: 6
                },
                {
                    name: "7 Months",
                    value: 7
                },
                {
                    name: "8 Months",
                    value: 8
                },
                {
                    name: "9 Months",
                    value: 9
                },
                {
                    name: "10 Months",
                    value: 10
                },
                {
                    name: "11 Months",
                    value: 11
                },
                {
                    name: "12 Months",
                    value: 12
                },
                {
                    name: "13 Month",
                    value: 13
                },
                {
                    name: "14 Months",
                    value: 14
                },
                {
                    name: "15 Months",
                    value: 15
                },
                {
                    name: "16 Months",
                    value: 16
                },
                {
                    name: "17 Months",
                    value: 17
                },
                {
                    name: "18 Months",
                    value: 18
                },
                {
                    name: "19 Months",
                    value: 19
                },
                {
                    name: "20 Months",
                    value: 20
                },
                {
                    name: "21 Months",
                    value: 21
                },
                {
                    name: "22 Months",
                    value: 22
                },
                {
                    name: "23 Months",
                    value: 23
                },
                {
                    name: "24 Months",
                    value: 24
                }
        ],
        stp_Installments: [
                {
                    name: "1 Month",
                    value: 1
                },
                {
                    name: "2 Months",
                    value: 2
                },
                {
                    name: "3 Months",
                    value: 3
                },
                {
                    name: "4 Months",
                    value: 4
                },
                {
                    name: "5 Months",
                    value: 5
                },
                {
                    name: "6 Months",
                    value: 6
                },
                {
                    name: "7 Months",
                    value: 7
                },
                {
                    name: "8 Months",
                    value: 8
                },
                {
                    name: "9 Months",
                    value: 9
                },
                {
                    name: "10 Months",
                    value: 10
                },
                {
                    name: "11 Months",
                    value: 11
                },
                {
                    name: "12 Months",
                    value: 12
                },
                {
                    name: "13 Month",
                    value: 13
                },
                {
                    name: "14 Months",
                    value: 14
                },
                {
                    name: "15 Months",
                    value: 15
                },
                {
                    name: "16 Months",
                    value: 16
                },
                {
                    name: "17 Months",
                    value: 17
                },
                {
                    name: "18 Months",
                    value: 18
                },
                {
                    name: "19 Months",
                    value: 19
                },
                {
                    name: "20 Months",
                    value: 20
                },
                {
                    name: "21 Months",
                    value: 21
                },
                {
                    name: "22 Months",
                    value: 22
                },
                {
                    name: "23 Months",
                    value: 23
                },
                {
                    name: "24 Months",
                    value: 24
                }
        ],
        passbook_Filters: [
                {
                    name: "Scheme Name",
                    value: "scheme_name"
                },
                {
                    name: "Transaction Status",
                    value: "transaction_status"
                },
                {
                    name: "Transaction Type",
                    value: "transaction_type"
                },
                {
                    name: "Goal",
                    value: "goal"
                }
        ]
    });
})(angular.module('fincartApp'));