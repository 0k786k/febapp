﻿(function (finApp) {
    'use strict';
    finApp.constant('constTrackDevice', {
        trackDevice: {
            WEB_PLATFORM: 'WBP',
            ANDROID_PLATFORM: 'AAPP',
            IOD_PLATFORM: 'APL'
        }
    });
})(angular.module('fincartApp'));