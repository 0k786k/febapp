﻿(function (finApp) {
    'use strict';
    finApp.constant('kycCafConstants', {
        days: [
            { value: '', name: "DAY" },
                { value: '01', name: "1" },
                { value: '02', name: "2" },
                { value: '03', name: "3" },
                { value: '04', name: "4" },
                { value: '05', name: "5" },
                { value: '06', name: "6" },
                { value: '07', name: "7" },
                { value: '08', name: "8" },
                { value: '09', name: "9" },
                { value: '10', name: "10" },
                { value: '11', name: "11" },
                { value: '12', name: "12" },
                { value: '13', name: "13" },
                { value: '14', name: "14" },
                { value: '15', name: "15" },
                { value: '16', name: "16" },
                { value: '17', name: "17" },
                { value: '18', name: "18" },
                { value: '19', name: "19" },
                { value: '20', name: "20" },
                { value: '21', name: "21" },
                { value: '22', name: "22" },
                { value: '23', name: "23" },
                { value: '24', name: "24" },
                { value: '25', name: "25" },
                { value: '26', name: "26" },
                { value: '27', name: "27" },
                { value: '28', name: "28" },
                { value: '29', name: "29" },
                { value: '30', name: "30" },
                { value: '31', name: "31" },
        ],
        months: [
            { value: '', name: "MONTH" },
                { value: '01', name: "January" },
                             { value: '02', name: "February" },
                             { value: '03', name: "March" },
                             { value: '04', name: "April" },
                             { value: '05', name: "May" },
                             { value: '06', name: "June" },
                             { value: '07', name: "July" },
                             { value: '08', name: "August" },
                             { value: '09', name: "September" },
                             { value: '10', name: "October" },
                             { value: '11', name: "November" },
                             { value: '12', name: "December" }
        ],
        docTypes: [
                {
                    value: "aadhaar",
                    name: "AADHAR CARD"
                },
                {
                    value: "drivingLicence",
                    name: "DRIVING LICENCE"
                },
                {
                    value: "passport",
                    name: "PASSPORT"
                },
                {
                    value: "voterid",
                    name: "VOTER ID"
                }
        ],
        incomeOptions: [
              //{
              //    value: 0,
              //    name: 'Select Annual Income'
              //}, 
              {
                  value: 31,
                  name: '0-100000'
              }, {
                  value: 32,
                  name: '100000-500000'
              }, {
                  value: 33,
                  name: '500000-1000000'
              }, {
                  value: 34,
                  name: '1000000-2500000'
              }, {
                  value: 35,
                  name: '2500000-10000000'
              }, {
                  value: 36,
                  name: 'ABOVE 10000000'
              }],
        occupationOptions: [
                //{
                //    value: "00",
                //    name: "Select Occupation"
                //},
                {
                    value: "01",
                    name: "Private Sector"
                }, {
                    value: "02",
                    name: "Public Sector"
                }, {
                    value: "03",
                    name: "Business"
                }, {
                    value: "04",
                    name: "Professional"
                }, {
                    value: "06",
                    name: "Retired"
                }, {
                    value: "07",
                    name: "Housewife"
                }, {
                    value: "08",
                    name: "Student"
                }, {
                    value: "10",
                    name: "Government Sector"
                }, {
                    value: "99",
                    name: "Others"
                }, {
                    value: "11",
                    name: "Self Employed"
                }, {
                    value: "12",
                    name: "Not Categorized"
                }],
        rtaOccupationOptions: [
                //{
                //    value: "00",
                //    name: "OCCUPATION"
                //},
                {
                    value: "01",
                    name: "BUSINESS"
                }, {
                    value: "02",
                    name: "SERVICE"
                }, {
                    value: "03",
                    name: "PROFESSIONAL"
                }, {
                    value: "04",
                    name: "AGRICULTURE"
                }, {
                    value: "05",
                    name: "RETIRED"
                }, {
                    value: "06",
                    name: "HOUSE WIFE"
                }, {
                    value: "07",
                    name: "STUDENT"
                }, {
                    value: "08",
                    name: "OTHERS"
                }, {
                    value: "99",
                    name: "UNKNOWN"
                }],
        genderOptions: [
                //{
                //    value: "0",
                //    name: "Select Gender"
                //},
                {
                    value: "1",
                    name: "Male"
                }, {
                    value: "2",
                    name: "Female"
                }, {
                    value: "3",
                    name: "Transgender"
                }],
        addressTypeOptions: [
                //{
                //    value: "00",
                //    name: "Select Address Type"
                //},
                {
                    value: "01",
                    name: "Residential/Business"
                }, {
                    value: "02",
                    name: "Residential"
                }, {
                    value: "03",
                    name: "Business"
                }, {
                    value: "04",
                    name: "Registered office"
                }, {
                    value: "05",
                    name: "Unspecified"
                }],
        maritalStatusOptions: [
                //{
                //    value: "0",
                //    name: "Select Marital Status"
                //},
                {
                    value: "1",
                    name: "Married"
                }, {
                    value: "2",
                    name: "Un-Married"
                }],
        wealthsourceOptions: [
            //{
            //    value: 0,
            //    name: 'Select Income Source'
            //},
            {
                value: 1,
                name: 'Salary'
            },
            {
                value: 2,
                name: 'Business Income'
            },
            {
                value: 3,
                name: 'Gift'
            },
            {
                value: 4,
                name: 'Ancestral Property'
            },
            {
                value: 5,
                name: 'Rental Income'
            },
            {
                value: 6,
                name: 'Prize Money'
            },
            {
                value: 7,
                name: 'Royalty'
            },
            {
                value: 8,
                name: 'Others'
            }],
        bankOptions: [
                      //{
                      //    value: "00",
                      //    name: "Select Bank Name"
                      //},
                      {
                          value: "ALB",
                          name: "Allahabad Bank"
                      },
                      {
                          value: "ADB",
                          name: "Andhra Bank"
                      },
                      {
                          value: "UTI",
                          name: "Axis Bank"
                      },
                      {
                          value: "BDN",
                          name: "Bandhan Bank Netbanking"
                      },
                      {
                          value: "BBK",
                          name: "Bank of Bahrain and Kuwait"
                      },
                      {
                          value: "BBR",
                          name: "Bank of Baroda"
                      },
                      {
                          value: "BBC",
                          name: "Bank of Baroda - Corporate Banking"
                      },
                      {
                          value: "BOM",
                          name: "Bank of Maharashtra"
                      },
                      {
                          value: "CNB",
                          name: "Canara Bank"
                      },
                      {
                          value: "CSB",
                          name: "Catholic Syrian Bank"
                      },
                      {
                          value: "CBI",
                          name: "Central Bank of India"
                      },
                      {
                          value: "CIT",
                          name: "CITI Bank"
                      },
                      {
                          value: "CRP",
                          name: "Corporation Bank"
                      },
                      {
                          value: "COB",
                          name: "Cosmos Bank"
                      },
                      {
                          value: "DCB",
                          name: "DCB Bank"
                      },
                      {
                          value: "DEN",
                          name: "Dena Bank"
                      },
                      {
                          value: "DBK",
                          name: "Deutsche Bank"
                      },
                      {
                          value: "DC2",
                          name: "Development Credit Bank - Corporate"
                      },
                      {
                          value: "DLB",
                          name: "Dhanlaxmi Bank"
                      },
                      {
                          value: "FBK",
                          name: "Federal Bank"
                      },
                      {
                          value: "HDF",
                          name: "HDFC Bank"
                      },
                      {
                          value: "HSB",
                          name: "HSBC Bank"
                      },
                      {
                          value: "ICI",
                          name: "ICICI Bank"
                      },
                      {
                          value: "IDB",
                          name: "IDBI Bank"
                      },
                      {
                          value: "INB",
                          name: "Indian Bank"
                      },
                      {
                          value: "IOB",
                          name: "Indian Overseas Bank"
                      },
                      {
                          value: "IDS",
                          name: "Indusind Bank"
                      },
                      {
                          value: "ING",
                          name: "ING Vysya Bank"
                      },
                      {
                          value: "JKB",
                          name: "Ju0026K Bank"
                      },
                      {
                          value: "JSB",
                          name: "Janata Sahakari bank"
                      },
                      {
                          value: "KJB",
                          name: "Kalyan Janata Sahakari Bank Netbanking"
                      },
                      {
                          value: "KBL",
                          name: "Karnataka Bank"
                      },
                      {
                          value: "KVB",
                          name: "Karur Vysya Bank"
                      },
                      {
                          value: "162",
                          name: "Kotak Mahindra Bank"
                      },
                      {
                          value: "LVC",
                          name: "Laxmi Vilas Bank - Corporate Net Banking"
                      },
                      {
                          value: "LVR",
                          name: "Laxmi Vilas Bank - Retail Net Banking"
                      },
                      {
                          value: "MSB",
                          name: "Mehsana Urban Co-operative Bank"
                      },
                      {
                          value: "NKB",
                          name: "NKGSB Bank"
                      },
                      {
                          value: "OBC",
                          name: "Oriental Bank Of Commerce"
                      },
                      {
                          value: "PMC",
                          name: "Punjab u0026 Maharastra Coop Bank"
                      },
                      {
                          value: "PSB",
                          name: "Punjab u0026 Sind Bank"
                      },
                      {
                          value: "PNB",
                          name: "Punjab National Bank"
                      },
                      {
                          value: "CPN",
                          name: "Punjab National Bank - Corporate Banking"
                      },
                      {
                          value: "RTN",
                          name: "Ratnakar Bank"
                      },
                      {
                          value: "RBS",
                          name: "RBS (The Royal Bank of Scotland)"
                      },
                      {
                          value: "SWB",
                          name: "Saraswat Bank"
                      },
                      {
                          value: "SVC",
                          name: "Shamrao Vitthal Co-operative Bank"
                      },
                      {
                          value: "SIB",
                          name: "South Indian Bank"
                      },
                      {
                          value: "SCB",
                          name: "Standard Chartered Bank"
                      },
                      {
                          value: "SBJ",
                          name: "State Bank Of Bikaner and Jaipur"
                      },
                      {
                          value: "SBH",
                          name: "State Bank of Hyderabad"
                      },
                      {
                          value: "SBI",
                          name: "State Bank of India"
                      },
                      {
                          value: "SBM",
                          name: "State Bank of Mysore"
                      },
                      {
                          value: "SBP",
                          name: "State Bank of Patiala"
                      },
                      {
                          value: "SBT",
                          name: "State Bank of Travencore"
                      },
                      {
                          value: "SYD",
                          name: "Syndicate Bank"
                      },
                      {
                          value: "TMB",
                          name: "Tamilnad Mercantile Bank"
                      },
                      {
                          value: "TNC",
                          name: "Tamilnadu State Coop Bank"
                      },
                      {
                          value: "UCO",
                          name: "UCO Bank"
                      },
                      {
                          value: "UBI",
                          name: "Union Bank of India"
                      },
                      {
                          value: "UNI",
                          name: "United Bank Of India"
                      },
                      {
                          value: "VJB",
                          name: "Vijaya Bank"
                      },
                      {
                          value: "YBK",
                          name: "Yes Bank"
                      }],
        accounttypeOptions: [
                        //{
                        //    value: "00",
                        //    name: "ACCOUNT TYPE"
                        //},
                        {
                            value: "SB",
                            name: "Savings"
                        },
                         {
                             value: "CA",
                             name: "Current"
                         }
        ],
        modeOfHoldingOptions: [
                             //{
                             //    value: "00",
                             //    name: "SELECT"
                             //},
                          {
                              value: "Single",
                              name: "Single"
                          },
                          {
                              value: "Joint",
                              name: "Joint"
                          }, {
                              value: "AOS",
                              name: "AOS"
                          }
        ]
    });
})(angular.module('fincartApp'));