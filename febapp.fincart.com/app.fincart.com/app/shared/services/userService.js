﻿(function (finApp) {
    'use strict';

    finApp.service('userService', ['$http', '$q', 'envService', 'usergoalFactory', 'userprofileFactory', 'Upload', '$window', '$rootScope', 'serverResponseModel', 'addToCartModel', function ($http, $q, envService, usergoalFactory, userprofileFactory, Upload, $window, $rootScope, serverResponseModel) {

        //#region (API V2)

        this.callUserRegisterApi = function (registerReq) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiUserRegisterUrl'),
                data: registerReq
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callOverviewSummaryApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiOverviewSummary')
            }
            $http(requestParams)
                .success(function (response) {

                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callFundsWithCategoryApi = function (usergoalId) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiFundsWithCategory') + usergoalId
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callDashboardNotificationsApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",

                url: envService.read('apiDashboardNotifications')
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSystematicRemindersApi = function (basicid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiSystematicReminders') + basicid
            }
            $http(requestParams)
                .success(function (response) {

                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callFullSummaryApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiFullSummary')
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSearchSchemesApi = function (srchScheme) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSearchSchemes'),
                data: srchScheme
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callRecommendedSchemesApi = function (rcmdScheme) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiRecommendedSchemes'),
                data: rcmdScheme
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callAllMemberListApi = function (grpLeaderBasicid, fundid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",

                url: envService.read('apiAllMemberList') + grpLeaderBasicid + "/" + fundid
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSystematicDatesApi = function (schemeId, mandateStatus, trxnType) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiSystematicDates') + schemeId + "/" + mandateStatus + "/" + trxnType
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callAllInvestmentsApi = function (basicid, profileid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiAllInvestments') + basicid + "/" + profileid
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callPassbookFiltersApi = function (basicid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiPassbookFilters') + basicid
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callPassbookApi = function (param) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiPassbook'),
                data: param
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        //Profile Start
        this.callLoggedInUserDetailsApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiLoggedInUserDetails')
            }
            $http(requestParams)
                .success(function (response) {

                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callBankDetailsByIFSCApi = function (IFSC) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiBankDetailsByIFSC') + IFSC
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callapiBankDetailsByMICRApi = function (MICR) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiBankDetailsByMICR') + MICR
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callProfilesApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiProfiles')
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callFetchUserApi = function (basicid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiFetchUser') + basicid
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callPinCodeDetailsApi = function (pincode) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiPinCodeDetails') + pincode
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callChangeAddressReqApi = function (addressChangeRequest) {

            var formData = new FormData();
            formData.append('address', addressChangeRequest.address);
            formData.append('city', addressChangeRequest.city);
            formData.append('state', addressChangeRequest.state);
            formData.append('pincode', addressChangeRequest.pincode);
            formData.append('country', addressChangeRequest.country);
            formData.append('basicid', addressChangeRequest.basicid);
            formData.append('devicetype', addressChangeRequest.devicetype);
            formData.append('reason', addressChangeRequest.reason);
            // Attach file
            formData.append('panFile', addressChangeRequest.panFile);
            formData.append('aadhaarFile', addressChangeRequest.aadhaarFile);

            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiChangeAddressReq'),
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callChangeBankReqApi = function (bankChangeRequest) {

            var formData = new FormData();
            formData.append('ifsc', bankChangeRequest.ifsc);
            formData.append('branchAddrs', bankChangeRequest.branchadd);
            formData.append('bankName', bankChangeRequest.bankname);
            formData.append('branchName', bankChangeRequest.branchname);
            formData.append('branchCity', bankChangeRequest.branchcity);
            formData.append('micr', bankChangeRequest.micr);
            formData.append('accountNumber', bankChangeRequest.accountnumber);
            formData.append('accountName', bankChangeRequest.accountname);
            formData.append('bankAccType', bankChangeRequest.bankAccType);
            formData.append('basicid', bankChangeRequest.BasicId);
            formData.append('devicetype', bankChangeRequest.devicetype);
            formData.append('reason', bankChangeRequest.reason);
            // Attach file
            formData.append('chequeFile', bankChangeRequest.chequeFile);

            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiChangeBankReq'),
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callChangeRequestStatusApi = function (basicid, type) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiChangeRequestStatus') + basicid + '/' + type
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callUpdateCafApi = function (cafdata) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "PUT",
                url: envService.read('apiUpdateCaf'),
                data: cafdata
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callInvestProfilelistApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiInvestProfilelist')
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callCreateInvestProfileApi = function (investProfile) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiCreateInvestProfile'),
                data: investProfile
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSaveProfilePicApi = function (file, basicid) {
            var formData = new FormData();
            formData.append('file', file);
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSaveProfilePic') + basicid,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callMandateListApi = function (basicid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiMandateList') + basicid
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };
        //Profile End

        //Cart Start
        this.callAddToCartApi = function (cart) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiAddToCart'),
                data: cart
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callOtherTransactionApi = function (otherCart) {
            // SIP, STP, SWP, SWITCH, R
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiOtherTransaction'),
                data: otherCart
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callCartApi = function (basicId, profileId) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiCart') + basicId + "/" + profileId
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callRemoveCartApi = function (cartid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "PUT",
                url: envService.read('apiRemoveCart') + cartid
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callPaymentModesApi = function (bankid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiPaymentModes') + bankid
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callPurchaseAttemptApi = function (cartPurchaseAttempt) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiPurchaseAttempt'),
                data: cartPurchaseAttempt
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callBilldeskPostStringApi = function (txnId) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiBilldeskPostString') + txnId
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callBilldskPaymentStatusApi = function (txnId) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiBilldskPaymentStatus') + txnId + "/web"
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callPaymentReturnUrlApi = function () {
            return envService.readAppUrl('apiPaymentReturnUrl');
        };

        this.callAuthTokenOnServerApi = function (token) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.readAppUrl('apiAuthTokenOnServer'),
                data: { "token": token }
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response.d);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        }

        this.callInstaSellApi = function (otherCart) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiInstaSell'),
                data: otherCart
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };
        //Cart End

        //KYC-CAF Start
        this.callCheckKycStatusApi = function (pannumber) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiCheckKycStatus') + pannumber
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyAccountCreationApi = function (fincartCaptchaRequest) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyAccountCreation'),
                data: fincartCaptchaRequest
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyPOIPOAFileUploadApi = function (POIPOAFileUpload) {

            var formData = new FormData();

            formData.append('basicid', POIPOAFileUpload.basicid);
            formData.append('type', POIPOAFileUpload.type);
            // Attach file
            formData.append('file1', POIPOAFileUpload.panfile1);
            formData.append('file2', POIPOAFileUpload.frontfile2);
            formData.append('file3', POIPOAFileUpload.backfile3);

            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyPOIPOAFileUpload'),
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyStepDeleteApi = function (fincartDeleteStep) {

            //Account
            //POIPOA
            //BASIC
            //VEDIO
            //BANK
            //SIGN

            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyStepDelete'),
                data: fincartDeleteStep
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyUpdatePOIPOAApi = function (fincartPOISave) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyUpdatePOIPOA'),
                data: fincartPOISave
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyUpdateKycFormApi = function (fincartKYcData) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyUpdateKycForm'),
                data: fincartKYcData
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyPhotoFileUploadApi = function (POIPOAFileUpload) {

            var formData = new FormData();
            formData.append('basicid', POIPOAFileUpload.basicid);
            // Attach file
            formData.append('file1', POIPOAFileUpload.panfile1);

            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyPhotoFileUpload'),
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyStartVedioApi = function (fincartPOISave) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyStartVedio'),
                data: fincartPOISave
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyVedioFileUploadApi = function (POIPOAFileUpload) {

            var formData = new FormData();
            formData.append('basicid', POIPOAFileUpload.basicid);
            // Attach file
            formData.append('file', POIPOAFileUpload.panfile1);

            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyVedioFileUpload'),
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyBankVerificationApi = function (fincartBankDetailsSave) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzyBankVerification'),
                data: fincartBankDetailsSave
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzySignUploadApi = function (POIPOAFileUpload) {

            var formData = new FormData();
            formData.append('basicid', POIPOAFileUpload.basicid);
            // Attach file
            formData.append('file', POIPOAFileUpload.panfile1);

            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiSignzySignUpload'),
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callSignzyNextStepApi = function (basicid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiSignzyNextStep') + basicid
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };
        //KYC-CAF End        

        //Goal Start
        this.callGoalCalculatorApi = function (finCalcModel) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiGoalCalculator'),
                data: finCalcModel
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callGoalCreateApi = function (goal) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiGoalCreate'),
                data: goal
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callGoalUpdateApi = function (goal) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "PUT",
                url: envService.read('apiGoalUpdate'),
                data: goal
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callGoalDeleteApi = function (usergoalId) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "DELETE",
                url: envService.read('apiGoalDelete') + usergoalId
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callGoalNameCodeListApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiGoalNameCodeList')
            }
            $http(requestParams)
                .success(function (response) {

                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callFetchAllGoalApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiFetchAllGoal')
            }
            $http(requestParams)
                .success(function (response) {

                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callFetchSingleGoalApi = function (usergoalid) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiFetchSingleGoal') + usergoalid
            }
            $http(requestParams)
                .success(function (response) {

                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callFetchAllQuickSipsApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiFetchAllQuickSips')
            }
            $http(requestParams)
                .success(function (response) {

                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callTaxSavingGoalApi = function () {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiTaxSavingGoal')
            }
            $http(requestParams)
                .success(function (response) {

                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callGoalInvestmentsApi = function (usergoalId) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiGoalInvestments') + usergoalId
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callCheckZeroFolioApi = function (goal) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiCheckZeroFolio'),
                data: goal
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };

        this.callCreateZeroFolioApi = function (goal) {
            var serverResp = serverResponseModel.ServerResponse;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiCreateZeroFolio'),
                data: goal
            }
            $http(requestParams)
                .success(function (response) {
                    serverResp = serverResponseModel.parseServerResponse(response);
                    deferred.resolve(serverResp);
                })
                .error(function (err) {
                    serverResp.errorCode = 111;
                    deferred.resolve(serverResp);
                });
            return deferred.promise;
        };
        //Goal End

        //#endregion (API V2)


        this.registerUser = function (userid, name, password, mobile, cname, ccode) {

            var goalsdata = usergoalFactory.getGoalData();
            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiUserRegistrationUrl'),
                data: {
                    "Name": name,
                    "Userid": userid,
                    "password": password,
                    "ClientStatus": goalsdata.fin_cliententerpoint,
                    "Mobile": mobile,
                    "companyCode": ccode,
                    "companyName": cname
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    //if (result.isdata) {
                    //    pending APi Update useremail and usermobile against goal tag and question tag with UUID
                    //}
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.setUserBasicDetail = function () {
            var deferred = $q.defer();
            var result = { isdata: false };
            var requestParams = {
                method: "GET",
                url: envService.read('apiUserBasicDetailUrl')
            }
            $http(requestParams)
                .success(function (response) {
                    userprofileFactory.setUserProfileDetails(response);
                    result.isdata = true;
                    deferred.resolve(result);

                })
                .error(function (response) {
                    result.isdata = false;
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.getSHA256 = function (data) {

            var deferred = $q.defer();
            var result = { isdata: false, hash: "" }
            var requestParams = {
                method: "POST",
                url: envService.read('apiGetSha256Url'),
                data: {
                    "urlSHA": data
                }

            }
            $http(requestParams)
                .success(function (response) {

                    result.isdata = true;
                    result.hash = response;


                    deferred.resolve(result);
                })
                .error(function (response) {

                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.postConfirmPurchase = function (data) {

            var cartTransactData = userprofileFactory.getCartTransDetails();
            var deferred = $q.defer();
            var result = { isdata: false, TxnId: "" }
            var requestParams = {
                method: "POST",
                url: envService.read('apiConfirmPurchaseUrl'),
                data: {
                    "Scode": cartTransactData.Scode,
                    "Fcode": cartTransactData.Fcode,
                    "Amount": cartTransactData.Amount,
                    "NAVDate": cartTransactData.NAVDate,
                    "DivOption": cartTransactData.DivOption,
                    "FolioNo": cartTransactData.FolioNo,
                    "BankName": cartTransactData.BankName,
                    "BankBranch": cartTransactData.BankBranch,
                    "Account_No": cartTransactData.Account_No,

                    "ProfileID": cartTransactData.ProfileID,
                    "cartId": cartTransactData.cartId,
                    "type": cartTransactData.type,
                    "startDate": cartTransactData.startDate,
                    "endDate": cartTransactData.endDate,
                    "noOfInstallment": cartTransactData.noOfInstallment,
                    "mandateId": cartTransactData.mandateId,
                    "firstSipDate": cartTransactData.firstSipDate,
                    "mDate": cartTransactData.mDate,
                    "frequency": cartTransactData.frequency,
                    "billDeskCode": cartTransactData.billDeskCode,
                    "billAmcCode": cartTransactData.billAmcCode,
                    "schemeName": cartTransactData.schemeName,
                    "investProfile": cartTransactData.investProfile,
                    "bankCode": cartTransactData.bankCode

                }

            }
            $http(requestParams)
                .success(function (response) {

                    result.isdata = true;
                    result.TxnId = response;
                    deferred.resolve(result);
                })
                .error(function (response) {

                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.postConfirmEKYC = function (grpleaderEmail) {

            var ekyc = userprofileFactory.getKYC();
            var deferred = $q.defer();
            var result = { isdata: false, camsPostData: "", basicid: "" }
            var requestParams = {
                method: "POST",
                url: envService.read('apiEKYCUrl'),
                data: {

                    "basicid": ekyc.basicid,
                    "pan": ekyc.pan,
                    "email": ekyc.email,
                    "aadhaarno": ekyc.aadhaarno,
                    "mobileno": ekyc.mobileno,
                    "invsttype": ekyc.invsttype,
                    "kycstatuscode": ekyc.kycstatuscode,
                    "itpanname": ekyc.itpanname,
                    "grpleader": grpleaderEmail
                }

            }
            $http(requestParams)
                .success(function (response) {


                    if (response) {
                        result.basicid = response.basicid;
                        ekyc.basicid = response.basicid;
                        userprofileFactory.setEKYC(ekyc);
                        result.isdata = true;
                    }


                    var appidcams = envService.read('apiAppIdForEkycUrl');
                    var useridcams = envService.read('apiUserIdForEkycUrl');
                    var passwordcams = envService.read('apiPasswordForEkycUrl');
                    var intermedidcams = envService.read('apiIntermediaryIdForEkycUrl');
                    var DeviceType = envService.read('DeviceTypeEkyc');
                    result.camsPostData = ekyc.basicid + "|" + ekyc.pan + "|" + ekyc.email + "|" + ekyc.aadhaarno + "|" + ekyc.mobileno + "|" + appidcams + "|" + useridcams + "|" + passwordcams + "|" + intermedidcams + "|" + response.sessid + "|" + DeviceType + "|" + ekyc.invsttype + "|" + grpleaderEmail;

                    deferred.resolve(result);
                })
                .error(function (response) {

                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.getEkycDetails = function (pan) {
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetEkycDetails') + pan

            }

            $http(requestParams)
                .success(function (response) {

                    userprofileFactory.setEKYC(response).then(function () {
                        deferred.resolve(response);
                    });

                })
                .error(function (response) {

                    deferred.resolve(response);
                });
            return deferred.promise;
        };



        this.setEkycDataOnServer = function (ekycdata) {

            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: 'index.aspx/SetEkycData',
                data: { "data": ekycdata }
            }
            $http(requestParams)
                .success(function (response) {
                    if (response.d === "ok") {
                        result.isdata = true;
                    }
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.checkUserKYC = function (pan) {
            userprofileFactory.clearuserPanDetails();
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiKYCStatusUrl') + pan
            }
            $http(requestParams)
                .success(function (response) {
                    userprofileFactory.setuserPanDetails(response);
                    deferred.resolve(response);
                })
                .error(function (response) {
                    deferred.resolve(response);
                });

            return deferred.promise;
        };

        this.checkKYCForCartTransaction = function (pan) {

            var deferred = $q.defer();
            var result = { hasKYC: false }
            var requestParams = {
                method: "GET",
                url: envService.read('apiKYCStatusUrl') + pan
            }
            $http(requestParams)
                .success(function (response) {
                    result.hasKYC = response.KYC_STATUS === "Y" ? true : false;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.checkPanExist = function (pan) {

            var result = { isdata: false };
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiPanExistUrl') + pan
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.checkmemberKYC = function (pan) {
            userprofileFactory.clearNewMemberPanDetails();
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiKYCStatusUrl') + pan
            }
            $http(requestParams)
                .success(function (response) {
                    userprofileFactory.setNewMemberPanDetails(response);
                    deferred.resolve(response);
                })
                .error(function (response) {
                    deferred.resolve(response);
                });

            return deferred.promise;
        };

        this.checkMinorKYC = function (pan) {
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiKYCStatusUrl') + pan
            }
            $http(requestParams)
                .success(function (response) {
                    userprofileFactory.setminorPanDetails(response);
                    deferred.resolve();
                })
                .error(function (response) {
                    deferred.resolve();
                });

            return deferred.promise;
        };

        this.checkGaurdianKYC = function (pan) {
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiKYCStatusUrl') + pan
            }
            $http(requestParams)
                .success(function (response) {
                    userprofileFactory.setgaurdianPanDetails(response);
                    deferred.resolve();
                })
                .error(function (response) {
                    deferred.resolve();
                });

            return deferred.promise;
        };

        //#region OLD CODE (set caf status)
        //this.setCafStatus = function () {

        //    var deferred = $q.defer();
        //    var requestParams = {
        //        method: "GET",
        //        url: envService.read('apiCafStatusUrl')
        //    }
        //    $http(requestParams)
        //        .success(function (response) {

        //            if (response.LoginStatusResult === "Dashboard" || response.LoginStatusResult === "CART") {
        //                userprofileFactory.setuserCafStatus(true);
        //            }
        //            else {
        //                userprofileFactory.setuserCafStatus(false);
        //            }
        //            deferred.resolve();

        //        })
        //        .error(function (response) {

        //            deferred.resolve();
        //        });

        //    return deferred.promise;
        //};
        //#endregion OLD CODE (set caf status)

        //region NEW CODE--------------------

        this.saveCafBasic = function () {

            var cafdata = userprofileFactory.getMemberCAF();
            var deferred = $q.defer();
            var result = { isdata: false, CAFDATA: [] }
            var requestParams = {
                method: "POST",
                url: envService.read('apiSaveCafBasicUrl'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Commcity": cafdata.commcity,
                    "Commstate": cafdata.commstate,
                    "Commcountry": cafdata.commcountry,
                    "Commpin": cafdata.commpin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name,
                    "Relation": cafdata.nominee1relation,
                    "Nominee_Dob": cafdata.nominee1dob,
                    "Nom1_percent": cafdata.nominee1share,
                    "Nom2_name": cafdata.nominee2name,
                    "Nom2_relation": cafdata.nominee2relation,
                    "Nom2_dob": cafdata.nominee2dob,
                    "Nom2_percent": cafdata.nominee2share,
                    "Nom3_Name": cafdata.nominee3name,
                    "Nom3_Relation": cafdata.nominee3relation,
                    "Nom3_Dob": cafdata.nominee3dob,
                    "Nom3_Percent": cafdata.nominee3share,
                    "Place_of_Birth": cafdata.birthplace,
                    "Country_of_Birth": cafdata.birthcountry,
                    "Source_Wealth": cafdata.wealthsource,
                    "Pol_Exp_Person": cafdata.ispep,
                    "Country_Tax_Res1": cafdata.taxcountry1,
                    "CountryPayer_Id_no": cafdata.taxidnumber1,
                    "Idenif_Type": cafdata.taxidtype1,
                    "Country_Tax_Res2": cafdata.taxcountry2,
                    "CountryPayer_Id_no2": cafdata.taxidnumber2,
                    "Idenif_Type2": cafdata.taxidtype2,
                    "Country_Tax_Res3": cafdata.taxcountry3,
                    "CountryPayer_Id_no3": cafdata.taxidnumber3,
                    "Idenif_Type3": cafdata.taxidtype3,

                    "MemberId": cafdata.memberid,
                    "basicId": cafdata.basicid,
                    "groupleader": cafdata.groupleader,
                    "profilePic": cafdata.profilepic,
                    "aadhar": cafdata.aadhar,
                    "CAFStatus": cafdata.cafstatus,
                    "CAFBasicStatus": cafdata.cafbasicstatus,
                    "CAFAddressStatus": cafdata.cafaddressstatus,
                    "CAFBankStatus": cafdata.cafbankstatus,
                    "CAFNomineeStatus": cafdata.cafnomineestatus,
                    "CAFFatcaStatus": cafdata.caffatcastatus,

                    "Nom1Id": cafdata.Nom1Id,
                    "Nom2Id": cafdata.Nom2Id,
                    "Nom3Id": cafdata.Nom3Id,
                    "Email": cafdata.email,
                    "Commaddress1": cafdata.corpaddress,
                    "sameAddress": cafdata.sameAddress

                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = true;

                    userprofileFactory.setMemberCafFromDatabase(response.cafDetails);
                    userprofileFactory.setVIEWPROFILE(response.viewProfiles);

                    if (response.cafDetails.MemberId && response.cafDetails.groupleader && response.cafDetails.MemberId === response.cafDetails.groupleader) {
                        userprofileFactory.setGroupLeaderCaf(response.cafDetails);
                    }
                    deferred.resolve(result);


                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.saveCafAddress = function () {
            var cafdata = userprofileFactory.getMemberCAF();
            var deferred = $q.defer();
            var result = { isdata: false, CAFDATA: [] }
            var requestParams = {
                method: "POST",
                url: envService.read('apiSaveCafAddressUrl'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Commcity": cafdata.commcity,
                    "Commstate": cafdata.commstate,
                    "Commcountry": cafdata.commcountry,
                    "Commpin": cafdata.commpin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name,
                    "Relation": cafdata.nominee1relation,
                    "Nominee_Dob": cafdata.nominee1dob,
                    "Nom1_percent": cafdata.nominee1share,
                    "Nom2_name": cafdata.nominee2name,
                    "Nom2_relation": cafdata.nominee2relation,
                    "Nom2_dob": cafdata.nominee2dob,
                    "Nom2_percent": cafdata.nominee2share,
                    "Nom3_Name": cafdata.nominee3name,
                    "Nom3_Relation": cafdata.nominee3relation,
                    "Nom3_Dob": cafdata.nominee3dob,
                    "Nom3_Percent": cafdata.nominee3share,
                    "Place_of_Birth": cafdata.birthplace,
                    "Country_of_Birth": cafdata.birthcountry,
                    "Source_Wealth": cafdata.wealthsource,
                    "Pol_Exp_Person": cafdata.ispep,
                    "Country_Tax_Res1": cafdata.taxcountry1,
                    "CountryPayer_Id_no": cafdata.taxidnumber1,
                    "Idenif_Type": cafdata.taxidtype1,
                    "Country_Tax_Res2": cafdata.taxcountry2,
                    "CountryPayer_Id_no2": cafdata.taxidnumber2,
                    "Idenif_Type2": cafdata.taxidtype2,
                    "Country_Tax_Res3": cafdata.taxcountry3,
                    "CountryPayer_Id_no3": cafdata.taxidnumber3,
                    "Idenif_Type3": cafdata.taxidtype3,


                    "MemberId": cafdata.memberid,
                    "basicId": cafdata.basicid,
                    "groupleader": cafdata.groupleader,
                    "profilePic": cafdata.profilepic,
                    "aadhar": cafdata.aadhar,
                    "CAFStatus": cafdata.cafstatus,
                    "CAFBasicStatus": cafdata.cafbasicstatus,
                    "CAFAddressStatus": cafdata.cafaddressstatus,
                    "CAFBankStatus": cafdata.cafbankstatus,
                    "CAFNomineeStatus": cafdata.cafnomineestatus,
                    "CAFFatcaStatus": cafdata.caffatcastatus,

                    "Email": cafdata.email,
                    "Commaddress1": cafdata.corpaddress,
                    "sameAddress": cafdata.sameAddress
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = true;

                    userprofileFactory.setMemberCafFromDatabase(response.cafDetails);
                    userprofileFactory.setVIEWPROFILE(response.viewProfiles);

                    if (response.cafDetails.MemberId && response.cafDetails.groupleader && response.cafDetails.MemberId === response.cafDetails.groupleader) {
                        userprofileFactory.setGroupLeaderCaf(response.cafDetails);
                    }
                    deferred.resolve(result);


                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.saveCafBank = function () {
            var cafdata = userprofileFactory.getMemberCAF();
            var deferred = $q.defer();
            var result = { isdata: false, CAFDATA: [] }
            var requestParams = {
                method: "POST",
                url: envService.read('apiSaveCafBankUrl'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Commcity": cafdata.commcity,
                    "Commstate": cafdata.commstate,
                    "Commcountry": cafdata.commcountry,
                    "Commpin": cafdata.commpin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name,
                    "Relation": cafdata.nominee1relation,
                    "Nominee_Dob": cafdata.nominee1dob,
                    "Nom1_percent": cafdata.nominee1share,
                    "Nom2_name": cafdata.nominee2name,
                    "Nom2_relation": cafdata.nominee2relation,
                    "Nom2_dob": cafdata.nominee2dob,
                    "Nom2_percent": cafdata.nominee2share,
                    "Nom3_Name": cafdata.nominee3name,
                    "Nom3_Relation": cafdata.nominee3relation,
                    "Nom3_Dob": cafdata.nominee3dob,
                    "Nom3_Percent": cafdata.nominee3share,
                    "Place_of_Birth": cafdata.birthplace,
                    "Country_of_Birth": cafdata.birthcountry,
                    "Source_Wealth": cafdata.wealthsource,
                    "Pol_Exp_Person": cafdata.ispep,
                    "Country_Tax_Res1": cafdata.taxcountry1,
                    "CountryPayer_Id_no": cafdata.taxidnumber1,
                    "Idenif_Type": cafdata.taxidtype1,
                    "Country_Tax_Res2": cafdata.taxcountry2,
                    "CountryPayer_Id_no2": cafdata.taxidnumber2,
                    "Idenif_Type2": cafdata.taxidtype2,
                    "Country_Tax_Res3": cafdata.taxcountry3,
                    "CountryPayer_Id_no3": cafdata.taxidnumber3,
                    "Idenif_Type3": cafdata.taxidtype3,


                    "MemberId": cafdata.memberid,
                    "basicId": cafdata.basicid,
                    "groupleader": cafdata.groupleader,
                    "profilePic": cafdata.profilepic,
                    "aadhar": cafdata.aadhar,
                    "CAFStatus": cafdata.cafstatus,
                    "CAFBasicStatus": cafdata.cafbasicstatus,
                    "CAFAddressStatus": cafdata.cafaddressstatus,
                    "CAFBankStatus": cafdata.cafbankstatus,
                    "CAFNomineeStatus": cafdata.cafnomineestatus,
                    "CAFFatcaStatus": cafdata.caffatcastatus,

                    "Nom1Id": cafdata.Nom1Id,
                    "Nom2Id": cafdata.Nom2Id,
                    "Nom3Id": cafdata.Nom3Id,
                    "Email": cafdata.email,
                    "Commaddress1": cafdata.corpaddress,
                    "sameAddress": cafdata.sameAddress
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = true;

                    userprofileFactory.setMemberCafFromDatabase(response.cafDetails);
                    userprofileFactory.setVIEWPROFILE(response.viewProfiles);

                    if (response.cafDetails.MemberId && response.cafDetails.groupleader && response.cafDetails.MemberId === response.cafDetails.groupleader) {
                        userprofileFactory.setGroupLeaderCaf(response.cafDetails);
                    }
                    deferred.resolve(result);


                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.saveCafNominee = function () {

            var cafdata = userprofileFactory.getMemberCAF();
            var deferred = $q.defer();
            var result = { isdata: false, CAFDATA: [] }
            var requestParams = {
                method: "POST",
                url: envService.read('apiSaveCafNomineeUrl'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Commcity": cafdata.commcity,
                    "Commstate": cafdata.commstate,
                    "Commcountry": cafdata.commcountry,
                    "Commpin": cafdata.commpin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name,
                    "Relation": cafdata.nominee1relation,
                    "Nominee_Dob": cafdata.nominee1dob,
                    "Nom1_percent": cafdata.nominee1share,
                    "Nom2_name": cafdata.nominee2name,
                    "Nom2_relation": cafdata.nominee2relation,
                    "Nom2_dob": cafdata.nominee2dob,
                    "Nom2_percent": cafdata.nominee2share,
                    "Nom3_Name": cafdata.nominee3name,
                    "Nom3_Relation": cafdata.nominee3relation,
                    "Nom3_Dob": cafdata.nominee3dob,
                    "Nom3_Percent": cafdata.nominee3share,
                    "Place_of_Birth": cafdata.birthplace,
                    "Country_of_Birth": cafdata.birthcountry,
                    "Source_Wealth": cafdata.wealthsource,
                    "Pol_Exp_Person": cafdata.ispep,
                    "Country_Tax_Res1": cafdata.taxcountry1,
                    "CountryPayer_Id_no": cafdata.taxidnumber1,
                    "Idenif_Type": cafdata.taxidtype1,
                    "Country_Tax_Res2": cafdata.taxcountry2,
                    "CountryPayer_Id_no2": cafdata.taxidnumber2,
                    "Idenif_Type2": cafdata.taxidtype2,
                    "Country_Tax_Res3": cafdata.taxcountry3,
                    "CountryPayer_Id_no3": cafdata.taxidnumber3,
                    "Idenif_Type3": cafdata.taxidtype3,


                    "MemberId": cafdata.memberid,
                    "basicId": cafdata.basicid,
                    "groupleader": cafdata.groupleader,
                    "profilePic": cafdata.profilepic,
                    "aadhar": cafdata.aadhar,
                    "CAFStatus": cafdata.cafstatus,
                    "CAFBasicStatus": cafdata.cafbasicstatus,
                    "CAFAddressStatus": cafdata.cafaddressstatus,
                    "CAFBankStatus": cafdata.cafbankstatus,
                    "CAFNomineeStatus": cafdata.cafnomineestatus,
                    "CAFFatcaStatus": cafdata.caffatcastatus,

                    "Nom1Id": cafdata.Nom1Id,
                    "Nom2Id": cafdata.Nom2Id,
                    "Nom3Id": cafdata.Nom3Id,
                    "Email": cafdata.email,
                    "Commaddress1": cafdata.corpaddress,
                    "sameAddress": cafdata.sameAddress
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = true;

                    userprofileFactory.setMemberCafFromDatabase(response.cafDetails);
                    userprofileFactory.setVIEWPROFILE(response.viewProfiles);

                    if (response.cafDetails.MemberId && response.cafDetails.groupleader && response.cafDetails.MemberId === response.cafDetails.groupleader) {
                        userprofileFactory.setGroupLeaderCaf(response.cafDetails);
                    }
                    deferred.resolve(result);


                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.saveCafFatca = function () {

            var cafdata = userprofileFactory.getMemberCAF();
            var deferred = $q.defer();
            var result = { isdata: false, CAFDATA: [] }
            var requestParams = {
                method: "POST",
                url: envService.read('apiSaveCafFatcaUrl'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Commcity": cafdata.commcity,
                    "Commstate": cafdata.commstate,
                    "Commcountry": cafdata.commcountry,
                    "Commpin": cafdata.commpin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name,
                    "Relation": cafdata.nominee1relation,
                    "Nominee_Dob": cafdata.nominee1dob,
                    "Nom1_percent": cafdata.nominee1share,
                    "Nom2_name": cafdata.nominee2name,
                    "Nom2_relation": cafdata.nominee2relation,
                    "Nom2_dob": cafdata.nominee2dob,
                    "Nom2_percent": cafdata.nominee2share,
                    "Nom3_Name": cafdata.nominee3name,
                    "Nom3_Relation": cafdata.nominee3relation,
                    "Nom3_Dob": cafdata.nominee3dob,
                    "Nom3_Percent": cafdata.nominee3share,
                    "Place_of_Birth": cafdata.birthplace,
                    "Country_of_Birth": cafdata.birthcountry,
                    "Source_Wealth": cafdata.wealthsource,
                    "Pol_Exp_Person": cafdata.ispep,
                    "Country_Tax_Res1": cafdata.taxcountry1,
                    "CountryPayer_Id_no": cafdata.taxidnumber1,
                    "Idenif_Type": cafdata.taxidtype1,
                    "Country_Tax_Res2": cafdata.taxcountry2,
                    "CountryPayer_Id_no2": cafdata.taxidnumber2,
                    "Idenif_Type2": cafdata.taxidtype2,
                    "Country_Tax_Res3": cafdata.taxcountry3,
                    "CountryPayer_Id_no3": cafdata.taxidnumber3,
                    "Idenif_Type3": cafdata.taxidtype3,


                    "MemberId": cafdata.memberid,
                    "basicId": cafdata.basicid,
                    "groupleader": cafdata.groupleader,
                    "profilePic": cafdata.profilepic,
                    "aadhar": cafdata.aadhar,
                    "CAFStatus": cafdata.cafstatus,
                    "CAFBasicStatus": cafdata.cafbasicstatus,
                    "CAFAddressStatus": cafdata.cafaddressstatus,
                    "CAFBankStatus": cafdata.cafbankstatus,
                    "CAFNomineeStatus": cafdata.cafnomineestatus,
                    "CAFFatcaStatus": cafdata.caffatcastatus,

                    "Nom1Id": cafdata.Nom1Id,
                    "Nom2Id": cafdata.Nom2Id,
                    "Nom3Id": cafdata.Nom3Id,
                    "Email": cafdata.email,
                    "Commaddress1": cafdata.corpaddress,
                    "isTaxOther": cafdata.istaxother,
                    "sameAddress": cafdata.sameAddress
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = true;

                    userprofileFactory.setMemberCafFromDatabase(response.cafDetails);
                    userprofileFactory.setVIEWPROFILE(response.viewProfiles);

                    if (response.cafDetails.MemberId && response.cafDetails.groupleader && response.cafDetails.MemberId === response.cafDetails.groupleader) {
                        userprofileFactory.setGroupLeaderCaf(response.cafDetails);
                    }

                    deferred.resolve(result);


                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.saveAllCaf = function () {

            var cafdata = userprofileFactory.getMemberCAF();
            var deferred = $q.defer();
            var result = { msg: "" }
            var requestParams = {
                method: "POST",
                url: envService.read('apiSaveCafVersion2'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Commcity": cafdata.commcity,
                    "Commstate": cafdata.commstate,
                    "Commcountry": cafdata.commcountry,
                    "Commpin": cafdata.commpin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name,
                    "Relation": cafdata.nominee1relation,
                    "Nominee_Dob": cafdata.nominee1dob,
                    "Nom1_percent": cafdata.nominee1share,
                    "Nom2_name": cafdata.nominee2name,
                    "Nom2_relation": cafdata.nominee2relation,
                    "Nom2_dob": cafdata.nominee2dob,
                    "Nom2_percent": cafdata.nominee2share,
                    "Nom3_Name": cafdata.nominee3name,
                    "Nom3_Relation": cafdata.nominee3relation,
                    "Nom3_Dob": cafdata.nominee3dob,
                    "Nom3_Percent": cafdata.nominee3share,
                    "Place_of_Birth": cafdata.birthplace,
                    "Country_of_Birth": cafdata.birthcountry,
                    "Source_Wealth": cafdata.wealthsource,
                    "Pol_Exp_Person": cafdata.ispep,
                    "Country_Tax_Res1": cafdata.taxcountry1,
                    "CountryPayer_Id_no": cafdata.taxidnumber1,
                    "Idenif_Type": cafdata.taxidtype1,
                    "Country_Tax_Res2": cafdata.taxcountry2,
                    "CountryPayer_Id_no2": cafdata.taxidnumber2,
                    "Idenif_Type2": cafdata.taxidtype2,
                    "Country_Tax_Res3": cafdata.taxcountry3,
                    "CountryPayer_Id_no3": cafdata.taxidnumber3,
                    "Idenif_Type3": cafdata.taxidtype3,

                    "MemberId": cafdata.memberid,
                    "basicId": cafdata.basicid,
                    "groupleader": cafdata.groupleader,
                    "profilePic": cafdata.profilepic,
                    "aadhar": cafdata.aadhar,
                    "CAFStatus": cafdata.cafstatus,
                    "CAFBasicStatus": cafdata.cafbasicstatus,
                    "CAFAddressStatus": cafdata.cafaddressstatus,
                    "CAFBankStatus": cafdata.cafbankstatus,
                    "CAFNomineeStatus": cafdata.cafnomineestatus,
                    "CAFFatcaStatus": cafdata.caffatcastatus,

                    "Nom1Id": cafdata.Nom1Id,
                    "Nom2Id": cafdata.Nom2Id,
                    "Nom3Id": cafdata.Nom3Id,
                    "Email": cafdata.email,
                    "Commaddress1": cafdata.corpaddress,
                    "isTaxOther": cafdata.istaxother,
                    "sameAddress": cafdata.sameAddress

                }
            }
            $http(requestParams)
                .success(function (response) {


                    result.msg = response.msg;
                    var jsonObj = JSON.parse(response.data);

                    userprofileFactory.setMemberCafFromDatabase(jsonObj.cafDetails);
                    userprofileFactory.setVIEWPROFILE(jsonObj.viewProfiles);

                    if (jsonObj.cafDetails.MemberId && jsonObj.cafDetails.groupleader && jsonObj.cafDetails.MemberId === jsonObj.cafDetails.groupleader) {
                        userprofileFactory.setGroupLeaderCaf(jsonObj.cafDetails);
                    }
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.getCafFromDatabase = function (basicid) {
            var deferred = $q.defer();
            var result = { isdata: false }

            var requestParams = {
                method: "GET",
                url: envService.read('apiGetCafFromDatabase') + basicid
            }

            $http(requestParams)
               .success(function (response) {
                   if (response) {
                       result.isdata = true;
                       userprofileFactory.setMemberCafFromDatabase(response);
                       deferred.resolve(result);

                   }
               })
           .error(function (response) {
               deferred.resolve(result);
           });
            return deferred.promise;
        };

        this.getCafViewProfiles = function () {
            var deferred = $q.defer();
            var result = {
                isdata: false
            }

            var self = this;
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetCafViewProfilesUrl')
            }

            $http(requestParams)
               .success(function (response) {
                   if (response) {
                       result.isdata = true;


                       userprofileFactory.setVIEWPROFILE(response);


                   }
                   deferred.resolve(result);
               })
           .error(function (response) {

               deferred.resolve(result);
           });
            return deferred.promise;
        };

        this.getKarvyKycStatusWithDBSync = function (basicid, pan, usertyp) {
            var deferred = $q.defer();
            var result = {
                isdata: false
            }

            var self = this;
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetKarvyKycStatusWithDBSyncUrl') + basicid + "/" + pan
            }

            $http(requestParams)
               .success(function (response) {
                   if (response) {
                       result.isdata = true;

                       if (usertyp === "G") {
                           userprofileFactory.setKarvyKycStatusInViewProfile(basicid, response);
                           userprofileFactory.setKarvyKycStatusInGroupLeaderCAF(response);
                       }
                       else {
                           userprofileFactory.setKarvyKycStatusInViewProfile(basicid, response);
                           userprofileFactory.setKarvyKycStatusInMemberCAF(response);
                       }

                   }
                   deferred.resolve(result);
               })
           .error(function (response) {

               deferred.resolve(result);
           });
            return deferred.promise;
        };

        this.updateKycStatusToInvestAndDB = function (basicid) {
            var deferred = $q.defer();
            var result = { isdata: false }


            var requestParams = {
                method: "PUT",
                url: envService.read('apiUpdateKycStatusUrl') + basicid
            }

            $http(requestParams)
               .success(function (response) {

                   result.isdata = response;
                   deferred.resolve(result);
               })
           .error(function (response) {

               deferred.resolve(result);
           });
            return deferred.promise;
        };

        this.saveProfilePic = function (file, basicid) {

            var deferred = $q.defer();
            var result = { isdata: false }


            Upload.upload({
                url: envService.read('apiSaveProfilePicUrl') + basicid,
                data: { file: file }
            })
            .then(function (response) {
                result.isdata = true;
                deferred.resolve(result);
            }, function (err) {

                deferred.resolve(result);;
            });

            return deferred.promise;
        };

        this.getCountryList = function () {
            var deferred = $q.defer();
            var result = { isdata: false, countrylist: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetCountryListUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.countrylist.length = 0;

                        angular.forEach(response, function (value, key) {

                            result.countrylist.push({
                                name: value


                            });
                        });
                    }

                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        }

        this.getCityList = function (stateCode) {
            var deferred = $q.defer();
            var result = { isdata: false, citylist: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetCityListUrl') + stateCode
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.citylist.length = 0;

                        angular.forEach(response, function (value, key) {

                            result.citylist.push({
                                name: value.name,
                                value: value.code



                            });

                        });
                    }

                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        }

        this.getStateList = function () {
            var deferred = $q.defer();
            var result = { isdata: false, statelist: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetStateListUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.statelist.length = 0;

                        angular.forEach(response, function (value, key) {

                            result.statelist.push({
                                name: value.name,
                                value: value.code

                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        }

        this.getPincodeDetails = function (pincode) {
            var deferred = $q.defer();

            var result = {
                isdata: false,
                pindetails: {
                    city: null,
                    state: null
                }
            }

            var requestParams = {
                method: "GET",
                url: envService.read('apiGetPincodeDetailsUrl') + pincode
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.pindetails.city = response.city;
                        result.pindetails.state = response.state;

                    }

                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        }

        //#endregion NEW CODE--------------------
        this.setBankDetailsByIFSC = function (ifsc) {
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiBankDetailsByIFSCUrl') + ifsc
            }
            $http(requestParams)
                .success(function (response) {

                    userprofileFactory.setbankDetails(response);
                    deferred.resolve();
                })
                .error(function (response) {
                    deferred.resolve();
                });

            return deferred.promise;
        };

        this.setBankDetailsByMICR = function (micr) {
            var deferred = $q.defer();
            var requestParams = {
                method: "GET",
                url: envService.read('apiBankDetailsByMICRUrl') + micr
            }
            $http(requestParams)
                .success(function (response) {
                    userprofileFactory.setbankDetails(response);
                    deferred.resolve();
                })
                .error(function (response) {
                    deferred.resolve();
                });

            return deferred.promise;
        };

        this.registerUserCAF = function () {

            var cafdata = userprofileFactory.getuserCafDetails();
            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiUserCafRegistrationUrl'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name ? cafdata.nominee1name : "",
                    "Relation": cafdata.nominee1relation ? cafdata.nominee1relation : "",
                    "Nominee_Dob": cafdata.nominee1dob ? cafdata.nominee1dob : "",
                    "Nom1_percent": cafdata.nominee1share ? cafdata.nominee1share : "0",
                    "Nom2_name": cafdata.nominee2name ? cafdata.nominee2name : "",
                    "Nom2_relation": cafdata.nominee2relation ? cafdata.nominee2relation : "",
                    "Nom2_dob": cafdata.nominee2dob ? cafdata.nominee2dob : "",
                    "Nom2_percent": cafdata.nominee2share ? cafdata.nominee2share : "0",
                    "Nom3_Name": cafdata.nominee3name ? cafdata.nominee3name : "",
                    "Nom3_Relation": cafdata.nominee3relation ? cafdata.nominee3relation : "",
                    "Nom3_Dob": cafdata.nominee3dob ? cafdata.nominee3dob : "",
                    "Nom3_Percent": cafdata.nominee3share ? cafdata.nominee3share : "0",
                    "Place_of_Birth": cafdata.birthplace ? cafdata.birthplace : "",
                    "Country_of_Birth": cafdata.birthcountry ? cafdata.birthcountry : "",
                    "Source_Wealth": cafdata.wealthsource ? cafdata.wealthsource : "",
                    "Pol_Exp_Person": cafdata.ispep ? cafdata.ispep : "",
                    "Country_Tax_Res1": cafdata.taxcountry1 ? cafdata.taxcountry1 : "",
                    "CountryPayer_Id_no": cafdata.taxidnumber1 ? cafdata.taxidnumber1 : "",
                    "Idenif_Type": cafdata.taxidtype1 ? cafdata.taxidtype1 : "",
                    "Country_Tax_Res2": cafdata.taxcountry2 ? cafdata.taxcountry2 : "",
                    "CountryPayer_Id_no2": cafdata.taxidnumber2 ? cafdata.taxidnumber2 : "",
                    "Idenif_Type2": cafdata.taxidtype2 ? cafdata.taxidtype2 : "",
                    "Country_Tax_Res3": cafdata.taxcountry3 ? cafdata.taxcountry3 : "",
                    "CountryPayer_Id_no3": cafdata.taxidnumber3 ? cafdata.taxidnumber3 : "",
                    "Idenif_Type3": cafdata.taxidtype3 ? cafdata.taxidtype3 : ""
                }
            }

            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);


                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.registerMemberCAF = function () {

            var cafdata = userprofileFactory.getMemberCafDetails();
            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiAddAnotherUser'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name,
                    "Relation": cafdata.nominee1relation,
                    "Nominee_Dob": cafdata.nominee1dob,
                    "Nom1_percent": cafdata.nominee1share,
                    "Nom2_name": cafdata.nominee2name,
                    "Nom2_relation": cafdata.nominee2relation,
                    "Nom2_dob": cafdata.nominee2dob,
                    "Nom2_percent": cafdata.nominee2share,
                    "Nom3_Name": cafdata.nominee3name,
                    "Nom3_Relation": cafdata.nominee3relation,
                    "Nom3_Dob": cafdata.nominee3dob,
                    "Nom3_Percent": cafdata.nominee3share,
                    "Place_of_Birth": cafdata.birthplace,
                    "Country_of_Birth": cafdata.birthcountry,
                    "Source_Wealth": cafdata.wealthsource,
                    "Pol_Exp_Person": cafdata.ispep,
                    "Country_Tax_Res1": cafdata.taxcountry1,
                    "CountryPayer_Id_no": cafdata.taxidnumber1,
                    "Idenif_Type": cafdata.taxidtype1,
                    "Country_Tax_Res2": cafdata.taxcountry2,
                    "CountryPayer_Id_no2": cafdata.taxidnumber2,
                    "Idenif_Type2": cafdata.taxidtype2,
                    "Country_Tax_Res3": cafdata.taxcountry3,
                    "CountryPayer_Id_no3": cafdata.taxidnumber3,
                    "Idenif_Type3": cafdata.taxidtype3
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);


                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.updateMemberCAF = function () {

            var cafdata = userprofileFactory.getMemberCafDetails();
            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "PUT",
                url: envService.read('apiUserCafUpdateUrl'),
                data: {
                    "ClientName": cafdata.panname,
                    "Status": cafdata.category,
                    "Address1": cafdata.address,
                    "City": cafdata.city,
                    "State": cafdata.state,
                    "Country": cafdata.country,
                    "Pin": cafdata.pin,
                    "Mobile": cafdata.mobile,
                    "Occupation": cafdata.occupation,
                    "Date_of_Birth": cafdata.dob,
                    "Guardname": cafdata.gpanname,
                    "Guardpan": cafdata.gpannumber,
                    "Guardian_DOB": cafdata.gdob,
                    "CityName": cafdata.nricity,
                    "StateName": cafdata.nristate,
                    "CountryName": cafdata.nricountry,
                    "AnnIncome": cafdata.income,
                    "First_pan": cafdata.pannumber,
                    "kycStatus": cafdata.haskyc ? 'Y' : 'N',
                    "Bank_name": cafdata.bankname,
                    "Acc_Type": cafdata.accounttype,
                    "Acc_No": cafdata.accountnumber,
                    "IFSC": cafdata.ifsc,
                    "NameAsPerBank": cafdata.bankaccountname,
                    "Ecsno": cafdata.micr,
                    "Branch": cafdata.branchname,
                    "Bankcity": cafdata.branchcity,
                    "Bank_Address": cafdata.branchaddress,
                    "ReqNominee": cafdata.hasnominee,
                    "Nominee_Name": cafdata.nominee1name,
                    "Relation": cafdata.nominee1relation,
                    "Nominee_Dob": cafdata.nominee1dob,
                    "Nom1_percent": cafdata.nominee1share,
                    "Nom2_name": cafdata.nominee2name,
                    "Nom2_relation": cafdata.nominee2relation,
                    "Nom2_dob": cafdata.nominee2dob,
                    "Nom2_percent": cafdata.nominee2share,
                    "Nom3_Name": cafdata.nominee3name,
                    "Nom3_Relation": cafdata.nominee3relation,
                    "Nom3_Dob": cafdata.nominee3dob,
                    "Nom3_Percent": cafdata.nominee3share,
                    "Place_of_Birth": cafdata.birthplace,
                    "Country_of_Birth": cafdata.birthcountry,
                    "Source_Wealth": cafdata.wealthsource,
                    "Pol_Exp_Person": cafdata.ispep,
                    "Country_Tax_Res1": cafdata.taxcountry1,
                    "CountryPayer_Id_no": cafdata.taxidnumber1,
                    "Idenif_Type": cafdata.taxidtype1,
                    "Country_Tax_Res2": cafdata.taxcountry2,
                    "CountryPayer_Id_no2": cafdata.taxidnumber2,
                    "Idenif_Type2": cafdata.taxidtype2,
                    "Country_Tax_Res3": cafdata.taxcountry3,
                    "CountryPayer_Id_no3": cafdata.taxidnumber3,
                    "Idenif_Type3": cafdata.taxidtype3
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);


                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.fetchMemberCafDetails = function (memberid) {
            var deferred = $q.defer();
            var result = { isdata: false }
            var self = this;
            var requestParams = {
                method: "GET",
                url: envService.read('apiUserCafDetailsUrl') + memberid
            }

            $http(requestParams)
               .success(function (response) {
                   if (response) {
                       result.isdata = true;

                       self.checkmemberKYC(response[0].First_pan).then(function (resultRes) {

                           userprofileFactory.setMemberCafDetailsFromApi(response[0], resultRes.KYC_STATUS);
                           deferred.resolve(result);
                       })
                   }
               })
           .error(function (response) {
               deferred.resolve(result);
           });
            return deferred.promise;
        };

        this.fetchUserCafDetails = function (memberid) {
            var deferred = $q.defer();
            var result = { isdata: false }
            var self = this;
            var requestParams = {
                method: "GET",
                url: envService.read('apiUserCafDetailsUrl') + memberid
            }

            $http(requestParams)
               .success(function (response) {
                   if (response) {
                       result.isdata = true;

                       self.checkmemberKYC(response[0].First_pan).then(function (resultRes) {

                           userprofileFactory.setUserCafDetailsFromApi(response[0], resultRes.KYC_STATUS);
                           deferred.resolve(result);
                       })
                   }
               })
           .error(function (response) {
               deferred.resolve(result);
           });
            return deferred.promise;
        };

        this.createMandate = function (userinvestwelliid, profileid, bankfullname, accountno, ifsc, micr, fmonth, fyear, tmonth, tyear, limit, until) {
            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiCreateMandateUrl'),
                data: {
                    "UserId": userinvestwelliid,
                    "ProfileID": profileid,
                    "BankName": bankfullname,
                    "AccountNo": accountno,
                    "IFSC": ifsc,
                    "MICR": micr,
                    "FromMonth": fmonth,
                    "FromYear": fyear,
                    "ToMonth": tmonth,
                    "ToYear": tyear,
                    "PerDayLimit": limit,
                    "UntilCancel": until ? "Y" : "N"
                }

            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.createInvestProfile = function (firstApplicant, secondApplicant, thirdApplicant, groupLeader, firstPan, secondPan, thirdPan, moh) {
            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiCreateInvestProfileUrl'),
                data: {

                    "FirstApplicant": firstApplicant != "00" ? firstApplicant : "",
                    "SecondApplicant": secondApplicant != "00" ? secondApplicant : "",
                    "ThirdApplicant": thirdApplicant != "00" ? thirdApplicant : "",
                    "groupLeader": groupLeader,
                    "FirstPAN": firstApplicant != "00" ? firstPan : "",
                    "SecondPAN": secondApplicant != "00" ? secondPan : "",
                    "ThirdPAN": thirdApplicant != "00" ? thirdPan : "",
                    "MOH": moh

                }

            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.memberList = function () {
            var deferred = $q.defer();
            var result = { isdata: false, members: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiMemberListUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.members.length = 0;
                        result.members.push({
                            groupleader: "",
                            value: "0",
                            name: "Select Member",
                            memberpan: "",
                            memberdob: "",
                            memberemail: ""
                        });
                        angular.forEach(response, function (value, key) {
                            result.members.push({
                                groupleader: value.groupleader,
                                value: value.userid,
                                name: value.username,
                                memberpan: value.PAN,
                                memberdob: value.DOB,
                                memberemail: value.email
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.memberListForCart = function () {
            var deferred = $q.defer();
            var result = { isdata: false, members: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiMemberListUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.members.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.members.push({
                                groupleader: value.groupleader,
                                value: value.userid,
                                name: value.username,
                                memberpan: value.PAN,
                                memberdob: value.DOB,
                                memberemail: value.email,
                                isactive: value.active === "Y" ? true : false
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getMandateList = function (memberid, active) {

            var deferred = $q.defer();
            var result = { isdata: false, mandates: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiMemberMandateUrl') + memberid + "/" + active
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.mandates.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.mandates.push({
                                name: value.MandateID + "-" + value.Bank,
                                value: value.MandateID,
                                UMRN_NO: value.UMRN_NO,
                                ProfileID: value.ProfileID,
                                Bank: value.Bank,
                                Branch: value.Branch,
                                AccountNo: value.AccountNo,
                                IFSC: value.IFSC,
                                MICR: value.MICR,
                                FromMonth: value.FromMonth,
                                FromYear: value.FromYear,
                                ToMonth: value.ToMonth,
                                ToYear: value.ToYear,
                                PerDayLimit: value.PerDayLimit
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getMandateListV2 = function (memberid, profileid, active) {

            var deferred = $q.defer();
            var result = { isdata: false, mandates: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiMemberMandateV2Url') + memberid + "/" + profileid + "/" + active
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.mandates.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.mandates.push({
                                name: value.MandateID + "-" + value.Bank,
                                value: value.MandateID,
                                UMRN_NO: value.UMRN_NO,
                                ProfileID: value.ProfileID,
                                Bank: value.Bank,
                                Branch: value.Branch,
                                AccountNo: value.AccountNo,
                                IFSC: value.IFSC,
                                MICR: value.MICR,
                                FromMonth: value.FromMonth,
                                FromYear: value.FromYear,
                                ToMonth: value.ToMonth,
                                ToYear: value.ToYear,
                                PerDayLimit: value.PerDayLimit
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getDocumnetList = function () {
            var deferred = $q.defer();
            var result = { isdata: false, documents: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiFileListUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.documents.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.documents.push({
                                id: value.id,
                                docType: value.docType,
                                fileName: value.fileName,
                                filePath: value.filePath,
                                createDate: value.createDate,
                                memberId: value.memberId

                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.uploadFiles = function (file, doctype, docname) {


            var deferred = $q.defer();
            var result = { isdata: false }

            Upload.upload({
                url: envService.read('apiFileUploadUrl') + doctype + "/" + docname,
                data: { file: file }
            })
            .then(function (response) {
                result.isdata = true;
                deferred.resolve(result);
            }, function (err) {

                deferred.resolve(result);;
            });

            return deferred.promise;
        };

        this.removeFiles = function (id) {

            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiFileRemoveUrl'),
                data: { id: id }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = true;
                    deferred.resolve(result);

                })
                .error(function (response) {

                    deferred.resolve();
                });

            return deferred.promise;
        };

        this.getMemberList = function () {
            var deferred = $q.defer();
            var result = { isdata: false, members: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiMemberListUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.members.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.members.push({
                                groupleader: value.groupleader,
                                value: value.userid,
                                name: value.username,
                                memberpan: value.PAN,
                                memberdob: value.DOB,
                                memberemail: value.email,
                                status: value.active === "Y" ? "YES" : "NO"
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getMemberListV2 = function () {
            var deferred = $q.defer();
            var result = { isdata: false, members: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiMemberListV2Url')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.members.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.members.push({
                                basicId: value.basicId,
                                MemberId: value.MemberId,
                                username: value.username,
                                PAN: value.PAN,
                                DOB: value.DOB,
                                email: value.email,
                                CAFStatus: value.CAFStatus === "1" ? "Y" : "N"
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getDbSnapshot = function () {
            var deferred = $q.defer();
            var result = { isdata: false, dbSnapshots: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetDbSnapshotUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.dbSnapshots.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.dbSnapshots.push({
                                initialval: value.initialval ? parseInt(Math.round(value.initialval)) : 0,
                                currentval: value.currentval ? parseInt(Math.round(value.currentval)) : 0,
                                gain: value.gain ? parseInt(Math.round(value.gain)) : 0,
                                dividend: value.dividend ? parseInt(Math.round(value.dividend)) : 0,
                                absr: value.AbsR ? parseInt(Math.round(value.AbsR)) : 0,
                                wdays: value.wdays ? parseInt(Math.round(value.wdays)) : 0,
                                cagr: value.CAGR ? parseInt(Math.round(value.CAGR)) : 0,
                                onedaychange: value.onedaychange ? parseInt(Math.round(value.onedaychange)) : 0

                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getAllDbSnapshot = function () {
            var deferred = $q.defer();
            var result = { isdata: false, dbSnapshots: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetAllDbSnapshotUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.dbSnapshots.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.dbSnapshots.push({
                                membername: value.ApplicantName,
                                initialval: value.initialval ? parseInt(Math.round(value.initialval)) : 0,
                                currentval: value.currentval ? parseInt(Math.round(value.currentval)) : 0,
                                gain: value.gain ? parseInt(Math.round(value.gain)) : 0,
                                dividend: value.divval ? parseInt(Math.round(value.divval)) : 0,
                                absr: value.AbsR ? parseInt(Math.round(value.AbsR)) : 0,
                                wdays: value.wdays ? parseInt(Math.round(value.wdays)) : 0,
                                cagr: value.CAGR ? parseInt(Math.round(value.CAGR)) : 0
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getAllAssetAllocation = function () {
            var deferred = $q.defer();
            var result = { isdata: false, AssetAllocations: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetAllAssetAllocationUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.AssetAllocations.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.AssetAllocations.push({
                                EMV: value.EquityMV ? parseInt(Math.round(value.EquityMV)) : 0,
                                DMV: value.DebtMV ? parseInt(Math.round(value.DebtMV)) : 0,
                                GMV: value.GoldMV ? parseInt(Math.round(value.GoldMV)) : 0,
                                TMV: value.TotalMV ? parseInt(Math.round(value.TotalMV)) : 0,
                                EIA: value.EquityIA ? parseInt(Math.round(value.EquityIA)) : 0,
                                DIA: value.debtIA ? parseInt(Math.round(value.debtIA)) : 0,
                                GIA: value.GoldIA ? parseInt(Math.round(value.GoldIA)) : 0
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getPassbookTransactions = function (memberid) {
            var deferred = $q.defer();
            var result = { isdata: false, transactions: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiPassbookUrl') + memberid
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.transactions.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.transactions.push({
                                Date: value.Date,
                                Applicant: value.Applicant,
                                Scheme_Name: value.Scheme_Name ? value.Scheme_Name : "",
                                Amount: Math.abs(parseFloat(value.Amount)),
                                type: value.type,
                                Nature: value.Nature,
                                userid: value.userid ? value.userid : "",
                                fcode: value.Fcode,
                                scode: value.Scode
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getAllTransactions = function () {
            var deferred = $q.defer();
            var result = { isdata: false, transactions: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiAllTransactionsUrl')
            }
            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.transactions.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.transactions.push({
                                purchase: value.Purchase ? parseInt(Math.round(value.Purchase)) : 0,
                                switchin: value.SwitchIn ? parseInt(Math.round(value.SwitchIn)) : 0,
                                switchout: value.Switchout ? parseInt(Math.round(value.Switchout)) : 0,
                                sell: value.Sell ? parseInt(Math.round(value.Sell)) : 0,
                                dividend: value.DivAmt ? parseInt(Math.round(value.DivAmt)) : 0,
                                currentval: value.CurrentVal ? parseInt(Math.round(value.CurrentVal)) : 0,
                                xiir: value.XIIR ? parseInt(Math.round(value.XIIR)) : 0
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getInvestmentsByMemberid = function (memberid) {
            var deferred = $q.defer();
            var result = { isdata: false, investments: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiInvetmentsByMemberUrl') + memberid
            }
            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.investments.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.investments.push({
                                schemename: value.sname,
                                initial: value.initialval ? parseInt(Math.round(value.initialval)) : 0,
                                current: value.currentval ? parseInt(Math.round(value.currentval)) : 0,
                                gain: value.gain ? parseInt(Math.round(value.gain)) : 0,
                                cagr: value.CAGR ? parseInt(Math.round(value.CAGR)) : 0,
                                change: value.change ? parseInt(Math.round(value.change)) : 0,
                                foliono: value.foliono,
                                fcode: value.fund_code,
                                scode: value.schem_code,
                                unit: value.unit ? parseInt(Math.round(value.unit)) : 0,
                                holdingdays: value.Holdingdays ? parseInt(Math.round(value.Holdingdays)) : 0,
                                obj: value.obj,
                                goals: []
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getInvestProfileList = function () {

            var deferred = $q.defer();
            var result = { isdata: false, profileList: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiProfileListUrl')
            }

            $http(requestParams)
                .success(function (response) {

                    if (response) {
                        result.isdata = true;
                        result.profileList.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.profileList.push({
                                groupleader: value.groupleader,
                                value: value.userid,
                                profileId: value.ProfileId,
                                FirstApplicant: value.FirstApplicant ? value.FirstApplicant : "N/A",
                                SecondApplicant: value.SecondApplicant ? value.SecondApplicant : "N/A",
                                ThirdApplicant: value.ThirdApplicant ? value.ThirdApplicant : "N/A",
                                email: value.email,
                                FirstPAN: value.FirstPAN,
                                SecondPAN: value.SecondPAN,
                                ThirdPAN: value.ThirdPAN,
                                HoldingMode: value.HoldingMode ? value.HoldingMode : "N/A"

                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getMemberListForInvest = function () {
            var deferred = $q.defer();
            var result = { isdata: false, members: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiMemberListUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {

                        result.isdata = true;
                        result.members.length = 0;
                        result.members.push({ name: "SELECT", value: "00", groupLeader: "" });
                        angular.forEach(response, function (value, key) {

                            result.members.push({
                                name: value.PAN + "-" + value.username,
                                value: value.userid,
                                pan: value.PAN,
                                dob: value.DOB,
                                memberid: value.userid,
                                groupLeader: value.groupleader
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.memberBankList = function (memberid) {
            var deferred = $q.defer();
            var result = { isdata: false, banks: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiMemberBankListUrl') + memberid
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.banks.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.banks.push({
                                value: value.Bank_Name + " - " + value.Acc_no,
                                name: value.Bank_Name + " - " + value.Acc_no,
                                bankname: value.Bank_Name,
                                accountno: value.Acc_no,
                                micr: value.ecsno,
                                ifsc: value.ifsc,
                                bankcode: value.BankCode,
                                branch: value.Branch,
                                branchadd: value.bank_address,
                                branchcity: value.City,
                                nameinbank: value.NameAsPerBank
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.profileList = function (memberid) {
            var deferred = $q.defer();
            var result = { isdata: false, profiles: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiProfileUrl') + memberid
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.profiles.length = 0;
                        angular.forEach(response, function (value, key) {

                            var nm = value.FirstApplicant + " - Joint1 - ";

                            if (value.SecondApplicant) {
                                nm = nm + value.SecondApplicant + " - Joint2 - ";
                            }
                            else {
                                nm = nm + "N/A - Joint2 - ";
                            }

                            if (value.ThirdApplicant) {
                                nm = nm + value.ThirdApplicant;
                            }
                            else {
                                nm = nm + "N/A";
                            }

                            result.profiles.push({
                                name: nm,
                                value: value.ProfileId,
                                pan1: value.FirstPAN,
                                pan2: value.SecondPAN,
                                pan3: value.ThirdPAN,
                                pan1name: value.FirstApplicant,
                                pan2name: value.SecondApplicant,
                                pan3name: value.ThirdApplicant,
                                HoldingMode: value.HoldingMode
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.folioListAll = function (memberid, profileid, fundcode) {
            var deferred = $q.defer();
            var result = { isdata: false, folios: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiAllFolioListUrl') + memberid + "/" + profileid + "/" + fundcode
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.folios.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.folios.push({
                                name: value.FolioNo,
                                value: value.FolioNo === 'NEW' ? '-' : value.FolioNo,
                                fundcode: value.Fcode,
                                schemecode: value.Scode,
                                profileid: value.ProfileID,
                                pan1: value.FirstPAN,
                                pan2: value.SecondPAN,
                                pan3: value.ThirdPAN
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.folioListByFund = function (memberid, profileid, fundcode) {
            var deferred = $q.defer();
            var result = { isdata: false, folios: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiFundFolioListUrl') + memberid + "/" + profileid + "/" + fundcode
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.folios.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.folios.push({
                                name: value.FolioNo,
                                value: value.FolioNo === 'NEW' ? '-' : value.FolioNo,
                                fundcode: value.Fcode,
                                schemecode: value.Scode,
                                profileid: value.ProfileID,
                                pan1: value.FirstPAN,
                                pan2: value.SecondPAN,
                                pan3: value.ThirdPAN
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.folioListByScheme = function (memberid, profileid, schemecode) {
            var deferred = $q.defer();
            var result = { isdata: false, folios: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiSchemeFolioListUrl') + memberid + "/" + profileid + "/" + fundcode + "/" + schemecode
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.folios.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.folios.push({
                                name: value.FolioNo,
                                value: value.FolioNo === 'NEW' ? '-' : value.FolioNo,
                                fundcode: value.Fcode,
                                schemecode: value.Scode,
                                profileid: value.ProfileID,
                                pan1: value.FirstPAN,
                                pan2: value.SecondPAN,
                                pan3: value.ThirdPAN
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.serverDate = function () {
            var deferred = $q.defer();
            var result = { isdata: false, Date: "" };
            var requestParams = {
                method: "GET",
                url: envService.read('apiServerDateUrl')
            }
            $http(requestParams)
                .success(function (response) {

                    result.isdata = true;
                    result.Date = response.Date;
                    deferred.resolve(result);
                })
            .error(function (response) {

                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.addReferral = function (frmname, toname, toemail, tomobile, rcode) {

            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiAddReferralUrl'),
                data: {
                    "FROMNAME": frmname,
                    "TONAME": toname,
                    "TOEMAIL": toemail,
                    "TOMOBILE": tomobile,
                    "RCODE": rcode
                }
            }

            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.addSubscription = function (subsName, subsEmail, subsContact) {

            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiAddSubscriptionUrl'),
                data: {
                    "Name": subsName,
                    "Email": subsEmail,
                    "ContactNo": subsContact
                }
            }

            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.viewReferral = function () {
            var deferred = $q.defer();
            var result = { isdata: false, referrals: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiViewReferralUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.referrals.length = 0;
                        angular.forEach(response, function (value, key) {
                            result.referrals.push({
                                name: value.TONAME,
                                email: value.TOEMAIL,
                                mobile: value.TOMOBILE,
                                rcode: value.RCODE,
                                referredon: value.CREATEDDATETIME
                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.forgotPassword = function (email) {
            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "GET",
                url: envService.read('apiForgotPasswordUrl') + email
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.validateForgotPasswordRequest = function (ftoken) {
            var deferred = $q.defer();
            var result = { isdata: false, passwordRequest: [{ userid: "", status: "" }] }
            var requestParams = {
                method: "GET",
                url: envService.read('apivalidateForgotPasswordRequestUrl') + ftoken
            }
            $http(requestParams)
                .success(function (response) {


                    if (response) {
                        result.isdata = true;
                        result.passwordRequest[0].userid = response.userid;
                        result.passwordRequest[0].status = response.status;
                    }
                    deferred.resolve(result);
                })
                .error(function (response) {


                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.resetPassword = function (userid, password) {

            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "PUT",
                url: envService.read('apiResetPasswordUrl'),
                data: {
                    "userid": userid,
                    "password": password
                }

            }
            $http(requestParams)
                .success(function (response) {

                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {

                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.changePassword = function (oldpassword, password) {
            var deferred = $q.defer();
            var result = { isdata: false, status: 0 }
            var requestParams = {
                method: "PUT",
                url: envService.read('apiChangePasswordUrl'),
                data: {
                    "oldpassword": oldpassword,
                    "password": password
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = true;
                    result.status = parseInt(response)
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.getNonAadharMembers = function () {

            var deferred = $q.defer();
            var result = { isdata: false, members: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetNonAadharMembersUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {

                        result.isdata = true;
                        result.members.length = 0;

                        angular.forEach(response, function (value, key) {

                            result.members.push({
                                clientName: value.clientName,
                                memberId: value.memberId,
                                aadharNo: value.aadharNo,
                                panNumber: value.panNumber,
                                dob: value.dob,
                                mobile: value.mobile,
                                email: value.email,
                                aadharStatus: value.aadharStatus,
                                basicid: value.basicid,
                                cs: value.Cs,
                                ks: value.Ks,
                                fs: value.Fs,
                                ss: value.Ss,
                                cr: value.Cr,
                                kr: value.Kr,
                                fr: value.Fr,
                                sr: value.Sr

                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.getCheckPanNameExistCAF = function (pan, name) {
            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "GET",
                url: envService.read('apiGetCheckPanNameExistCAFUrl') + pan + "/" + name
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {

                        result.isdata = response;

                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.updateMemberAadharNo = function (name, memberId, aadharNo) {

            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiPostUpdateMemberAadharUrl'),
                data: {

                    "clientName": name,
                    "memberId": memberId,
                    "aadharNo": aadharNo
                }
            }

            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.updateUserCafToInitial = function (id) {

            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "PUT",
                url: envService.read('apiPutUpdateCAFToInitialUrl') + id,

            }

            $http(requestParams)
                .success(function (response) {

                    result.isdata = true;

                    userprofileFactory.setMemberCafFromDatabase(response.cafDetails);
                    userprofileFactory.setVIEWPROFILE(response.viewProfiles);

                    if (response.cafDetails.MemberId && response.cafDetails.groupleader && response.cafDetails.MemberId === response.cafDetails.groupleader) {
                        userprofileFactory.setGroupLeaderCaf(response.cafDetails);
                    }
                    deferred.resolve(result);



                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        };

        this.addAlert = function (type, msg) {
            $rootScope.alerts.push({ 'type': type, 'msg': msg });
        }

        this.closeAlert = function (index) {

            $rootScope.alerts.splice(index, 1);
        };

        this.faqQuestionsList = function () {

            var deferred = $q.defer();
            var result = { isdata: false, faqList: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiFaqQuestionListUrl')
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;
                        result.faqList.length = 0;

                        angular.forEach(response, function (value, key) {
                            result.faqList.push({
                                question: value.question,
                                id: value.id,
                                answer: value.answer

                            });
                        });
                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.AddUserfaqQuestion = function (question, email, name, mobile) {

            var deferred = $q.defer();
            var result = { isdata: false }
            var requestParams = {
                method: "POST",
                url: envService.read('apiAddUserFaqQuestionUrl'),
                data: {
                    "question": question,
                    "email": email,
                    "name": name,
                    "mobile": mobile

                }
            }
            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;

                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.updateFaqQuestionCounts = function (qids) {

            var deferred = $q.defer();
            var result = { isdata: false }

            var requestParams = {
                method: "PUT",
                url: envService.read('apiAddFaqQuestionCounts'),
                data: {
                    "Qids": qids
                }
            }
            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.isdata = true;

                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        };

        this.saveAddhaarLinkStatus = function (id, sid, rta) {

            var deferred = $q.defer();
            var result = { isdata: false }

            var requestParams = {
                method: "PUT",
                url: envService.read('apiUpdateAadhaarStatusUrl') + id + "/" + sid + "/" + rta
            }
            $http(requestParams)
                .success(function (response) {
                    result.isdata = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });




            return deferred.promise;
        };

        this.getNewGuid = function () {

            var deferred = $q.defer();
            var result = { data: "" }
            var requestParams = {
                method: "GET",
                url: envService.read('apiNewGuidUrl')
            }
            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.data = response;

                    }
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.decryptData = function (data) {

            var deferred = $q.defer();
            var result = { data: "" }
            var requestParams = {
                method: "GET",
                url: envService.read('apiDecryptDataUrl') + data
            }
            $http(requestParams)
                .success(function (response) {
                    if (response) {
                        result.data = response;

                    }
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.getTrackerGUID = function () {
            var result;
            var deferred = $q.defer();
            // GET GUID
            if (localStorage.getItem("BUUID_UTC") === null) {

                var requestParams = {
                    method: "GET",
                    url: envService.read('apiUserUniqueIdUrl')
                }
                $http(requestParams)
                    .success(function (response) {
                        if (response) {
                            localStorage.setItem('BUUID_UTC', response.ID);
                            result = response.ID;
                        }
                        deferred.resolve(result);
                    })
                    .error(function (response) {
                        deferred.resolve(result);
                    });


            }
            else {
                result = localStorage.getItem('BUUID_UTC');
                deferred.resolve(result);
            }

            return deferred.promise;
        }

        this.getTrackerUserConfig = function () {

            var result;
            var deferred = $q.defer();

            var module = {
                options: [],
                header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor, window.opera],
                dataos: [
                    { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
                    { name: 'Windows', value: 'Win', version: 'NT' },
                    { name: 'iPhone', value: 'iPhone', version: 'OS' },
                    { name: 'iPad', value: 'iPad', version: 'OS' },
                    { name: 'Kindle', value: 'Silk', version: 'Silk' },
                    { name: 'Android', value: 'Android', version: 'Android' },
                    { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
                    { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
                    { name: 'Macintosh', value: 'Mac', version: 'OS X' },
                    { name: 'Linux', value: 'Linux', version: 'rv' },
                    { name: 'Palm', value: 'Palm', version: 'PalmOS' }
                ],
                databrowser: [
                    { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
                    { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
                    { name: 'Safari', value: 'Safari', version: 'Version' },
                    { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
                    { name: 'Opera', value: 'Opera', version: 'Opera' },
                    { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
                    { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
                ],
                init: function () {
                    var agent = this.header.join(' '),
                        os = this.matchItem(agent, this.dataos),
                        browser = this.matchItem(agent, this.databrowser);

                    return { os: os, browser: browser };
                },
                matchItem: function (string, data) {
                    var i = 0,
                        j = 0,
                        html = '',
                        regex,
                        regexv,
                        match,
                        matches,
                        version;

                    for (i = 0; i < data.length; i += 1) {
                        regex = new RegExp(data[i].value, 'i');
                        match = regex.test(string);
                        if (match) {
                            regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
                            matches = string.match(regexv);
                            version = '';
                            if (matches) { if (matches[1]) { matches = matches[1]; } }
                            if (matches) {
                                matches = matches.split(/[._]+/);
                                for (j = 0; j < matches.length; j += 1) {
                                    if (j === 0) {
                                        version += matches[j] + '.';
                                    } else {
                                        version += matches[j];
                                    }
                                }
                            } else {
                                version = '0';
                            }
                            return {
                                name: data[i].name,
                                version: parseFloat(version)
                            };
                        }
                    }
                    return { name: 'unknown', version: 0 };
                }
            };

            var e = module.init(),
                debug = '';

            debug += 'os.name=' + e.os.name + '|';
            debug += 'os.version=' + e.os.version + '|';
            debug += 'browser.name=' + e.browser.name + '|';
            debug += 'browser.version=' + e.browser.version + '|';
            debug += 'navigator.userAgent=' + navigator.userAgent + '|';
            debug += 'navigator.appVersion=' + navigator.appVersion + '|';
            debug += 'navigator.platform=' + navigator.platform + '|';
            debug += 'navigator.vendor=' + navigator.vendor + '|';
            result = debug
            deferred.resolve(result);


            return deferred.promise;
        }

        this.eventTracker = function (usr, evt, ttype, dvtype) {

            var deferred = $q.defer();
            var result;
            var self = this;

            self.getTrackerUserConfig().then(function (resConfig) {
                self.getTrackerGUID().then(function (resGUID) {

                    var requestParams = {
                        method: "POST",
                        url: envService.read('apiEventTrackerUrl'),
                        data: {
                            trackCode: '',
                            user: usr,
                            device: resConfig,
                            ipAddress: '',
                            eventName: evt,
                            evtData: '',
                            trackType: ttype,
                            browserUUID: resGUID,
                            Actdevice: dvtype,
                            ActdeviceVersion: resConfig
                        }
                    }
                    $http(requestParams)
                        .success(function (response) {
                            result = response;
                            deferred.resolve(result);
                        })
                        .error(function (response) {
                            result = false;
                            deferred.resolve(result);
                        });

                });
            });

            return deferred.promise;
        }

        this.sendBankChangeRequest = function (ifsc, branchadd, bankname, branchname, branchcity, micr, accountnumber, accountname, bankAccType, BasicId, devicetype, reason, CChequeFile) {

            var deferred = $q.defer();
            var result = { msg: "" }
            var files = [];
            files.push(CChequeFile);

            var formData = new FormData();

            formData.append('ifsc', ifsc);
            formData.append('branchAddrs', branchadd);
            formData.append('bankName', bankname);
            formData.append('branchName', branchname);
            formData.append('branchCity', branchcity);
            formData.append('micr', micr);
            formData.append('accountNumber', accountnumber);
            formData.append('accountName', accountname);
            formData.append('bankAccType', bankAccType);
            formData.append('basicid', BasicId);
            formData.append('devicetype', devicetype);
            formData.append('reason', reason);
            // Attach file
            formData.append('chequeFile', files[0]);


            var requestParams = {
                method: "POST",
                url: envService.read('apiBankChangeRequestUrl'),
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.msg = response.msg;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

        this.sendAddressChangeRequest = function (address, city, state, pincode, country, devicetype, reason, basicid, panFile, aadhaarFile) {

            var deferred = $q.defer();
            var result = { msg: "" }
            var files = [];
            files.push(panFile);
            files.push(aadhaarFile);
            var formData = new FormData();

            formData.append('address', address);
            formData.append('city', city);
            formData.append('state', state);
            formData.append('pincode', pincode);
            formData.append('country', country);
            formData.append('basicid', basicid);
            formData.append('devicetype', devicetype);
            formData.append('reason', reason);
            // Attach file
            formData.append('panFile', files[0]);
            formData.append('aadhaarFile', files[1]);

            var requestParams = {
                method: "POST",
                url: envService.read('apiAddressChangeRequestUrl'),
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }
            $http(requestParams)
                .success(function (response) {
                    result.msg = response.msg;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;


        }

        this.getCafEditRequests = function (basicid, type) {
            var deferred = $q.defer();
            var result = { msg: "", requests: [] }
            var requestParams = {
                method: "GET",
                url: envService.read('apiCafEditChangeRequestListUrl') + basicid + "/" + type
            }

            $http(requestParams)
                .success(function (response) {
                    if (response) {

                        if (response.msg === "success") {
                            result.msg = response.msg;
                            result.requests.length = 0;
                            var objJson = angular.fromJson(response.data);

                            if (objJson) {
                                angular.forEach(objJson.cafeditRequests, function (value, key) {

                                    result.requests.push({
                                        cafRequestId: value.CafRequestId,
                                        loggedUserId: value.Logged_UserId,
                                        clientName: value.Client_Name,
                                        cafRequestType: value.CafRequestType,
                                        cafRequestStatus: value.CafRequestStatus,
                                        pan: value.Pan,
                                        aadhar: value.Aadhar,
                                        passport: value.Passport,
                                        hufBank: value.hufbank,
                                        hufPan: value.hufpan,
                                        cancelCheque: value.cancelcheque,
                                        itin: value.Itin

                                    });
                                });

                            }


                        } else {
                            result.msg = response.msg;

                        }

                    }
                    deferred.resolve(result);
                })
            .error(function (response) {
                deferred.resolve(result);
            });

            return deferred.promise;
        }

        this.ekycRegisterWindowEvt = function () {
            var deferred = $q.defer();
            $window.addEventListener('message', function (e) {

                if (e.data !== 'undefined') {
                    $rootScope.$broadcast('ekyc.windowmsg', e);
                }

            });
            return deferred.promise;
        }

        this.getCafReportToken = function (basicid) {
            var deferred = $q.defer();
            var result = { isdata: false, hash: "" }
            var requestParams = {
                method: "GET",
                url: envService.read('apiCafReportTokenUrl') + basicid

            }
            $http(requestParams)
                .success(function (response) {

                    result.isdata = true;
                    result.hash = response;
                    deferred.resolve(result);
                })
                .error(function (response) {
                    deferred.resolve(result);
                });

            return deferred.promise;
        }

    }]);
})(angular.module('fincartApp'));
