﻿(function (finApp) {
    'use strict';
    finApp.service('Calculator',['Finance', function (Finance) {

        //Calculate Current Amount
        this.houseCost = function (annualIncome, years) {
            var result = 0;
            var exact_income = annualIncome * (Math.pow((1.06), years));
            var emi_capacity = exact_income * (0.5 / 12);
            var loan_eligibility = (emi_capacity / 900) * 100000;
            result = Math.round(loan_eligibility / 0.75);//0.85 replaced with 0.75 because we are changing down payment from 15% to 25%
            return result;
        };

        this.travelCost = function (location, type) {
            
            var result = 0;
            if (type === "001") {//budget
                switch (location) {
                    case '001': result = 200000;//usa
                        break;
                    case '002': result = 150000;//australia
                        break;
                    case '003': result = 110000;//europe
                        break;
                    case '004': result = 40000;//asia
                        break;
                    case '005': result = 20000;//india
                        break;
                }
            }
            else if (type === "002") {//comfy
                switch (location) {
                    case '001': result = 250000;//usa
                        break;
                    case '002': result = 200000;//australia
                        break;
                    case '003': result = 160000;//europe
                        break;
                    case '004': result = 60000;//asia
                        break;
                    case '005': result = 30000;//india
                        break;
                }
            }
            else {//luxury
                switch (location) {
                    case '001': result = 350000;//usa
                        break;
                    case '002': result = 275000;//australia
                        break;
                    case '003': result = 250000;//europe
                        break;
                    case '004': result = 80000;//asia
                        break;
                    case '005': result = 40000;//india
                        break;
                }
            }
            return result;
        };

        this.carCost = function (type) {
            
            var result = 0;
            switch (type) {
                case '001': result = 350000;//small
                    break;
                case '002': result = 600000;//hatchback
                    break;
                case '003': result = 1000000;//sedan
                    break;
                case '004': result = 1500000;//suv
                    break;
                case '005': result = 3500000;//luxury
                    break;
            }
            return result;
        };

        this.bikeCost = function (type) {
            var result = 0;
            switch (type) {
                case '250': result = 150000;//250cc
                    break;
                case '500': result = 200000;//500cc
                    break;
                case '800': result = 500000;//800cc
                    break;
                case '1000': result = 1000000;//1000cc
                    break;
                case '1200': result = 1500000;//1200cc
                    break;
            }
            return result;
        };

        this.studyCost = function (location, fundtype, gradtype) {
            
            var result = 0;
            switch (location) {
                case '001'://usa
                    if (fundtype === "private") {
                        if (gradtype === "under_grad") {
                            result = 12000000;
                        }
                        else {
                            //post_grad
                            result = 9000000;
                        }
                    }
                    else {
                        if (gradtype === "under_grad") {
                            result = 4800000;
                        }
                        else {
                            //post_grad
                            result = 3600000;
                        }
                    }
                    break;
                case '002'://europe
                    if (fundtype === "private") {
                        if (gradtype === "under_grad") {
                            result = 4000000;
                        }
                        else {
                            //post_grad
                            result = 2500000;
                        }
                    }
                    else {
                        if (gradtype === "under_grad") {
                            result = 2000000;
                        }
                        else {
                            //post_grad
                            result = 1250000;
                        }
                    }
                    break;
                case '003'://aisa
                    if (fundtype === "private") {
                        if (gradtype === "under_grad") {
                            result = 7000000;
                        }
                        else {
                            //post_grad
                            result = 4200000;
                        }
                    }
                    else {
                        if (gradtype === "under_grad") {
                            result = 3500000;
                        }
                        else {
                            //post_grad
                            result = 2100000;
                        }
                    }
                    break;
                case '004'://india
                    if (fundtype === "private") {
                        if (gradtype === "under_grad") {
                            result = 1500000;
                        }
                        else {
                            //post_grad
                            result = 2000000;
                        }
                    }
                    else {
                        if (gradtype === "under_grad") {
                            result = 750000;
                        }
                        else {
                            //post_grad
                            result = 1000000;
                        }
                    }
                    break;
            }
            return result;
        };

        this.weddingCost = function (type) {
            var result = 0;
            switch (type) {
                case '001': result = 500000;//simple
                    break;
                case '002': result = 1500000;//moderate
                    break;
                case '003': result = 3000000;//lavish
                    break;
            }
            return result;
        };

        this.wealthCost = function (type) {
            var result = 0;
            switch (type) {
                case '001': result = 500000;//simple
                    break;
                case '002': result = 1500000;//moderate
                    break;
                case '003': result = 3000000;//awesome
                    break;
            }
            return result;
        };

        this.familyplanningCost = function (type) {
            var result = 0;
            switch (type) {
                case '001': result = 500000;//budget
                    break;
                case '002': result = 1000000;//comfy
                    break;
                case '003': result = 2000000;//luxury
                    break;
            }
            return result;
        };

        this.childstudyCost = function (location, fundtype, gradtype) {
            var result = 0;
            switch (location) {
                case '001'://usa
                    if (fundtype === "private") {
                        if (gradtype === "under_grad") {
                            result = 12000000;
                        }
                        else {
                            //post_grad
                            result = 9000000;
                        }
                    }
                    else {
                        if (gradtype === "under_grad") {
                            result = 4800000;
                        }
                        else {
                            //post_grad
                            result = 3600000;
                        }
                    }
                    break;
                case '002'://europe
                    if (fundtype === "private") {
                        if (gradtype === "under_grad") {
                            result = 4000000;
                        }
                        else {
                            //post_grad
                            result = 2500000;
                        }
                    }
                    else {
                        if (gradtype === "under_grad") {
                            result = 2000000;
                        }
                        else {
                            //post_grad
                            result = 1250000;
                        }
                    }
                    break;
                case '003'://aisa
                    if (fundtype === "private") {
                        if (gradtype === "under_grad") {
                            result = 7000000;
                        }
                        else {
                            //post_grad
                            result = 4200000;
                        }
                    }
                    else {
                        if (gradtype === "under_grad") {
                            result = 3500000;
                        }
                        else {
                            //post_grad
                            result = 2100000;
                        }
                    }
                    break;
                case '004'://india
                    if (fundtype === "private") {
                        if (gradtype === "under_grad") {
                            result = 1500000;
                        }
                        else {
                            //post_grad
                            result = 2000000;
                        }
                    }
                    else {
                        if (gradtype === "under_grad") {
                            result = 750000;
                        }
                        else {
                            //post_grad
                            result = 1000000;
                        }
                    }
                    break;
            }
            return result;
        };

        this.childweddingCost = function (type, annualIncome, gender) {
            var result = 0;
            switch (type) {
                case '001'://simple
                    if (gender === "001") {
                        if (annualIncome < 1000000) {
                            result = 800000;
                        }
                        else if (annualIncome < 2000000) {
                            result = 1000000;
                        }
                        else if (annualIncome < 3500000) {
                            result = 1500000;
                        }
                        else if (annualIncome < 5000000) {
                            result = 2000000;
                        }
                        else {
                            result = 2500000;
                        }
                    }
                    else {
                        if (annualIncome < 1000000) {
                            result = 1100000;
                        }
                        else if (annualIncome < 2000000) {
                            result = 1500000;
                        }
                        else if (annualIncome < 3500000) {
                            result = 1500000;
                        }
                        else if (annualIncome < 5000000) {
                            result = 2300000;
                        }
                        else {
                            result = 3000000;
                        }
                    }
                    break;
                case '002'://moderate
                    if (gender === "001") {
                        if (annualIncome < 1000000) {
                            result = 1100000;
                        }
                        else if (annualIncome < 2000000) {
                            result = 1500000;
                        }
                        else if (annualIncome < 3500000) {
                            result = 2300000;
                        }
                        else if (annualIncome < 5000000) {
                            result = 3000000;
                        }
                        else {
                            result = 3800000;
                        }
                    }
                    else {
                        if (annualIncome < 1000000) {
                            result = 1700000;
                        }
                        else if (annualIncome < 2000000) {
                            result = 2300000;
                        }
                        else if (annualIncome < 3500000) {
                            result = 2300000;
                        }
                        else if (annualIncome < 5000000) {
                            result = 3400000;
                        }
                        else {
                            result = 4500000;
                        }
                    }
                    break;
                case '003'://lavish
                    if (gender === "001") {
                        if (annualIncome < 1000000) {
                            result = 1500000;
                        }
                        else if (annualIncome < 2000000) {
                            result = 2000000;
                        }
                        else if (annualIncome < 3500000) {
                            result = 3000000;
                        }
                        else if (annualIncome < 5000000) {
                            result = 4000000;
                        }
                        else {
                            result = 5000000;
                        }
                    }
                    else {
                        if (annualIncome < 1000000) {
                            result = 2300000;
                        }
                        else if (annualIncome < 2000000) {
                            result = 3000000;
                        }
                        else if (annualIncome < 3500000) {
                            result = 3000000;
                        }
                        else if (annualIncome < 5000000) {
                            result = 4500000;
                        }
                        else {
                            result = 6000000;
                        }
                    }
                    break;
            }
            return result;
        };


        //Calculate GET & INVEST Amount
        this.emergencyFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, expenses, emi, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 0 / 100;

            if (type == 'R') {

                var fvlrate = 0 / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {

                var corpus_goal_today = (expenses + emi) * 6;

                var corpus_goal_start = corpus_goal_today;

                if (gLumpsum > 0) {
                    corpus_goal_start = gLumpsum;
                }

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = 18;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = 0 / 100;
                var pvnper = 18;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.termInsuranceFund = function (annualIncome, gLumpsum, gSip, iLumpsum, iSip, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }
            var times = 10;

            result.getLumpsum = annualIncome * times;
            result.getSip = 0;
            result.investLumpsum = 0;
            result.investSip = 0;

            return result;
        };

        this.healthInsuranceFund = function (annualIncome, gLumpsum, gSip, iLumpsum, iSip, childCount, maritialstatus, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            //result.getLumpsum = annualIncome * times;
            result.getSip = 0;
            result.investLumpsum = 0;
            result.investSip = 0;

            if (maritialstatus === "001") {
                if (annualIncome < 500000) {
                    result.getLumpsum = 500000;
                }
                else if (annualIncome < 1000000) {
                    result.getLumpsum = 500000;
                }
                else if (annualIncome < 2000000) {
                    result.getLumpsum = 1000000;
                }
                else if (annualIncome < 5000000) {
                    result.getLumpsum = 1500000;
                }
                else {
                    result.getLumpsum = 2000000;
                }
            }
            else if (maritialstatus === "002") {
                if (annualIncome < 500000) {
                    result.getLumpsum = 800000;
                }
                else if (annualIncome < 1000000) {
                    result.getLumpsum = 800000;
                }
                else if (annualIncome < 2000000) {
                    result.getLumpsum = 1500000;
                }
                else if (annualIncome < 5000000) {
                    result.getLumpsum = 2300000;
                }
                else {
                    result.getLumpsum = 3000000;
                }
            }
            else if (maritialstatus === "003") {
                if (childCount === 1) {
                    if (annualIncome < 500000) {
                        result.getLumpsum = 900000;
                    }
                    else if (annualIncome < 1000000) {
                        result.getLumpsum = 900000;
                    }
                    else if (annualIncome < 2000000) {
                        result.getLumpsum = 1800000;
                    }
                    else if (annualIncome < 5000000) {
                        result.getLumpsum = 2600000;
                    }
                    else {
                        result.getLumpsum = 3500000;
                    }
                }
                else if (childCount === 2) {
                    if (annualIncome < 500000) {
                        result.getLumpsum = 1000000;
                    }
                    else if (annualIncome < 1000000) {
                        result.getLumpsum = 1000000;
                    }
                    else if (annualIncome < 2000000) {
                        result.getLumpsum = 2000000;
                    }
                    else if (annualIncome < 5000000) {
                        result.getLumpsum = 3000000;
                    }
                    else {
                        result.getLumpsum = 4000000;
                    }
                }
                else if (childCount === 3) {
                    if (annualIncome < 500000) {
                        result.getLumpsum = 1000000;
                    }
                    else if (annualIncome < 1000000) {
                        result.getLumpsum = 1000000;
                    }
                    else if (annualIncome < 2000000) {
                        result.getLumpsum = 2000000;
                    }
                    else if (annualIncome < 5000000) {
                        result.getLumpsum = 3000000;
                    }
                    else {
                        result.getLumpsum = 4000000;
                    }
                }
            }

            return result;
        };

        this.studyFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, type) {
            
            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 10 / 100;

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype)));

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.carFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, type) {
            
            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype)));

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.bikeFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype)));

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.houseFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, type) {
            
            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;
            var downpayment_rate = 25 / 100;//we are changing down payment from 15% to 25%

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = gLumpsum / downpayment_rate;

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * downpayment_rate;

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.wealthFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 0 / 100;

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = gLumpsum;

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.otherFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype)));

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.travelFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, people, type) {
            
            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype))) * people;

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount * people;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.weddingFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;

            if (type === 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype)));

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.childstudyFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, age, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var years = 18 - age;
            var inflation = 10 / 100;

            if (type === 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype)));

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.childweddingFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, age, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var years = 24 - age;
            var inflation = 6 / 100;

            if (type === 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype)));

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.familyplanningFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 10 / 100;

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var corpus_goal_today = Math.abs(Math.round(Finance.PV(inflation, years, 0, -gLumpsum, pvtype)));

                if (currentAmount > 0) {
                    corpus_goal_today = currentAmount;
                }

                var corpus_goal_start = corpus_goal_today * (Math.pow((1 + inflation), years));

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.businessFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, years, startupCost, monthlyExpence, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {
                var monthly_pvrate = 0.6821 / 100;
                var monthly_pvnper = 18;
                var monthly_pvpmt = -(monthlyExpence * (Math.pow((1 + inflation), years)));
                var monthly_pvfv = 0;
                var monthly_pvtype = 1

                var monthly_corpus_goal_start = Math.abs(Math.round(Finance.PV(monthly_pvrate, monthly_pvnper, monthly_pvpmt, monthly_pvfv, monthly_pvtype)));

                var corpus_goal_start = startupCost + monthly_corpus_goal_start;

                if (gLumpsum > 0) {
                    corpus_goal_start = gLumpsum;
                }

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.retirementFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, currentAge, retirementAge, lifeExpectency, monthlyExpence, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;
            var years = retirementAge - currentAge;
            var post_retirement_years = lifeExpectency - retirementAge;
            var monthlyExpence_At_Retitement = monthlyExpence * (Math.pow((1 + inflation), years));

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {

                var today_pvrate = 0.116 / 100;
                var today_pvnper = post_retirement_years * 12;
                var today_pvpmt = -monthlyExpence_At_Retitement;
                var today_pvfv = 0;
                var today_pvtype = 1

                var corpus_goal_start = Math.abs(Math.round(Finance.PV(today_pvrate, today_pvnper, today_pvpmt, today_pvfv, today_pvtype)));

                if (gLumpsum > 0) {
                    corpus_goal_start = gLumpsum;
                }

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        this.sabbaticalFund = function (currentAmount, gLumpsum, gSip, iLumpsum, iSip, startTime, endTime, monthlyExpence, type) {

            var result = { getLumpsum: gLumpsum, getSip: gSip, investLumpsum: iLumpsum, investSip: iSip }

            var inflation = 6 / 100;
            var years = startTime;
            var monthlyExpence_At_Sabbatical = monthlyExpence * (Math.pow((1 + inflation), years));

            if (type == 'R') {

                var fvlrate = Finance.ROR(years, "") / 100;
                var fvlnper = years;
                var fvlpmt = 0;
                var fvlpv = -iLumpsum;
                var fvltype = 1

                var fvsrate = Finance.ROR(years, "M") / 100;
                var fvsnper = years * 12;
                var fvspmt = -iSip;
                var fvspv = 0;
                var fvstype = 1

                result.getLumpsum = Math.abs(Math.round(Finance.FV(fvlrate, fvlnper, fvlpmt, fvlpv, fvltype)));
                result.getSip = Math.abs(Math.round(Finance.FV(fvsrate, fvsnper, fvspmt, fvspv, fvstype)));
            }
            else {

                var today_pvrate = 0.116 / 100;
                var today_pvnper = endTime * 12;
                var today_pvpmt = -monthlyExpence_At_Sabbatical;
                var today_pvfv = 0;
                var today_pvtype = 1

                var corpus_goal_start = Math.abs(Math.round(Finance.PV(today_pvrate, today_pvnper, today_pvpmt, today_pvfv, today_pvtype)));

                if (gLumpsum > 0) {
                    corpus_goal_start = gLumpsum;
                }

                var pmrate = Finance.ROR(years, "M") / 100;
                var pmnper = years * 12;
                var pmpv = 0;
                var pmfv = -corpus_goal_start;
                var pmtype = 1

                var pvrate = Finance.ROR(years, "") / 100;
                var pvnper = years;
                var pvpmt = 0;
                var pvfv = -corpus_goal_start;
                var pvtype = 1

                result.investLumpsum = Math.abs(Math.round(Finance.PV(pvrate, pvnper, pvpmt, pvfv, pvtype)));
                result.investSip = Math.abs(Math.round(Finance.PMT(pmrate, pmnper, pmpv, pmfv, pmtype)));
            }

            return result;
        };

        //Calculate Range Slider Value

        this.carSliderRange = function (cartype, investmenttype) {
            var result = { start: 200000, slide: 50000, end: 600000 };
            if (investmenttype === "S") {
                result.start = 500; result.slide = 500; result.end = 100000;
            }
            else {
                switch (cartype) {
                    case '001': result.start = 200000; result.slide = 50000; result.end = 600000;//small
                        break;
                    case '002': result.start = 500000; result.slide = 100000; result.end = 1500000;//hatchback
                        break;
                    case '003': result.start = 600000; result.slide = 100000; result.end = 2000000;//sedan
                        break;
                    case '004': result.start = 1000000; result.slide = 250000; result.end = 3500000;//suv
                        break;
                    case '005': result.start = 3500000; result.slide = 500000; result.end = 9500000;//luxury
                        break;
                }
            }
            return result;
        };

        this.standardSliderRange = function (investmenttype,purchaseType) {
            var result = { start: 500, slide: 500, end: 1000000 };
            if (investmenttype === "L" && purchaseType === "FRESH") {
                result.start = 5000; result.slide = 500; result.end = 1000000;
            }
            else if(investmenttype === "L" && purchaseType === "ADDITIONAL") {
                result.start = 1000; result.slide = 500; result.end = 1000000;
            }
            else if(investmenttype === "S" && purchaseType === "FRESH") {
                result.start = 5000; result.slide = 500; result.end = 1000000;
            }
            else if (investmenttype === "S" && purchaseType === "ADDITIONAL") {
                result.start = 500; result.slide = 500; result.end = 1000000;
            }
            else
            {
                result.start = 500; result.slide = 500; result.end = 1000000;
            }
            return result;
        };
    }]);
})(angular.module('fincartApp'));
