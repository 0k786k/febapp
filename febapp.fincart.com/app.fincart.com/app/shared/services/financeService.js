﻿(function (finApp) {
    'use strict';
    finApp.service('Finance', function () {

        this.PV = function (rate, periods, payment, future, type) {
            // Initialize type
            var type = (typeof type === 'undefined') ? 0 : type;

            // Evaluate rate and periods (TODO: replace with secure expression evaluator)
            rate = eval(rate);
            periods = eval(periods);

            // Return present value
            if (rate === 0) {
                return -payment * periods - future;
            } else {
                return (((1 - Math.pow(1 + rate, periods)) / rate) * payment * (1 + rate * type) - future) / Math.pow(1 + rate, periods);
            }
        };

        this.FV = function (rate, nper, pmt, pv, type) {
            if (!type) type = 0;

            var pow = Math.pow(1 + rate, nper);
            var fv = 0;

            if (rate) {
                fv = (pmt * (1 + rate * type) * (1 - pow) / rate) - pv * pow;
            } else {
                fv = -1 * (pv + pmt * nper);
            }

            return fv;
        };

        this.PMT = function (rate, nper, pv, fv, type) {
            if (!fv) fv = 0;
            if (!type) type = 0;

            if (rate == 0) return -(pv + fv) / nper;

            var pvif = Math.pow(1 + rate, nper);
            var pmt = rate / (pvif - 1) * -(pv * pvif + fv);

            if (type == 1) {
                pmt /= (1 + rate);
            };

            return pmt;
        };

        this.ROR = function (time,type) {

            var ror = 6;

            if (type === "M") {//for monthly
                if (time <= 1) {
                    ror = 0.4868
                }
                else if (time <= 3) {
                    ror = 0.5262
                }
                else if (time <= 4) {
                    ror = 0.6821
                }
                else if (time <= 7) {
                    ror = 0.7974
                }
                else {
                    ror = 0.9489
                }
            }
            else {
                if (time <= 1) {
                    ror = 6
                }
                else if (time<=3) {
                    ror = 6.5
                }
                else if (time <= 4) {
                    ror = 8.5
                }
                else if (time <= 7) {
                    ror = 10
                }
                else {
                    ror = 12
                }
            }

            return ror;
        };

    });
})(angular.module('fincartApp'));
