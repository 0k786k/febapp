﻿(function (finApp) {
    'use strict';
    finApp.service('oauthService', ['$http', '$q', '$interval', 'oauthConstants', 'oauthTokenFactory', 'transactionFactory', 'envService', function ($http, $q, $interval, oauthConstants, oauthTokenFactory, transactionFactory, envService) {

        this.login = function (username, password) {
            var that = this;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiUserLoginUrl'),
                data: "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password) + "&grant_type=" + oauthConstants.oauth['grant_type_password'] + "&client_id=" + oauthConstants.oauth['client_id'] + "&client_secret=" + oauthConstants.oauth['client_secret'],
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }

            $http(requestParams)
                .success(function (response) {
                    if (response.error) {
                        oauthTokenFactory.setisloggedin(false);
                    }
                    else {
                        oauthTokenFactory.setisloggedin(true);
                    }
                    oauthTokenFactory.setUserToken(response);
                    transactionFactory.destroyCurrentBunchTxnId();
                    that.startTokenRefreshTimer(oauthTokenFactory.getUserToken().expires_in);
                    deferred.resolve();
                })
                .error(function (response) {
                    oauthTokenFactory.setUserToken(response);
                    oauthTokenFactory.setisloggedin(false);
                    transactionFactory.destroyCurrentBunchTxnId();
                    deferred.resolve();
                });

            return deferred.promise;
        };

        this.refreshUser = function (action) {
            var that = this;
            var deferred = $q.defer();
            var requestParams = {
                method: "POST",
                url: envService.read('apiUserLoginUrl'),
                data: "refresh_token=" + oauthTokenFactory.getUserToken().refresh_token + "&grant_type=" + oauthConstants.oauth['grant_type_refresh_token'] + "&client_id=" + oauthConstants.oauth['client_id'] + "&client_secret=" + oauthConstants.oauth['client_secret'],
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }

            $http(requestParams)
                .success(function (response) {
                    oauthTokenFactory.setUserToken(response);
                    if (action) {
                        that.startTokenRefreshTimer(oauthTokenFactory.getUserToken().expires_in);
                    }
                    oauthTokenFactory.setisloggedin(true);
                    deferred.resolve();
                })
                .error(function (response) {
                    oauthTokenFactory.setUserToken(response);
                    oauthTokenFactory.setisloggedin(false);
                    deferred.resolve();
                });

            return deferred.promise;
        }

        this.logout = function () {
            var deferred = $q.defer();
            // remove user from local storage and clear http auth header
            oauthTokenFactory.destroyUserToken();
            oauthTokenFactory.setisloggedin(false);
            deferred.resolve();
            return deferred.promise;
        }

        this.startTokenRefreshTimer = function (timeoutDuration) {
            var that = this;

            var createRefreshTimer = function (timerValue) {
                if (oauthTokenFactory.getisloggedin() && timerValue) {

                    var timer = $interval(function () {
                        that.startTokenRefreshTimer.apply(that);
                        $interval.cancel(timer);
                    }, (timerValue - 60) * 1000);

                }
            };

            if (timeoutDuration) {
                createRefreshTimer(timeoutDuration);
            }
            else {
                that.refreshUser(false).then(function () {
                    if (oauthTokenFactory.getisloggedin()) {
                        createRefreshTimer(oauthTokenFactory.getUserToken().expires_in);
                    }
                });
            }
        }
    }]);
})(angular.module('fincartApp'));
