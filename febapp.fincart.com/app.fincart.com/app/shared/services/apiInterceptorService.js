﻿(function (finApp) {
    'use strict';
    finApp.service('apiInterceptorService', ['$rootScope', '$localStorage', function ($rootScope, $localStorage) {

        var service = this;
        service.request = function (config) {
            if ($localStorage.currentToken) {
                config.headers.Authorization = 'Bearer ' + $localStorage.currentToken.access_token;
                $rootScope.$broadcast('authorized');
            }
            else {
                $rootScope.$broadcast('unauthorized');
            }
            return config;
        };
        service.responseError = function (response) {
            if (response.status === 401) {
                delete $localStorage.currentToken;
                $rootScope.$broadcast('unauthorized');
            }
            return response;
        };

    }]);
})(angular.module('fincartApp'));
