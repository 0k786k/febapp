﻿(function (finApp) {
    'use strict';
    finApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise('/user/login');
        $httpProvider.interceptors.push('apiInterceptorService');
        $stateProvider

        //login Page
            .state('login', {
                url: '/user/:type',
                views: {
                    'mainpageview': {
                        templateUrl: '/app/components/login/login.html',
                        controller: 'loginController'
                    }
                }
            })

        //Goal Page
        .state('goal', {
            url: '/goal/:goalcode/:goalid',
            views: {
                'mainpageview': {
                    templateUrl: '/app/components/goal/goal.html',
                    controller: 'goalController'
                }
            }
        })

        //Dashboard master
            .state('dashboard', {
                url: '/dashboard',
                views: {
                    'mainpageview': {
                        templateUrl: '/app/components/dashboard/master.html',
                        controller: 'masterController'
                    }
                },
                redirectTo: 'dashboard.overview',
            })

         //Dashboard Overview
             .state('dashboard.overview', {
                 url: '/overview',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/overview/overview.html',
                         controller: 'overviewController'
                     }
                 }
             })

        //Dashboard SearchScheme
             .state('dashboard.searchScheme', {
                 url: '/searchScheme/:goalid',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/searchScheme/searchScheme.html',
                         controller: 'searchSchemeController'
                     }
                 }
             })

        //Dashboard Full Summary
             .state('dashboard.summaryFullView', {
                 url: '/summaryFullView',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/fullSummary/fullSummary.html',
                         controller: 'fullSummaryController'
                     }
                 }
             })

          //Dashboard Full Allocation
             .state('dashboard.allocationStatistics', {
                 url: '/allocationStatistics',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/allocationStatistics/allocationStatistics.html',
                         controller: 'allocationStatisticsController'
                     }
                 }
             })


        //Dashboard Systematic Reminder
             .state('dashboard.systematicReminder', {
                 url: '/systematicReminder',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/systematicReminder/systematicReminder.html',
                         controller: 'systematicReminderController'
                     }
                 }
             })

          //Dashboard Passbook
             .state('dashboard.passbook', {
                 url: '/passbook',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/passbook/passbook.html',
                         controller: 'passbookController'
                     }
                 }
             })


         //Dashboard Goal Full View
             .state('dashboard.goalFullView', {
                 url: '/goalFullView',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/goalFullView/goalFullView.html',
                         controller: 'goalFullViewController'
                     }
                 }
             })

         //Dashboard Quick Sip
             .state('dashboard.quickSip', {
                 url: '/quickSip',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/quickSip/quickSip.html',
                         controller: 'quickSipController'
                     }
                 }
             })

         //Dashboard Tax Saving
             .state('dashboard.taxSaving', {
                 url: '/taxSaving',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/taxSaving/taxSaving.html',
                         controller: 'taxSavingController'
                     }
                 }
             })

            //Dashboard Financial Planning
             .state('dashboard.financialplanning', {
                 url: '/financialplanning',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/financialplanning/financialplanning.html',
                         controller: 'financialplanningController'
                     }
                 }
             })

            //Dashboard Prime Services
             .state('dashboard.primeservices', {
                 url: '/primeservices',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/primeservices/primeservices.html',
                         controller: 'primeservicesController'
                     }
                 }
             })
        //Dashboard RecommendedScheme
             .state('dashboard.recommendedScheme', {
                 url: '/recommendedScheme/:risk/:amt/:time/:goalid',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/recommendedScheme/recommendedScheme.html',
                         controller: 'recommendedSchemeController'
                     }
                 }
             })

        //Dashboard Cart
             .state('dashboard.cart', {
                 url: '/cart',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/cart/cart.html',
                         controller: 'cartController'
                     }
                 }
             })

        //Dashboard Payment Status
             .state('dashboard.paymentstatus', {
                 url: '/paymentstatus',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/paymentstatus/paymentstatus.html',
                         controller: 'paymentstatusController'
                     }
                 }
             })

        //Dashboard All Investments
             .state('dashboard.allinvestments', {
                 url: '/allinvestments',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/allinvestments/allinvestments.html',
                         controller: 'allinvestmentsController'
                     }
                 }
             })

         //Dashboard Goal Investments
             .state('dashboard.goalinvestments', {
                 url: '/goalinvestments/:goalid',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/goalinvestments/goalinvestments.html',
                         controller: 'goalinvestmentsController'
                     }
                 }
             })

        //Dashboard KYC CAF Add
             .state('dashboard.account', {
                 url: '/account',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/kyccaf/kyccafAdd.html',
                         controller: 'kyccafAddController'
                     }
                 }
             })

            //Dashboard KYC CAF View
             .state('dashboard.profile', {
                 url: '/profile',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/kyccaf/kyccafView.html',
                         controller: 'kyccafViewController'
                     }
                 }
             })

            //Dashboard KYC CAF Edit
             .state('dashboard.editprofile', {
                 url: '/editprofile',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/kyccaf/kyccafEdit.html',
                         controller: 'kyccafEditController'
                     }
                 }
             })

            //Dashboard Investment Account
             .state('dashboard.investmentccount', {
                 url: '/investmentccount',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/investmentAccount/investmentAccount.html',
                         controller: 'investmentAccountController'
                     }
                 }
             })

            //Dashboard Mandate
             .state('dashboard.mandate', {
                 url: '/mandate',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/mandate/mandate.html',
                         controller: 'mandateController'
                     }
                 }
             })

            //Dashboard Saving Account
             .state('dashboard.savingaccount', {
                 url: '/savingaccount',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/savingAccount/savingAccount.html',
                         controller: 'savingAccountController'
                     }
                 }
             })

            //Dashboard Newgoal
             .state('dashboard.newgoal', {
                 url: '/newgoal',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/setgoal/newgoal.html',
                         controller: 'newgoalController'
                     }
                 }
             })

        //Dashboard SetGoal
             .state('dashboard.setgoal', {
                 url: '/setgoal/:goalcode/:goalid',
                 views: {
                     'dashboardview': {
                         templateUrl: '/app/components/dashboard/setgoal/setgoal.html',
                         controller: 'setgoalController'
                     }
                 }
             })
    }]);
})(angular.module('fincartApp'));