﻿(function (finApp) {
    'use strict';
    finApp.controller('loginController', ['$scope', '$state', '$rootScope', '$sce', '$timeout', '$stateParams', 'SweetAlert', 'errorConstants', 'validationFactory', 'oauthTokenFactory', 'userFactory', 'registerReqModel', 'oauthService', 'userService', function ($scope, $state, $rootScope, $sce, $timeout, $stateParams, SweetAlert, errorConstants, validationFactory, oauthTokenFactory, userFactory, registerReqModel, oauthService, userService) {

        $scope.btntxt = "Lets Go";
        $scope.btntxtR = "Get Started";
        $scope.currnPage = "L";
        $scope.emaildisable = false;
        $scope.passworddisable = false;
        $scope.btndisable = false;
        $scope.emaildisableR = false;
        $scope.namedisable = false;
        $scope.mobiledisable = false;
        $scope.passworddisableR = false;
        $scope.chkStatus = true;

        if ($stateParams.type === "login") {
            $scope.currnPage = "L";
        }
        else {
            $scope.currnPage = "R";
        }


        $scope.setCurrPage = function (page) {
            $scope.currnPage = page;
        }

        $scope.action = function () {
            if ($scope.currnPage == "L") {
                $scope.login();
            }
            else if ($scope.currnPage == "R") {
                $scope.register();
            }
            else {

            }
        }

        $scope.tc = function () {
            $scope.chkStatus = !$scope.chkStatus;
        };

        $scope.login = function () {
            if (!$scope.btndisable) {
                if ($scope.txtemail) {
                    if ($scope.txtpassword) {
                        $scope.btntxt = "Please wait...";
                        $scope.emaildisable = true;
                        $scope.passworddisable = true;
                        $scope.btndisable = true;

                        oauthService.login($scope.txtemail, $scope.txtpassword).then(function () {
                            if (oauthTokenFactory.getisloggedin()) {

                                userService.callLoggedInUserDetailsApi().then(function (serverResp) {
                                    if (serverResp.status) {
                                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                                            userFactory.setUserLoggedInDetails(serverResp.data);
                                            $timeout(function () {
                                                $scope.emaildisable = false;
                                                $scope.passworddisable = false;
                                                $scope.btndisable = false;
                                                $scope.btntxt = "Lets Go";
                                                $rootScope.startUserSession();
                                                $state.go('dashboard');
                                            }, 600);
                                        }
                                        else {
                                            $scope.emaildisable = false;
                                            $scope.passworddisable = false;
                                            $scope.btndisable = false;
                                            $scope.btntxt = "Lets Go";
                                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        }
                                    }
                                    else {
                                        $scope.emaildisable = false;
                                        $scope.passworddisable = false;
                                        $scope.btndisable = false;
                                        $scope.btntxt = "Lets Go";
                                        SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                    }
                                });

                            }
                            else {
                                $scope.btntxt = "Lets Go";
                                $scope.emaildisable = false;
                                $scope.passworddisable = false;
                                $scope.btndisable = false;
                                SweetAlert.swal("Invalid Credentials!", "", "error");
                            }
                        });
                    }
                    else {
                        SweetAlert.swal("Please Enter Password!", "", "");
                    }
                }
                else {
                    SweetAlert.swal("Please Enter Email!", "", "");
                }
            }
        }

        $scope.register = function () {

            if (!$scope.btndisable) {
                if ($scope.txtemailR) {
                    if (validationFactory.validEmail($scope.txtemailR)) {
                        if ($scope.txtname) {
                            if ($scope.txtmobile) {
                                if (validationFactory.validMobile($scope.txtmobile, '+91')) {
                                    if ($scope.txtpasswordR) {
                                        if ($scope.txtpasswordR.length > 7) {
                                            $scope.btntxtR = "Please wait...";
                                            $scope.emaildisableR = true;
                                            $scope.namedisable = true;
                                            $scope.mobiledisable = true;
                                            $scope.passworddisableR = true;
                                            $scope.btndisable = true;

                                            var registerReq = registerReqModel.RegisterReq;
                                            registerReq.Name = $scope.txtname;
                                            registerReq.Userid = $scope.txtemailR;
                                            registerReq.password = $scope.txtpasswordR;
                                            registerReq.ClientStatus = "";
                                            registerReq.Mobile = $scope.txtmobile;
                                            registerReq.trackerCode = "";

                                            userService.callUserRegisterApi(registerReq).then(function (serverResp) {
                                                if (serverResp.status) {
                                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                        
                                                        oauthService.login($scope.txtemailR, $scope.txtpasswordR).then(function () {
                                                            if (oauthTokenFactory.getisloggedin()) {

                                                                userService.callLoggedInUserDetailsApi().then(function (serverResp) {
                                                                    if (serverResp.status) {
                                                                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                                            userFactory.setUserLoggedInDetails(serverResp.data);
                                                                            $timeout(function () {
                                                                                $scope.btntxtR = "Get Started";
                                                                                $scope.emaildisableR = false;
                                                                                $scope.namedisable = false;
                                                                                $scope.mobiledisable = false;
                                                                                $scope.passworddisableR = false;
                                                                                $scope.btndisable = false;
                                                                                $rootScope.startUserSession();
                                                                                $state.go('dashboard');
                                                                            }, 600);
                                                                        }
                                                                        else {
                                                                            $scope.btntxtR = "Get Started";
                                                                            $scope.emaildisableR = false;
                                                                            $scope.namedisable = false;
                                                                            $scope.mobiledisable = false;
                                                                            $scope.passworddisableR = false;
                                                                            $scope.btndisable = false;
                                                                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                                        }
                                                                    }
                                                                    else {
                                                                        $scope.btntxtR = "Get Started";
                                                                        $scope.emaildisableR = false;
                                                                        $scope.namedisable = false;
                                                                        $scope.mobiledisable = false;
                                                                        $scope.passworddisableR = false;
                                                                        $scope.btndisable = false;
                                                                        SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                                                    }
                                                                });

                                                            }
                                                            else {
                                                                $scope.btntxtR = "Get Started";
                                                                $scope.emaildisableR = false;
                                                                $scope.namedisable = false;
                                                                $scope.mobiledisable = false;
                                                                $scope.passworddisableR = false;
                                                                $scope.btndisable = false;
                                                                SweetAlert.swal("Invalid Credentials!", "", "error");
                                                            }
                                                        });


                                                    }
                                                    else {
                                                        $scope.btntxtR = "Get Started";
                                                        $scope.emaildisableR = false;
                                                        $scope.namedisable = false;
                                                        $scope.mobiledisable = false;
                                                        $scope.passworddisableR = false;
                                                        $scope.btndisable = false;
                                                        SweetAlert.swal(serverResp.msg, "", "error");
                                                        //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                    }
                                                }
                                                else {
                                                    $scope.btntxtR = "Get Started";
                                                    $scope.emaildisableR = false;
                                                    $scope.namedisable = false;
                                                    $scope.mobiledisable = false;
                                                    $scope.passworddisableR = false;
                                                    $scope.btndisable = false;
                                                    SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                                }
                                            });
                                        }
                                        else {
                                            SweetAlert.swal("Password must be minimum 8 characters!", "", "");
                                        }
                                    }
                                    else {
                                        SweetAlert.swal("Please enter password!", "", "");
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please enter valid indian mobile number!", "", "");
                                }
                            }
                            else {
                                SweetAlert.swal("Please enter mobile number!", "", "");
                            }
                        }
                        else {
                            SweetAlert.swal("Please enter name!", "", "");
                        }
                    }
                    else {
                        SweetAlert.swal("Please enter valid email!", "", "");
                    }
                }
                else {
                    SweetAlert.swal("Please enter email!", "", "");
                }
            }
        }

    }]);
})(angular.module('fincartApp'));
