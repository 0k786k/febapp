﻿(function (finApp) {
    'use strict';
    finApp.controller('goalController', ['$scope', '$state', '$window', '$stateParams', '$rootScope', '$mdDialog', '$mdPanel', '$filter', '$q', 'oauthTokenFactory', 'userFactory', 'validationFactory', 'SweetAlert', 'errorConstants', 'goalConstants', 'finCalcModel', 'goalresultModel', 'addGoalModel', 'userService', function ($scope, $state, $window, $stateParams, $rootScope, $mdDialog, $mdPanel, $filter, $q, oauthTokenFactory, userFactory, validationFactory, SweetAlert, errorConstants, goalConstants, finCalcModel, goalresultModel, addGoalModel, userService) {

            $scope.currgoalcode = $stateParams.goalcode.toUpperCase();
            $scope.currgoalid = $stateParams.goalid ? $stateParams.goalid : "0";
            $scope.currtypecode = "";
            $scope.pfAmt = 0;
            $scope.licAmt = 0;
            $scope.loanAmt = 0;
            $scope.savingAmt = 0;
            $scope.totalSavedTaxAmt = 0;
            $scope.totalReqSavedTaxAmt = 150000;

            $rootScope.currActiveTab = $scope.currgoalcode == "FG220" ? "quickSip" : $scope.currgoalcode == "FG104" ? "taxSaving" : "newgoal";

            if (goalConstants.goals.filter(function (obj) { return obj.value == $scope.currgoalcode; }).length > 0) {
                $scope.goalname = goalConstants.goals.filter(function (obj) { return obj.value == $scope.currgoalcode; })[0].name;
            }
            else {
                $scope.currtypecode = $scope.currgoalcode;
                $scope.currgoalcode = 'FG14';
                $scope.goalname = "Other";
            }

            $scope.isGoalLoaded = false;
            $scope.isLocationOther = false;
            $scope.loadedGoal = null;

            $scope.goalImg = "../../../../assets/img/Goal/image/" + $scope.currgoalcode + ".png";
            $scope.goalIcon = "../../../../assets/img/Goal/icon/" + $scope.currgoalcode + ".png";

            $scope.minStartDate = new Date(new Date().getFullYear(), new Date().getMonth() + 12, new Date().getDate());
            $scope.minEndDate = new Date(new Date().getFullYear(), new Date().getMonth() + 12, new Date().getDate());
            $scope.taxMinEndDate = new Date(new Date().getFullYear(), new Date().getMonth() + 36, new Date().getDate());

            $rootScope.mainheader = $scope.currgoalid != '0' ? $scope.currgoalcode == "FG104" ? "Update Your " + $scope.goalname : "Update Your " + $scope.goalname + " Goal" : $scope.currgoalcode == "FG104" ? "Set Your " + $scope.goalname : "Set Your " + $scope.goalname + " Goal";
            $rootScope.mainheaderimg = $scope.goalIcon;

            $scope.allLocations = [];
            $scope.selectedLocation = "";

            $scope.otherGoalNameQuestion = "What is your goal?";
            $scope.otherGoalNameQuestionPlaceHolder = "Enter goal name";
            $scope.goalTimeQuestion = "Target year to buy " + $scope.goalname + "?";
            $scope.goalAmtQuestion = "How much money would you need to buy a " + $scope.goalname + "?";
            $scope.goalMonthlyExpenceQuestion = "";
            $scope.goalLocationQuestion = "Where do you want to " + $scope.goalname + "?";

            if ($scope.currgoalcode == "FG220" || $scope.currgoalcode == "FG104" || $scope.currgoalcode == "FG309") {
                $scope.currtypecode = $scope.currgoalcode;
                $scope.currgoalcode = 'FG14';
            }

            if ($scope.currgoalcode == "FG2") {
                $scope.goalAmtQuestion = "How much money do you need to start " + $scope.goalname + "?";
                $scope.goalTimeQuestion = "Target year to start " + $scope.goalname + "?";
                $scope.goalMonthlyExpenceQuestion = "How much money do you need to run your business every month?";
            }
            else if ($scope.currgoalcode == "FG4") {
                $scope.goalAmtQuestion = "How much down payment would you need to buy a " + $scope.goalname + "?";
            }
            else if ($scope.currgoalcode == "FG5") {
                $scope.otherGoalNameQuestion = "Where is this place?";
                $scope.otherGoalNameQuestionPlaceHolder = "Enter place name";
                $scope.goalAmtQuestion = "What is the approximate cost?";
                $scope.goalTimeQuestion = "Target year for " + $scope.goalname + "?";
                $scope.allLocations = goalConstants.studyLocations;
            }
            else if ($scope.currgoalcode == "FG6") {
                $scope.goalTimeQuestion = "Target year for " + $scope.goalname + "?";
                $scope.allLocations = goalConstants.travelLocations;
            }
            else if ($scope.currgoalcode == "FG7") {
                $scope.goalAmtQuestion = "How much money do you need for " + $scope.goalname + "?";
                $scope.goalTimeQuestion = "Target year for " + $scope.goalname + "?";
            }
            else if ($scope.currgoalcode == "FG8") {
                $scope.goalAmtQuestion = "How much " + $scope.goalname + " do you need?";
                $scope.goalTimeQuestion = "Target year for " + $scope.goalname + "?";
            }
            else if ($scope.currgoalcode == "FG9") {
                $scope.goalMonthlyExpenceQuestion = "What is your current monthly expence?";
            }
            else if ($scope.currgoalcode == "FG10") {
                $scope.goalTimeQuestion = "For how long?";
                $scope.goalMonthlyExpenceQuestion = "What is your current monthly expence?";
            }
            else if ($scope.currgoalcode == "FG11") {
                $scope.goalAmtQuestion = "How much money do you need for " + $scope.goalname + "?";
                $scope.goalTimeQuestion = "Target year for " + $scope.goalname + "?";
            }
            else if ($scope.currgoalcode == "FG12") {
                $scope.goalTimeQuestion = "Target year for " + $scope.goalname + "?";
                $scope.allLocations = goalConstants.childStudyLocations;
            }
            else if ($scope.currgoalcode == "FG13") {
                $scope.goalAmtQuestion = "How much money do you need for " + $scope.goalname + "?";
                $scope.goalTimeQuestion = "Target year for " + $scope.goalname + "?";
            }
            else if ($scope.currgoalcode == "FG14") {
                if ($scope.currtypecode == "FG220") {
                    $scope.otherGoalNameQuestion = "What name would you give to your Quick Sip?";
                    $scope.goalAmtQuestion = "How much money you can invest monthly?";
                    $scope.goalTimeQuestion = "How long you want to invest?";
                }
                else {
                    $scope.goalAmtQuestion = "How much money do you need for this goal?";
                    $scope.goalTimeQuestion = "Target year for this goal?";
                }
            }

            $scope.locationChange = function () {
                if ($scope.selectedLocation.trim().toUpperCase() == "005") {
                    $scope.isLocationOther = true;
                }
                else {
                    $scope.isLocationOther = false;
                }
            }

            $scope.taxAmtChange = function () {
                $scope.pfAmt = $scope.txtPF ? parseInt($scope.txtPF) : 0;
                $scope.licAmt = $scope.txtLIC ? parseInt($scope.txtLIC) : 0;
                $scope.loanAmt = $scope.txtLOAN ? parseInt($scope.txtLOAN) : 0;
                $scope.savingAmt = $scope.txtSAVING ? parseInt($scope.txtSAVING) : 0;

                $scope.totalSavedTaxAmt = $scope.pfAmt + $scope.licAmt + $scope.loanAmt + $scope.savingAmt;
                if ($scope.totalSavedTaxAmt < 149999) {
                    $scope.totalReqSavedTaxAmt = 150000 - $scope.totalSavedTaxAmt;
                }
                else {
                    $scope.totalSavedTaxAmt = 150000;
                    $scope.totalReqSavedTaxAmt = 0;
                }
            }

            $scope.validateForm = function () {

                var deferred = $q.defer();
                var result = { status: false, calcmodel: null, errmsg: null }

                var FinCalcModel = finCalcModel.FinCalcModel;
                FinCalcModel.type = "CAMT";
                FinCalcModel.currentAmount = "0";
                FinCalcModel.startDate = $scope.currtypecode == "FG104" ? $scope.totalSavedTaxAmt : null;
                FinCalcModel.endDate = null;
                FinCalcModel.goalCode = $scope.currgoalcode;
                FinCalcModel.otherGoalName = null;
                FinCalcModel.childName = $scope.currtypecode == "FG104" ? $scope.loanAmt : null;
                FinCalcModel.travelPeople = $scope.currtypecode == "FG104" ? $scope.pfAmt : null;
                FinCalcModel.locationCode = null;
                FinCalcModel.budgetType = null;
                FinCalcModel.businessStartupCost = $scope.currtypecode == "FG104" ? $scope.licAmt : null;
                FinCalcModel.monthlyexpence = $scope.currtypecode == "FG104" ? $scope.savingAmt : null;

                switch ($scope.currgoalcode) {
                    case 'FG2':
                        if ($scope.txtamt) {
                            if ($scope.txtmonthlyexp) {
                                if ($scope.txtdate) {
                                    result.status = true;
                                    FinCalcModel.businessStartupCost = $scope.txtamt;
                                    FinCalcModel.monthlyexpence = $scope.txtmonthlyexp;
                                    FinCalcModel.endDate = $scope.txtdate;
                                    result.calcmodel = FinCalcModel;
                                }
                                else {
                                    result.errmsg = "Please enter target years!";
                                }
                            }
                            else {
                                result.errmsg = "Please enter monthly expence!";
                            }
                        }
                        else {
                            result.errmsg = "Please enter amount!";
                        }
                        deferred.resolve(result);
                        break;
                    case 'FG5':
                        if ($scope.selectedLocation) {
                            if ($scope.selectedLocation.trim().toUpperCase() == '005') {
                                if ($scope.txtothergoalname) {
                                    if ($scope.txtamt) {
                                        if ($scope.txtdate) {
                                            result.status = true;
                                            FinCalcModel.locationCode = $scope.selectedLocation;
                                            FinCalcModel.endDate = $scope.txtdate;
                                            FinCalcModel.otherGoalName = $scope.txtothergoalname;
                                            FinCalcModel.currentAmount = $scope.txtamt;
                                            result.calcmodel = FinCalcModel;
                                        }
                                        else {
                                            result.errmsg = "Please enter target years!";
                                        }
                                    }
                                    else {
                                        result.errmsg = "Please enter amount!";
                                    }
                                }
                                else {
                                    result.errmsg = "Please enter place name!";
                                }
                            }
                            else {
                                if ($scope.txtdate) {
                                    result.status = true;
                                    FinCalcModel.locationCode = $scope.selectedLocation;
                                    FinCalcModel.endDate = $scope.txtdate;
                                    result.calcmodel = FinCalcModel;
                                }
                                else {
                                    result.errmsg = "Please enter target years!";
                                }
                            }
                        }
                        else {
                            result.errmsg = "Please select location!";
                        }
                        deferred.resolve(result);
                        break;
                    case 'FG6':
                        if ($scope.selectedLocation) {
                            if ($scope.txtpeople) {
                                if ($scope.txtdate) {
                                    result.status = true;
                                    FinCalcModel.locationCode = $scope.selectedLocation;
                                    FinCalcModel.travelPeople = $scope.txtpeople;
                                    FinCalcModel.endDate = $scope.txtdate;
                                    result.calcmodel = FinCalcModel;
                                }
                                else {
                                    result.errmsg = "Please enter target years!";
                                }
                            }
                            else {
                                result.errmsg = "Please enter number of people!";
                            }
                        }
                        else {
                            result.errmsg = "Please select location!";
                        }
                        deferred.resolve(result);
                        break;
                    case 'FG9':
                        if ($scope.txtcurrage) {
                            if ($scope.txtretage) {
                                if ($scope.txtmonthlyexp) {
                                    result.status = true;
                                    FinCalcModel.startDate = $scope.txtcurrage;
                                    FinCalcModel.endDate = $scope.txtretage;
                                    FinCalcModel.monthlyexpence = $scope.txtmonthlyexp;
                                    result.calcmodel = FinCalcModel;
                                }
                                else {
                                    result.errmsg = "Please enter monthly expence!";
                                }
                            }
                            else {
                                result.errmsg = "Please enter retirement age!";
                            }
                        }
                        else {
                            result.errmsg = "Please enter current age!";
                        }
                        deferred.resolve(result);
                        break;
                    case 'FG10':
                        if ($scope.txtmonthlyexp) {
                            if ($scope.txtstartdate) {
                                if ($scope.txtdate) {
                                    result.status = true;
                                    FinCalcModel.startDate = $scope.txtstartdate;
                                    FinCalcModel.endDate = $scope.txtdate;
                                    FinCalcModel.monthlyexpence = $scope.txtmonthlyexp;
                                    result.calcmodel = FinCalcModel;
                                }
                                else {
                                    result.errmsg = "Please enter end date!";
                                }
                            }
                            else {
                                result.errmsg = "Please enter start date!";
                            }
                        }
                        else {
                            result.errmsg = "Please enter monthly expence!";
                        }
                        deferred.resolve(result);
                        break;
                    case 'FG12':
                        if ($scope.txtchildname) {
                            if ($scope.selectedLocation) {
                                if ($scope.txtdate) {
                                    result.status = true;
                                    FinCalcModel.childName = $scope.txtchildname;
                                    FinCalcModel.locationCode = $scope.selectedLocation;
                                    FinCalcModel.endDate = $scope.txtdate;
                                    result.calcmodel = FinCalcModel;
                                }
                                else {
                                    result.errmsg = "Please enter target year!";
                                }
                            }
                            else {
                                result.errmsg = "Please select location!";
                            }
                        }
                        else {
                            result.errmsg = "Please enter child name!";
                        }
                        deferred.resolve(result);
                        break;
                    case 'FG13':
                        if ($scope.txtchildname) {
                            if ($scope.txtamt) {
                                if ($scope.txtdate) {
                                    result.status = true;
                                    FinCalcModel.childName = $scope.txtchildname;
                                    FinCalcModel.currentAmount = $scope.txtamt;
                                    FinCalcModel.endDate = $scope.txtdate;
                                    result.calcmodel = FinCalcModel;
                                }
                                else {
                                    result.errmsg = "Please enter target year!";
                                }
                            }
                            else {
                                result.errmsg = "Please enter amount!";
                            }
                        }
                        else {
                            result.errmsg = "Please enter child name!";
                        }
                        deferred.resolve(result);
                        break;
                    case 'FG14':
                        if ($scope.currtypecode != "FG104") {
                            if ($scope.txtothergoalname) {
                                if ($scope.txtamt) {
                                    if ($scope.txtdate) {
                                        result.status = true;
                                        if ($scope.currtypecode) {
                                            FinCalcModel.locationCode = $scope.currtypecode;
                                            if ($scope.currtypecode == "FG220") {
                                                FinCalcModel.type = "ISAMT";
                                            }
                                        }
                                        FinCalcModel.otherGoalName = $scope.txtothergoalname;
                                        FinCalcModel.currentAmount = $scope.txtamt;
                                        FinCalcModel.endDate = $scope.txtdate;
                                        result.calcmodel = FinCalcModel;
                                    }
                                    else {
                                        if ($scope.currtypecode) {
                                            result.errmsg = "Please enter date!";
                                        }
                                        else {
                                            result.errmsg = "Please enter target year!";
                                        }
                                    }
                                }
                                else {
                                    result.errmsg = "Please enter amount!";
                                }
                            }
                            else {
                                if ($scope.currtypecode) {
                                    result.errmsg = "Please enter quick SIP name!";
                                }
                                else {
                                    result.errmsg = "Please enter goal name!";
                                }
                            }
                        }
                        else {
                            result.status = true;
                            $scope.txtothergoalname = 'Tax Saving 2019';
                            FinCalcModel.type = "ILAMT";
                            FinCalcModel.locationCode = $scope.currtypecode;
                            FinCalcModel.otherGoalName = $scope.txtothergoalname;
                            FinCalcModel.currentAmount = $scope.totalReqSavedTaxAmt;
                            FinCalcModel.endDate = $filter('date')($scope.taxMinEndDate, "yyyy-MM-dd");
                            result.calcmodel = FinCalcModel;
                        }
                        deferred.resolve(result);
                        break;
                    default:
                        if ($scope.txtamt) {
                            if ($scope.txtdate) {
                                result.status = true;
                                FinCalcModel.currentAmount = $scope.txtamt;
                                FinCalcModel.endDate = $scope.txtdate;
                                result.calcmodel = FinCalcModel;
                            }
                            else {
                                result.errmsg = "Please enter target years!";
                            }
                        }
                        else {
                            result.errmsg = "Please enter amount!";
                        }
                        deferred.resolve(result);
                        break;
                }

                return deferred.promise;
            }

            $scope.bindGoalData = function () {
                $rootScope.showLoader();
                if ($scope.currgoalid) {
                    if ($scope.currgoalid != '0') {
                        userService.callFetchSingleGoalApi($scope.currgoalid).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $scope.loadedGoal = addGoalModel.parseAddGoal(serverResp.data);

                                    if ($scope.loadedGoal.typeCode.trim().toUpperCase() != 'FG104') {
                                        $scope.txtothergoalname = $scope.loadedGoal.otherGoalName;
                                        $scope.txtchildname = $scope.loadedGoal.childName;
                                        $scope.txtcurrage = parseFloat($scope.loadedGoal.age);
                                        $scope.txtretage = parseFloat($scope.loadedGoal.retirementAge);
                                        $scope.selectedLocation = $scope.loadedGoal.typeCode;
                                        if ($scope.selectedLocation.trim().toUpperCase() == '005') {
                                            $scope.isLocationOther = true;
                                        }
                                        $scope.txtpeople = parseFloat($scope.loadedGoal.people);
                                        $scope.txtamt = parseFloat($scope.loadedGoal.presentValue);
                                        $scope.txtmonthlyexp = parseFloat($scope.loadedGoal.monthlyAmount);
                                        $scope.txtstartdate = $scope.loadedGoal.goal_StartDate;
                                        $scope.txtdate = $scope.loadedGoal.goal_EndDate;
                                    }
                                    else {
                                        $scope.txtothergoalname = $scope.loadedGoal.otherGoalName;
                                        $scope.txtPF = parseFloat($scope.loadedGoal.people);
                                        $scope.txtLIC = parseFloat($scope.loadedGoal.retirementAge);
                                        $scope.txtLOAN = parseFloat($scope.loadedGoal.presentValue);
                                        $scope.txtSAVING = parseFloat($scope.loadedGoal.monthlyAmount);
                                        $scope.taxAmtChange();
                                    }
                                    $scope.isGoalLoaded = true;
                                    $rootScope.hideLoader();
                                }
                                else {
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                    $scope.isGoalLoaded = true;
                                    $rootScope.hideLoader();
                                }
                            }
                            else {
                                $scope.isGoalLoaded = true;
                                $rootScope.hideLoader();
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });
                    }
                    else {
                        $scope.isGoalLoaded = true;
                        $rootScope.hideLoader();
                    }
                }
                else {
                    $scope.isGoalLoaded = true;
                    $rootScope.hideLoader();
                }
            }

            $scope.next = function (ev) {
                if (!$scope.btndisable) {
                    $scope.validateForm().then(function (result) {
                        if (result.status) {
                            $scope.btndisable = true;
                            userService.callGoalCalculatorApi(result.calcmodel).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                        $scope.btndisable = false;
                                        $scope.openModal(ev, serverResp.data, result.calcmodel)
                                    }
                                    else {
                                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        $scope.btndisable = false;
                                    }
                                }
                                else {
                                    $scope.btndisable = false;
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });
                        }
                        else {
                            SweetAlert.swal(result.errmsg, "", "warning");
                        }
                    });
                }
            }

            $scope.openModal = function (ev, dataForPopup, modelForPopup) {
                $mdDialog.show({
                    locals: { dataToPass: dataForPopup, goalReqmodel: modelForPopup, usergoalid: $scope.currgoalid },
                    controller: goalResultModalController,
                    templateUrl: 'goalResultModal.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                .then(function (val) {
                    if ($scope.currgoalcode != 'FG14') {
                        if ($scope.currgoalid != "0") {
                            SweetAlert.swal("Your " + $scope.goalname + " Goal updated successfully!", "", "success");
                        }
                        else {
                            SweetAlert.swal("Your " + $scope.goalname + " Goal updated successfully!", "", "success");
                        }
                        $state.go('dashboard.goalFullView');
                    }
                    else {
                        if ($scope.currtypecode == "FG104") {
                            if ($scope.currgoalid != "0") {
                                SweetAlert.swal("Your " + $scope.txtothergoalname + " updated successfully!", "", "success");
                            }
                            else {
                                SweetAlert.swal("Your " + $scope.txtothergoalname + " saved successfully!", "", "success");
                            }
                            $state.go('dashboard.taxSaving');
                        }
                        else if ($scope.currtypecode == "FG220") {
                            if ($scope.currgoalid != "0") {
                                SweetAlert.swal("Your Quick SIP for " + $scope.txtothergoalname + " updated successfully!", "", "success");
                            }
                            else {
                                SweetAlert.swal("Your Quick SIP for " + $scope.txtothergoalname + " saved successfully!", "", "success");
                            }
                            $state.go('dashboard.quickSip');
                        }
                        else {
                            if ($scope.currgoalid != "0") {
                                SweetAlert.swal("Your " + $scope.txtothergoalname + " Goal updated successfully!", "", "success");
                            }
                            else {
                                SweetAlert.swal("Your " + $scope.txtothergoalname + " Goal saved successfully!", "", "success");
                            }
                            $state.go('dashboard.goalFullView');
                        }
                    }
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    //$scope.status = 'You cancelled the dialog.';
                });
            };

            function goalResultModalController($scope, $mdDialog, dataToPass, goalReqmodel, usergoalid, SweetAlert, errorConstants, userFactory, goalresultModel, addGoalModel, userService) {

                $scope.goalReqmodel = goalReqmodel;
                $scope.presentValue = $scope.goalReqmodel.currentAmount
                $scope.currUserGoalId = usergoalid;
                $scope.isProcessing = false;
                $scope.isDirtyData = false;
                $scope.userdetails = userFactory.getUserLoggedInDetails();
                $scope.investType = $scope.goalReqmodel.locationCode != "FG104" ? "S" : "L";
                $scope.goalReqmodel.type = $scope.goalReqmodel.locationCode != "FG104" ? "ISAMT" : "ILAMT";

                $scope.setGoalResult = function (dataToPass) {
                    $scope.goalResult = goalresultModel.parseGoalresult(dataToPass);
                    $scope.getAmt = $scope.investType == "S" ? $scope.goalResult.getSip : $scope.goalResult.getLumpsum;
                    $scope.investAmt = $scope.investType == "S" ? parseInt($scope.goalResult.investSip) : parseInt($scope.goalResult.investLumpsum);
                    $scope.time = $scope.goalResult.time;
                }

                $scope.chkChange = function (val) {
                    $scope.investType = val;
                    $scope.goalReqmodel.type = $scope.investType == "S" ? "ISAMT" : "ILAMT";
                    if (!$scope.isDirtyData) {
                        $scope.getAmt = $scope.investType == "S" ? $scope.goalResult.getSip : $scope.goalResult.getLumpsum;
                        $scope.investAmt = $scope.investType == "S" ? parseInt($scope.goalResult.investSip) : parseInt($scope.goalResult.investLumpsum);
                    }
                }

                $scope.amtChange = function () {
                    $scope.isDirtyData = true;
                }

                $scope.recalculate = function () {
                    if ($scope.investAmt) {
                        $scope.goalReqmodel.currentAmount = $scope.investAmt;
                        $scope.isProcessing = true;

                        userService.callGoalCalculatorApi($scope.goalReqmodel).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $scope.setGoalResult(serverResp.data);
                                    $scope.isProcessing = false;
                                    $scope.isDirtyData = false;
                                }
                                else {
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                    $scope.isProcessing = false;
                                }
                            }
                            else {
                                $scope.isProcessing = false;
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });
                    }
                    else {
                        SweetAlert.swal("Please enter invest amount!", "", "warning");
                    }
                }

                $scope.saveTransact = function () {
                    if ($scope.investAmt) {
                        if (parseFloat($scope.investAmt) > 0.0) {

                            $scope.isProcessing = true;

                            var goal = addGoalModel.AddGoal;
                            goal.userGoalId = null;
                            goal.basicid = $scope.userdetails.basicid;
                            goal.Relation = null;
                            goal.childName = null;
                            goal.age = null;
                            goal.gender = null;
                            goal.annualIncome = null;
                            goal.goalCode = $scope.goalReqmodel.goalCode;
                            goal.otherGoalName = null;
                            goal.typeCode = $scope.goalReqmodel.locationCode ? $scope.goalReqmodel.locationCode : "";
                            goal.monthlyAmount = $scope.goalReqmodel.monthlyexpence;
                            goal.presentValue = $scope.presentValue;
                            goal.risk = 'M';
                            goal.goal_StartDate = null;
                            goal.goal_EndDate = $scope.goalReqmodel.endDate;
                            goal.inflationRate = $scope.goalResult.Inflation;
                            goal.ror = $scope.investType == "S" ? $scope.goalResult.ROR : $scope.goalResult.RORL;
                            goal.PMT = $scope.goalResult.PMT;
                            goal.downPaymentRate = null;
                            goal.trnx_Type = $scope.investType;
                            goal.getAmount = $scope.investType == "S" ? $scope.goalResult.getSip : $scope.goalResult.getLumpsum;
                            goal.retirementAge = null;
                            goal.investAmount = $scope.investType == "S" ? $scope.goalResult.investSip : $scope.goalResult.investLumpsum;
                            goal.people = null;

                            if ($scope.goalReqmodel.goalCode == 'FG4') {
                                goal.downPaymentRate = "25";
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG5') {
                                goal.otherGoalName = $scope.goalReqmodel.otherGoalName;
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG6') {
                                goal.people = $scope.goalReqmodel.travelPeople;
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG9') {
                                goal.age = $scope.goalReqmodel.startDate;
                                goal.retirementAge = $scope.goalReqmodel.endDate;
                                goal.goal_EndDate = null;
                            }
                            if ($scope.goalReqmodel.goalCode == 'FG10') {
                                goal.goal_StartDate = $scope.goalReqmodel.startDate;
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG12' || $scope.goalReqmodel.goalCode == 'FG13') {
                                goal.Relation = '002'
                                goal.childName = $scope.goalReqmodel.childName;
                                goal.age = "4";
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG14') {
                                goal.otherGoalName = $scope.goalReqmodel.otherGoalName;
                            }

                            if($scope.goalReqmodel.locationCode == "FG104"){
                                goal.people = $scope.goalReqmodel.travelPeople;
                                goal.retirementAge = $scope.goalReqmodel.businessStartupCost;
                                goal.presentValue = $scope.goalReqmodel.childName;
                                goal.goal_StartDate = $scope.goalReqmodel.startDate;
                            }

                            userService.callGoalCreateApi(goal).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                        $scope.isProcessing = false;

                                        if (goal.typeCode) {
                                            if (goal.typeCode.trim().toUpperCase() == "FG104") {
                                                $scope.userdetails.isTaxSaving = true;
                                            }
                                            else if (goal.typeCode.trim().toUpperCase() == "FG220") {
                                                $scope.userdetails.isQuicksip = true;
                                            }
                                            else {
                                                $scope.userdetails.isGoal = true;
                                            }
                                        }
                                        else {
                                            $scope.userdetails.isGoal = true;
                                        }

                                        userFactory.setUserLoggedInDetails($scope.userdetails);

                                        $scope.hide(goal.goalCode);
                                    }
                                    else {
                                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        $scope.isProcessing = false;
                                    }
                                }
                                else {
                                    $scope.isProcessing = false;
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });
                        }
                        else {
                            SweetAlert.swal("Please enter invest amount!", "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please enter invest amount!", "", "warning");
                    }

                }

                $scope.updateGoal = function () {
                    if ($scope.investAmt) {
                        if (parseFloat($scope.investAmt) > 0.0) {

                            $scope.isProcessing = true;

                            var goal = addGoalModel.AddGoal;
                            goal.userGoalId = $scope.currUserGoalId;
                            goal.basicid = $scope.userdetails.basicid;
                            goal.Relation = null;
                            goal.childName = null;
                            goal.age = null;
                            goal.gender = null;
                            goal.annualIncome = null;
                            goal.goalCode = $scope.goalReqmodel.goalCode;
                            goal.otherGoalName = null;
                            goal.typeCode = $scope.goalReqmodel.locationCode ? $scope.goalReqmodel.locationCode : "";
                            goal.monthlyAmount = $scope.goalReqmodel.monthlyexpence;
                            goal.presentValue = $scope.presentValue;
                            goal.risk = 'M';
                            goal.goal_StartDate = null;
                            goal.goal_EndDate = $scope.goalReqmodel.endDate;
                            goal.inflationRate = $scope.goalResult.Inflation;
                            goal.ror = $scope.investType == "S" ? $scope.goalResult.ROR : $scope.goalResult.RORL;
                            goal.PMT = $scope.goalResult.PMT;
                            goal.downPaymentRate = null;
                            goal.trnx_Type = $scope.investType;
                            goal.getAmount = $scope.investType == "S" ? $scope.goalResult.getSip : $scope.goalResult.getLumpsum;
                            goal.retirementAge = null;
                            goal.investAmount = $scope.investType == "S" ? $scope.goalResult.investSip : $scope.goalResult.investLumpsum;
                            goal.people = null;

                            if ($scope.goalReqmodel.goalCode == 'FG4') {
                                goal.downPaymentRate = "25";
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG5') {
                                goal.otherGoalName = $scope.goalReqmodel.otherGoalName;
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG6') {
                                goal.people = $scope.goalReqmodel.travelPeople;
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG9') {
                                goal.age = $scope.goalReqmodel.startDate;
                                goal.retirementAge = $scope.goalReqmodel.endDate;
                                goal.goal_EndDate = null;
                            }
                            if ($scope.goalReqmodel.goalCode == 'FG10') {
                                goal.goal_StartDate = $scope.goalReqmodel.startDate;
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG12' || $scope.goalReqmodel.goalCode == 'FG13') {
                                goal.Relation = '002'
                                goal.childName = $scope.goalReqmodel.childName;
                                goal.age = "4";
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG14') {
                                goal.otherGoalName = $scope.goalReqmodel.otherGoalName;
                            }

                            if ($scope.goalReqmodel.locationCode == "FG104") {
                                goal.people = $scope.goalReqmodel.travelPeople;
                                goal.retirementAge = $scope.goalReqmodel.businessStartupCost;
                                goal.presentValue = $scope.goalReqmodel.childName;
                                goal.goal_StartDate = $scope.goalReqmodel.startDate;
                            }

                            userService.callGoalUpdateApi(goal).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                        $scope.isProcessing = false;
                                        $scope.hide(goal.goalCode);
                                    }
                                    else {
                                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        $scope.isProcessing = false;
                                    }
                                }
                                else {
                                    $scope.isProcessing = false;
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });
                        }
                        else {
                            SweetAlert.swal("Please enter invest amount!", "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please enter invest amount!", "", "warning");
                    }

                }

                $scope.hide = function (val) {
                    $mdDialog.hide(val);
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.setGoalResult(dataToPass);
            }

            $scope.bindGoalData();
    }]);
})(angular.module('fincartApp'));