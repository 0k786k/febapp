﻿(function (finApp) {
    'use strict';
    finApp.controller('taxSavingController', ['$scope', '$state', '$rootScope', '$mdDialog', 'SweetAlert', 'errorConstants', 'oauthTokenFactory', 'userFactory', 'UserAllGoalsListModel', 'userService', function ($scope, $state, $rootScope, $mdDialog, SweetAlert, errorConstants, oauthTokenFactory, userFactory, UserAllGoalsListModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "My Tax Saving";
            $rootScope.mainheaderimg = "../../../../assets/img/Menu/Icon/TaxSaving.png";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");

            $scope.goalImg = "../../../../assets/img/Goal/image/FG104.png";
            $scope.goalIcon = "../../../../assets/img/Goal/icon/FG104.png";

            $scope.userdetails = userFactory.getUserLoggedInDetails();
            $scope.userAllGoalsList = [];
            $scope.dataForGoalRemovePopup = { goalName: '', goalCode: '', goalId: 0 };

            $rootScope.showLoader();

            $scope.bindGoals = function () {
                userService.callTaxSavingGoalApi().then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.userAllGoalsList = UserAllGoalsListModel.parseUserAllGoalsList(serverResp.data);
                            $rootScope.hideLoader();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }

                });
            }

            $scope.goalRemove = function (ev, objGoal) {
                $scope.openPopUp(ev, objGoal.goalName, objGoal.goalCode, objGoal.userGoalId)
            }

            $scope.goalEdit = function (ev, objGoal) {
                $state.go('dashboard.setgoal', { 'goalcode': objGoal.goalCode == 'FG14' ? objGoal.typeCode : objGoal.goalCode, 'goalid': objGoal.userGoalId });
            }

            $scope.recommendedScheme = function (ev, objGoal) {
                $state.go('dashboard.recommendedScheme', { 'risk': 'M', 'amt': objGoal.investAmount, 'time': objGoal.duration, 'goalid': objGoal.userGoalId });
            }

            $scope.investedScheme = function (ev, objGoal) {
                $state.go('dashboard.goalinvestments', { 'goalid': objGoal.userGoalId });
            }

            $scope.completeProfile = function (ev) {
                $rootScope.prmBasicId = $scope.userdetails.basicid;
                $state.go('dashboard.account');
            }

            $scope.openPopUp = function (ev, goalname, goalcode, goalid) {

                $scope.dataForGoalRemovePopup.goalName = goalname;
                $scope.dataForGoalRemovePopup.goalCode = goalcode;
                $scope.dataForGoalRemovePopup.goalId = goalid;

                $mdDialog.show({
                    locals: { dataToPass: $scope.dataForGoalRemovePopup },
                    controller: GoalRemoveController,
                    templateUrl: 'removegoal.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                .then(function () {
                    SweetAlert.swal("Tax Saving Removed Successfully!!", "", "success");
                    $rootScope.showLoader();
                    $scope.bindGoals();
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    //$scope.status = 'You cancelled the dialog.';
                });
            };

            function GoalRemoveController($scope, $mdDialog, dataToPass, SweetAlert, errorConstants, userService) {

                $scope.isGoalRemoving = false;
                $scope.goalImg = "../../../../assets/img/Goal/icon/" + dataToPass.goalCode + ".png";
                $scope.goalName = "Your " + dataToPass.goalName + ".";

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $scope.isGoalRemoving = false;
                    $mdDialog.cancel();
                };

                $scope.removeGoal = function () {
                    $scope.isGoalRemoving = true;
                    userService.callGoalDeleteApi(dataToPass.goalId).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.isGoalRemoving = false;
                                $mdDialog.hide();
                            }
                            else {
                                $scope.isGoalRemoving = false;
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $scope.isGoalRemoving = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                };
            }

            $scope.bindGoals();
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));