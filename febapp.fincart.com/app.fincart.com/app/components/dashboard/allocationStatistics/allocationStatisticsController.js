﻿(function (finApp) {
    'use strict';
    finApp.controller('allocationStatisticsController', ['$scope', '$state', '$rootScope', 'SweetAlert', 'errorConstants', 'oauthTokenFactory', 'userService', 'investFullSummaryListModel', function ($scope, $state, $rootScope, SweetAlert, errorConstants, oauthTokenFactory, userService, investFullSummaryListModel) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Full Allocation Summary";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = "overview";

            $scope.isData = true;
            $scope.FullSummaryList = [];

            $rootScope.showLoader();

            userService.callFullSummaryApi().then(function (serverResp) {
                if (serverResp.status) {
                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                        $scope.FullSummaryList = investFullSummaryListModel.parseInvestFullSummaryList(serverResp.data);

                        $scope.options = {
                            chart: {
                                type: 'pieChart',
                                height: 310,
                                x: function (d) { return d.key; },
                                y: function (d) { return d.y; },
                                showLabels: true,
                                duration: 500,
                                labelThreshold: 0.01,
                                labelSunbeamLayout: false,
                                donut: false,
                                growOnHover: true,
                                legend: {
                                    margin: {
                                        top: 5,
                                        right: 35,
                                        bottom: 5,
                                        left: 0
                                    }
                                }
                            }
                        };

                        $scope.getdata = function (allocation) {
                            return [
                                  {
                                      key: "Equity (" + allocation.equityPerc + "%)",
                                      y: allocation.equityCurrValue
                                  },
                                  {
                                      key: "Debt (" + allocation.debtPerc + "%)",
                                      y: allocation.debtCurrValue
                                  },
                                  {
                                      key: "Hybrid (" + allocation.hybridPerc + "%)",
                                      y: allocation.hybridCurrValue
                                  }
                            ]
                        };

                        if ($scope.FullSummaryList.fullSummaryList.length < 1) {
                            $scope.isData = false;
                        }

                        $rootScope.hideLoader();
                    }
                    else {
                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                    }
                }
                else {
                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                }

            });

        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));