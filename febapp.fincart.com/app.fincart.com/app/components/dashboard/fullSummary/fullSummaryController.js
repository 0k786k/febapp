﻿(function (finApp) {
    'use strict';
    finApp.controller('fullSummaryController', ['$scope', '$state', '$rootScope', 'SweetAlert', 'errorConstants', 'oauthTokenFactory', 'userService', 'investFullSummaryListModel', 'investShortSummaryModel', function ($scope, $state, $rootScope, SweetAlert, errorConstants, oauthTokenFactory, userService, investFullSummaryListModel, investShortSummaryModel) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Full Investment Summary";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = "overview";

            $scope.FullSummaryList = [];
            $scope.ShortSummary = [];

            $rootScope.showLoader();

            userService.callFullSummaryApi().then(function (serverResp) {
                if (serverResp.status) {
                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                        $scope.FullSummaryList = investFullSummaryListModel.parseInvestFullSummaryList(serverResp.data);
                        userService.callOverviewSummaryApi().then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $scope.ShortSummary = investShortSummaryModel.parseInvestShortSummary(serverResp.data);
                                    $rootScope.hideLoader();
                                }
                                else {
                                    $rootScope.hideLoader();
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                }
                            }
                            else {
                                $rootScope.hideLoader();
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });

                    }
                    else {
                        $rootScope.hideLoader();
                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                    }
                }
                else {
                    $rootScope.hideLoader();
                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                }

            });


        }
        else {
            $state.go('login', { type: 'login' });
        }

    }]);
})(angular.module('fincartApp'));