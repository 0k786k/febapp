﻿(function (finApp) {
    'use strict';
    finApp.controller('overviewController', ['$scope', '$state', '$rootScope', '$sce', 'SweetAlert', 'errorConstants', 'dashboardConstants', 'oauthTokenFactory', 'userFactory', 'userService', 'investShortSummaryModel', 'UserAllGoalsListModel', 'dashboardAlertsModel', function ($scope, $state, $rootScope, $sce, SweetAlert, errorConstants, dashboardConstants, oauthTokenFactory, userFactory, userService, investShortSummaryModel, UserAllGoalsListModel, dashboardAlertsModel) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Dashboard";
            $rootScope.mainheaderimg = "../../../../assets/img/Menu/Icon/Dashboard.png";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");

            $scope.ShortSummary = [];
            $scope.AllGoal = [];
            $scope.investedGoals = [];
            $scope.createdGoals = [];
            $scope.isDataLoaded = false;
            $scope.approachedGoals = [];
            $scope.impactStories = dashboardConstants.impactStorySlider;
            $scope.userdetails = userFactory.getUserLoggedInDetails();

            $rootScope.showLoader();

            userService.callOverviewSummaryApi().then(function (serverResp) {
                if (serverResp.status) {
                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                        $scope.ShortSummary = investShortSummaryModel.parseInvestShortSummary(serverResp.data);
                        $scope.bindGoals();
                    }
                    else {
                        $rootScope.hideLoader();
                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                    }
                }
                else {
                    $rootScope.hideLoader();
                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                }

            });

            $scope.bindGoals = function () {
                userService.callFetchAllGoalApi().then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.AllGoal = UserAllGoalsListModel.parseUserAllGoalsList(serverResp.data);

                            angular.forEach(dashboardConstants.dashboardGoalSlider, function (value, key) {
                                if (value.code.trim().toUpperCase() != 'FG12' && value.code.trim().toUpperCase() != 'FG13' && value.code.trim().toUpperCase() != 'FG14') {
                                    if ($scope.AllGoal.userAllGoals.filter(function (obj) { return obj.goalCode.trim().toUpperCase() === value.code.trim().toUpperCase() }).length>0) {
                                        //$scope.approachedGoals.splice($scope.approachedGoals.findIndex(function (obj) { return obj.code.trim().toUpperCase() === value.goalCode.trim().toUpperCase() }), 1);
                                    }
                                    else {
                                        $scope.approachedGoals.push(value);
                                    }
                                }
                                else {
                                    $scope.approachedGoals.push(value);
                                }
                            });

                            $scope.investedGoals = $scope.AllGoal.userAllGoals.filter(function (obj) { return parseFloat(obj.investedAmt) > 0.0; });
                            $scope.createdGoals = $scope.AllGoal.userAllGoals.filter(function (obj) { return parseFloat(obj.investedAmt) < 1.0; });

                            $scope.slickConfigInvestedGoal = {
                                dots: true,
                                autoplay: true,
                                infinite: true,
                                autoplaySpeed: 2000,
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                method: {}
                            };

                            $scope.slickConfigCreatedUGoal = {
                                dots: true,
                                autoplay: true,
                                infinite: true,
                                autoplaySpeed: 2000,
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                method: {}
                            };

                            $scope.slickConfigCreatedDGoal = {
                                dots: true,
                                autoplay: true,
                                infinite: true,
                                autoplaySpeed: 2000,
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                method: {}
                            };

                            $scope.slickConfigStartUGoal = {
                                dots: true,
                                autoplay: true,
                                infinite: true,
                                autoplaySpeed: 2000,
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                method: {}
                            };

                            $scope.slickConfigStartDGoal = {
                                dots: true,
                                autoplay: true,
                                infinite: true,
                                autoplaySpeed: 2000,
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                method: {}
                            };

                            $scope.slickConfigImpactStory = {
                                dots: true,
                                autoplay: true,
                                infinite: true,
                                autoplaySpeed: 2000,
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                method: {}
                            };

                            $scope.bindDashboardNotifications();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }

                });
            }

            $scope.bindDashboardNotifications = function () {
                userService.callDashboardNotificationsApi().then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.dashboardNotifications = dashboardAlertsModel.parseDashboardAlerts(serverResp.data);
                            userFactory.setsipNotifyDetails($scope.dashboardNotifications.sysmSipTopReminders);
                            userFactory.settotalcartDetails($scope.dashboardNotifications.cartCount);
                            $rootScope.sipNotifyData = userFactory.getsipNotifyDetails().sipNotifyData;
                            $rootScope.cartTotalCount = userFactory.gettotalcartDetails().cartTotalCount;
                            $scope.isDataLoaded = true;
                            $rootScope.hideLoader();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }

                });
            }

            $scope.gotonewgoal = function (ev, goalcode) {
                if (goalcode.toUpperCase() == 'FG309') {
                    $state.go('dashboard.savingaccount');
                }
                else if (goalcode.toUpperCase() == 'FG104') {
                    //$state.go('dashboard.setgoal', { 'goalcode': goalcode.toUpperCase(), 'goalid': 0 });
                }
                else {
                    $state.go('dashboard.setgoal', { 'goalcode': goalcode.toUpperCase(), 'goalid': 0 });
                }
            }

            $scope.validateurl = function (url) {
                return $sce.trustAsResourceUrl(url);
            }

            $scope.completeProfile = function () {
                if ($scope.userdetails.kycstatus.trim().toLowerCase() != 'pending' && $scope.userdetails.cafstatus.trim().toLowerCase() == 'success') {
                    $state.go('dashboard.profile');
                }
                else {
                    $rootScope.prmBasicId = $scope.userdetails.basicid;
                    $state.go('dashboard.account');
                }
            }

        }
        else {
            $state.go('login', { type: 'login' });
        }

    }]);
})(angular.module('fincartApp'));