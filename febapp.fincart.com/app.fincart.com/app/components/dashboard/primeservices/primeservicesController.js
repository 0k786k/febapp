﻿(function (finApp) {
    'use strict';
    finApp.controller('primeservicesController', ['$scope', '$state', '$rootScope', '$sce', 'SweetAlert', 'errorConstants', 'dashboardConstants', 'oauthTokenFactory', 'userFactory', 'userService', 'investShortSummaryModel', 'UserAllGoalsListModel', 'dashboardAlertsModel', function ($scope, $state, $rootScope, $sce, SweetAlert, errorConstants, dashboardConstants, oauthTokenFactory, userFactory, userService, investShortSummaryModel, UserAllGoalsListModel, dashboardAlertsModel) {

        if (oauthTokenFactory.getisloggedin()) {
            $rootScope.mainheader = "Prime Services";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");

        }
        else {
            $state.go('login', { type: 'login' });
        }

    }]);
})(angular.module('fincartApp'));