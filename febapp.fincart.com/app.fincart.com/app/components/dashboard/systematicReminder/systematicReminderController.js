﻿(function (finApp) {
    'use strict';
    finApp.controller('systematicReminderController', ['$scope', '$state', '$rootScope', 'SweetAlert', 'errorConstants', 'oauthTokenFactory', 'userFactory', 'systematicRemindersListModel', 'userService', function ($scope, $state, $rootScope, SweetAlert, errorConstants, oauthTokenFactory, userFactory, systematicRemindersListModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "All Systematic Reminder's";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = "overview";

            $scope.upcomingSysTxn = [];

            $rootScope.showLoader();

            userService.callSystematicRemindersApi(userFactory.getUserLoggedInDetails().basicid).then(function (serverResp) {
                if (serverResp.status) {
                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                        $scope.upcomingSysTxn = systematicRemindersListModel.parseSystematicRemindersList(serverResp.data);
                        $rootScope.hideLoader();
                    }
                    else {
                        $rootScope.hideLoader();
                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                    }
                }
                else {
                    $rootScope.hideLoader();
                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                }
            });

        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));