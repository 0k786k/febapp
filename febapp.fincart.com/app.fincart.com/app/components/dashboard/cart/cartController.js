﻿(function (finApp) {
    'use strict';
    finApp.controller('cartController', ['$scope', '$state', '$window', '$stateParams', '$rootScope', '$mdDialog', '$mdBottomSheet', '$filter', 'oauthTokenFactory', 'userFactory', 'transactionFactory', 'SweetAlert', 'errorConstants', 'transactionConstants', 'allMemberListModel', 'userCartDetailsListModel', 'billdeskPaymentModeListModel', 'cartPurchaseAttemptModel', 'userService', function ($scope, $state, $window, $stateParams, $rootScope, $mdDialog, $mdBottomSheet, $filter, oauthTokenFactory, userFactory, transactionFactory, SweetAlert, errorConstants, transactionConstants, allMemberListModel, userCartDetailsListModel, billdeskPaymentModeListModel, cartPurchaseAttemptModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Cart";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");

            $rootScope.showLoader();
            $scope.isMemberLoaded = false;
            $scope.isCartLoaded = false;

            $scope.AllMemberList = []
            $scope.MemberList = [];
            $scope.ProfileList = [];
            $scope.BankList = [];
            $scope.PaymentModesList = [];
            $scope.CartList = [];
            $scope.CartIdList = [];
            $scope.totalAmt = 0;
            $scope.dataForCartRemovePopup = { schemeName: '', cartId: 0 };
            $scope.dataForPaymentGatewayPopup = { cartPurchaseAttempt: null, bankName: '', accountNo: '', paymentModes: [] };

            userService.callAllMemberListApi(userFactory.getUserLoggedInDetails().basicid, "0").then(function (serverResp) {
                if (serverResp.status) {
                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                        debugger;
                        $scope.AllMemberList = allMemberListModel.parseAllMemberListModel(serverResp.data);

                        $scope.MemberList = $scope.AllMemberList.memberList;
                        $scope.ddlMember = $scope.MemberList[0].basicid;

                        $scope.ProfileList = $scope.AllMemberList.profileList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                        $scope.ddlProfile = $scope.ProfileList[0].profileID;

                        $scope.BankList = $scope.AllMemberList.bankList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                        $scope.ddlBank = $scope.BankList[0].bankid;

                        $scope.isMemberLoaded = true;
                        $scope.bindPaymentModes();
                        //$rootScope.hideLoader();

                    }
                    else {
                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        $rootScope.hideLoader();
                    }
                }
                else {
                    $rootScope.hideLoader();
                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                }
            });

            $scope.bindPaymentModes = function () {
                userService.callPaymentModesApi($scope.ddlBank).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.PaymentModesList = billdeskPaymentModeListModel.parseBilldeskPaymentModeList(serverResp.data);
                            $scope.bindCart();
                            //$rootScope.hideLoader();
                        }
                        else {
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            $rootScope.hideLoader();
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });
            }

            $scope.bindCart = function () {
                userService.callCartApi($scope.ddlMember, $scope.ddlProfile).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.CartList = userCartDetailsListModel.parseUserCartDetailsList(serverResp.data);
                            $scope.isCartLoaded = true;
                            $rootScope.hideLoader();
                        }
                        else {
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            $rootScope.hideLoader();
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });
            }

            $scope.memberChange = function () {
                $scope.isCartLoaded = false;
                $rootScope.showLoader();

                $scope.ProfileList.length = 0
                $scope.ProfileList = $scope.AllMemberList.profileList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                $scope.ddlProfile = $scope.ProfileList[0].profileID;

                $scope.BankList.length = 0
                $scope.BankList = $scope.AllMemberList.bankList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                $scope.ddlBank = $scope.BankList[0].bankid;

                $scope.bindCart();
            };

            $scope.profileChange = function () {
                $scope.isCartLoaded = false;
                $rootScope.showLoader();
                $scope.bindCart();
            };

            $scope.toggle = function (ev, item, list) {
                var idx = list.indexOf(item);
                if (idx > -1) {
                    list.splice(idx, 1);
                    $scope.totalAmt = $scope.totalAmt - parseInt(item.amount);
                }
                else {
                    if (list.length == 5) {
                        $scope.showMaxLimitAlert(ev);
                    }
                    else {
                        list.push(item);
                        $scope.totalAmt = $scope.totalAmt + parseInt(item.amount);
                    }
                }
            };

            $scope.exists = function (item, list) {
                return list.indexOf(item) > -1;
            };

            $scope.showMaxLimitAlert = function (ev) {
                // Appending dialog to document.body to cover sidenav in docs app
                // Modal dialogs should fully cover application
                // to prevent interaction outside of dialog
                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(false)
                    .title('Max Limit Alert!')
                    .textContent('You can select only 5 cart items to transact in one shot.')
                    .ariaLabel('Max Limit Alert')
                    .ok('Got it!')
                    .targetEvent(ev)
                );
            };

            $scope.showPaymentPopup = function (ev) {

                $scope.dataForPaymentGatewayPopup.bankName = $scope.BankList[0].bankName;
                $scope.dataForPaymentGatewayPopup.accountNo = $scope.BankList[0].accountNo;
                $scope.dataForPaymentGatewayPopup.paymentModes = $scope.PaymentModesList.PaymentModeList;

                $mdBottomSheet.show({
                    locals: { dataToPass: $scope.dataForPaymentGatewayPopup },
                    templateUrl: 'payment-popup-template.html',
                    controller: PaymentPopupCtrl,
                    clickOutsideToClose: true
                }).then(function (selectedMode) {

                    var cartIds = "";
                    angular.forEach($scope.CartIdList, function (value, key) {
                        cartIds = cartIds + "_" + value.cartId;
                    });

                    cartIds = cartIds.substr(1);

                    for (var i = $scope.CartIdList.length + 1; i <= 5; i++) {
                        cartIds = cartIds + "_NA";
                    }

                    var cartPurchaseAttempt = cartPurchaseAttemptModel.CartPurchaseAttempt;
                    cartPurchaseAttempt.basicID = $scope.ddlMember;
                    cartPurchaseAttempt.ProfileID = $scope.ddlProfile;
                    cartPurchaseAttempt.trxntblId = 0;
                    cartPurchaseAttempt.cartIdBunch = cartIds;
                    cartPurchaseAttempt.paymodeId = selectedMode.payModeId;
                    cartPurchaseAttempt.returnUrl = userService.callPaymentReturnUrlApi();
                    cartPurchaseAttempt.productId = selectedMode.productId;
                    $scope.openPopUp(ev, '', 0, cartPurchaseAttempt, 'redirectPay');

                }).catch(function (error) {
                    // User clicked outside or hit escape
                });
            };

            function PaymentPopupCtrl($scope, $mdBottomSheet, dataToPass) {

                $scope.selectedMode = null;
                $scope.bankName = dataToPass.bankName;
                $scope.accountNo = dataToPass.accountNo;
                $scope.items = dataToPass.paymentModes.filter(function (obj) { return obj.status.trim().toUpperCase() == 'Y'; });

                $scope.listItemClick = function (item) {
                    $scope.selectedMode = item;
                };

                $scope.checkedMode = function (item) {
                    if (item == $scope.selectedMode) {
                        return true;
                    }
                    else {
                        return false;
                    }
                };

                $scope.payment = function () {
                    $mdBottomSheet.hide($scope.selectedMode);
                };
            }

            $scope.openPopUp = function (ev, schName, cartId, cartPurchaseAttempt, popupType) {

                $scope.dataForCartRemovePopup.schemeName = schName;
                $scope.dataForCartRemovePopup.cartId = cartId;
                $scope.dataForPaymentGatewayPopup.cartPurchaseAttempt = cartPurchaseAttempt;

                $mdDialog.show({
                    locals: { dataToPass: popupType == 'removecart' ? $scope.dataForCartRemovePopup : $scope.dataForPaymentGatewayPopup },
                    controller: popupType == 'removecart' ? CartRemoveController : RedirectPaymentGatewayController,
                    templateUrl: popupType == 'removecart' ? 'removecart.tmpl.html' : 'redirectPaymentGateway.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                .then(function () {
                    SweetAlert.swal("Scheme Removed Successfully!!", "", "success");
                    $rootScope.showLoader();
                    $scope.bindCart();
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    //$scope.status = 'You cancelled the dialog.';
                });
            };

            function CartRemoveController($scope, $mdDialog, $filter, dataToPass, SweetAlert, errorConstants, userFactory, userService) {

                $scope.isCartRemoving = false;
                $scope.schemeName = dataToPass.schemeName;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $scope.isCartRemoving = false;
                    $mdDialog.cancel();
                };

                $scope.removeCart = function () {
                    $scope.isCartRemoving = true;
                    userService.callRemoveCartApi(dataToPass.cartId).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                userFactory.decrementcart(1);
                                $rootScope.cartTotalCount = userFactory.gettotalcartDetails().cartTotalCount;
                                $scope.isCartRemoving = false;
                                $mdDialog.hide();
                            }
                            else {
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                $scope.isCartRemoving = false;
                            }
                        }
                        else {
                            $scope.isCartRemoving = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                };
            }

            function RedirectPaymentGatewayController($scope, $mdDialog, $window, dataToPass, transactionConstants, userService, transactionFactory) {

                userService.callPurchaseAttemptApi(dataToPass.cartPurchaseAttempt).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {

                            transactionFactory.setCurrentBunchTxnId(serverResp.data);

                            userService.callBilldeskPostStringApi(transactionFactory.getCurrentBunchTxnId()).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {

                                        var redirectUrl = serverResp.data;

                                        userService.callAuthTokenOnServerApi(oauthTokenFactory.getUserToken().access_token).then(function (serverResp) {
                                            if (serverResp.status) {
                                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                    $window.location.href = redirectUrl;
                                                }
                                                else {
                                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                    $scope.cancel();
                                                }
                                            }
                                            else {
                                                $scope.cancel();
                                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                            }
                                        });

                                    }
                                    else {
                                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        $scope.cancel();
                                    }
                                }
                                else {
                                    $scope.cancel();
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });

                        }
                        else {
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            $scope.cancel();
                        }
                    }
                    else {
                        $scope.cancel();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });

                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
            }
        }
        else {
            $state.go('login', { type: 'login' });
        }

    }]);
})(angular.module('fincartApp'));