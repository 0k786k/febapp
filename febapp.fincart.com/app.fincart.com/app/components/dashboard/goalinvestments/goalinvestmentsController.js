﻿(function (finApp) {
    'use strict';
    finApp.controller('goalinvestmentsController', ['$scope', '$state', '$window', '$stateParams', '$rootScope', '$mdDialog', '$mdPanel', '$filter', 'oauthTokenFactory', 'userFactory', 'SweetAlert', 'errorConstants', 'transactionConstants', 'allMemberListModel', 'allInvestmentsListModel', 'validDateListModel', 'addToCartModel', 'otherTransactionV2Model', 'fundsWithCategoryModel', 'searchSchemesModel', 'searchSchemesResultListModel', 'userService', function ($scope, $state, $window, $stateParams, $rootScope, $mdDialog, $mdPanel, $filter, oauthTokenFactory, userFactory, SweetAlert, errorConstants, transactionConstants, allMemberListModel, allInvestmentsListModel, validDateListModel, addToCartModel, otherTransactionV2Model, fundsWithCategoryModel, searchSchemesModel, searchSchemesResultListModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Goal Investments";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = "";
            $scope.isInvestmentsLoaded = false;
            $scope.goalName = "";
            $scope.goalImg = "";

            $scope.morefunds = function () {
                $state.go('dashboard.searchScheme', { goalid: $stateParams.goalid });
            }

            $scope.more = {
                items: [
                  'Transact'//,
                  //'View Details'
                ]
            };

            $scope.menuTemplate = '' +
            '<div class="menu-panel" md-whiteframe="4" style="background-color: #fff;">' +
            '  <div class="menu-content">' +
            '    <div class="menu-item" ng-repeat="item in ctrl.items">' +
            '      <button class="md-button" ng-click="menuAction($event,item,ctrl.investment)">' +
            '        <span>{{item}}</span>' +
            '      </button>' +
            '    </div>' +
            '  </div>' +
            '</div>';

            $scope.showToolbarMenu = function ($event, menu, invst) {
                var template = $scope.menuTemplate;

                var position = $mdPanel.newPanelPosition()
                    .relativeTo($event.target)
                    .addPanelPosition(
                      $mdPanel.xPosition.ALIGN_START,
                      $mdPanel.yPosition.BELOW
                    );

                var config = {
                    //id: 'toolbar_' + menu.name,
                    attachTo: angular.element(document.body),
                    controller: PanelMenuCtrl,
                    controllerAs: 'ctrl',
                    template: template,
                    position: position,
                    panelClass: 'menu-panel-container',
                    locals: {
                        items: menu.items,
                        investment: invst
                    },
                    openFrom: $event,
                    focusOnOpen: false,
                    zIndex: 100,
                    propagateContainerEvents: true,
                    clickOutsideToClose: true,
                    groupName: ['toolbar', 'menus']
                };

                $mdPanel.open(config);
            };

            function PanelMenuCtrl($scope, mdPanelRef, $mdDialog) {
                $scope.menuAction = function (ev, item, invest) {
                    //alert(item + "--" + invest.schemeId);
                    mdPanelRef && mdPanelRef.close();
                    $scope.openPopUp(ev, item, invest)
                };

                $scope.openPopUp = function (ev, item, invst) {
                    if (item.trim().toLowerCase() == "transact") {
                        $mdDialog.show({
                            locals: { dataToPass: invst },
                            controller: txnController,
                            templateUrl: 'transaction.tmpl.html',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: false,
                            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                        })
                    .then(function (val) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function () {
                        //$scope.status = 'You cancelled the dialog.';
                    });
                    }
                    else {

                    }
                };
            }

            $scope.loadInvestments = function () {
                $rootScope.showLoader();
                userService.callGoalInvestmentsApi($stateParams.goalid).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {

                            $scope.AllInvestments = allInvestmentsListModel.parseAllInvestmentsList(serverResp.data);

                            if ($scope.AllInvestments.allInvestments.length > 0) {
                                if ($scope.AllInvestments.allInvestments[0].goalList.length > 0) {

                                    var goal = $scope.AllInvestments.allInvestments[0].goalList.filter(function (obj) { return obj.userGoalId == $stateParams.goalid; })[0];
                                    if (goal) {
                                        $scope.goalName = goal.goalName;
                                        $scope.goalImg = goal.goalImg;

                                        if (goal.typeCode) {
                                            if (goal.typeCode.trim().toUpperCase() == 'FG220') {
                                                $rootScope.currActiveTab = "quickSip";
                                                $rootScope.mainheader = $scope.goalName + " Investments";
                                                $rootScope.mainheaderimg = $scope.goalImg;
                                            }
                                            else {
                                                $rootScope.mainheader = $scope.goalName + " Goal Investments";
                                                $rootScope.mainheaderimg = $scope.goalImg;
                                                $rootScope.currActiveTab = "goalFullView";
                                            }
                                        }
                                        else {
                                            $rootScope.mainheader = $scope.goalName + " Goal Investments";
                                            $rootScope.mainheaderimg = $scope.goalImg;
                                            $rootScope.currActiveTab = "goalFullView";
                                        }
                                    }

                                    $scope.isInvestmentsLoaded = true;
                                }
                            }

                            $rootScope.hideLoader();
                        }
                        else {
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            $rootScope.hideLoader();
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });
            }

            function txnController($scope, $mdDialog, $filter, dataToPass, SweetAlert, errorConstants, transactionConstants, userFactory, allMemberListModel, validDateListModel, addToCartModel, otherTransactionV2Model, fundsWithCategoryModel, searchSchemesResultListModel, userService) {

                $scope.isLoading = false;
                $scope.isProcessing = false;
                $scope.isSchemeAdded = false;
                $scope.chkState = false;
                $scope.ValidDateList = [];
                $scope.selectedTab = "onetime";
                $scope.noOfInstallment = transactionConstants.installments;
                $scope.swp_Installments = transactionConstants.swp_Installments;
                $scope.stp_Installments = transactionConstants.stp_Installments
                $scope.schemeName = dataToPass.schemeName;
                $scope.goals = dataToPass.goalList;
                $scope.ddlUserGoalId = $scope.goals[0].userGoalId;
                $scope.availAmt = dataToPass.currValue;
                $scope.availUnit = dataToPass.balUnits;
                $scope.addMinAmt = dataToPass.addminAmt;
                $scope.addMaxAmt = dataToPass.addMaxAmt;
                $scope.sipMinAmt = dataToPass.sipminAmt;
                $scope.sipMaxAmt = dataToPass.sipmaxAmt;
                $scope.bankName = dataToPass.bankName;
                $scope.bankAccNo = dataToPass.bankAccNo;
                $scope.FundsWithCategoryList = [];
                $scope.FundList = [];
                $scope.ObjectiveList = [];
                $scope.SubObjectiveList = [];
                $scope.schemeList = [];
                $scope.ddlSwitchInScheme = "";

                $scope.loadFundCategory = function (isStp) {

                    $scope.isLoading = true;

                    userService.callFundsWithCategoryApi($scope.ddlUserGoalId).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {

                                $scope.FundsWithCategoryList = fundsWithCategoryModel.parseFundsWithCategory(serverResp.data);
                                $scope.FundList = $scope.FundsWithCategoryList.funds;
                                $scope.ObjectiveList = $scope.FundsWithCategoryList.obj;
                                $scope.SubObjectiveList = $scope.FundsWithCategoryList.subObj;

                                $scope.ddlObjective = $scope.ObjectiveList[0].objName;
                                $scope.loadSchemes(isStp);
                                //$scope.isLoading = false;
                            }
                            else {
                                $scope.isLoading = false;
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $scope.isLoading = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }

                $scope.loadSchemes = function (isStp) {
                    var srchScheme = searchSchemesModel.SearchSchemes;
                    srchScheme.fundid = dataToPass.fundid;
                    srchScheme.obj = $scope.ddlObjective;
                    srchScheme.subObj = "";
                    srchScheme.schemeName = "";
                    srchScheme.userGoalId = $scope.ddlUserGoalId;

                    userService.callSearchSchemesApi(srchScheme).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {

                                $scope.schemeList = searchSchemesResultListModel.parseSearchSchemesResultList(serverResp.data);

                                if ($scope.schemeList.searchSchemesResult.length > 0) {
                                    $scope.ddlSwitchInScheme = $scope.schemeList.searchSchemesResult[0].schemeId;
                                }
                                else {
                                    $scope.ddlSwitchInScheme = "";
                                }

                                if (isStp) {
                                    $scope.bindValidDates(dataToPass.schemeId, dataToPass.mandateId ? 'Y' : 'N', 'STP');
                                }
                                else {
                                    $scope.isLoading = false;
                                }
                            }
                            else {
                                $scope.isLoading = false;
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $scope.isLoading = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }

                $scope.fundCategoryChange = function () {
                    $scope.isLoading = true;
                    $scope.loadSchemes();
                }

                $scope.bindValidDates = function (schemeId, mandatestatus, txntype) {
                    $scope.isLoading = true;
                    userService.callSystematicDatesApi(schemeId, mandatestatus, txntype).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.ValidDateList = validDateListModel.parseValidDateList(serverResp.data);
                                $scope.ddlValidDate = $scope.ValidDateList.validdates[0].Day;
                                $scope.ddlInstallment = $scope.noOfInstallment[0].value;
                                $scope.ddlSwpInstallment = $scope.swp_Installments[0].value;
                                $scope.ddlStpInstallment = $scope.stp_Installments[0].value;
                                $scope.isLoading = false;
                            }
                            else {
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                $scope.isLoading = false;
                            }
                        }
                        else {
                            $scope.isLoading = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }

                $scope.hide = function (val) {
                    $scope.isLoading = false;
                    $scope.isProcessing = false;
                    $mdDialog.hide(val);
                };

                $scope.cancel = function () {
                    $scope.isSchemeAdded = false;
                    $mdDialog.cancel();
                };

                $scope.tabClick = function (currtab) {
                    $scope.selectedTab = currtab;
                    $scope.txtAmount = "";

                    if (currtab == "monthly") {
                        $scope.bindValidDates(dataToPass.schemeId, dataToPass.mandateId ? 'Y' : 'N', 'SIP');
                    }
                    else if (currtab == "swp") {
                        $scope.bindValidDates(dataToPass.schemeId, dataToPass.mandateId ? 'Y' : 'N', 'SWP');
                    }
                    else if (currtab == "switch") {
                        $scope.loadFundCategory(false);
                    }
                    else if (currtab == "stp") {
                        $scope.loadFundCategory(true);
                    }
                }

                $scope.allAmt = function () {
                    if ($scope.chkState) {
                        $scope.txtAmount = parseFloat($scope.availAmt);
                    }
                    else {
                        $scope.txtAmount = "";
                    }
                }

                $scope.txtAmtChange = function () {
                    if ($scope.txtAmount == parseFloat($scope.availAmt)) {
                        $scope.chkState = true;
                    }
                    else {
                        $scope.chkState = false;
                    }
                }

                $scope.addtocart = function () {
                    if ($scope.txtAmount) {

                        if (parseFloat($scope.txtAmount) >= parseFloat($scope.addMinAmt)) {

                            if (parseFloat($scope.txtAmount) <= parseFloat($scope.addMaxAmt)) {

                                $scope.isLoading = true;
                                $scope.isProcessing = true;
                                var crt = addToCartModel.AddToCart;

                                crt.basicID = dataToPass.basicID;
                                crt.ProfileID = dataToPass.ProfileID;
                                crt.purSchemeId = dataToPass.schemeId;
                                crt.tranType = 'NOR';
                                crt.Amount = $scope.txtAmount;
                                crt.PurFolioNo = dataToPass.folioNo;
                                crt.bankId = dataToPass.bankId;
                                crt.userGoalId = $scope.ddlUserGoalId;

                                userService.callAddToCartApi(crt).then(function (serverResp) {
                                    if (serverResp.status) {
                                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                                            userFactory.incrementcart(1);
                                            $rootScope.cartTotalCount = userFactory.gettotalcartDetails().cartTotalCount;
                                            SweetAlert.swal("Scheme Added To Cart!!", "", "success");
                                            $scope.isLoading = false;
                                            $scope.isProcessing = false;
                                            $scope.isSchemeAdded = true;
                                        }
                                        else {
                                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                            $scope.isLoading = false;
                                            $scope.isProcessing = false;
                                        }
                                    }
                                    else {
                                        $scope.isLoading = false;
                                        $scope.isProcessing = false;
                                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                    }
                                });

                            }
                            else {
                                SweetAlert.swal("Max Investment Rs." + $scope.addMaxAmt, "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Min Investment Rs." + $scope.addMinAmt, "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please Enter Amount", "", "warning");
                    }
                };

                $scope.sipgenerate = function () {
                    if ($scope.txtAmount) {

                        if (parseFloat($scope.txtAmount) >= parseFloat($scope.sipMinAmt)) {

                            if (parseFloat($scope.txtAmount) <= parseFloat($scope.sipMaxAmt)) {

                                var othrcrt = otherTransactionV2Model.OtherTransactionV2;

                                othrcrt.basicID = dataToPass.basicID;
                                othrcrt.ProfileID = dataToPass.ProfileID;
                                othrcrt.purSchemeId = dataToPass.schemeId;
                                othrcrt.sellSchemeId = "";
                                othrcrt.tranType = "SIP";
                                othrcrt.PurFolioNo = dataToPass.folioNo;
                                othrcrt.SellFolioNo = "";
                                othrcrt.Amount = $scope.txtAmount;
                                othrcrt.No_of_Installment = $scope.ddlInstallment;
                                othrcrt.MDate = $scope.ddlValidDate;
                                othrcrt.userGoalId = $scope.ddlUserGoalId;
                                othrcrt.Units = "";
                                othrcrt.bankId = dataToPass.bankId;
                                othrcrt.mandateId = dataToPass.mandateId;
                                othrcrt.startDate = $scope.ValidDateList.validdates.filter(function (obj) { return obj.Day.trim().toUpperCase() === $scope.ddlValidDate.trim().toUpperCase() })[0].Date;

                                $scope.additionaltxn(othrcrt);

                            }
                            else {
                                SweetAlert.swal("Max Investment Rs." + $scope.sipMaxAmt, "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Min Investment Rs." + $scope.sipMinAmt, "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please Enter Amount", "", "warning");
                    }
                };

                $scope.sell = function () {
                    if ($scope.txtAmount) {

                        if (parseFloat($scope.txtAmount) >= 0.0) {

                            if (parseFloat($scope.txtAmount) <= parseFloat($scope.availAmt)) {

                                var othrcrt = otherTransactionV2Model.OtherTransactionV2;

                                othrcrt.basicID = dataToPass.basicID;
                                othrcrt.ProfileID = dataToPass.ProfileID;
                                //othrcrt.purSchemeId = "";
                                othrcrt.sellSchemeId = dataToPass.schemeId;
                                othrcrt.tranType = "R";
                                //othrcrt.PurFolioNo = "";
                                othrcrt.SellFolioNo = dataToPass.folioNo;
                                othrcrt.Amount = $scope.chkState ? "0" : $scope.txtAmount;
                                //othrcrt.No_of_Installment = "0";
                                //othrcrt.MDate = "";
                                othrcrt.userGoalId = $scope.ddlUserGoalId;
                                othrcrt.Units = $scope.chkState ? $scope.availUnit : "0";
                                othrcrt.bankId = dataToPass.bankId;
                                othrcrt.mandateId = dataToPass.mandateId;

                                $scope.additionaltxn(othrcrt);

                            }
                            else {
                                SweetAlert.swal("Max Investment Rs." + $scope.availAmt, "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Min Investment Rs.1", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please Enter Amount", "", "warning");
                    }
                };

                $scope.swp = function () {
                    if ($scope.txtAmount) {

                        if (parseFloat($scope.txtAmount) >= 0.0) {

                            if (parseFloat($scope.txtAmount) <= parseFloat($scope.availAmt)) {

                                var othrcrt = otherTransactionV2Model.OtherTransactionV2;

                                othrcrt.basicID = dataToPass.basicID;
                                othrcrt.ProfileID = dataToPass.ProfileID;
                                //othrcrt.purSchemeId = "";
                                othrcrt.sellSchemeId = dataToPass.schemeId;
                                othrcrt.tranType = "SWP";
                                //othrcrt.PurFolioNo = "";
                                othrcrt.SellFolioNo = dataToPass.folioNo;
                                othrcrt.Amount = $scope.chkState ? "0" : $scope.txtAmount;
                                othrcrt.No_of_Installment = $scope.ddlSwpInstallment;
                                othrcrt.MDate = $scope.ddlValidDate;
                                othrcrt.userGoalId = $scope.ddlUserGoalId;
                                othrcrt.Units = $scope.chkState ? $scope.availUnit : "0";
                                othrcrt.bankId = dataToPass.bankId;
                                othrcrt.mandateId = dataToPass.mandateId;
                                othrcrt.startDate = $scope.ValidDateList.validdates.filter(function (obj) { return obj.Day.trim().toUpperCase() === $scope.ddlValidDate.trim().toUpperCase() })[0].Date;

                                $scope.additionaltxn(othrcrt);

                            }
                            else {
                                SweetAlert.swal("Max Investment Rs." + $scope.availAmt, "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Min Investment Rs.1", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please Enter Amount", "", "warning");
                    }
                };

                $scope.switch = function () {
                    if ($scope.ddlSwitchInScheme) {
                        if ($scope.txtAmount) {

                            if (parseFloat($scope.txtAmount) >= 0.0) {

                                if (parseFloat($scope.txtAmount) <= parseFloat($scope.availAmt)) {

                                    var othrcrt = otherTransactionV2Model.OtherTransactionV2;

                                    othrcrt.basicID = dataToPass.basicID;
                                    othrcrt.ProfileID = dataToPass.ProfileID;
                                    othrcrt.purSchemeId = $scope.ddlSwitchInScheme;
                                    othrcrt.sellSchemeId = dataToPass.schemeId;
                                    othrcrt.tranType = "SWITCH";
                                    othrcrt.PurFolioNo = dataToPass.folioNo;
                                    othrcrt.SellFolioNo = dataToPass.folioNo;
                                    othrcrt.Amount = $scope.chkState ? "0" : $scope.txtAmount;
                                    //othrcrt.No_of_Installment = "0";
                                    //othrcrt.MDate = "";
                                    othrcrt.userGoalId = $scope.ddlUserGoalId;
                                    othrcrt.Units = $scope.chkState ? $scope.availUnit : "0";
                                    othrcrt.bankId = dataToPass.bankId;
                                    othrcrt.mandateId = dataToPass.mandateId;

                                    $scope.additionaltxn(othrcrt);

                                }
                                else {
                                    SweetAlert.swal("Max Investment Rs." + $scope.availAmt, "", "warning");
                                }
                            }
                            else {
                                SweetAlert.swal("Min Investment Rs.1", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Please Enter Amount", "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please Select Switch-In Scheme", "", "warning");
                    }
                };

                $scope.stp = function () {
                    if ($scope.ddlSwitchInScheme) {
                        if ($scope.txtAmount) {

                            if (parseFloat($scope.txtAmount) >= 0.0) {

                                if (parseFloat($scope.txtAmount) <= parseFloat($scope.availAmt)) {

                                    var othrcrt = otherTransactionV2Model.OtherTransactionV2;

                                    othrcrt.basicID = dataToPass.basicID;
                                    othrcrt.ProfileID = dataToPass.ProfileID;
                                    othrcrt.purSchemeId = $scope.ddlSwitchInScheme;
                                    othrcrt.sellSchemeId = dataToPass.schemeId;
                                    othrcrt.tranType = "STP";
                                    othrcrt.PurFolioNo = dataToPass.folioNo;
                                    othrcrt.SellFolioNo = dataToPass.folioNo;
                                    othrcrt.Amount = $scope.chkState ? "0" : $scope.txtAmount;
                                    othrcrt.No_of_Installment = $scope.ddlStpInstallment;
                                    othrcrt.MDate = $scope.ddlValidDate;
                                    othrcrt.userGoalId = $scope.ddlUserGoalId;
                                    othrcrt.Units = $scope.chkState ? $scope.availUnit : "0";
                                    othrcrt.bankId = dataToPass.bankId;
                                    othrcrt.mandateId = dataToPass.mandateId;
                                    othrcrt.startDate = $scope.ValidDateList.validdates.filter(function (obj) { return obj.Day.trim().toUpperCase() === $scope.ddlValidDate.trim().toUpperCase() })[0].Date;

                                    $scope.additionaltxn(othrcrt);

                                }
                                else {
                                    SweetAlert.swal("Max Investment Rs." + $scope.availAmt, "", "warning");
                                }
                            }
                            else {
                                SweetAlert.swal("Min Investment Rs.1", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Please Enter Amount", "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please Select Switch-In Scheme", "", "warning");
                    }
                };

                $scope.additionaltxn = function (othrcrt) {
                    $scope.isLoading = true;
                    $scope.isProcessing = true;
                    userService.callOtherTransactionApi(othrcrt).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {

                                othrcrt.Amount = parseFloat(othrcrt.Amount) > 0.0 ? "Rs." + othrcrt.Amount : "all amount";

                                if (othrcrt.tranType == "SIP") {
                                    var sipDate = $scope.ValidDateList.validdates.filter(function (obj) { return obj.Day == $scope.ddlValidDate; });
                                    SweetAlert.swal("SIP of " + othrcrt.Amount + " is registered for '" + $scope.schemeName + "'. Amount will be deducted from " + $filter('date')(sipDate[0].Date, 'medium', '+0530').replace(' 12:00:00 AM', '') + ".", "", "success");
                                }
                                else if (othrcrt.tranType == "R") {
                                    SweetAlert.swal("Redemption of " + othrcrt.Amount + " is registered from '" + $scope.schemeName + "'. Amount will be credited to (" + $scope.bankName + " - " + $scope.bankAccNo + ") within 3 working days.", "", "success");
                                }
                                else if (othrcrt.tranType == "SWP") {
                                    var swpDate = $scope.ValidDateList.validdates.filter(function (obj) { return obj.Day == $scope.ddlValidDate; });
                                    SweetAlert.swal("SWP of " + othrcrt.Amount + " is registered from '" + $scope.schemeName + "'. Amount will be credited to (" + $scope.bankName + " - " + $scope.bankAccNo + ") from " + $filter('date')(swpDate[0].Date, 'medium', '+0530').replace(' 12:00:00 AM', '') + " to next " + $scope.swp_Installments.filter(function (obj) { return obj.value == parseInt($scope.ddlSwpInstallment); })[0].name + ".", "", "success");
                                }
                                else if (othrcrt.tranType == "SWITCH") {
                                    SweetAlert.swal("SWITCH of " + othrcrt.Amount + " is registered from '" + $scope.schemeName + "'. Amount will be switched to '" + $scope.schemeList.searchSchemesResult.filter(function (obj) { return obj.schemeId == $scope.ddlSwitchInScheme; })[0].schemeName + "' within 3 working days.", "", "success");
                                }
                                else if (othrcrt.tranType == "STP") {
                                    var stpDate = $scope.ValidDateList.validdates.filter(function (obj) { return obj.Day == $scope.ddlValidDate; });
                                    SweetAlert.swal("STP of " + othrcrt.Amount + " is registered from '" + $scope.schemeName + "'. Amount will be switched to '" + $scope.schemeList.searchSchemesResult.filter(function (obj) { return obj.schemeId == $scope.ddlSwitchInScheme; })[0].schemeName + "' from " + $filter('date')(stpDate[0].Date, 'medium', '+0530').replace(' 12:00:00 AM', '') + " to next " + $scope.swp_Installments.filter(function (obj) { return obj.value == parseInt($scope.ddlSwpInstallment); })[0].name + ".", "", "success");
                                }
                                $scope.hide(othrcrt.tranType);
                            }
                            else {
                                if (othrcrt.tranType == "R") {
                                    SweetAlert.swal("Previous Redemption in '" + $scope.schemeName + "' already in process", "", "warning");
                                }
                                else if (othrcrt.tranType == "SWP") {
                                    SweetAlert.swal("Previous SWP in '" + $scope.schemeName + "' already in process", "", "warning");
                                }
                                else if (othrcrt.tranType == "SWITCH") {
                                    SweetAlert.swal("Previous SWITCH in '" + $scope.schemeName + "' already in process", "", "warning");
                                }
                                else if (othrcrt.tranType == "STP") {
                                    SweetAlert.swal("Previous STP in '" + $scope.schemeName + "' already in process", "", "warning");
                                }
                                else {
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                }
                                $scope.isLoading = false;
                                $scope.isProcessing = false;
                            }
                        }
                        else {
                            $scope.isLoading = false;
                            $scope.isProcessing = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                };
            }

            $scope.loadInvestments();
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));