﻿(function (finApp) {
    'use strict';
    finApp.controller('masterController', ['$scope', '$state', '$rootScope', 'oauthTokenFactory', 'userFactory', function ($scope, $state, $rootScope, oauthTokenFactory, userFactory) {
        if (oauthTokenFactory.getisloggedin()) {
            $scope.userdetails = userFactory.getUserLoggedInDetails();
            $rootScope.grpLdrProfilePic = $scope.userdetails.profilepic;
            $rootScope.sipNotifyData = userFactory.getsipNotifyDetails().sipNotifyData;
            $rootScope.cartTotalCount = userFactory.gettotalcartDetails().cartTotalCount;
            $rootScope.userCafStatus = $scope.userdetails.cafstatus.trim().toLowerCase();

            $scope.gotoProfile = function () {
                if ($scope.userdetails.kycstatus.trim().toLowerCase() != 'pending' && $scope.userdetails.cafstatus.trim().toLowerCase() == 'success') {
                    $state.go('dashboard.profile');
                }
                else {
                    $rootScope.prmBasicId = $scope.userdetails.basicid;
                    $state.go('dashboard.account');
                }
            }
        }
        else {
            $state.go('login');
        }
    }]);
})(angular.module('fincartApp'));