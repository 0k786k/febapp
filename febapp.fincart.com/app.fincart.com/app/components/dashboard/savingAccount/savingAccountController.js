﻿(function (finApp) {
    'use strict';
    finApp.controller('savingAccountController', ['$scope', '$state', '$rootScope', '$mdPanel', '$mdDialog', '$q', '$filter', 'SweetAlert', 'errorConstants', 'kycCafConstants', 'oauthTokenFactory', 'userFactory', 'allMemberListModel', 'addGoalModel', 'bestSaverSchemeDetailsModel', 'addToCartModel', 'otherTransactionV2Model', 'userService', function ($scope, $state, $rootScope, $mdPanel, $mdDialog, $q, $filter, SweetAlert, errorConstants, kycCafConstants, oauthTokenFactory, userFactory, allMemberListModel, addGoalModel, bestSaverSchemeDetailsModel, addToCartModel, otherTransactionV2Model, userService) {

        if (oauthTokenFactory.getisloggedin()) {
            $rootScope.mainheader = "Best Saver Account";
            $rootScope.mainheaderimg = "../../../../assets/img/Menu/Icon/BestSaverAccount.png";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");

            $scope.goalImg = "../../../../assets/img/Goal/image/FG309.png";
            $scope.goalIcon = "../../../../assets/img/Goal/icon/FG309.png";

            $scope.userdetails = userFactory.getUserLoggedInDetails();
            $scope.isMemberLoaded = false;
            $scope.AllMemberList = []
            $scope.MemberList = [];
            $scope.schemeData = null;
            $scope.isInstaShow = false;

            $scope.bindMemberList = function () {
                if ($scope.userdetails.cafstatus.trim().toLowerCase() == 'success') {
                    $rootScope.showLoader();
                    userService.callAllMemberListApi($scope.userdetails.basicid, "0").then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.AllMemberList = allMemberListModel.parseAllMemberListModel(serverResp.data);
                                $scope.MemberList = $scope.AllMemberList.memberList;
                                if ($scope.MemberList.length > 0) {
                                    $scope.ddlMember = $scope.MemberList[0].basicid;
                                    $scope.checkZeroFolio();
                                }
                                else {
                                    $scope.isMemberLoaded = true;
                                    $rootScope.hideLoader();
                                }
                            }
                            else {
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                $rootScope.hideLoader();
                            }
                        }
                        else {
                            $rootScope.hideLoader();
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }
                else {
                    $scope.isMemberLoaded = true;
                }
            }

            $scope.memberChange = function () {
                $scope.isMemberLoaded = false;
                $rootScope.showLoader();
                $scope.checkZeroFolio();
            };

            $scope.checkZeroFolio = function () {

                var goal = addGoalModel.AddGoal;
                goal.userGoalId = null;
                goal.basicid = $scope.ddlMember;
                goal.Relation = null;
                goal.childName = null;
                goal.age = null;
                goal.gender = null;
                goal.annualIncome = null;
                goal.goalCode = 'FG14';
                goal.otherGoalName = 'Best Saver Account';
                goal.typeCode = 'FG309';
                goal.monthlyAmount = "0";
                goal.presentValue = "0";
                goal.risk = 'M';
                goal.goal_StartDate = null;
                goal.goal_EndDate = null;
                goal.inflationRate = null;
                goal.ror = null;
                goal.PMT = null;
                goal.downPaymentRate = null;
                goal.trnx_Type = "L";
                goal.getAmount = "0";
                goal.retirementAge = null;
                goal.investAmount = "0";
                goal.people = "0";

                userService.callCheckZeroFolioApi(goal).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.schemeData = bestSaverSchemeDetailsModel.parseBestSaverSchemeDetails(serverResp.data);
                            $scope.isInstaShow = $scope.schemeData.FreeAmt ? parseFloat(($scope.schemeData.FreeAmt - (-$scope.schemeData.InProcessAmt)).toFixed(2)) > 0.0 ? true : false : false;
                            $scope.isMemberLoaded = true;
                            $rootScope.hideLoader();
                        }
                        else {
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            $rootScope.hideLoader();
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });
            }

            $scope.openModal = function (ev, type) {
                $mdDialog.show({
                    locals: { dataToPass: $scope.schemeData },
                    controller: type.trim().toUpperCase() == 'D' ? depositModalController : withdrawModalController,
                    templateUrl: type.trim().toUpperCase() == 'D' ? 'deposit.tmpl.html' : 'withdraw.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                .then(function (val) {
                    if (val.trim().toUpperCase() == "D") {
                        SweetAlert.swal("Scheme Added To Cart!!", "", "success");
                        $state.go('dashboard.cart');
                    }
                    else if (val.trim().toUpperCase() == "D") {
                        SweetAlert.swal("Your Redemption Initiated Successfully!!", "", "success");
                        $scope.isMemberLoaded = false;
                        $rootScope.showLoader();
                        $scope.checkZeroFolio();
                    }
                    else {
                        SweetAlert.swal("Your Redemption Initiated Successfully. Amount will be credited within 30 mins.", "", "success");
                        $scope.isMemberLoaded = false;
                        $rootScope.showLoader();
                        $scope.checkZeroFolio();
                    }
                }, function () {
                    //$scope.status = 'You cancelled the dialog.';
                });
            };

            function depositModalController($scope, $mdDialog, dataToPass, SweetAlert, errorConstants, userFactory, addToCartModel, addGoalModel, bestSaverSchemeDetailsModel, userService) {

                $scope.saveTransact = function () {
                    if ($scope.investAmt) {
                        if (parseFloat($scope.investAmt) > 0.0) {

                            $scope.isProcessing = true;

                            var goal = addGoalModel.AddGoal;
                            goal.userGoalId = null;
                            goal.basicid = $scope.userdetails.basicid;
                            goal.Relation = null;
                            goal.childName = null;
                            goal.age = null;
                            goal.gender = null;
                            goal.annualIncome = null;
                            goal.goalCode = $scope.goalReqmodel.goalCode;
                            goal.otherGoalName = null;
                            goal.typeCode = $scope.goalReqmodel.locationCode;
                            goal.monthlyAmount = $scope.goalReqmodel.monthlyexpence;
                            goal.presentValue = $scope.presentValue;
                            goal.risk = 'M';
                            goal.goal_StartDate = null;
                            goal.goal_EndDate = $scope.goalReqmodel.endDate;
                            goal.inflationRate = $scope.goalResult.Inflation;
                            goal.ror = $scope.investType == "S" ? $scope.goalResult.ROR : $scope.goalResult.RORL;
                            goal.PMT = $scope.goalResult.PMT;
                            goal.downPaymentRate = null;
                            goal.trnx_Type = $scope.investType;
                            goal.getAmount = $scope.investType == "S" ? $scope.goalResult.getSip : $scope.goalResult.getLumpsum;
                            goal.retirementAge = null;
                            goal.investAmount = $scope.investType == "S" ? $scope.goalResult.investSip : $scope.goalResult.investLumpsum;
                            goal.people = null;

                            if ($scope.goalReqmodel.goalCode == 'FG4') {
                                goal.downPaymentRate = "25";
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG6') {
                                goal.people = $scope.goalReqmodel.travelPeople;
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG9') {
                                goal.age = $scope.goalReqmodel.startDate;
                                goal.retirementAge = $scope.goalReqmodel.endDate;
                                goal.goal_EndDate = null;
                            }
                            if ($scope.goalReqmodel.goalCode == 'FG10') {
                                goal.goal_StartDate = $scope.goalReqmodel.startDate;
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG12' || $scope.goalReqmodel.goalCode == 'FG13') {
                                goal.Relation = '002'
                                goal.childName = $scope.goalReqmodel.childName;
                                goal.age = "4";
                            }
                            else if ($scope.goalReqmodel.goalCode == 'FG14') {
                                goal.otherGoalName = $scope.goalReqmodel.otherGoalName;
                            }

                            userService.callGoalCreateApi(goal).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                        $scope.isProcessing = false;
                                        $scope.hide(goal.goalCode);
                                    }
                                    else {
                                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        $scope.isProcessing = false;
                                    }
                                }
                                else {
                                    $scope.isProcessing = false;
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });
                        }
                        else {
                            SweetAlert.swal("Please enter invest amount!", "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please enter invest amount!", "", "warning");
                    }

                }

                $scope.DepositeAmount = function () {
                    if (dataToPass.folioNo != "0") {
                        if (parseFloat($scope.txtdepositAmount) > 999.00) {
                            if (dataToPass) {
                                if (dataToPass.basicid) {
                                    $scope.isProcessing = true;

                                    var crt = addToCartModel.AddToCart;
                                    crt.basicID = dataToPass.basicid;
                                    crt.ProfileID = dataToPass.profileid;
                                    crt.purSchemeId = dataToPass.Scheme_Id;
                                    crt.tranType = 'NOR';
                                    crt.Amount = $scope.txtdepositAmount;
                                    crt.PurFolioNo = dataToPass.folioNo;
                                    crt.bankId = dataToPass.bankid;
                                    crt.userGoalId = dataToPass.Usergoalid;

                                    userService.callAddToCartApi(crt).then(function (serverResp) {
                                        if (serverResp.status) {
                                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                userFactory.incrementcart(1);
                                                $rootScope.cartTotalCount = userFactory.gettotalcartDetails().cartTotalCount;
                                                $scope.hide('D');
                                            }
                                            else {
                                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                $scope.isProcessing = false;
                                            }
                                        }
                                        else {
                                            $scope.isProcessing = false;
                                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                        }
                                    });
                                }
                            }
                        }
                        else {
                            SweetAlert.swal("Min. Deposit Amount is Rs. 1000!!", "", "warning");
                        }
                    }
                    else {
                        if (parseFloat($scope.txtdepositAmount) > 999.00) {

                            $scope.isProcessing = true;

                            var goal = addGoalModel.AddGoal;
                            goal.userGoalId = null;
                            goal.basicid = dataToPass.basicid;
                            goal.Relation = null;
                            goal.childName = null;
                            goal.age = null;
                            goal.gender = null;
                            goal.annualIncome = null;
                            goal.goalCode = 'FG14';
                            goal.otherGoalName = 'Best Saver Account';
                            goal.typeCode = 'FG309';
                            goal.monthlyAmount = "0";
                            goal.presentValue = "0";
                            goal.risk = 'M';
                            goal.goal_StartDate = null;
                            goal.goal_EndDate = null;
                            goal.inflationRate = null;
                            goal.ror = null;
                            goal.PMT = null;
                            goal.downPaymentRate = null;
                            goal.trnx_Type = "L";
                            goal.getAmount = "0";
                            goal.retirementAge = null;
                            goal.investAmount = "0";
                            goal.people = "0";

                            userService.callCreateZeroFolioApi(goal).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                        dataToPass = bestSaverSchemeDetailsModel.parseBestSaverSchemeDetails(serverResp.data);
                                        if (dataToPass.basicid) {
                                            var crt = addToCartModel.AddToCart;
                                            crt.basicID = dataToPass.basicid;
                                            crt.ProfileID = dataToPass.profileid;
                                            crt.purSchemeId = dataToPass.Scheme_Id;
                                            crt.tranType = 'NOR';
                                            crt.Amount = $scope.txtdepositAmount;
                                            crt.PurFolioNo = dataToPass.folioNo;
                                            crt.bankId = dataToPass.bankid;
                                            crt.userGoalId = dataToPass.Usergoalid;

                                            userService.callAddToCartApi(crt).then(function (serverResp) {
                                                if (serverResp.status) {
                                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                        userFactory.incrementcart(1);
                                                        $rootScope.cartTotalCount = userFactory.gettotalcartDetails().cartTotalCount;
                                                        $scope.hide('D');
                                                    }
                                                    else {
                                                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                        $scope.isProcessing = false;
                                                    }
                                                }
                                                else {
                                                    $scope.isProcessing = false;
                                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                                }
                                            });
                                        }
                                    }
                                    else {
                                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        $scope.isProcessing = false;
                                    }
                                }
                                else {
                                    $scope.isProcessing = false;
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });
                        }
                        else {
                            SweetAlert.swal("Min. Deposit Amount is Rs. 1000!!", "", "warning");
                        }
                    }
                }

                $scope.hide = function (val) {
                    $scope.isProcessing = false;
                    $mdDialog.hide(val);
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
            }

            function withdrawModalController($scope, $mdDialog, dataToPass, SweetAlert, errorConstants, userFactory, otherTransactionV2Model, userService) {

                $scope.WithdrawAmount = function () {
                    if (parseFloat($scope.txtwithdrawAmount) > 0.0) {
                        if (parseFloat($scope.txtwithdrawAmount) >= parseFloat(dataToPass.MinAmt)) {
                            if (parseFloat($scope.txtwithdrawAmount) <= parseFloat(dataToPass.FreeAmt)) {

                                var othrcrt = otherTransactionV2Model.OtherTransactionV2;
                                othrcrt.basicID = dataToPass.basicid;
                                othrcrt.ProfileID = dataToPass.profileid;
                                //othrcrt.purSchemeId = "";
                                othrcrt.sellSchemeId = dataToPass.Scheme_Id;
                                othrcrt.tranType = "R";
                                //othrcrt.PurFolioNo = "";
                                othrcrt.SellFolioNo = dataToPass.folioNo;
                                //othrcrt.No_of_Installment = "0";
                                //othrcrt.MDate = "";
                                othrcrt.userGoalId = dataToPass.Usergoalid;
                                othrcrt.bankId = dataToPass.bankid;
                                othrcrt.mandateId = dataToPass.mandateid;

                                if (parseFloat($scope.txtwithdrawAmount) >= parseFloat(dataToPass.RestrictedAmt) && parseFloat($scope.txtwithdrawAmount) <= parseFloat(dataToPass.Insta_Amount)) {
                                    othrcrt.Amount = $scope.txtwithdrawAmount;
                                    othrcrt.Units = "0";
                                    userService.callInstaSellApi(othrcrt).then(function (serverResp) {
                                        if (serverResp.status) {
                                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                $scope.hide('IW');
                                            }
                                            else {
                                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                $scope.isProcessing = false;
                                            }
                                        }
                                        else {
                                            $scope.isProcessing = false;
                                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                        }
                                    });
                                }
                                else {
                                    othrcrt.Amount = parseFloat($scope.txtwithdrawAmount) < parseFloat(dataToPass.FreeAmt) ? $scope.txtwithdrawAmount : "0";
                                    othrcrt.Units = parseFloat($scope.txtwithdrawAmount) === parseFloat(dataToPass.FreeAmt) ? dataToPass.Freeunits : "0";
                                    userService.callOtherTransactionApi(othrcrt).then(function (serverResp) {
                                        if (serverResp.status) {
                                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                $scope.hide('W');
                                            }
                                            else {
                                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                $scope.isProcessing = false;
                                            }
                                        }
                                        else {
                                            $scope.isProcessing = false;
                                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                        }
                                    });
                                }
                            }
                            else {
                                SweetAlert.swal("Withdrawal Amount cannot be greater than available amount", "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Min. Withdrawal Amount is " + dataToPass.MinAmt, "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Min. Withdrawal Amount is " + dataToPass.MinAmt, "", "warning");
                    }
                }

                $scope.hide = function (val) {
                    $mdDialog.hide(val);
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
            }

            $scope.completeProfile = function (ev) {
                $rootScope.prmBasicId = $scope.userdetails.basicid;
                $state.go('dashboard.account');
            }

            $scope.bindMemberList();
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));