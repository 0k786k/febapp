﻿(function (finApp) {
    'use strict';
    finApp.controller('investmentAccountController', ['$scope', '$state', '$rootScope', '$mdPanel', '$mdDialog', '$q', '$filter', 'SweetAlert', 'errorConstants', 'kycCafConstants', 'oauthTokenFactory', 'userFactory', 'investmentAccountsListModel', 'allMemberListModel', 'investProfileModel', 'userService', function ($scope, $state, $rootScope, $mdPanel, $mdDialog, $q, $filter, SweetAlert, errorConstants, kycCafConstants, oauthTokenFactory, userFactory, investmentAccountsListModel, allMemberListModel, investProfileModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Investment Account";
            $rootScope.mainheaderimg = "../../../../assets/img/Menu/Icon/InvestmentAccount.png";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");

            $scope.profiles = [];

            $scope.more = {
                items: [
                  'Add Investment Account'
                ]
            };

            $scope.menuTemplate = '' +
                '<div class="menu-panel" md-whiteframe="4" style="background-color: #fff;">' +
                '  <div class="menu-content">' +
                '    <div class="menu-item" ng-repeat="item in ctrl.items">' +
                '      <button class="md-button" ng-click="menuAction($event,item)">' +
                '        <span>{{item}}</span>' +
                '      </button>' +
                '    </div>' +
                '  </div>' +
                '</div>';

            $scope.showToolbarMenu = function ($event, menu) {
                var template = $scope.menuTemplate;

                var position = $mdPanel.newPanelPosition()
                    .relativeTo($event.target)
                    .addPanelPosition(
                      $mdPanel.xPosition.ALIGN_START,
                      $mdPanel.yPosition.BELOW
                    );

                var config = {
                    //id: 'toolbar_' + menu.name,
                    attachTo: angular.element(document.body),
                    controller: PanelMenuCtrl,
                    controllerAs: 'ctrl',
                    template: template,
                    position: position,
                    panelClass: 'menu-panel-container',
                    locals: {
                        items: menu.items
                    },
                    openFrom: $event,
                    focusOnOpen: false,
                    zIndex: 100,
                    propagateContainerEvents: true,
                    clickOutsideToClose: true,
                    groupName: ['toolbar', 'menus'],
                    onCloseSuccess :function (ev, item) {
                        $scope.openPopUp(ev, item)
                    }
                };

                $mdPanel.open(config);
            };

            $scope.openPopUp = function (ev, item) {

                $scope.dataForPopup = { grpLeaderBasicid: userFactory.getUserLoggedInDetails().basicid, fundid: "0" };

                $mdDialog.show({
                    locals: { dataToPass: $scope.dataForPopup },
                    controller: addController,
                    templateUrl: 'add.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
            .then(function () {
                $scope.bindInvestmentAccount();
            }, function () {
                //$scope.status = 'You cancelled the dialog.';
            });

            };

            function PanelMenuCtrl($scope, mdPanelRef, $mdDialog) {
                $scope.menuAction = function (ev, item) {
                    mdPanelRef && mdPanelRef.close(ev, item);
                };
            }

            function addController($scope, $mdDialog, $q, $filter, dataToPass, SweetAlert, errorConstants, kycCafConstants, allMemberListModel, investProfileModel, userService) {

                $scope.isProcessing = false;

                $scope.MemberList = [];
                $scope.firstApplicantList = [];
                $scope.secondApplicantList = [];
                $scope.thirdApplicantList = [];
                $scope.ddlFirstApplicant = null;
                $scope.ddlSecondApplicant = null;
                $scope.ddlThirdApplicant = null;
                $scope.isSingleMoh = true;

                $scope.modeOfHoldings = kycCafConstants.modeOfHoldingOptions;
                $scope.ddlModeOfHolding = $scope.modeOfHoldings[0].value;

                $scope.mohChange = function () {

                    $scope.ddlFirstApplicant = null;
                    $scope.ddlSecondApplicant = null;
                    $scope.ddlThirdApplicant = null;
                    $scope.secondApplicantList = [];
                    $scope.thirdApplicantList = [];

                    if ($scope.ddlModeOfHolding.trim().toUpperCase() == "SINGLE") {
                        $scope.isSingleMoh = true;
                    }
                    else {
                        $scope.isSingleMoh = false;
                    }
                }

                $scope.firstApplicantChange = function () {
                    $scope.ddlSecondApplicant = null;
                    $scope.ddlThirdApplicant = null;
                    $scope.thirdApplicantList = [];
                    $scope.secondApplicantList = $filter('filter')($scope.firstApplicantList, { basicid: '!' + $scope.ddlFirstApplicant + '' });
                }

                $scope.secondApplicantChange = function () {
                    $scope.ddlThirdApplicant = null;
                    $scope.thirdApplicantList = $filter('filter')($scope.secondApplicantList, { basicid: '!' + $scope.ddlSecondApplicant + '' });
                }

                $scope.validateForm = function () {

                    var deferred = $q.defer();
                    var result = { status: false, errmsg: null }

                    if ($scope.ddlModeOfHolding.trim().toUpperCase() == "SINGLE") {
                        if ($scope.ddlFirstApplicant) {
                            result.status = true;
                        }
                        else {
                            result.errmsg = "Please select first applicant!";
                        }
                    }
                    else {
                        if ($scope.ddlFirstApplicant) {
                            if ($scope.ddlSecondApplicant) {
                                result.status = true;
                            }
                            else {
                                result.errmsg = "Please select second applicant!";
                            }
                        }
                        else {
                            result.errmsg = "Please select first applicant!";
                        }
                    }
                    deferred.resolve(result);

                    return deferred.promise;
                }

                $scope.addInvestmentAccount = function () {
                    $scope.validateForm().then(function (result) {
                        if (result.status) {
                            $scope.isProcessing = true;
                            var iprof = investProfileModel.InvestProfile;
                            iprof.FirstApplBasicId = $scope.ddlFirstApplicant;
                            iprof.SecondApplBasicId = $scope.ddlSecondApplicant;
                            iprof.ThirdApplBasicId = $scope.ddlThirdApplicant;
                            iprof.holdingMode = $scope.ddlModeOfHolding;

                            userService.callCreateInvestProfileApi(iprof).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                        $scope.hide();
                                    }
                                    else {
                                        $scope.isProcessing = false;
                                        SweetAlert.swal(serverResp.msg, "", "error");
                                        //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                    }
                                }
                                else {
                                    $scope.isProcessing = false;
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });
                        }
                        else {
                            SweetAlert.swal(result.errmsg, "", "warning");
                        }
                    });
                }

                $scope.hide = function () {
                    $scope.isProcessing = false;
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.bindApplicants = function () {
                    $scope.isProcessing = true;
                    userService.callAllMemberListApi(dataToPass.grpLeaderBasicid, dataToPass.fundid).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.MemberList = allMemberListModel.parseAllMemberListModel(serverResp.data).memberList;
                                $scope.firstApplicantList = $scope.MemberList;
                                $scope.isProcessing = false;
                            }
                            else {
                                $scope.isProcessing = false;
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $scope.isProcessing = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }

                $scope.bindApplicants();
            }

            $scope.bindInvestmentAccount = function () {
                $rootScope.showLoader();
                userService.callInvestProfilelistApi().then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.profiles = investmentAccountsListModel.parseInvestmentAccountsList(serverResp.data);
                            $rootScope.hideLoader();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });
            }

            $scope.bindInvestmentAccount();
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));