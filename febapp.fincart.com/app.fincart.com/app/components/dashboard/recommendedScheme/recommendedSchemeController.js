﻿(function (finApp) {
    'use strict';
    finApp.controller('recommendedSchemeController', ['$scope', '$state', '$stateParams', '$rootScope', '$mdDialog', '$filter', 'oauthTokenFactory', 'userFactory', 'SweetAlert', 'errorConstants', 'transactionConstants', 'fundsWithCategoryModel', 'searchSchemesResultListModel', 'allMemberListModel', 'validDateListModel', 'addToCartModel', 'otherTransactionV2Model', 'recommendedSchemeModel', 'userService', function ($scope, $state, $stateParams, $rootScope, $mdDialog, $filter, oauthTokenFactory, userFactory, SweetAlert, errorConstants, transactionConstants, fundsWithCategoryModel, searchSchemesResultListModel, allMemberListModel, validDateListModel, addToCartModel, otherTransactionV2Model, recommendedSchemeModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Recommended Scheme";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = "";
            $scope.isSchemeLoaded = false;
            $scope.goalName = "";
            $scope.goalImg = "";
            $scope.typecode = "";
            $scope.schemeList = [];
            $scope.dataForPopup = { grpLeaderBasicid: 0, goalid: $stateParams.goalid, fundid: 0, schemeName: '', schemeId: 0, txnType: '', minamt: 0, maxamt: 0 };
            $scope.rdRisk = $stateParams.risk;

            $scope.morefunds = function () {
                $state.go('dashboard.searchScheme', { goalid: $stateParams.goalid });
            }

            $scope.bindRecommendedSchemes = function () {
                $rootScope.showLoader();

                var rcmdScheme = recommendedSchemeModel.RecommendedScheme;
                rcmdScheme.risk = $scope.rdRisk;
                rcmdScheme.amount = $stateParams.amt;
                rcmdScheme.duration = $stateParams.time;
                rcmdScheme.usergoalId = $stateParams.goalid;

                userService.callRecommendedSchemesApi(rcmdScheme).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {

                            $scope.schemeList = searchSchemesResultListModel.parseSearchSchemesResultList(serverResp.data);

                            if ($scope.schemeList.searchSchemesResult.length > 0) {
                                $scope.goalName = $scope.schemeList.searchSchemesResult[0].goalName;
                                $scope.goalImg = $scope.schemeList.searchSchemesResult[0].goalImg;

                                $scope.typecode = $scope.schemeList.searchSchemesResult[0].typeCode;

                                if ($scope.typecode) {
                                    if ($scope.typecode.trim().toUpperCase() == 'FG220') {
                                        $rootScope.currActiveTab = "quickSip";
                                    }
                                    else {
                                        $rootScope.currActiveTab = "goalFullView";
                                    }
                                }
                                else {
                                    $rootScope.currActiveTab = "goalFullView";
                                }


                                $rootScope.mainheader = "Recommended Scheme for " + $scope.goalName;
                                $rootScope.mainheaderimg = $scope.goalImg;
                            }

                            $scope.slickConfig = {
                                dots: true,
                                autoplay: true,
                                infinite: true,
                                autoplaySpeed: 2000,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                method: {}
                            };

                            $scope.isSchemeLoaded = true;
                            $rootScope.hideLoader();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });
            }

            $scope.riskChange = function () {
                $scope.bindRecommendedSchemes();
            }

            $scope.openPopUp = function (ev, schName, fundid, schId, txntype, minamt, maxamt) {
                $scope.dataForPopup.grpLeaderBasicid = userFactory.getUserLoggedInDetails().basicid;
                $scope.dataForPopup.fundid = fundid;
                $scope.dataForPopup.schemeName = schName;
                $scope.dataForPopup.schemeId = schId;
                $scope.dataForPopup.txnType = txntype;
                $scope.dataForPopup.minamt = minamt;
                $scope.dataForPopup.maxamt = maxamt;

                $mdDialog.show({
                    locals: { dataToPass: $scope.dataForPopup },
                    controller: DialogController,
                    templateUrl: 'addtocart.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                .then(function () {
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    //$scope.status = 'You cancelled the dialog.';
                });
            };

            function DialogController($scope, $mdDialog, $filter, dataToPass, SweetAlert, errorConstants, transactionConstants, userFactory, allMemberListModel, addToCartModel, otherTransactionV2Model, userService) {
                $scope.isLoading = true;
                $scope.isProcessing = false;
                $scope.isSchemeAdded = false;
                $scope.AllMemberList = []
                $scope.MemberList = [];
                $scope.ProfileList = [];
                $scope.MandateList = [];
                $scope.FolioList = [];
                $scope.ValidDateList = [];
                $scope.BankList = [];
                $scope.noOfInstallment = transactionConstants.installments
                $scope.schemeName = dataToPass.schemeName;
                $scope.txnType = dataToPass.txnType;
                $scope.minamt = dataToPass.minamt;
                $scope.maxamt = dataToPass.maxamt;

                userService.callAllMemberListApi(dataToPass.grpLeaderBasicid, dataToPass.fundid).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {

                            $scope.AllMemberList = allMemberListModel.parseAllMemberListModel(serverResp.data);

                            $scope.MemberList = $scope.AllMemberList.memberList;
                            $scope.ddlMember = $scope.MemberList[0].basicid;

                            $scope.ProfileList = $scope.AllMemberList.profileList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                            $scope.ddlProfile = $scope.ProfileList[0].profileID;

                            $scope.MandateList = $scope.AllMemberList.mandateList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                            $scope.ddlMandate = $scope.MandateList[0].mandateNo;

                            $scope.FolioList = $scope.AllMemberList.folioList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                            $scope.ddlFolio = $scope.FolioList[0].folioNo;

                            $scope.BankList = $scope.AllMemberList.bankList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                            $scope.ddlBank = $scope.BankList[0].bankid;


                            if ($scope.txnType != 'L') {
                                $scope.bindValidDates(dataToPass.schemeId, $scope.ddlMandate != 'No Mandate Yet' ? 'Y' : 'N', 'SIP');
                            }
                            else {
                                $scope.isLoading = false;
                            }

                        }
                        else {
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            $scope.isLoading = false;
                        }
                    }
                    else {
                        $scope.isLoading = false;
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });

                $scope.bindValidDates = function (schemeId, mandatestatus, txntype) {
                    userService.callSystematicDatesApi(schemeId, mandatestatus, txntype).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.ValidDateList = validDateListModel.parseValidDateList(serverResp.data);
                                $scope.ddlValidDate = $scope.ValidDateList.validdates[0].Day;
                                $scope.ddlInstallment = $scope.noOfInstallment[0].value;
                                $scope.isLoading = false;
                            }
                            else {
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                $scope.isLoading = false;
                            }
                        }
                        else {
                            $scope.isLoading = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }

                $scope.memberChange = function () {
                    $scope.isLoading = true;

                    $scope.ProfileList.length = 0
                    $scope.ProfileList = $scope.AllMemberList.profileList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                    $scope.ddlProfile = $scope.ProfileList[0].profileID;

                    $scope.MandateList.length = 0
                    $scope.MandateList = $scope.AllMemberList.mandateList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                    $scope.ddlMandate = $scope.MandateList[0].mandateNo;

                    $scope.FolioList.length = 0
                    $scope.FolioList = $scope.AllMemberList.folioList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                    $scope.ddlFolio = $scope.FolioList[0].folioNo;

                    $scope.BankList.length = 0
                    $scope.BankList = $scope.AllMemberList.bankList.filter(function (obj) { return obj.basicid == $scope.ddlMember; });
                    $scope.ddlBank = $scope.BankList[0].bankid;

                    if ($scope.txnType != 'L') {
                        $scope.bindValidDates(dataToPass.schemeId, $scope.ddlMandate != 'No Mandate Yet' ? 'Y' : 'N', 'SIP');
                    }
                    else {
                        $scope.isLoading = false;
                    }
                };

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $scope.isSchemeAdded = false;
                    $mdDialog.cancel();
                };

                $scope.addtocart = function () {
                    if ($scope.txtAmount) {

                        if (parseFloat($scope.txtAmount) >= parseFloat($scope.minamt)) {

                            if (parseFloat($scope.txtAmount) <= parseFloat($scope.maxamt)) {

                                $scope.isLoading = true;
                                $scope.isProcessing = true;
                                var crt = addToCartModel.AddToCart;

                                crt.basicID = $scope.ddlMember;
                                crt.ProfileID = $scope.ddlProfile;
                                crt.purSchemeId = dataToPass.schemeId;
                                crt.tranType = $scope.txnType == 'L' ? 'NOR' : 'ISIP';
                                crt.Amount = $scope.txtAmount;
                                crt.No_of_Installment = $scope.ddlInstallment;
                                crt.MDate = $scope.ddlValidDate;
                                crt.startDate = $scope.txnType == 'L' ? "" : $scope.ValidDateList.validdates.filter(function (obj) { return obj.Day == $scope.ddlValidDate; })[0].Date;
                                crt.PurFolioNo = $scope.ddlFolio;
                                crt.bankId = $scope.ddlBank;
                                crt.userGoalId = dataToPass.goalid;

                                userService.callAddToCartApi(crt).then(function (serverResp) {
                                    if (serverResp.status) {
                                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                                            userFactory.incrementcart(1);
                                            $rootScope.cartTotalCount = userFactory.gettotalcartDetails().cartTotalCount;
                                            SweetAlert.swal("Scheme Added To Cart!!", "", "success");
                                            $scope.isLoading = false;
                                            $scope.isProcessing = false;
                                            $scope.isSchemeAdded = true;
                                        }
                                        else {
                                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                            $scope.isLoading = false;
                                            $scope.isProcessing = false;
                                        }
                                    }
                                    else {
                                        $scope.isLoading = false;
                                        $scope.isProcessing = false;
                                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                    }
                                });

                            }
                            else {
                                SweetAlert.swal("Max Investment Rs." + $scope.maxamt, "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Min Investment Rs." + $scope.minamt, "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please Enter Amount", "", "warning");
                    }
                };

                $scope.sipgenerate = function () {
                    if ($scope.txtAmount) {

                        if (parseFloat($scope.txtAmount) >= parseFloat($scope.minamt)) {

                            if (parseFloat($scope.txtAmount) <= parseFloat($scope.maxamt)) {

                                $scope.isLoading = true;
                                $scope.isProcessing = true;
                                var othrcrt = otherTransactionV2Model.OtherTransactionV2;

                                othrcrt.basicID = $scope.ddlMember;
                                othrcrt.ProfileID = $scope.ddlProfile;
                                othrcrt.purSchemeId = dataToPass.schemeId;
                                othrcrt.sellSchemeId = "";
                                othrcrt.tranType = "SIP";
                                othrcrt.PurFolioNo = $scope.ddlFolio;
                                othrcrt.SellFolioNo = "";
                                othrcrt.Amount = $scope.txtAmount;
                                othrcrt.No_of_Installment = $scope.ddlInstallment;
                                othrcrt.MDate = $scope.ddlValidDate;
                                othrcrt.userGoalId = dataToPass.goalid;
                                othrcrt.Units = "";
                                othrcrt.bankId = $scope.ddlBank;
                                othrcrt.mandateId = $scope.ddlMandate;

                                userService.callOtherTransactionApi(othrcrt).then(function (serverResp) {
                                    if (serverResp.status) {
                                        if (serverResp.status.toUpperCase() === "SUCCESS") {

                                            var sipDate = $scope.ValidDateList.validdates.filter(function (obj) { return obj.Day == $scope.ddlValidDate; });

                                            SweetAlert.swal("SIP of Rs." + $scope.txtAmount + " is registered for '" + $scope.schemeName + "'. Amount will be deducted from " + $filter('date')(sipDate[0].Date, 'medium', '+0530').replace(' 12:00:00 AM', '') + ".", "", "success");

                                            $scope.isLoading = false;
                                            $scope.isProcessing = false;
                                            $mdDialog.cancel();
                                        }
                                        else {
                                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                            $scope.isLoading = false;
                                            $scope.isProcessing = false;
                                        }
                                    }
                                    else {
                                        $scope.isLoading = false;
                                        $scope.isProcessing = false;
                                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                    }
                                });

                            }
                            else {
                                SweetAlert.swal("Max Investment Rs." + $scope.maxamt, "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Min Investment Rs." + $scope.minamt, "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please Enter Amount", "", "warning");
                    }
                };
            }

            $scope.bindRecommendedSchemes();
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));