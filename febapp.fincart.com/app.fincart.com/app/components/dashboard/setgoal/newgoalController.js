﻿(function (finApp) {
    'use strict';
    finApp.controller('newgoalController', ['$scope', '$state', '$window', '$stateParams', '$rootScope', '$mdDialog', '$mdPanel', '$filter', '$q', 'oauthTokenFactory', 'userFactory', 'validationFactory', 'SweetAlert', 'errorConstants', 'finCalcModel', 'goalresultModel', 'UserAllGoalsListModel', 'userService', function ($scope, $state, $window, $stateParams, $rootScope, $mdDialog, $mdPanel, $filter, $q, oauthTokenFactory, userFactory, validationFactory, SweetAlert, errorConstants, finCalcModel, goalresultModel, UserAllGoalsListModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Set New Goal";
            $rootScope.mainheaderimg = "../../../../assets/img/Menu/Icon/SetNewGoals.png";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");
            $scope.userdetails = userFactory.getUserLoggedInDetails();

            $scope.isHome = false;
            $scope.isEducation = false;
            $scope.isBike = false;
            $scope.isTravel = false;
            $scope.isCar = false;
            $scope.isWedding = false;
            $scope.isFamilyPlanning = false;
            $scope.isSabbatical = false;
            $scope.isRetirement = false;
            $scope.isBusiness = false;
            $scope.isWealth = false;

            $scope.findCreatedGoalData = function () {
                $rootScope.showLoader();
                userService.callGoalNameCodeListApi().then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.userAllGoalsList = UserAllGoalsListModel.parseUserAllGoalsList(serverResp.data);

                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG4'; }).length < 1) {
                                $scope.isHome = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG5'; }).length < 1) {
                                $scope.isEducation = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG1'; }).length < 1) {
                                $scope.isBike = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG6'; }).length < 1) {
                                $scope.isTravel = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG3'; }).length < 1) {
                                $scope.isCar = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG7'; }).length < 1) {
                                $scope.isWedding = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG11'; }).length < 1) {
                                $scope.isFamilyPlanning = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG10'; }).length < 1) {
                                $scope.isSabbatical = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG9'; }).length < 1) {
                                $scope.isRetirement = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG2'; }).length < 1) {
                                $scope.isBusiness = true;
                            }
                            if ($scope.userAllGoalsList.userAllGoals.filter(function (obj) { return obj.goalCode == 'FG8'; }).length < 1) {
                                $scope.isWealth = true;
                            }
                            $rootScope.hideLoader();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }

                });
            }

            $scope.next = function (ev, goalcode) {
                $state.go('dashboard.setgoal', { 'goalcode': goalcode, 'goalid': 0 });
            }

            $scope.findCreatedGoalData();
        }
        else {
            $state.go('login', { type: 'login' });
        }

    }]);
})(angular.module('fincartApp'));