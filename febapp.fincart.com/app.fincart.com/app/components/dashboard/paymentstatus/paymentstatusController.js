﻿(function (finApp) {
    'use strict';
    finApp.controller('paymentstatusController', ['$scope', '$state', '$window', '$stateParams', '$rootScope', '$mdDialog', '$mdBottomSheet', '$filter', 'oauthTokenFactory', 'userFactory', 'transactionFactory', 'SweetAlert', 'errorConstants', 'transactionConstants', 'allMemberListModel', 'userCartDetailsListModel', 'billdeskPaymentModeListModel', 'cartPurchaseAttemptModel', 'billdskPaymentLogModel', 'userService', function ($scope, $state, $window, $stateParams, $rootScope, $mdDialog, $mdBottomSheet, $filter, oauthTokenFactory, userFactory, transactionFactory, SweetAlert, errorConstants, transactionConstants, allMemberListModel, userCartDetailsListModel, billdeskPaymentModeListModel, cartPurchaseAttemptModel, billdskPaymentLogModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "Payment Status";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = "cart";

            $rootScope.showLoader();

            $scope.paymentstatusdata = null;
            $scope.isCorrectData = false;
            $scope.isWrongData = false;

            $scope.gotocart = function () {
                $state.go('dashboard.cart');
            }

            $scope.gotodashboard = function () {
                $state.go('dashboard');
            }

            if (transactionFactory.getCurrentBunchTxnId()) {
                userService.callBilldskPaymentStatusApi(transactionFactory.getCurrentBunchTxnId()).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.paymentstatusdata = serverResp.data;
                            userFactory.decrementcart($scope.paymentstatusdata.txnLogDetails.length);
                            $rootScope.cartTotalCount = userFactory.gettotalcartDetails().cartTotalCount;
                            transactionFactory.destroyCurrentBunchTxnId()
                            $scope.isCorrectData = true;
                            $rootScope.hideLoader();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });
            }
            else {
                $scope.isWrongData = true;
                $rootScope.hideLoader();
                SweetAlert.swal("Invalid Access!!", "", "error");
            }
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));