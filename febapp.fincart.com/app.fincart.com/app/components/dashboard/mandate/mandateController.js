﻿(function (finApp) {
    'use strict';
    finApp.controller('mandateController', ['$scope', '$state', '$rootScope', '$mdPanel', '$mdDialog', '$q', '$filter', 'SweetAlert', 'errorConstants', 'kycCafConstants', 'oauthTokenFactory', 'userFactory', 'mandateDetailsListModel', 'userService', function ($scope, $state, $rootScope, $mdPanel, $mdDialog, $q, $filter, SweetAlert, errorConstants, kycCafConstants, oauthTokenFactory, userFactory, mandateDetailsListModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {
            if ($rootScope.prmBasicId) {
                $rootScope.mainheader = "Bank Mandate's";
                $rootScope.mainheaderimg = "";
                $rootScope.currActiveTab = "profile";

                $scope.userdetails = userFactory.getUserLoggedInDetails();

                $scope.mandates = [];
                $scope.clientname = "";

                $scope.bindMandates = function () {
                    $rootScope.showLoader();
                    userService.callMandateListApi($rootScope.prmBasicId).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.mandates = mandateDetailsListModel.parseMandateDetailsList(serverResp.data);
                                if ($scope.mandates.mandateList.length > 0) {
                                    $scope.clientname = $scope.mandates.mandateList[0].clientname;
                                }
                                $rootScope.hideLoader();
                            }
                            else {
                                $rootScope.hideLoader();
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $rootScope.hideLoader();
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }

                $scope.bindMandates();
            }
            else {
                $state.go('dashboard.profile');
            }
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));