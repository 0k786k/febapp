﻿(function (finApp) {
    'use strict';
    finApp.controller('kyccafViewController', ['$scope', '$state', '$rootScope', 'SweetAlert', 'errorConstants', 'oauthTokenFactory', 'userService', 'investFullSummaryListModel', 'investShortSummaryModel', 'cafViewProfileResultModel', function ($scope, $state, $rootScope, SweetAlert, errorConstants, oauthTokenFactory, userService, investFullSummaryListModel, investShortSummaryModel, cafViewProfileResultModel) {

        if (oauthTokenFactory.getisloggedin()) {

            $rootScope.mainheader = "My Profile";
            $rootScope.mainheaderimg = "";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");
            $rootScope.prmBasicId = null;

            $scope.groupLeader = null;
            $scope.members = [];

            $rootScope.showLoader();

            $scope.loadProfiles = function () {
                userService.callProfilesApi().then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.usercafProfileList = cafViewProfileResultModel.parseCafViewProfileResult(serverResp.data);
                            $scope.members = $scope.usercafProfileList.CafViewProfile;
                            delete $scope.usercafProfileList.CafViewProfile
                            $scope.groupLeader = $scope.usercafProfileList;
                            $rootScope.grpLdrProfilePic = $scope.groupLeader.profilepic;
                            $rootScope.hideLoader();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }

                });
            }

            $scope.editProfile = function (user) {
                if (user.cafStatus == 'success') {
                    $rootScope.prmBasicId = user.basic_id;
                    $state.go('dashboard.editprofile');
                }
                else {
                    $rootScope.prmBasicId = user.basic_id;
                    $state.go('dashboard.account');
                }
            }

            $scope.viewMandate = function (user) {
                $rootScope.prmBasicId = user.basic_id;
                $state.go('dashboard.mandate');
            }

            $scope.addMember = function () {
                $rootScope.prmBasicId = "0";
                $state.go('dashboard.account');
            }

            $scope.fileUploadImageClick = function (id) {
                angular.element('#' + id).click();
            }

            $scope.changeProfilePic = function ($event, basicid) {
                if ($event.target.files.length > 0) {
                    $event.target.files[0]
                    $rootScope.showLoader();
                    userService.callSaveProfilePicApi($event.target.files[0], basicid).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.loadProfiles();
                            }
                            else {
                                $rootScope.hideLoader();
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $rootScope.hideLoader();
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }

                    });

                }
            }

            $scope.loadProfiles();
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));