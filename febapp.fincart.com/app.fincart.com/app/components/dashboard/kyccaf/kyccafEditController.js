﻿(function (finApp) {
    'use strict';
    finApp.controller('kyccafEditController', ['$scope', '$state', '$rootScope', '$sce', '$mdDialog', '$filter', '$q', '$mdPanel', 'SweetAlert', 'errorConstants', 'kycCafConstants', 'oauthTokenFactory', 'validationFactory', 'userCAFDataModel', 'pincodeDetailsModel', 'addressChangeRequestModel', 'getBankInfoResultModel', 'bankChangeRequestModel', 'cafeditRequestsResultModel', 'userCAFNomineeModel', 'userService', function ($scope, $state, $rootScope, $sce, $mdDialog, $filter, $q, $mdPanel, SweetAlert, errorConstants, kycCafConstants, oauthTokenFactory, validationFactory, userCAFDataModel, pincodeDetailsModel, addressChangeRequestModel, getBankInfoResultModel, bankChangeRequestModel, cafeditRequestsResultModel, userCAFNomineeModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {
            if ($rootScope.prmBasicId) {
                $rootScope.mainheader = "Edit Profile";
                $rootScope.mainheaderimg = "";
                $rootScope.currActiveTab = "profile";
                $scope.userCaf = null;

                $scope.loadData = function () {
                    $rootScope.showLoader();
                    userService.callFetchUserApi($rootScope.prmBasicId).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.userCaf = userCAFDataModel.parseUserCAFData(serverResp.data);
                                if ($scope.userCaf.AnnualIncome && kycCafConstants.incomeOptions.filter(function (obj) { return obj.value === parseInt($scope.userCaf.AnnualIncome) })[0]) {

                                    $scope.userCaf.AnnualIncome = kycCafConstants.incomeOptions.filter(function (obj) { return obj.value === parseInt($scope.userCaf.AnnualIncome) })[0].name;
                                }
                                if ($scope.userCaf.occupation && kycCafConstants.rtaOccupationOptions.filter(function (obj) { return obj.value === $scope.userCaf.occupation })[0]) {
                                    $scope.userCaf.occupation = kycCafConstants.rtaOccupationOptions.filter(function (obj) { return obj.value === $scope.userCaf.occupation })[0].name;
                                }
                                if ($scope.userCaf.ftcaWealthSource && kycCafConstants.wealthsourceOptions.filter(function (obj) { return obj.value === parseInt($scope.userCaf.ftcaWealthSource) })[0]) {
                                    $scope.userCaf.ftcaWealthSource = kycCafConstants.wealthsourceOptions.filter(function (obj) { return obj.value === parseInt($scope.userCaf.ftcaWealthSource) })[0].name;
                                }
                                $rootScope.hideLoader();
                            }
                            else {
                                $rootScope.hideLoader();
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $rootScope.hideLoader();
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }

                $scope.more = {
                    items: [
                      'Change Request',
                      'View All Requests'
                    ]
                };

                $scope.more2 = {
                    items: [
                      'Edit Nominee'
                    ]
                };

                $scope.menuTemplate = '' +
                '<div class="menu-panel" md-whiteframe="4" style="background-color: #fff;">' +
                '  <div class="menu-content">' +
                '    <div class="menu-item" ng-repeat="item in ctrl.items">' +
                '      <button class="md-button" ng-click="menuAction($event,item,ctrl.updateObj,ctrl.updateType)">' +
                '        <span>{{item}}</span>' +
                '      </button>' +
                '    </div>' +
                '  </div>' +
                '</div>';

                $scope.menuTemplate2 = '' +
                '<div class="menu-panel" md-whiteframe="4" style="background-color: #fff;">' +
                '  <div class="menu-content">' +
                '    <div class="menu-item" ng-repeat="item in ctrl.items">' +
                '      <button class="md-button" ng-click="menuAction($event,item,ctrl.updateObj,ctrl.updateType)">' +
                '        <span>{{item}}</span>' +
                '      </button>' +
                '    </div>' +
                '  </div>' +
                '</div>';

                $scope.showToolbarMenu = function ($event, menu, updateObj, type) {
                    var template = type == "nominee" ? $scope.menuTemplate2 : $scope.menuTemplate;

                    var position = $mdPanel.newPanelPosition()
                        .relativeTo($event.target)
                        .addPanelPosition(
                          $mdPanel.xPosition.ALIGN_START,
                          $mdPanel.yPosition.BELOW
                        );

                    var config = {
                        //id: 'toolbar_' + menu.name,
                        attachTo: angular.element(document.body),
                        controller: PanelMenuCtrl,
                        controllerAs: 'ctrl',
                        template: template,
                        position: position,
                        panelClass: 'menu-panel-container',
                        locals: {
                            items: menu.items,
                            updateObj: updateObj,
                            updateType: type
                        },
                        openFrom: $event,
                        focusOnOpen: false,
                        zIndex: 100,
                        propagateContainerEvents: true,
                        clickOutsideToClose: true,
                        groupName: ['toolbar', 'menus'],
                        onCloseSuccess: function (ev, item) {
                            $scope.openPopUp(ev, item.toElement.innerText, updateObj, type)
                        }
                    };

                    $mdPanel.open(config);
                };

                $scope.openPopUp = function (ev, item, updateObj, type) {
                    $scope.dataForPopup = { data: updateObj, updateType: type }

                    $mdDialog.show({
                        locals: { dataToPass: $scope.dataForPopup },
                        controller: item == 'View All Requests' ? viewController : updateController,
                        templateUrl: item == 'View All Requests' ? 'view.tmpl.html' : 'update.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                    })
                .then(function (val) {
                    if (val == 'address') {
                        SweetAlert.swal("Your request for address change has been sent..!!", "", "success");
                    }
                    else if (val == 'bank') {
                        SweetAlert.swal("Your request for bank change has been sent..!!", "", "success");
                    }
                    else {
                        SweetAlert.swal("Nominee details updated successfully..!!", "", "success");
                        $scope.loadData();
                    }
                }, function () {
                    //$scope.status = 'You cancelled the dialog.';
                });

                };

                function PanelMenuCtrl($scope, mdPanelRef, $mdDialog) {
                    $scope.menuAction = function (ev, item, updateObj, type) {
                        mdPanelRef && mdPanelRef.close(ev, item);
                    };
                }

                function updateController($scope, $sce, $mdDialog, $filter, $q, dataToPass, SweetAlert, errorConstants, kycCafConstants, validationFactory, userFactory, pincodeDetailsModel, addressChangeRequestModel, getBankInfoResultModel, bankChangeRequestModel, userCAFNomineeModel, userService) {

                    $scope.isProcessing = false;
                    $scope.panfiles = [];
                    $scope.aadharfiles = [];
                    $scope.chequefiles = [];
                    $scope.statecity = null;
                    $scope.bankOptions = kycCafConstants.bankOptions;

                    $scope.dataholder = {
                        srcPan: "../../../../assets/img/folder.png",
                        panUploadImgText: "Upload Your PAN Copy.",
                        srcAadhar: "../../../../assets/img/folder.png",
                        aadharUploadImgText: "Upload Your Aadhar Card Copy.",
                        srcCheque: "../../../../assets/img/folder.png",
                        chequeUploadImgText: "Upload Your Canceled Cheque Copy."
                    }

                    $scope.updateType = dataToPass.updateType.trim().toLowerCase() == 'nominee' ? 'Update Nominee' : dataToPass.updateType.trim().toLowerCase() == 'address' ? 'Change Address Request' : 'Change Bank Request';
                    $scope.reqDataType = dataToPass.updateType.trim().toLowerCase() == 'nominee' ? '1' : dataToPass.updateType.trim().toLowerCase() == 'address' ? '2' : '3';

                    $scope.fileUploadImageClick = function (id) {
                        angular.element('#' + id).click();
                    }

                    $scope.changeFileUpload = function ($event, type) {
                        if ($event.target.files.length > 0) {
                            if (type.trim().toLowerCase() === "pan") {
                                $scope.dataholder.srcPan = URL.createObjectURL($event.target.files[0]);
                                $scope.dataholder.panUploadImgText = $event.target.files[0].name;
                                if ($scope.panfiles.length > 0) {
                                    $scope.panfiles[0] = $event.target.files[0];
                                }
                                else {
                                    $scope.panfiles.push($event.target.files[0]);
                                }
                            }
                            else if (type.trim().toLowerCase() === "aadhar") {
                                $scope.dataholder.srcAadhar = URL.createObjectURL($event.target.files[0]);
                                $scope.dataholder.aadharUploadImgText = $event.target.files[0].name;
                                if ($scope.aadharfiles.length > 0) {
                                    $scope.aadharfiles[0] = $event.target.files[0];
                                }
                                else {
                                    $scope.aadharfiles.push($event.target.files[0]);
                                }
                            }
                            else if (type.trim().toLowerCase() === "cheque") {
                                $scope.dataholder.srcCheque = URL.createObjectURL($event.target.files[0]);
                                $scope.dataholder.chequeUploadImgText = $event.target.files[0].name;
                                if ($scope.chequefiles.length > 0) {
                                    $scope.chequefiles[0] = $event.target.files[0];
                                }
                                else {
                                    $scope.chequefiles.push($event.target.files[0]);
                                }
                            }
                        }
                    }

                    $scope.setPinDetails = function (state, city) {
                        $scope.pincodeCS = $sce.trustAsHtml("State : <span style='color:#0056a7;'>" + state + "</span> City : <span style='color:#0056a7;'>" + city + "</span>");
                    }

                    $scope.getPincodeDetails = function () {
                        $scope.setPinDetails('Loading...', 'Loading...');
                        userService.callPinCodeDetailsApi($scope.txtPincode).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $scope.statecity = pincodeDetailsModel.parsePincodeDetails(serverResp.data);
                                    $scope.setPinDetails($scope.statecity.state, $scope.statecity.city);
                                }
                                else {
                                    $scope.setPinDetails('', '');
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                }
                            }
                            else {
                                $scope.setPinDetails('', '');
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });
                    }

                    $scope.ifscChange = function () {
                        if ($scope.txtIfsc.length > 10) {
                            if (validationFactory.validIFSC($scope.txtIfsc)) {
                                $scope.txtBranchAdd = "";
                                $scope.txtBranchName = "";
                                $scope.txtBranchCity = "";
                                $scope.txtMicr = "";
                                $scope.ddlbankname = "";
                                $scope.txtAccountNumber = "";
                                $scope.txtAccountName = "";
                                $scope.isProcessing = true;
                                userService.callBankDetailsByIFSCApi($scope.txtIfsc).then(function (serverResp) {
                                    if (serverResp.status) {
                                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                                            $scope.bankdata = getBankInfoResultModel.parseGetBankInfoResult(serverResp.data);
                                            if ($scope.bankdata.BankName) {
                                                $scope.txtBranchAdd = $scope.bankdata.BankAddress;
                                                $scope.txtBranchName = $scope.bankdata.Branch;
                                                $scope.txtBranchCity = $scope.bankdata.City;
                                                $scope.txtMicr = $scope.bankdata.MICR;

                                                $scope.banknm = $scope.bankOptions.filter(function (obj) { return obj.name.trim().toUpperCase() === $scope.bankdata.BankName.trim().toUpperCase() })[0];
                                                $scope.ddlbankname = $scope.banknm ? $scope.banknm.value : "";
                                                SweetAlert.swal("We found your bank details..", "", "success");
                                                $scope.isProcessing = false;
                                            }
                                            else {
                                                SweetAlert.swal("Sorry..!! we are not able to locate your bank details. Fill details manually!", "", "error");
                                                $scope.isProcessing = false;
                                            }
                                        }
                                        else {
                                            $scope.isProcessing = false;
                                            SweetAlert.swal(serverResp.msg, "", "error");
                                            //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        }
                                    }
                                    else {
                                        $scope.isProcessing = false;
                                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                    }
                                });
                            }
                            else {
                                SweetAlert.swal("Invalid IFSC!", "", "warning");
                            }
                        }
                    }

                    $scope.addressChangeRequest = function () {
                        if ($scope.txtAddress) {
                            if ($scope.txtPincode) {
                                if ($scope.dataholder.srcPan && $scope.dataholder.srcPan != "../../../../assets/img/folder.png") {
                                    if ($scope.dataholder.srcAadhar && $scope.dataholder.srcAadhar != "../../../../assets/img/folder.png") {

                                        $scope.isProcessing = true;
                                        var addressChangeRequest = addressChangeRequestModel.AddressChangeRequest;
                                        addressChangeRequest.basicid = $rootScope.prmBasicId;
                                        addressChangeRequest.address = $scope.txtAddress;
                                        addressChangeRequest.city = $scope.statecity ? $scope.statecity.city : dataToPass.data.prmCity;
                                        addressChangeRequest.state = $scope.statecity ? $scope.statecity.state : dataToPass.data.prmState;
                                        addressChangeRequest.pincode = $scope.txtPincode;
                                        addressChangeRequest.country = "India";
                                        addressChangeRequest.devicetype = "WEB";
                                        addressChangeRequest.reason = "update it";
                                        addressChangeRequest.panFile = $scope.panfiles[0];
                                        addressChangeRequest.aadhaarFile = $scope.aadharfiles[0];

                                        userService.callChangeAddressReqApi(addressChangeRequest).then(function (serverResp) {
                                            if (serverResp.status) {
                                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                    $scope.hide(dataToPass.updateType.trim().toLowerCase());
                                                }
                                                else {
                                                    $scope.isProcessing = false;
                                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                }
                                            }
                                            else {
                                                $scope.isProcessing = false;
                                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                            }
                                        });
                                    }
                                    else {
                                        SweetAlert.swal("Please upload Aadhar card copy!", "", "warning");
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please upload PAN card copy!", "", "warning");
                                }
                            }
                            else {
                                SweetAlert.swal("Please enter Pincode", "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Please enter Address!", "", "warning");
                        }
                    }

                    $scope.bankChangeRequest = function () {
                        if ($scope.txtIfsc) {
                            if (validationFactory.validIFSC($scope.txtIfsc)) {
                                if ($scope.ddlbankname) {
                                    if ($scope.txtBranchAdd) {
                                        if ($scope.txtBranchName) {
                                            if ($scope.txtBranchCity) {
                                                if ($scope.txtAccountNumber) {
                                                    if ($scope.txtAccountName) {
                                                        if ($scope.dataholder.srcCheque && $scope.dataholder.srcCheque != "../../../../assets/img/folder.png") {
                                                            $scope.isProcessing = true;
                                                            var bankChangeRequest = bankChangeRequestModel.BankChangeRequest;
                                                            bankChangeRequest.BasicId = $rootScope.prmBasicId;
                                                            bankChangeRequest.ifsc = $scope.txtIfsc;
                                                            bankChangeRequest.branchadd = $scope.txtBranchAdd;
                                                            bankChangeRequest.bankname = $scope.bankOptions.filter(function (obj) { return obj.value.trim().toUpperCase() === $scope.ddlbankname.trim().toUpperCase() })[0].name;
                                                            bankChangeRequest.branchname = $scope.txtBranchName;
                                                            bankChangeRequest.branchcity = $scope.txtBranchCity;
                                                            bankChangeRequest.micr = $scope.txtMicr;
                                                            bankChangeRequest.accountnumber = $scope.txtAccountNumber;
                                                            bankChangeRequest.accountname = $scope.txtAccountName;
                                                            bankChangeRequest.bankAccType = "SB";
                                                            bankChangeRequest.devicetype = "WEB";
                                                            bankChangeRequest.reason = "update it";
                                                            bankChangeRequest.chequeFile = $scope.chequefiles[0];

                                                            userService.callChangeBankReqApi(bankChangeRequest).then(function (serverResp) {
                                                                if (serverResp.status) {
                                                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                                        $scope.hide(dataToPass.updateType.trim().toLowerCase());
                                                                    }
                                                                    else {
                                                                        $scope.isProcessing = false;
                                                                        SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                                    }
                                                                }
                                                                else {
                                                                    $scope.isProcessing = false;
                                                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            SweetAlert.swal("Please upload Canceled Cheque copy!", "", "warning");
                                                        }
                                                    }
                                                    else {
                                                        SweetAlert.swal("Please enter name as per bank!", "", "warning");
                                                    }
                                                }
                                                else {
                                                    SweetAlert.swal("Please enter account number!", "", "warning");
                                                }
                                            }
                                            else {
                                                SweetAlert.swal("Please enter branch city!", "", "warning");
                                            }
                                        }
                                        else {
                                            SweetAlert.swal("Please enter branch name!", "", "warning");
                                        }
                                    }
                                    else {
                                        SweetAlert.swal("Please enter branch address!", "", "warning");
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please select bank", "", "warning");
                                }
                            }
                            else {
                                SweetAlert.swal("Invalid IFSC!", "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Please enter IFSC!", "", "warning");
                        }
                    }

                    $scope.updateNominee = function () {
                        $scope.validateNominee().then(function (result) {
                            if (result.status) {
                                if (result.isNomineeChecked) {
                                    if (!result.idDupNominee) {
                                        if (result.isShareOk) {
                                            $scope.setNomineeData().then(function (result) {
                                                if (result.status) {
                                                    $scope.isProcessing = true;
                                                    userService.callUpdateCafApi(dataToPass.data).then(function (serverResp) {
                                                        if (serverResp.status) {
                                                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                                $scope.hide(dataToPass.updateType.trim().toLowerCase());
                                                            }
                                                            else {
                                                                $scope.isProcessing = false;
                                                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                            }
                                                        }
                                                        else {
                                                            $scope.isProcessing = false;
                                                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                                        }
                                                    });
                                                }
                                                else {
                                                    SweetAlert.swal("Oop's something went wrong!!", "", "warning");
                                                }
                                            });
                                        }
                                        else {
                                            SweetAlert.swal("Total of shares should be 100% !!", "", "warning");
                                        }
                                    }
                                    else {
                                        SweetAlert.swal("Names of nominee's could not be duplicate!", "", "warning");
                                    }
                                }
                            }
                        });
                    }

                    $scope.nominee1 = function () {
                        var deferred = $q.defer();
                        var result = { status: false }
                        if ($scope.txtNom1Name) {
                            if ($scope.txtNom1Rel) {
                                if ($scope.txtNom1Dob) {
                                    if ($scope.txtNom1Share) {
                                        result.status = true;
                                        deferred.resolve(result);
                                    }
                                    else {
                                        SweetAlert.swal("Please enter first nominee share%!", "", "warning");
                                        deferred.resolve(result);
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please enter first nominee valid DOB!", "", "warning");
                                    deferred.resolve(result);
                                }
                            }
                            else {
                                SweetAlert.swal("Please enter first nominee relation!", "", "warning");
                                deferred.resolve(result);
                            }
                        }
                        else {
                            SweetAlert.swal("Please enter first nominee name!", "", "warning");
                            deferred.resolve(result);
                        }
                        return deferred.promise;
                    }

                    $scope.nominee2 = function () {
                        var deferred = $q.defer();
                        var result = { status: false }
                        if ($scope.txtNom2Share) {
                            if ($scope.txtNom2Name) {
                                if ($scope.txtNom2Rel) {
                                    if ($scope.txtNom2Dob) {
                                        if ($scope.txtNom2Share) {
                                            result.status = true;
                                            deferred.resolve(result);
                                        }
                                        else {
                                            SweetAlert.swal("Please enter second nominee share%!", "", "warning");
                                            deferred.resolve(result);
                                        }
                                    }
                                    else {
                                        SweetAlert.swal("Please enter second nominee valid DOB!", "", "warning");
                                        deferred.resolve(result);
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please enter second nominee relation!", "", "warning");
                                    deferred.resolve(result);
                                }
                            }
                            else {
                                SweetAlert.swal("Please enter second nominee name!", "", "warning");
                                deferred.resolve(result);
                            }
                        }
                        else {
                            result.status = true;
                            deferred.resolve(result);
                        }
                        return deferred.promise;
                    }

                    $scope.nominee3 = function () {
                        var deferred = $q.defer();
                        var result = { status: false }
                        if ($scope.txtNom3Share) {
                            if ($scope.txtNom3Name) {
                                if ($scope.txtNom3Rel) {
                                    if ($scope.txtNom3Dob) {
                                        if ($scope.txtNom3Share) {
                                            result.status = true;
                                            deferred.resolve(result);
                                        }
                                        else {
                                            SweetAlert.swal("Please enter third nominee share%!", "", "warning");
                                            deferred.resolve(result);
                                        }
                                    }
                                    else {
                                        SweetAlert.swal("Please enter third nominee valid DOB!", "", "warning");
                                        deferred.resolve(result);
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please enter third nominee relation!", "", "warning");
                                    deferred.resolve(result);
                                }
                            }
                            else {
                                SweetAlert.swal("Please enter third nominee name!", "", "warning");
                                deferred.resolve(result);
                            }
                        }
                        else {
                            result.status = true;
                            deferred.resolve(result);
                        }

                        return deferred.promise;
                    }

                    $scope.validateNominee = function () {
                        var deferred = $q.defer();
                        var result = { status: false, idDupNominee: false, isShareOk: false, isNomineeChecked: false }
                        $scope.nominee1().then(function (resultNom1) {
                            if (resultNom1.status) {
                                $scope.nominee2().then(function (resultNom2) {
                                    if (resultNom2.status) {
                                        $scope.nominee3().then(function (resultNom3) {

                                            if (resultNom3.status) {

                                                var isDupName = false;
                                                var nom1 = false;
                                                var nom2 = false;
                                                var nom3 = false;

                                                // CHECK DUPLICATE NAMES IN NOMINEE
                                                if ($scope.txtNom1Name) {
                                                    if ($scope.txtNom2Name && $scope.txtNom1Name === $scope.txtNom2Name) {
                                                        result.idDupNominee = true;
                                                        nom1 = true;
                                                        nom2 = true;
                                                    }
                                                    else if ($scope.txtNom3Name && ($scope.txtNom1Name === $scope.txtNom3Name)) {
                                                        result.idDupNominee = true;
                                                        nom1 = true;
                                                        nom3 = true;
                                                    }
                                                    else if ($scope.txtNom2Name && ($scope.txtNom2Name === $scope.txtNom3Name)) {
                                                        result.idDupNominee = true;
                                                        nom2 = true;
                                                        nom3 = true;
                                                    }
                                                }

                                                // CHECK TOTAL OF SAHRES
                                                var share1 = parseInt($scope.txtNom1Share ? $scope.txtNom1Share : 0);
                                                var share2 = parseInt($scope.txtNom2Share ? $scope.txtNom2Share : 0);
                                                var share3 = parseInt($scope.txtNom3Share ? $scope.txtNom3Share : 0);

                                                var total = share1 + share2 + share3;
                                                if (total === 100 && share1 > 0) {
                                                    if ($scope.txtNom2Name && share2 < 1) {
                                                        result.isShareOk = false;
                                                    }
                                                    else if ($scope.txtNom3Name && share3 < 1) {
                                                        result.isShareOk = false;
                                                    }
                                                    else {
                                                        result.isShareOk = true;
                                                    }

                                                } else { result.isShareOk = false; }

                                                result.isNomineeChecked = true;
                                                result.status = true;
                                                deferred.resolve(result);

                                            }
                                        });
                                    }
                                });
                            }
                        });
                        return deferred.promise;
                    }

                    $scope.setNomineeData = function () {
                        var deferred = $q.defer();
                        var result = { status: false }

                        if (dataToPass.data.cafNominee.length > 2) {
                            dataToPass.data.cafNominee[2].basicId = $scope.txtNom3BasicId;
                            dataToPass.data.cafNominee[2].nomId = $scope.txtNom3Id;
                            dataToPass.data.cafNominee[2].nomName = $scope.txtNom3Name;
                            dataToPass.data.cafNominee[2].nomDOB = $scope.txtNom3Dob ? $filter('date')($scope.txtNom3Dob, "yyyy-MM-dd") : "";
                            dataToPass.data.cafNominee[2].nomRealtion = $scope.txtNom3Rel;
                            dataToPass.data.cafNominee[2].nomShare = $scope.txtNom3Share;
                            dataToPass.data.cafNominee[2].nomGuardName = $scope.txtNom3GuardName;
                            dataToPass.data.cafNominee[2].nomGuardPan = $scope.txtNom3GuardPan;
                        }

                        if (dataToPass.data.cafNominee.length > 1) {
                            dataToPass.data.cafNominee[1].basicId = $scope.txtNom2BasicId;
                            dataToPass.data.cafNominee[1].nomId = $scope.txtNom2Id;
                            dataToPass.data.cafNominee[1].nomName = $scope.txtNom2Name;
                            dataToPass.data.cafNominee[1].nomDOB = $scope.txtNom2Dob ? $filter('date')($scope.txtNom2Dob, "yyyy-MM-dd") : "";
                            dataToPass.data.cafNominee[1].nomRealtion = $scope.txtNom2Rel;
                            dataToPass.data.cafNominee[1].nomShare = $scope.txtNom2Share;
                            dataToPass.data.cafNominee[1].nomGuardName = $scope.txtNom2GuardName;
                            dataToPass.data.cafNominee[1].nomGuardPan = $scope.txtNom2GuardPan;
                        }

                        if (dataToPass.data.cafNominee.length > 0) {
                            dataToPass.data.cafNominee[0].basicId = $scope.txtNom1BasicId;
                            dataToPass.data.cafNominee[0].nomId = $scope.txtNom1Id;
                            dataToPass.data.cafNominee[0].nomName = $scope.txtNom1Name;
                            dataToPass.data.cafNominee[0].nomDOB = $scope.txtNom1Dob ? $filter('date')($scope.txtNom1Dob, "yyyy-MM-dd") : "";
                            dataToPass.data.cafNominee[0].nomRealtion = $scope.txtNom1Rel;
                            dataToPass.data.cafNominee[0].nomShare = $scope.txtNom1Share;
                            dataToPass.data.cafNominee[0].nomGuardName = $scope.txtNom1GuardName;
                            dataToPass.data.cafNominee[0].nomGuardPan = $scope.txtNom1GuardPan;
                        }

                        result.status = true;
                        deferred.resolve(result);
                        return deferred.promise;
                    }

                    $scope.hide = function (val) {
                        $scope.isProcessing = false;
                        $mdDialog.hide(val);
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    if (dataToPass.updateType.trim().toLowerCase() == 'address') {
                        $scope.txtAddress = dataToPass.data.prmAddress;
                        $scope.txtPincode = dataToPass.data.prmPincode;
                        $scope.setPinDetails(dataToPass.data.prmState, dataToPass.data.prmCity)
                    }
                    else if (dataToPass.updateType.trim().toLowerCase() == 'bank') {
                        $scope.txtIfsc = dataToPass.data.ifsc;
                        $scope.banknm = $scope.bankOptions.filter(function (obj) { return obj.name.trim().toUpperCase() === dataToPass.data.bankName.trim().toUpperCase() })[0];
                        $scope.ddlbankname = $scope.banknm ? $scope.banknm.value : "";
                        $scope.txtBranchAdd = dataToPass.data.bankAddress;
                        $scope.txtBranchName = dataToPass.data.branchName;
                        $scope.txtBranchCity = dataToPass.data.branchCity;
                        $scope.txtMicr = dataToPass.data.micr;
                        $scope.txtAccountNumber = dataToPass.data.accNo;
                        $scope.txtAccountName = dataToPass.data.nameAsPerBank;
                    }
                    else {
                        if (dataToPass.data.cafNominee.length > 2) {
                            $scope.txtNom3BasicId = dataToPass.data.cafNominee[2].basicId;
                            $scope.txtNom3Id = dataToPass.data.cafNominee[2].nomId;
                            $scope.txtNom3Name = dataToPass.data.cafNominee[2].nomName;
                            $scope.txtNom3Rel = dataToPass.data.cafNominee[2].nomRealtion;
                            $scope.txtNom3Dob = dataToPass.data.cafNominee[2].nomDOB;
                            $scope.txtNom3Share = dataToPass.data.cafNominee[2].nomShare;
                            $scope.txtNom3GuardName = dataToPass.data.cafNominee[2].nomGuardName;
                            $scope.txtNom3GuardPan = dataToPass.data.cafNominee[2].nomGuardPan;
                        }
                        if (dataToPass.data.cafNominee.length > 1) {
                            $scope.txtNom2BasicId = dataToPass.data.cafNominee[1].basicId;
                            $scope.txtNom2Id = dataToPass.data.cafNominee[1].nomId;
                            $scope.txtNom2Name = dataToPass.data.cafNominee[1].nomName;
                            $scope.txtNom2Rel = dataToPass.data.cafNominee[1].nomRealtion;
                            $scope.txtNom2Dob = dataToPass.data.cafNominee[1].nomDOB;
                            $scope.txtNom2Share = dataToPass.data.cafNominee[1].nomShare;
                            $scope.txtNom2GuardName = dataToPass.data.cafNominee[1].nomGuardName;
                            $scope.txtNom2GuardPan = dataToPass.data.cafNominee[1].nomGuardPan;
                        }
                        if (dataToPass.data.cafNominee.length > 0) {
                            $scope.txtNom1BasicId = dataToPass.data.cafNominee[0].basicId;
                            $scope.txtNom1Id = dataToPass.data.cafNominee[0].nomId;
                            $scope.txtNom1Name = dataToPass.data.cafNominee[0].nomName;
                            $scope.txtNom1Rel = dataToPass.data.cafNominee[0].nomRealtion;
                            $scope.txtNom1Dob = dataToPass.data.cafNominee[0].nomDOB;
                            $scope.txtNom1Share = dataToPass.data.cafNominee[0].nomShare;
                            $scope.txtNom1GuardName = dataToPass.data.cafNominee[0].nomGuardName;
                            $scope.txtNom1GuardPan = dataToPass.data.cafNominee[0].nomGuardPan;
                        }
                    }
                }

                function viewController($scope, $mdDialog, $filter, dataToPass, SweetAlert, errorConstants, kycCafConstants, userFactory, cafeditRequestsResultModel, userService) {

                    $scope.isProcessing = true;

                    $scope.viewType = dataToPass.updateType.trim().toLowerCase() == 'address' ? 'All Address Request' : 'All Bank Request';
                    $scope.reqDataType = dataToPass.updateType.trim().toLowerCase() == 'address' ? '2' : '3';

                    userService.callChangeRequestStatusApi($rootScope.prmBasicId, $scope.reqDataType).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.previousChangeReqData = cafeditRequestsResultModel.parseCafeditRequestsResult(serverResp.data);
                                $scope.isProcessing = false;
                            }
                            else {
                                $scope.isProcessing = false;
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $scope.isProcessing = false;
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });

                    $scope.hide = function () {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };
                }

                $scope.loadData();
            }
            else {
                $state.go('dashboard.profile');
            }
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));