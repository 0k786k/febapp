﻿(function (finApp) {
    'use strict';
    finApp.controller('kyccafAddController', ['$scope', '$state', '$window', '$stateParams', '$rootScope', '$mdDialog', '$mdPanel', '$filter', '$q', 'oauthTokenFactory', 'userFactory', 'validationFactory', 'SweetAlert', 'errorConstants', 'transactionConstants', 'kycCafConstants', 'kycModel', 'fincartCaptchaRequestModel', 'POIPOAFileUploadModel', 'fincartPOIResponseModel', 'fincartDeleteStepModel', 'fincartPOISaveModel', 'fincartKYcDataModel', 'fincartStartVedioResponseModel', 'getBankInfoResultModel', 'fincartBankDetailsSaveModel', 'signzyUserProfileStageModel', 'userService', function ($scope, $state, $window, $stateParams, $rootScope, $mdDialog, $mdPanel, $filter, $q, oauthTokenFactory, userFactory, validationFactory, SweetAlert, errorConstants, transactionConstants, kycCafConstants, kycModel, fincartCaptchaRequestModel, POIPOAFileUploadModel, fincartPOIResponseModel, fincartDeleteStepModel, fincartPOISaveModel, fincartKYcDataModel, fincartStartVedioResponseModel, getBankInfoResultModel, fincartBankDetailsSaveModel, signzyUserProfileStageModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {
            if ($rootScope.prmBasicId) {
                $rootScope.mainheader = "Paperless Account Opening";
                $rootScope.mainheaderimg = "";
                $rootScope.currActiveTab = "profile";

                $scope.kycdata = null;
                $scope.vedioFileData = null;
                $scope.addnomineepart = '';
                $scope.currPage = "";
                $scope.isStepScreen = false;
                $scope.isPageLoaded = false;
                $scope.Screen = "INFO"
                $scope.btndisable = false;
                $scope.txtEkycPan = "";
                $scope.img_url = "";
                $scope.accType = "01";
                $scope.captchaModel = null;
                $scope.bankvalue = "";
                $scope.isbankResp = false;

                $scope.docTypes = kycCafConstants.docTypes;

                $scope.incomeOptions = kycCafConstants.incomeOptions;

                $scope.occupationOptions = kycCafConstants.occupationOptions;

                $scope.genderOptions = kycCafConstants.genderOptions;

                $scope.addressTypeOptions = kycCafConstants.addressTypeOptions;

                $scope.maritalStatusOptions = kycCafConstants.maritalStatusOptions;

                $scope.wealthsourceOptions = kycCafConstants.wealthsourceOptions;

                $scope.bankOptions = kycCafConstants.bankOptions;

                $scope.accounttypeOptions = kycCafConstants.accounttypeOptions;

                $scope.bankData = fincartBankDetailsSaveModel.FincartBankDetailsSave;

                $scope.dataholder = {
                    srcPan: "../../../../assets/img/folder.png",
                    panUploadImgText: "Upload your PAN copy.",
                    srcPoaF: "../../../../assets/img/folder.png",
                    poaFUploadImgText: "Upload your address proof front copy.",
                    srcPoaB: "../../../../assets/img/folder.png",
                    poaBUploadImgText: "Upload your address proof back copy.",
                    srcBankCanceledCheque: "../../../../assets/img/folder.png",
                    bankCanceledChequeUploadImgText: "Upload your cancel cheque.",
                    srcSign: "../../../../assets/img/folder.png",
                    bankSignText: "Upload your signature.",
                    srcPhoto: "../../../../assets/img/folder.png",
                    photoUploadImgText: "Upload your Photo."
                }


                $scope.ddlDocTypes = "";
                $scope.ddloccupation = "";
                $scope.ddlgender = "";
                $scope.ddladdressType = "";
                $scope.ddlmaritalStatus = "";
                $scope.ddlincome = "";
                $scope.ddlwealthsource = "";

                $scope.poaPoifiles = [];
                $scope.bankChequefiles = [];
                $scope.signfiles = [];
                $scope.photofiles = [];

                $rootScope.showLoader();

                $scope.userdetails = userFactory.getUserLoggedInDetails();
                $scope.groupLeaderUserId = $scope.userdetails.userid;

                if ($rootScope.prmBasicId == $scope.userdetails.basicid) {
                    $scope.txtEkycName = $scope.userdetails.name;
                    $scope.txtEkycEmail = $scope.userdetails.userid;
                    $scope.txtEkycMobile = $scope.userdetails.mobile;
                }

                $scope.setMemberDetails = function (basicid) {
                    userService.callSignzyNextStepApi(basicid).then(function (serverResp) {
                        if (serverResp.status) {
                            if (serverResp.status.toUpperCase() === "SUCCESS") {
                                $scope.currentMemberStage = signzyUserProfileStageModel.parseSignzyUserProfileStage(serverResp.data);

                                $scope.kycdata = kycModel.KYC;
                                $scope.kycdata.KYC_STATUS = $scope.currentMemberStage.isKYC ? "Y" : "N";

                                $scope.userdetails.isGoal = $scope.currentMemberStage.isGoal;
                                $scope.userdetails.isQuicksip = $scope.currentMemberStage.isQuickSip;
                                $scope.userdetails.isAccountDone = $scope.currentMemberStage.isAccount;
                                $scope.userdetails.isPOIPOADone = $scope.currentMemberStage.isPOIPOA;
                                $scope.userdetails.isBasicDone = $scope.currentMemberStage.isBasic;
                                $scope.userdetails.isVideoDone = $scope.currentMemberStage.isVedio;
                                $scope.userdetails.isBankDone = $scope.currentMemberStage.isBank;
                                $scope.userdetails.isSignDone = $scope.currentMemberStage.isSign;
                                $scope.isPageLoaded = true;
                                if ($scope.userdetails.isAccountDone || $scope.userdetails.isPOIPOADone || $scope.userdetails.isBasicDone || $scope.userdetails.isVideoDone || $scope.userdetails.isBankDone || $scope.userdetails.isSignDone) {
                                    $scope.isStepScreen = true;
                                }
                                $rootScope.hideLoader();
                            }
                            else {
                                $rootScope.hideLoader();
                                SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                            }
                        }
                        else {
                            $rootScope.hideLoader();
                            $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                        }
                    });
                }

                $scope.setMemberDetails($rootScope.prmBasicId);

                $scope.start = function () {
                    $scope.currPage = "P";
                }

                $scope.openModal = function (ev, type, dataForPopup) {

                    var ctrl = kycProcessModalController;
                    var tmpl = 'kycProcessModal.tmpl.html';

                    if (type == "poipoa") {
                        ctrl = poiPoaProcessModalController;
                        tmpl = 'poiPoaConfirmModal.tmpl.html';
                    }
                    else if (type == "edit") {
                        ctrl = editModalController;
                        tmpl = 'editModal.tmpl.html';
                    }
                    else if (type == "S") {
                        ctrl = bankConfirmModalController;
                        tmpl = 'bankConfirmModal.tmpl.html';
                    }

                    $mdDialog.show({
                        locals: { dataToPass: dataForPopup },
                        controller: ctrl,
                        templateUrl: tmpl,
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                    })
                    .then(function (val) {
                        if (type == "edit") {
                            if (val.trim().toUpperCase() == 'C') {
                                $scope.userdetails.isAccountDone = false;
                                $scope.userdetails.isPOIPOADone = false;
                                $scope.userdetails.isBasicDone = false;
                                $scope.userdetails.isVideoDone = false;
                                $scope.userdetails.isBankDone = false;
                                $scope.userdetails.isSignDone = false;
                            }
                            else if (val.trim().toUpperCase() == 'I') {
                                $scope.userdetails.isPOIPOADone = false;
                                $scope.userdetails.isBasicDone = false;
                                $scope.userdetails.isVideoDone = false;
                                $scope.userdetails.isBankDone = false;
                                $scope.userdetails.isSignDone = false;
                            }
                            else if (val.trim().toUpperCase() == 'B') {
                                $scope.userdetails.isBasicDone = false;
                                $scope.userdetails.isVideoDone = false;
                                $scope.userdetails.isBankDone = false;
                                $scope.userdetails.isSignDone = false;
                            }
                            else if (val.trim().toUpperCase() == 'V') {
                                $scope.userdetails.isVideoDone = false;
                                $scope.userdetails.isBankDone = false;
                                $scope.userdetails.isSignDone = false;
                                $scope.Screen = "INFO"
                            }
                            else if (val.trim().toUpperCase() == 'BNK') {
                                $scope.userdetails.isBankDone = false;
                                $scope.userdetails.isSignDone = false;
                            }
                            $scope.setPage(val);
                        }
                        else {
                            if (val == 'S') {
                                $scope.updateBank('S');
                            }
                            else {
                                if (val == 'B') {
                                    $scope.userdetails.isPOIPOADone = true;
                                }
                                $scope.setPage(val);
                            }
                        }
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function () {
                        //$scope.status = 'You cancelled the dialog.';
                    });
                };

                function kycProcessModalController($scope, $mdDialog, dataToPass, SweetAlert, errorConstants) {

                    $scope.kycData = dataToPass;

                    $scope.hide = function (val) {
                        $mdDialog.hide(val);
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.confirmStep = function (val) {
                        $scope.hide(val);
                    };
                }

                function poiPoaProcessModalController($scope, $mdDialog, dataToPass, SweetAlert, errorConstants, fincartDeleteStepModel, fincartPOISaveModel, userService) {

                    $scope.poiPoaData = dataToPass.poiPoaData;

                    $scope.isProcessing = false;

                    $scope.hide = function (val) {
                        $scope.isProcessing = false;
                        $mdDialog.hide(val);
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.editStep = function (step) {

                        $scope.isProcessing = true;

                        var fincartDeleteStep = fincartDeleteStepModel.FincartDeleteStep
                        fincartDeleteStep.basicid = dataToPass.basicid;
                        fincartDeleteStep.step = step;

                        userService.callSignzyStepDeleteApi(fincartDeleteStep).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $mdDialog.cancel();
                                }
                                else {
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                    $scope.isProcessing = false;
                                }
                            }
                            else {
                                $scope.isProcessing = false;
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });
                    };

                    $scope.updatePoiPoa = function (val) {

                        $scope.isProcessing = true;

                        var fincartPOISave = fincartPOISaveModel.FincartPOISave;
                        fincartPOISave.basicid = dataToPass.basicid;

                        userService.callSignzyUpdatePOIPOAApi(fincartPOISave).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $mdDialog.hide(val);
                                }
                                else {
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                    $scope.isProcessing = false;
                                }
                            }
                            else {
                                $scope.isProcessing = false;
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });
                    };
                }

                function editModalController($scope, $mdDialog, dataToPass, SweetAlert, errorConstants, fincartDeleteStepModel, userService) {

                    $scope.editData = dataToPass;
                    $scope.isProcessing = false;

                    $scope.hide = function (val) {
                        $scope.isProcessing = false;
                        $mdDialog.hide(val);
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.confirmToEdit = function () {

                        $scope.isProcessing = true;

                        var fincartDeleteStep = fincartDeleteStepModel.FincartDeleteStep
                        fincartDeleteStep.basicid = $scope.editData.basicid;
                        fincartDeleteStep.step = $scope.editData.step;

                        userService.callSignzyStepDeleteApi(fincartDeleteStep).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $mdDialog.hide($scope.editData.backStep);
                                }
                                else {
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                    $scope.isProcessing = false;
                                }
                            }
                            else {
                                $scope.isProcessing = false;
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });

                    };
                }

                function bankConfirmModalController($scope, $mdDialog, dataToPass, SweetAlert, errorConstants) {

                    $scope.nxtStep = dataToPass;

                    $scope.hide = function (val) {
                        $mdDialog.hide(val);
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.confirmToPennyTransfer = function (val) {
                        $scope.hide(val);
                    };
                }

                $scope.setPage = function (val) {
                    $scope.currPage = val;
                }

                $scope.fileUploadImageClick = function (id) {
                    angular.element('#' + id).click();
                }

                $scope.changeFileUpload = function ($event, type) {
                    if ($event.target.files.length > 0) {
                        if (type.trim().toLowerCase() === "sign") {
                            $scope.dataholder.srcSign = URL.createObjectURL($event.target.files[0]);
                            $scope.dataholder.bankSignText = $event.target.files[0].name;

                            if ($scope.signfiles.length > 0) {
                                $scope.signfiles[0] = $event.target.files[0];
                            }
                            else {
                                $scope.signfiles.push($event.target.files[0]);
                            }

                        }
                        else if (type.trim().toLowerCase() === "bank") {
                            $scope.dataholder.srcBankCanceledCheque = URL.createObjectURL($event.target.files[0]);
                            $scope.dataholder.bankCanceledChequeUploadImgText = $event.target.files[0].name;

                            if ($scope.bankChequefiles.length > 0) {
                                $scope.bankChequefiles[0] = $event.target.files[0];
                            }
                            else {
                                $scope.bankChequefiles.push($event.target.files[0]);
                            }

                        }
                        else if (type.trim().toLowerCase() === "photo") {
                            $scope.dataholder.srcPhoto = URL.createObjectURL($event.target.files[0]);
                            $scope.dataholder.photoUploadImgText = $event.target.files[0].name;

                            if ($scope.photofiles.length > 0) {
                                $scope.photofiles[0] = $event.target.files[0];
                            }
                            else {
                                $scope.photofiles.push($event.target.files[0]);
                            }

                        }
                        else if (type.trim().toLowerCase() === "poi") {
                            $scope.dataholder.srcPan = URL.createObjectURL($event.target.files[0]);
                            $scope.dataholder.panUploadImgText = $event.target.files[0].name;

                            if ($scope.poaPoifiles.length > 0) {
                                $scope.poaPoifiles[0] = $event.target.files[0];
                            }
                            else {
                                $scope.poaPoifiles.push($event.target.files[0]);
                            }

                        }
                        else if (type.trim().toLowerCase() === "poaf") {
                            $scope.dataholder.srcPoaF = URL.createObjectURL($event.target.files[0]);
                            $scope.dataholder.poaFUploadImgText = $event.target.files[0].name;
                            if ($scope.poaPoifiles.length > 1) {
                                $scope.poaPoifiles[1] = $event.target.files[0];
                            }
                            else {
                                $scope.poaPoifiles.push($event.target.files[0]);
                            }
                        }
                        else {
                            $scope.dataholder.srcPoaB = URL.createObjectURL($event.target.files[0]);
                            $scope.dataholder.poaBUploadImgText = $event.target.files[0].name;
                            if ($scope.poaPoifiles.length > 2) {
                                $scope.poaPoifiles[2] = $event.target.files[0];
                            }
                            else {
                                $scope.poaPoifiles.push($event.target.files[0]);
                            }
                        }
                    }
                }

                $scope.setEditStep = function (ev, step, backStep) {
                    $scope.editData = { step: step, backStep: backStep, basicid: $rootScope.prmBasicId };
                    $scope.openModal(ev, 'edit', $scope.editData);
                }

                $scope.addmorenominee = function (val) {
                    $scope.addnomineepart = val
                }

                $scope.chkNomChange = function (val) {
                    $scope.addmorenominee(val);
                }

                $scope.ValidateNominee = function () {
                    var deferred = $q.defer();
                    var result = { status: false, idDupNominee: false, isShareOk: false, isNomineeChecked: false }
                    if ($scope.checkboxModelNominee) {
                        $scope.nominee1().then(function (resultNom1) {
                            if (resultNom1.status) {
                                $scope.nominee2().then(function (resultNom2) {
                                    if (resultNom2.status) {
                                        $scope.nominee3().then(function (resultNom3) {

                                            if (resultNom3.status) {

                                                var isDupName = false;
                                                var nom1 = false;
                                                var nom2 = false;
                                                var nom3 = false;

                                                // CHECK DUPLICATE NAMES IN NOMINEE
                                                if ($scope.txtname1) {
                                                    if ($scope.txtname2 && $scope.txtname1 === $scope.txtname2) {
                                                        result.idDupNominee = true;
                                                        nom1 = true;
                                                        nom2 = true;
                                                    }
                                                    else if ($scope.txtname3 && ($scope.txtname1 === $scope.txtname3)) {
                                                        result.idDupNominee = true;
                                                        nom1 = true;
                                                        nom3 = true;
                                                    }
                                                    else if ($scope.txtname2 && ($scope.txtname2 === $scope.txtname3)) {
                                                        result.idDupNominee = true;
                                                        nom2 = true;
                                                        nom3 = true;
                                                    }
                                                }

                                                // CHECK TOTAL OF SAHRES
                                                var share1 = parseInt($scope.txtshare1 ? $scope.txtshare1 : 0);
                                                var share2 = parseInt($scope.txtshare2 ? $scope.txtshare2 : 0);
                                                var share3 = parseInt($scope.txtshare3 ? $scope.txtshare3 : 0);

                                                var total = share1 + share2 + share3;
                                                if (total === 100 && share1 > 0) {
                                                    if ($scope.txtname2 && share2 < 1) {
                                                        result.isShareOk = false;
                                                    }
                                                    else if ($scope.txtname3 && share3 < 1) {
                                                        result.isShareOk = false;
                                                    }
                                                    else {
                                                        result.isShareOk = true;
                                                    }

                                                } else { result.isShareOk = false; }

                                                result.isNomineeChecked = true;
                                                result.status = true;
                                                deferred.resolve(result);

                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                    else {
                        result.status = true;
                        deferred.resolve(result);
                    }
                    return deferred.promise;
                }

                $scope.IsValidDOB = function (objDOB) {

                    var deferred = $q.defer();
                    var res = false;
                    UserService.serverDate().then(function (currdate) {
                        if (currdate.isdata) {
                            if ($scope.getAge(currdate.Date, objDOB) >= 18) {
                                res = true;
                            }
                            else {
                                res = false;
                            }
                        }
                        deferred.resolve(res);
                    });
                    return deferred.promise;
                }

                $scope.getAge = function (currDateString, birthDateString) {


                    var todayDatePart;


                    if (currDateString.indexOf('/') > -1) {
                        todayDatePart = currDateString.split("/");
                    }
                    else if (currDateString.indexOf('-') > -1) {
                        todayDatePart = currDateString.split("-");
                    }


                    var today = new Date(todayDatePart[2], todayDatePart[1] - 1, todayDatePart[0]);
                    var birthDate = new Date(birthDateString);


                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }
                    return age;
                }

                $scope.nominee1 = function () {
                    var deferred = $q.defer();
                    var result = { status: false }
                    if ($scope.txtname1) {
                        if ($scope.txtrelation1) {
                            if ($scope.txtdob1) {
                                if ($scope.txtshare1) {
                                    result.status = true;
                                    deferred.resolve(result);
                                }
                                else {
                                    SweetAlert.swal("Please enter first nominee share%!", "", "warning");
                                    deferred.resolve(result);
                                }
                            }
                            else {
                                SweetAlert.swal("Please enter first nominee valid DOB!", "", "warning");
                                deferred.resolve(result);
                            }
                        }
                        else {
                            SweetAlert.swal("Please enter first nominee relation!", "", "warning");
                            deferred.resolve(result);
                        }
                    }
                    else {
                        SweetAlert.swal("Please enter first nominee name!", "", "warning");
                        deferred.resolve(result);
                    }
                    return deferred.promise;
                }

                $scope.nominee2 = function () {
                    var deferred = $q.defer();
                    var result = { status: false }
                    if ($scope.txtshare2) {
                        if ($scope.txtname2) {
                            if ($scope.txtrelation2) {
                                if ($scope.txtdob2) {
                                    if ($scope.txtshare2) {
                                        result.status = true;
                                        deferred.resolve(result);
                                    }
                                    else {
                                        SweetAlert.swal("Please enter second nominee share%!", "", "warning");
                                        deferred.resolve(result);
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please enter second nominee valid DOB!", "", "warning");
                                    deferred.resolve(result);
                                }
                            }
                            else {
                                SweetAlert.swal("Please enter second nominee relation!", "", "warning");
                                deferred.resolve(result);
                            }
                        }
                        else {
                            SweetAlert.swal("Please enter second nominee name!", "", "warning");
                            deferred.resolve(result);
                        }
                    }
                    else {
                        result.status = true;
                        deferred.resolve(result);
                    }
                    return deferred.promise;
                }

                $scope.nominee3 = function () {
                    var deferred = $q.defer();
                    var result = { status: false }
                    if ($scope.txtshare3) {
                        if ($scope.txtname3) {
                            if ($scope.txtrelation3) {
                                if ($scope.txtdob3) {
                                    if ($scope.txtshare3) {
                                        result.status = true;
                                        deferred.resolve(result);
                                    }
                                    else {
                                        SweetAlert.swal("Please enter third nominee share%!", "", "warning");
                                        deferred.resolve(result);
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please enter third nominee valid DOB!", "", "warning");
                                    deferred.resolve(result);
                                }
                            }
                            else {
                                SweetAlert.swal("Please enter third nominee relation!", "", "warning");
                                deferred.resolve(result);
                            }
                        }
                        else {
                            SweetAlert.swal("Please enter third nominee name!", "", "warning");
                            deferred.resolve(result);
                        }
                    }
                    else {
                        result.status = true;
                        deferred.resolve(result);
                    }

                    return deferred.promise;
                }

                $scope.ifscChange = function () {
                    if (!$scope.btndisable) {
                        if ($scope.bankData.ifsc.length > 10) {
                            if (validationFactory.validIFSC($scope.bankData.ifsc)) {
                                $scope.btndisable = true;
                                userService.callBankDetailsByIFSCApi($scope.bankData.ifsc).then(function (serverResp) {
                                    if (serverResp.status) {
                                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                                            var bankdetails = getBankInfoResultModel.parseGetBankInfoResult(serverResp.data);
                                            if (bankdetails.BankName) {
                                                $scope.bankData.branch = bankdetails.Branch;
                                                $scope.bankData.bankaddress = bankdetails.BankAddress;
                                                $scope.bankData.micr = bankdetails.MICR;
                                                $scope.bankData.bankcity = bankdetails.City;

                                                var banknm = $scope.bankOptions.filter(function (obj) { return obj.name.trim().toUpperCase() === bankdetails.BankName.trim().toUpperCase() })[0];
                                                $scope.bankvalue = banknm ? banknm.value : "";
                                                SweetAlert.swal("We found your bank details..", "", "success");
                                                $scope.btndisable = false;
                                            }
                                            else {
                                                $scope.isbankResp = true;
                                                $scope.bankData.branch = "";
                                                $scope.bankData.bankaddress = "";
                                                $scope.bankData.micr = "";
                                                $scope.bankData.bankcity = "";
                                                $scope.bankvalue = "";
                                                SweetAlert.swal("Sorry..!! we are not able to locate your bank details. Fill details manually!", "", "error");
                                                $scope.btndisable = false;
                                            }
                                        }
                                        else {
                                            $scope.isbankResp = false;
                                            SweetAlert.swal(serverResp.msg, "", "error");
                                            //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                            $scope.btndisable = false;
                                        }
                                    }
                                    else {
                                        $scope.isbankResp = false;
                                        $scope.btndisable = false;
                                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                    }
                                });
                            }
                            else {
                                $scope.isbankResp = false;
                                SweetAlert.swal("Invalid IFSC!", "", "warning");
                            }
                        }
                        else {
                            $scope.isbankResp = false;
                        }
                    }
                }

                $scope.validateBank = function (ev, val) {
                    if ($scope.bankData.ifsc) {
                        if ($scope.bankData.ifsc.length > 10 && validationFactory.validIFSC($scope.bankData.ifsc)) {
                            if ($scope.bankData.accountnumber && validationFactory.validAccNumber($scope.bankData.accountnumber)) {
                                if ($scope.bankData.AccountType) {
                                    var banknm = $scope.bankOptions.filter(function (obj) { return obj.value.trim().toUpperCase() === $scope.bankvalue.trim().toUpperCase() })[0];
                                    if (banknm) {
                                        if ($scope.bankData.nameasperbank) {
                                            if ($scope.bankData.micr) {
                                                if ($scope.bankData.bankcity) {
                                                    if ($scope.bankData.branch) {
                                                        if ($scope.bankData.bankaddress) {
                                                            $scope.openModal(ev, val, val);
                                                        }
                                                        else {
                                                            SweetAlert.swal("Please enter bank branch address!", "", "warning");
                                                        }
                                                    }
                                                    else {
                                                        SweetAlert.swal("Please enter bank branch name!", "", "warning");
                                                    }
                                                }
                                                else {
                                                    SweetAlert.swal("Please enter bank city!", "", "warning");
                                                }
                                            }
                                            else {
                                                SweetAlert.swal("Please enter MICR!", "", "warning");
                                            }
                                        }
                                        else {
                                            SweetAlert.swal("Please enter name as per bank!", "", "warning");
                                        }
                                    }
                                    else {
                                        SweetAlert.swal("Please select bank name!", "", "warning");
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please select account type!", "", "warning");
                                }
                            }
                            else {
                                SweetAlert.swal("Invalid account number!", "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Invalid IFSC!", "", "warning");
                        }
                    }
                    else {
                        SweetAlert.swal("Please enter IFSC!", "", "warning");
                    }
                }

                $scope.nxtScreen = function (val) {
                    $scope.Screen = val;
                }

                //Api Callings start
                $scope.checkkyc = function (ev) {
                    if (!$scope.btndisable) {
                        if ($scope.txtEkycPan) {
                            if (validationFactory.validPan($scope.txtEkycPan, $scope.accType)) {
                                $scope.btndisable = true;

                                userService.callCheckKycStatusApi($scope.txtEkycPan).then(function (serverResp) {
                                    if (serverResp.status) {
                                        if (serverResp.status.toUpperCase() === "SUCCESS") {

                                            $scope.kycdata = kycModel.parseKYC(serverResp.data);
                                            $scope.txtEkycName = $scope.kycdata.PAN_NAME;
                                            $scope.btndisable = false;
                                            $scope.openModal(ev, 'kyc', $scope.kycdata);

                                        }
                                        else {
                                            SweetAlert.swal(serverResp.msg, "", "error");
                                            //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                            $scope.btndisable = false;
                                        }
                                    }
                                    else {
                                        $scope.btndisable = false;
                                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                    }
                                });

                            }
                            else {
                                SweetAlert.swal("Invalid PAN", "", "warning");
                                $scope.btndisable = false;
                            }
                        } else {
                            SweetAlert.swal("Please Enter PAN", "", "warning");
                        }
                    }
                }

                $scope.signzyaccountcreation = function (val) {
                    if (!$scope.btndisable) {

                        if ($scope.txtEkycName) {

                            if ($scope.txtEkycEmail) {

                                if (validationFactory.validEmail($scope.txtEkycEmail)) {

                                    if ($scope.txtEkycMobile) {

                                        if (validationFactory.validMobile($scope.txtEkycMobile, '+91')) {

                                            $scope.btndisable = true;

                                            var fincartCaptchaRequest = fincartCaptchaRequestModel.FincartCaptchaRequest;

                                            fincartCaptchaRequest.pannumber = $scope.txtEkycPan;
                                            fincartCaptchaRequest.email = $scope.txtEkycEmail;
                                            fincartCaptchaRequest.mobile = $scope.txtEkycMobile;
                                            fincartCaptchaRequest.name = $scope.txtEkycName;
                                            fincartCaptchaRequest.basicid = $rootScope.prmBasicId;
                                            fincartCaptchaRequest.GroupLeader = $scope.groupLeaderUserId;
                                            fincartCaptchaRequest.IsKYC = $scope.kycdata.KYC_STATUS;

                                            userService.callSignzyAccountCreationApi(fincartCaptchaRequest).then(function (serverResp) {
                                                if (serverResp.status) {
                                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                        $rootScope.prmBasicId = serverResp.data.basicid;
                                                        $scope.userdetails.isAccountDone = true;
                                                        $scope.btndisable = false;
                                                        $scope.setPage(val);
                                                    }
                                                    else {
                                                        SweetAlert.swal(serverResp.msg, "", "error");
                                                        //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                        $scope.btndisable = false;
                                                    }
                                                }
                                                else {
                                                    $scope.btndisable = false;
                                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                                }
                                            });

                                        }
                                        else {
                                            SweetAlert.swal("Invalid Investor Mobile", "", "warning");
                                        }
                                    }
                                    else {
                                        SweetAlert.swal("Please Enter Investor Mobile", "", "warning");
                                    }
                                }
                                else {
                                    SweetAlert.swal("Invalid Investor Email", "", "warning");
                                }
                            }
                            else {
                                SweetAlert.swal("Please Enter Investor Email", "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Please Enter PAN", "", "warning");
                        }
                    }
                }

                $scope.uploadPoiPoa = function (ev) {
                    if (!$scope.btndisable) {

                        if ($scope.dataholder.srcPan && $scope.dataholder.srcPan != "../../../../assets/img/folder.png") {
                            if ($scope.ddlDocTypes) {
                                if ($scope.dataholder.srcPoaF && $scope.dataholder.srcPoaF != "../../../../assets/img/folder.png") {

                                    if ($scope.dataholder.srcPoaB && $scope.dataholder.srcPoaB != "../../../../assets/img/folder.png") {

                                        $scope.btndisable = true;

                                        var POIPOAFileUpload = POIPOAFileUploadModel.POIPOAFileUpload;
                                        POIPOAFileUpload.basicid = $rootScope.prmBasicId;
                                        POIPOAFileUpload.type = $scope.ddlDocTypes;
                                        POIPOAFileUpload.panfile1 = $scope.poaPoifiles[0];
                                        POIPOAFileUpload.frontfile2 = $scope.poaPoifiles[1];
                                        POIPOAFileUpload.backfile3 = $scope.poaPoifiles[2];

                                        userService.callSignzyPOIPOAFileUploadApi(POIPOAFileUpload).then(function (serverResp) {
                                            if (serverResp.status) {
                                                if (serverResp.status.toUpperCase() === "SUCCESS") {

                                                    var poiPoaData = fincartPOIResponseModel.parseFincartPOIResponse(serverResp.data);
                                                    $scope.addressData = { poiPoaData: poiPoaData, basicid: $rootScope.prmBasicId };
                                                    $scope.btndisable = false;
                                                    $scope.openModal(ev, 'poipoa', $scope.addressData);
                                                }
                                                else {
                                                    SweetAlert.swal(serverResp.msg, "", "error");
                                                    //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                    $scope.btndisable = false;
                                                }
                                            }
                                            else {
                                                $scope.btndisable = false;
                                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                            }
                                        });
                                    }
                                    else {
                                        SweetAlert.swal("Please upload address proof back copy!", "", "warning");
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please upload address proof front copy!", "", "warning");
                                }
                            }
                            else {
                                SweetAlert.swal("Please select address proof type!", "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Please upload PAN card copy!", "", "warning");
                        }
                    }
                }

                $scope.updateKycForm = function (ev, val) {
                    if (!$scope.btndisable) {

                        if ($scope.txtEkycMotherName) {
                            if ($scope.ddlgender) {
                                if ($scope.ddlmaritalStatus) {
                                    if ($scope.ddloccupation) {
                                        if ($scope.ddladdressType) {
                                            if ($scope.ddlincome) {
                                                if ($scope.ddlwealthsource) {

                                                    $scope.ValidateNominee().then(function (result) {
                                                        if (result.status) {
                                                            if (result.isNomineeChecked) {
                                                                if (!result.idDupNominee) {
                                                                    if (result.isShareOk) {

                                                                        $scope.btndisable = true;

                                                                        var fincartKYcData = fincartKYcDataModel.FincartKYcData;
                                                                        fincartKYcData.basicid = $rootScope.prmBasicId;
                                                                        fincartKYcData.gender = $scope.ddlgender;
                                                                        fincartKYcData.martialStatus = $scope.ddlmaritalStatus;
                                                                        fincartKYcData.mothersName = $scope.txtEkycMotherName;
                                                                        fincartKYcData.occupation = $scope.ddloccupation;
                                                                        fincartKYcData.addressType = $scope.ddladdressType;
                                                                        fincartKYcData.annualIncome = $scope.ddlincome;
                                                                        fincartKYcData.sourceofIncome = $scope.ddlwealthsource;
                                                                        fincartKYcData.Nominee_Name1 = $scope.txtname1;
                                                                        fincartKYcData.Relation1 = $scope.txtrelation1;
                                                                        fincartKYcData.Nominee_Dob1 = $scope.txtdob1 ? $filter('date')($scope.txtdob1, "yyyy-MM-dd") : "";
                                                                        fincartKYcData.Nom1_percent = $scope.txtshare1;
                                                                        fincartKYcData.Nominee_Name2 = $scope.txtname2;
                                                                        fincartKYcData.Relation2 = $scope.txtrelation2;
                                                                        fincartKYcData.Nominee_Dob2 = $scope.txtdob2 ? $filter('date')($scope.txtdob2, "yyyy-MM-dd") : "";
                                                                        fincartKYcData.Nom2_percent = $scope.txtshare2;
                                                                        fincartKYcData.Nominee_Name3 = $scope.txtname3;
                                                                        fincartKYcData.Relation3 = $scope.txtrelation3;
                                                                        fincartKYcData.Nominee_Dob3 = $scope.txtdob3 ? $filter('date')($scope.txtdob3, "yyyy-MM-dd") : "";
                                                                        fincartKYcData.Nom3_percent = $scope.txtshare3;

                                                                        userService.callSignzyUpdateKycFormApi(fincartKYcData).then(function (serverResp) {
                                                                            if (serverResp.status) {
                                                                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                                                    $scope.userdetails.isBasicDone = true;
                                                                                    if (val == 'BNK') {
                                                                                        $scope.userdetails.isVideoDone = true;
                                                                                    }
                                                                                    $scope.btndisable = false;
                                                                                    $scope.setPage(val);
                                                                                }
                                                                                else {
                                                                                    SweetAlert.swal(serverResp.msg, "", "error");
                                                                                    //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                                                    $scope.btndisable = false;
                                                                                }
                                                                            }
                                                                            else {
                                                                                $scope.btndisable = false;
                                                                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                                                            }
                                                                        });

                                                                    }
                                                                    else {
                                                                        SweetAlert.swal("Total of shares should be 100% !!", "", "warning");
                                                                    }
                                                                }
                                                                else {
                                                                    SweetAlert.swal("Names of nominee's could not be duplicate!", "", "warning");
                                                                }
                                                            }
                                                            else {

                                                                $scope.btndisable = true;

                                                                var fincartKYcData = fincartKYcDataModel.FincartKYcData;
                                                                fincartKYcData.basicid = $rootScope.prmBasicId;
                                                                fincartKYcData.gender = $scope.ddlgender;
                                                                fincartKYcData.martialStatus = $scope.ddlmaritalStatus;
                                                                fincartKYcData.mothersName = $scope.txtEkycMotherName;
                                                                fincartKYcData.occupation = $scope.ddloccupation;
                                                                fincartKYcData.addressType = $scope.ddladdressType;
                                                                fincartKYcData.annualIncome = $scope.ddlincome;
                                                                fincartKYcData.sourceofIncome = $scope.ddlwealthsource;
                                                                fincartKYcData.Nominee_Name1 = $scope.txtname1;
                                                                fincartKYcData.Relation1 = $scope.txtrelation1;
                                                                fincartKYcData.Nominee_Dob1 = $scope.txtdob1 ? $filter('date')($scope.txtdob1, "yyyy-MM-dd") : "";
                                                                fincartKYcData.Nom1_percent = $scope.txtshare1;
                                                                fincartKYcData.Nominee_Name2 = $scope.txtname2;
                                                                fincartKYcData.Relation2 = $scope.txtrelation2;
                                                                fincartKYcData.Nominee_Dob2 = $scope.txtdob2 ? $filter('date')($scope.txtdob2, "yyyy-MM-dd") : "";
                                                                fincartKYcData.Nom2_percent = $scope.txtshare2;
                                                                fincartKYcData.Nominee_Name3 = $scope.txtname3;
                                                                fincartKYcData.Relation3 = $scope.txtrelation3;
                                                                fincartKYcData.Nominee_Dob3 = $scope.txtdob3 ? $filter('date')($scope.txtdob3, "yyyy-MM-dd") : "";
                                                                fincartKYcData.Nom3_percent = $scope.txtshare3;

                                                                userService.callSignzyUpdateKycFormApi(fincartKYcData).then(function (serverResp) {
                                                                    if (serverResp.status) {
                                                                        if (serverResp.status.toUpperCase() === "SUCCESS") {

                                                                            $scope.btndisable = false;
                                                                            $scope.setPage(val);
                                                                        }
                                                                        else {
                                                                            SweetAlert.swal(serverResp.msg, "", "error");
                                                                            //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                                            $scope.btndisable = false;
                                                                        }
                                                                    }
                                                                    else {
                                                                        $scope.btndisable = false;
                                                                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                                                    }
                                                                });

                                                            }
                                                        }
                                                    });
                                                }
                                                else {
                                                    SweetAlert.swal("Please select wealth source!", "", "warning");
                                                }
                                            }
                                            else {
                                                SweetAlert.swal("Please select income!", "", "warning");
                                            }
                                        }
                                        else {
                                            SweetAlert.swal("Please select address type!", "", "warning");
                                        }
                                    }
                                    else {
                                        SweetAlert.swal("Please select occupation!", "", "warning");
                                    }
                                }
                                else {
                                    SweetAlert.swal("Please select marital status!", "", "warning");
                                }
                            }
                            else {
                                SweetAlert.swal("Please select gender!", "", "warning");
                            }
                        }
                        else {
                            SweetAlert.swal("Please enter mother name!", "", "warning");
                        }
                    }
                }

                $scope.uploadPhoto = function (ev) {
                    if (!$scope.btndisable) {

                        if ($scope.dataholder.srcPhoto && $scope.dataholder.srcPhoto != "../../../../assets/img/folder.png") {

                            $scope.btndisable = true;

                            var POIPOAFileUpload = POIPOAFileUploadModel.POIPOAFileUpload;
                            POIPOAFileUpload.basicid = $rootScope.prmBasicId;
                            POIPOAFileUpload.panfile1 = $scope.photofiles[0];

                            userService.callSignzyPhotoFileUploadApi(POIPOAFileUpload).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {

                                        var fincartPOISave = fincartPOISaveModel.FincartPOISave;
                                        fincartPOISave.basicid = $rootScope.prmBasicId;

                                        userService.callSignzyStartVedioApi(fincartPOISave).then(function (serverResp) {
                                            if (serverResp.status) {
                                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                                    var fincartStartVedioResponse = fincartStartVedioResponseModel.parseFincartStartVedioResponse(serverResp.data);
                                                    $scope.vedioNumber = fincartStartVedioResponse.number;
                                                    $scope.btndisable = false;
                                                    $scope.nxtScreen('VDO');
                                                }
                                                else {
                                                    SweetAlert.swal(serverResp.msg, "", "error");
                                                    //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                                    $scope.btndisable = false;
                                                }
                                            }
                                            else {
                                                $scope.btndisable = false;
                                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                            }
                                        });

                                    }
                                    else {
                                        SweetAlert.swal(serverResp.msg, "", "error");
                                        //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        $scope.btndisable = false;
                                    }
                                }
                                else {
                                    $scope.btndisable = false;
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });
                        }
                        else {
                            SweetAlert.swal("Please upload selfie!", "", "warning");
                        }
                    }
                }

                $scope.saveVedio = function (val) {

                    if (!$scope.btndisable) {
                        $scope.btndisable = true;

                        var POIPOAFileUpload = POIPOAFileUploadModel.POIPOAFileUpload;
                        POIPOAFileUpload.basicid = $rootScope.prmBasicId;
                        POIPOAFileUpload.panfile1 = $scope.vedioFileData;

                        userService.callSignzyVedioFileUploadApi(POIPOAFileUpload).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $scope.vedioFileData = null;
                                    document.querySelector('#save-to-disk').style.display = 'none';
                                    $scope.userdetails.isVideoDone = true;
                                    $scope.btndisable = false;
                                    $scope.setPage(val);
                                }
                                else {
                                    SweetAlert.swal(serverResp.msg, "", "error");
                                    //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                    $scope.btndisable = false;
                                }
                            }
                            else {
                                $scope.btndisable = false;
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });

                    }
                }

                $scope.updateBank = function (val) {
                    if (!$scope.btndisable) {
                        $scope.btndisable = true;
                        var banknm = $scope.bankOptions.filter(function (obj) { return obj.value.trim().toUpperCase() === $scope.bankvalue.trim().toUpperCase() })[0];
                        $scope.bankData.basicid = $rootScope.prmBasicId;
                        $scope.bankData.bankname = banknm.name;
                        userService.callSignzyBankVerificationApi($scope.bankData).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $scope.userdetails.isBankDone = true;
                                    $scope.btndisable = false;
                                    $scope.setPage(val);
                                }
                                else {
                                    SweetAlert.swal(serverResp.msg, "", "error");
                                    //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                    $scope.btndisable = false;
                                }
                            }
                            else {
                                $scope.btndisable = false;
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });
                    }
                }

                $scope.uploadSignature = function (ev, val) {
                    if (!$scope.btndisable) {

                        if ($scope.dataholder.srcSign && $scope.dataholder.srcSign != "../../../../assets/img/folder.png") {

                            $scope.btndisable = true;

                            var POIPOAFileUpload = POIPOAFileUploadModel.POIPOAFileUpload;
                            POIPOAFileUpload.basicid = $rootScope.prmBasicId;
                            POIPOAFileUpload.panfile1 = $scope.signfiles[0];

                            userService.callSignzySignUploadApi(POIPOAFileUpload).then(function (serverResp) {
                                if (serverResp.status) {
                                    if (serverResp.status.toUpperCase() === "SUCCESS") {
                                        $scope.userdetails.isSignDone = true;
                                        $scope.btndisable = false;
                                        $scope.userdetails.kycstatus = $scope.kycdata.KYC_STATUS == "Y" ? "success" : "inprocess";
                                        $scope.userdetails.cafstatus = "success";
                                        $rootScope.userCafStatus = "success";
                                        userFactory.setUserLoggedInDetails($scope.userdetails);
                                        $scope.setPage(val);
                                    }
                                    else {
                                        SweetAlert.swal(serverResp.msg, "", "error");
                                        //SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                        $scope.btndisable = false;
                                    }
                                }
                                else {
                                    $scope.btndisable = false;
                                    $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                                }
                            });

                        }
                        else {
                            SweetAlert.swal("Please upload signature!", "", "warning");
                        }
                    }
                }

                $scope.proceed = function () {
                    if ($scope.userdetails.isTaxSaving) {
                        $state.go('dashboard.taxSaving');
                    }
                    else if ($scope.userdetails.isQuicksip) {
                        $state.go('dashboard.quickSip');
                    }
                    else if ($scope.userdetails.isGoal) {
                        $state.go('dashboard.goalFullView');
                    }
                    else {
                        $state.go('dashboard.newgoal');
                    }
                }
                //Api Callings end

                //Vedio Start

                $scope.params = {};
                $scope.r = /([^&=]+)=?([^&]*)/g;

                $scope.d = function (s) {
                    return decodeURIComponent(s.replace(/\+/g, ' '));
                }

                $scope.match = $window.location.search;
                $scope.search = $scope.match;

                while ($scope.match = $scope.r.exec($scope.search.substring(1))) {
                    $scope.params[$scope.d($scope.match[1])] = $scope.d($scope.match[2]);

                    if ($scope.d($scope.match[2]) === 'true' || $scope.d($scope.match[2]) === 'false') {
                        $scope.params[$scope.d($scope.match[1])] = $scope.d($scope.match[2]) === 'true' ? true : false;
                    }
                }

                $window.params = $scope.params;

                $scope.addStreamStopListener = function (stream, callback) {
                    stream.addEventListener('ended', function () {
                        callback();
                        callback = function () { };
                    }, false);
                    stream.addEventListener('inactive', function () {
                        callback();
                        callback = function () { };
                    }, false);
                    stream.getTracks().forEach(function (track) {
                        track.addEventListener('ended', function () {
                            callback();
                            callback = function () { };
                        }, false);
                        track.addEventListener('inactive', function () {
                            callback();
                            callback = function () { };
                        }, false);
                    });
                }

                $scope.video = document.createElement('video');
                $scope.video.controls = false;
                $scope.mediaElement = getHTMLMediaElement($scope.video, {
                    title: ' ',
                    buttons: [/*'full-screen', 'take-snapshot'*/],
                    showOnMouseEnter: false,
                    width: 360
                });
                //angular.element('#recording-player').appendChild($scope.mediaElement);
                document.getElementById('recording-player').appendChild($scope.mediaElement);

                $scope.div = document.createElement('section');
                $scope.mediaElement.media.parentNode.appendChild($scope.div);
                $scope.mediaElement.media.muted = false;
                $scope.mediaElement.media.autoplay = true;
                $scope.mediaElement.media.playsinline = true;
                $scope.div.appendChild($scope.mediaElement.media);

                $scope.recordingPlayer = $scope.mediaElement.media;
                $scope.mimeType = 'video/mpeg\;codecs=h264';
                $scope.fileExtension = 'mp4';
                $scope.type = 'video';
                $scope.recorderType;
                $scope.defaultWidth;
                $scope.defaultHeight;
                $scope.timeSlice = false;
                $scope.MY_DOMAIN = 'fincart.com';
                $scope.videoBitsPerSecond;
                $scope.btnStartRecording = document.querySelector('#btn-start-recording');
                $scope.recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '<img src="../../../../assets/img/video-RecordingN.png" style="height: 9.2em;width: 19.5em;">';

                $scope.btnStartRecording.onclick = function (event) {
                    var button = $scope.btnStartRecording;

                    if (button.innerHTML === '<i class="material-icons">pause_circle_outline</i> Stop Recording') {
                        button.disabled = true;
                        button.disableStateWaiting = true;
                        setTimeout(function () {
                            button.disabled = false;
                            button.disableStateWaiting = false;
                            document.querySelector('#save-to-disk').style.display = 'block';
                        }, 2000);

                        button.innerHTML = '<i class="material-icons">play_arrow</i> Start Recording';

                        $scope.stopStream = function () {

                            if (button.stream && button.stream.stop) {
                                button.stream.stop();
                                button.stream = null;
                            }

                            if (button.stream instanceof Array) {
                                button.stream.forEach(function (stream) {
                                    stream.stop();
                                });
                                button.stream = null;
                            }

                            $scope.videoBitsPerSecond = null;
                            //$scope.recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '';
                            var html = 'Recording Stopped';
                            html += '<br>Size: ' + bytesToSize(button.recordRTC.getBlob().size);
                            $scope.recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = html;
                        }

                        if (button.recordRTC) {
                            if (button.recordRTC.length) {
                                button.recordRTC[0].stopRecording(function (url) {
                                    if (!button.recordRTC[1]) {
                                        button.recordingEndedCallback(url);
                                        $scope.stopStream();

                                        $scope.saveToDiskOrOpenNewTab(button.recordRTC[0]);
                                        return;
                                    }

                                    button.recordRTC[1].stopRecording(function (url) {
                                        button.recordingEndedCallback(url);
                                        $scope.stopStream();
                                    });
                                });
                            }
                            else {
                                button.recordRTC.stopRecording(function (url) {
                                    if (button.blobs && button.blobs.length) {
                                        var blob = new File(button.blobs, getFileName($scope.fileExtension), {
                                            type: $scope.mimeType
                                        });

                                        button.recordRTC.getBlob = function () {
                                            return blob;
                                        };

                                        url = URL.createObjectURL(blob);
                                    }

                                    button.recordingEndedCallback(url);
                                    $scope.saveToDiskOrOpenNewTab(button.recordRTC);
                                    $scope.stopStream();
                                });
                            }
                        }

                        return;
                    }

                    if (!event) return;

                    button.disabled = true;

                    var commonConfig = {
                        onMediaCaptured: function (stream) {
                            button.stream = stream;
                            if (button.mediaCapturedCallback) {
                                button.mediaCapturedCallback();
                            }

                            button.innerHTML = '<i class="material-icons">pause_circle_outline</i> Stop Recording';
                            button.disabled = false;
                            document.querySelector('#save-to-disk').style.display = 'none';
                        },
                        onMediaStopped: function () {
                            button.innerHTML = '<i class="material-icons">replay</i> Retake';
                            if (!button.disableStateWaiting) {
                                button.disabled = false;
                            }
                        },
                        onMediaCapturingFailed: function (error) {
                            console.error('onMediaCapturingFailed:', error);

                            if (error.toString().indexOf('no audio or video tracks available') !== -1) {
                                alert('RecordRTC failed to start because there are no audio or video tracks available.');
                            }

                            if (error.name === 'PermissionDeniedError' && DetectRTC.browser.name === 'Firefox') {
                                alert('Firefox requires version >= 52. Firefox also requires HTTPs.');
                            }

                            commonConfig.onMediaStopped();
                        }
                    };

                    $scope.captureAudioPlusVideo(commonConfig);

                    button.mediaCapturedCallback = function () {
                        if (typeof MediaRecorder === 'undefined') { // opera or chrome etc.
                            button.recordRTC = [];

                            if (!$scope.params.bufferSize) {
                                // it fixes audio issues whilst recording 720p
                                $scope.params.bufferSize = 16384;
                            }

                            var options = {
                                type: 'audio', // hard-code to set "audio"
                                leftChannel: $scope.params.leftChannel || false,
                                disableLogs: $scope.params.disableLogs || false,
                                video: recordingPlayer
                            };

                            if ($scope.params.sampleRate) {
                                options.sampleRate = parseInt($scope.params.sampleRate);
                            }

                            if ($scope.params.bufferSize) {
                                options.bufferSize = parseInt($scope.params.bufferSize);
                            }

                            if ($scope.params.frameInterval) {
                                options.frameInterval = parseInt($scope.params.frameInterval);
                            }

                            if ($scope.recorderType) {
                                options.recorderType = $scope.recorderType;
                            }

                            if ($scope.videoBitsPerSecond) {
                                options.videoBitsPerSecond = $scope.videoBitsPerSecond;
                            }

                            options.ignoreMutedMedia = false;
                            var audioRecorder = RecordRTC(button.stream, options);

                            options.type = type;
                            var videoRecorder = RecordRTC(button.stream, options);

                            // to sync audio/video playbacks in browser!
                            videoRecorder.initRecorder(function () {
                                audioRecorder.initRecorder(function () {
                                    audioRecorder.startRecording();
                                    videoRecorder.startRecording();
                                });
                            });

                            button.recordRTC.push(audioRecorder, videoRecorder);

                            button.recordingEndedCallback = function () {
                                var audio = new Audio();
                                audio.src = audioRecorder.toURL();
                                audio.controls = true;
                                audio.autoplay = true;

                                $scope.recordingPlayer.parentNode.appendChild(document.createElement('hr'));
                                $scope.recordingPlayer.parentNode.appendChild(audio);

                                if (audio.paused) audio.play();
                            };
                            return;
                        }

                        var options = {
                            type: $scope.type,
                            mimeType: $scope.mimeType,
                            disableLogs: $scope.params.disableLogs || false,
                            getNativeBlob: false, // enable it for longer recordings
                            video: $scope.recordingPlayer
                        };

                        if ($scope.recorderType) {
                            options.recorderType = $scope.recorderType;

                            if ($scope.recorderType == WhammyRecorder || $scope.recorderType == GifRecorder || $scope.recorderType == WebAssemblyRecorder) {
                                options.canvas = options.video = {
                                    width: defaultWidth || 320,
                                    height: defaultHeight || 240
                                };
                            }
                        }

                        if ($scope.videoBitsPerSecond) {
                            options.videoBitsPerSecond = $scope.videoBitsPerSecond;
                        }

                        if ($scope.timeSlice && typeof MediaRecorder !== 'undefined') {
                            options.timeSlice = $scope.timeSlice;
                            button.blobs = [];
                            options.ondataavailable = function (blob) {
                                button.blobs.push(blob);
                            };
                        }

                        options.ignoreMutedMedia = false;
                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function (url) {
                            $scope.setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        $scope.recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '<img src="https://www.webrtc-experiment.com/images/progress.gif" style="width:auto;"> Recording';
                    };
                };

                $scope.captureVideo = function (config) {
                    $scope.captureUserMedia({ video: true }, function (videoStream) {
                        config.onMediaCaptured(videoStream);

                        $scope.addStreamStopListener(videoStream, function () {
                            config.onMediaStopped();
                        });
                    }, function (error) {
                        config.onMediaCapturingFailed(error);
                    });
                }

                $scope.captureAudio = function (config) {
                    $scope.captureUserMedia({ audio: true }, function (audioStream) {
                        config.onMediaCaptured(audioStream);

                        $scope.addStreamStopListener(audioStream, function () {
                            config.onMediaStopped();
                        });
                    }, function (error) {
                        config.onMediaCapturingFailed(error);
                    });
                }

                $scope.captureAudioPlusVideo = function (config) {
                    $scope.captureUserMedia({ video: true, audio: true }, function (audioVideoStream) {
                        config.onMediaCaptured(audioVideoStream);

                        if (audioVideoStream instanceof Array) {
                            audioVideoStream.forEach(function (stream) {
                                $scope.addStreamStopListener(stream, function () {
                                    config.onMediaStopped();
                                });
                            });
                            return;
                        }

                        $scope.addStreamStopListener(audioVideoStream, function () {
                            config.onMediaStopped();
                        });
                    }, function (error) {
                        config.onMediaCapturingFailed(error);
                    });
                }

                $scope.isMyOwnDomain = function () {
                    return document.domain.indexOf($scope.MY_DOMAIN) !== -1;
                }

                $scope.isLocalHost = function () {
                    // "chrome.exe" --enable-usermedia-screen-capturing
                    // or firefox => about:config => "media.getusermedia.screensharing.allowed_domains" => add "localhost"
                    return document.domain === 'localhost' || document.domain === '127.0.0.1';
                }

                $scope.setVideoBitrates = function () {
                    $scope.videoBitsPerSecond = null;
                    return;
                }

                $scope.getFrameRates = function (mediaConstraints) {
                    if (!mediaConstraints.video) {
                        return mediaConstraints;
                    }

                    return mediaConstraints;
                }

                $scope.setGetFromLocalStorage = function (selectors) {
                    selectors.forEach(function (selector) {
                        var storageItem = selector.replace(/\.|#/g, '');
                        if (localStorage.getItem(storageItem)) {
                            document.querySelector(selector).value = localStorage.getItem(storageItem);
                        }

                        $scope.addEventListenerToUploadLocalStorageItem(selector, ['change', 'blur'], function () {
                            localStorage.setItem(storageItem, document.querySelector(selector).value);
                        });
                    });
                }

                $scope.addEventListenerToUploadLocalStorageItem = function (selector, arr, callback) {
                    arr.forEach(function (event) {
                        document.querySelector(selector).addEventListener(event, callback, false);
                    });
                }

                $scope.getVideoResolutions = function (mediaConstraints) {
                    if (!mediaConstraints.video) {
                        return mediaConstraints;
                    }
                    return mediaConstraints;
                }

                $scope.captureUserMedia = function (mediaConstraints, successCallback, errorCallback) {
                    if (mediaConstraints.video == true) {
                        mediaConstraints.video = {};
                    }

                    $scope.setVideoBitrates();

                    mediaConstraints = $scope.getVideoResolutions(mediaConstraints);
                    mediaConstraints = $scope.getFrameRates(mediaConstraints);

                    var isBlackBerry = !!(/BB10|BlackBerry/i.test(navigator.userAgent || ''));
                    if (isBlackBerry && !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia)) {
                        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
                        navigator.getUserMedia(mediaConstraints, successCallback, errorCallback);
                        return;
                    }

                    navigator.mediaDevices.getUserMedia(mediaConstraints).then(function (stream) {
                        successCallback(stream);

                        $scope.setVideoURL(stream, true);
                    }).catch(function (error) {
                        if (error && (error.name === 'ConstraintNotSatisfiedError' || error.name === 'OverconstrainedError')) {
                            alert('Your camera or browser does NOT supports selected resolutions or frame-rates. \n\nPlease select "default" resolutions.');
                        }
                        else if (error && error.message) {
                            alert(error.message);
                        }
                        else {
                            alert('Unable to make getUserMedia request. Please check browser console logs.');
                        }

                        errorCallback(error);
                    });
                }

                $scope.setMediaContainerFormat = function (arrayOfOptionsSupported) {
                    var options = Array.prototype.slice.call(
                        mediaContainerFormat.querySelectorAll('option')
                    );

                    var localStorageItem;

                    var selectedItem;
                    options.forEach(function (option) {
                        option.disabled = true;

                        if (arrayOfOptionsSupported.indexOf(option.value) !== -1) {
                            option.disabled = false;

                            if (localStorageItem && arrayOfOptionsSupported.indexOf(localStorageItem) != -1) {
                                if (option.value != localStorageItem) return;
                                option.selected = true;
                                selectedItem = option;
                                return;
                            }

                            if (!selectedItem) {
                                option.selected = true;
                                selectedItem = option;
                            }
                        }
                    });
                }

                $scope.isMimeTypeSupported = function (mimeType) {
                    if (typeof MediaRecorder === 'undefined') {
                        return false;
                    }

                    if (typeof MediaRecorder.isTypeSupported !== 'function') {
                        return true;
                    }

                    return MediaRecorder.isTypeSupported(mimeType);
                }

                if (typeof MediaRecorder === 'undefined' && (DetectRTC.browser.name === 'Edge' || DetectRTC.browser.name === 'Safari')) {
                    // webp isn't supported in Microsoft Edge
                    // neither MediaRecorder API
                    // so lets disable both video/screen recording options

                    console.warn('Neither MediaRecorder API nor webp is supported in ' + DetectRTC.browser.name + '. You cam merely record audio.');

                    $scope.setMediaContainerFormat(['pcm']);
                }

                $scope.stringify = function (obj) {
                    var result = '';
                    Object.keys(obj).forEach(function (key) {
                        if (typeof obj[key] === 'function') {
                            return;
                        }

                        if (result.length) {
                            result += ',';
                        }

                        result += key + ': ' + obj[key];
                    });

                    return result;
                }

                $scope.mediaRecorderToStringify = function (mediaRecorder) {
                    var result = '';
                    result += 'mimeType: ' + mediaRecorder.mimeType;
                    result += ', state: ' + mediaRecorder.state;
                    result += ', audioBitsPerSecond: ' + mediaRecorder.audioBitsPerSecond;
                    result += ', videoBitsPerSecond: ' + mediaRecorder.videoBitsPerSecond;
                    if (mediaRecorder.stream) {
                        result += ', streamid: ' + mediaRecorder.stream.id;
                        result += ', stream-active: ' + mediaRecorder.stream.active;
                    }
                    return result;
                }

                $scope.getFailureReport = function () {
                    var info = 'RecordRTC seems failed. \n\n' + $scope.stringify(DetectRTC.browser) + '\n\n' + DetectRTC.osName + ' ' + DetectRTC.osVersion + '\n';

                    if (typeof recorderType !== 'undefined' && recorderType) {
                        info += '\nrecorderType: ' + recorderType.name;
                    }

                    if (typeof mimeType !== 'undefined') {
                        info += '\nmimeType: ' + mimeType;
                    }

                    Array.prototype.slice.call(document.querySelectorAll('select')).forEach(function (select) {
                        info += '\n' + (select.id || select.className) + ': ' + select.value;
                    });

                    if ($scope.btnStartRecording.recordRTC) {
                        info += '\n\ninternal-recorder: ' + $scope.btnStartRecording.recordRTC.getInternalRecorder().name;

                        if ($scope.btnStartRecording.recordRTC.getInternalRecorder().getAllStates) {
                            info += '\n\nrecorder-states: ' + $scope.btnStartRecording.recordRTC.getInternalRecorder().getAllStates();
                        }
                    }

                    if ($scope.btnStartRecording.stream) {
                        info += '\n\naudio-tracks: ' + getTracks($scope.btnStartRecording.stream, 'audio').length;
                        info += '\nvideo-tracks: ' + getTracks($scope.btnStartRecording.stream, 'video').length;
                        info += '\nstream-active? ' + !!$scope.btnStartRecording.stream.active;

                        $scope.btnStartRecording.stream.getTracks().forEach(function (track) {
                            info += '\n' + track.kind + '-track-' + (track.label || track.id) + ': (enabled: ' + !!track.enabled + ', readyState: ' + track.readyState + ', muted: ' + !!track.muted + ')';

                            if (track.getConstraints && Object.keys(track.getConstraints()).length) {
                                info += '\n' + track.kind + '-track-getConstraints: ' + stringify(track.getConstraints());
                            }

                            if (track.getSettings && Object.keys(track.getSettings()).length) {
                                info += '\n' + track.kind + '-track-getSettings: ' + stringify(track.getSettings());
                            }
                        });
                    }

                    if ($scope.timeSlice && $scope.btnStartRecording.recordRTC) {
                        info += '\ntimeSlice: ' + $scope.timeSlice;

                        if ($scope.btnStartRecording.recordRTC.getInternalRecorder().getArrayOfBlobs) {
                            var blobSizes = [];
                            $scope.btnStartRecording.recordRTC.getInternalRecorder().getArrayOfBlobs().forEach(function (blob) {
                                blobSizes.push(blob.size);
                            });
                            info += '\nblobSizes: ' + blobSizes;
                        }
                    }

                    else if ($scope.btnStartRecording.recordRTC && $scope.btnStartRecording.recordRTC.getBlob()) {
                        info += '\n\nblobSize: ' + bytesToSize($scope.btnStartRecording.recordRTC.getBlob().size);
                    }

                    if ($scope.btnStartRecording.recordRTC && $scope.btnStartRecording.recordRTC.getInternalRecorder() && $scope.btnStartRecording.recordRTC.getInternalRecorder().getInternalRecorder && $scope.btnStartRecording.recordRTC.getInternalRecorder().getInternalRecorder()) {
                        info += '\n\ngetInternalRecorder: ' + $scope.mediaRecorderToStringify($scope.btnStartRecording.recordRTC.getInternalRecorder().getInternalRecorder());
                    }

                    return info;
                }

                $scope.saveToDiskOrOpenNewTab = function (recordRTC) {
                    if (!recordRTC.getBlob().size) {
                        var info = $scope.getFailureReport();
                        console.log('blob', recordRTC.getBlob());
                        console.log('recordrtc instance', recordRTC);
                        console.log('report', info);

                        if (mediaContainerFormat.value !== 'default') {
                            alert('RecordRTC seems failed recording using ' + mediaContainerFormat.value + '. Please choose "default" option from the drop down and record again.');
                        }
                        else {
                            alert('RecordRTC seems failed. Unexpected issue. You can read the email in your console log. \n\nPlease report using disqus chat below.');
                        }

                        if (mediaContainerFormat.value !== 'vp9' && DetectRTC.browser.name === 'Chrome') {
                            alert('Please record using VP9 encoder. (select from the dropdown)');
                        }
                    }

                    if (!recordRTC) return alert('No recording found.');

                    var fileName = $scope.getFileName($scope.fileExtension);
                    var file = new File([recordRTC.getBlob()], fileName, {
                        type: $scope.mimeType
                    });

                    $scope.vedioFileData = file;

                    //var fileName = $scope.getFileName($scope.fileExtension);

                    //document.querySelector('#save-to-disk').onclick = function () {

                    //    if (!recordRTC) return alert('No recording found.');

                    //    var file = new File([recordRTC.getBlob()], fileName, {
                    //        type: $scope.mimeType
                    //    });

                    //    //invokeSaveAsDialog(file, file.name);
                    //};



                }

                $scope.getRandomString = function () {
                    if ($window.crypto && $window.crypto.getRandomValues && navigator.userAgent.indexOf('Safari') === -1) {
                        var a = $window.crypto.getRandomValues(new Uint32Array(3)),
                            token = '';
                        for (var i = 0, l = a.length; i < l; i++) {
                            token += a[i].toString(36);
                        }
                        return token;
                    } else {
                        return (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                    }
                }

                $scope.getFileName = function (fileExtension) {
                    var d = new Date();
                    var year = d.getUTCFullYear();
                    var month = d.getUTCMonth();
                    var date = d.getUTCDate();
                    return 'vedio.' + fileExtension;
                    //return 'RecordRTC-' + year + month + date + '-' + $scope.getRandomString() + '.' + fileExtension;
                }

                $scope.getURL = function (arg) {
                    var url = arg;

                    if (arg instanceof Blob || arg instanceof File) {
                        url = URL.createObjectURL(arg);
                    }

                    if (arg instanceof RecordRTC || arg.getBlob) {
                        url = URL.createObjectURL(arg.getBlob());
                    }

                    if (arg instanceof MediaStream || arg.getTracks) {
                        // url = URL.createObjectURL(arg);
                    }

                    return url;
                }

                $scope.setVideoURL = function (arg, forceNonImage) {
                    var url = $scope.getURL(arg);

                    var parentNode = $scope.recordingPlayer.parentNode;
                    parentNode.removeChild($scope.recordingPlayer);
                    parentNode.innerHTML = '';

                    var elem = 'video';
                    if ($scope.type == 'gif' && !forceNonImage) {
                        elem = 'img';
                    }
                    if ($scope.type == 'audio') {
                        elem = 'audio';
                    }

                    $scope.recordingPlayer = document.createElement(elem);

                    if (arg instanceof MediaStream) {
                        $scope.recordingPlayer.muted = true;
                    }

                    $scope.recordingPlayer.addEventListener('loadedmetadata', function () {
                        if (navigator.userAgent.toLowerCase().indexOf('android') == -1) return;

                        // android
                        setTimeout(function () {
                            if (typeof $scope.recordingPlayer.play === 'function') {
                                $scope.recordingPlayer.play();
                            }
                        }, 2000);
                    }, false);

                    $scope.recordingPlayer.poster = '';

                    if (arg instanceof MediaStream) {
                        $scope.recordingPlayer.srcObject = arg;
                    }
                    else {
                        $scope.recordingPlayer.src = url;
                    }

                    if (typeof $scope.recordingPlayer.play === 'function') {
                        $scope.recordingPlayer.play();
                    }

                    $scope.recordingPlayer.addEventListener('ended', function () {
                        url = $scope.getURL(arg);

                        if (arg instanceof MediaStream) {
                            $scope.recordingPlayer.srcObject = arg;
                        }
                        else {
                            $scope.recordingPlayer.src = url;
                        }
                    });

                    parentNode.appendChild($scope.recordingPlayer);
                }

                //Vedio End
            }
            else {
                $state.go('dashboard');
            }
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));