﻿(function (finApp) {
    'use strict';
    finApp.controller('passbookController', ['$scope', '$state', '$rootScope', '$filter', 'SweetAlert', 'errorConstants', 'transactionConstants', 'oauthTokenFactory', 'userFactory', 'passbookfiltersModel', 'passbookParamModel', 'passbookItemsModel', 'userService', function ($scope, $state, $rootScope, $filter, SweetAlert, errorConstants, transactionConstants, oauthTokenFactory, userFactory, passbookfiltersModel, passbookParamModel, passbookItemsModel, userService) {

        if (oauthTokenFactory.getisloggedin()) {
            $rootScope.mainheader = "Passbook";
            $rootScope.mainheaderimg = "../../../../assets/img/Menu/Icon/Passbook.png";
            $rootScope.currActiveTab = $state.current.name.replace("dashboard.", "");
            $scope.basicidForFilters = userFactory.getUserLoggedInDetails().basicid;
            $scope.passbookFilters = transactionConstants.passbook_Filters;
            $scope.allFilters = [];
            $scope.passbookItems = [];
            $scope.passbookAllItems = [];

            $rootScope.showLoader();

            $scope.loadFilters = function () {
                userService.callPassbookFiltersApi($scope.basicidForFilters).then(function (serverResp) {
                    if (serverResp.status) {
                        if (serverResp.status.toUpperCase() === "SUCCESS") {
                            $scope.allFilters = passbookfiltersModel.parsePassbookfilters(serverResp.data);
                            $scope.ddlMember = $scope.allFilters.memberNames[0].Value;
                            $rootScope.hideLoader();
                        }
                        else {
                            $rootScope.hideLoader();
                            SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                        }
                    }
                    else {
                        $rootScope.hideLoader();
                        $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                    }
                });
            }

            $scope.loadFilters();

            $scope.search = function () {
                if ($scope.txtstartdate) {
                    if ($scope.txtenddate) {
                        $rootScope.showLoader();
                        var passbookParam = passbookParamModel.PassbookParam;
                        passbookParam.basicid = $scope.ddlMember;
                        passbookParam.trxntypeid = "";
                        passbookParam.schemeid = "";
                        passbookParam.goalcode = "";
                        passbookParam.trxnstatus = "";
                        passbookParam.openDate = $filter('date')($scope.txtstartdate, "yyyy-MM-dd");
                        passbookParam.closeDate = $filter('date')($scope.txtenddate, "yyyy-MM-dd");
                        passbookParam.foliono = "";

                        userService.callPassbookApi(passbookParam).then(function (serverResp) {
                            if (serverResp.status) {
                                if (serverResp.status.toUpperCase() === "SUCCESS") {
                                    $scope.passbookAllItems = passbookItemsModel.parsePassbookItems(serverResp.data);
                                    $scope.passbookItems = $scope.passbookAllItems.passbookItms;
                                    $rootScope.hideLoader();
                                }
                                else {
                                    $rootScope.hideLoader();
                                    SweetAlert.swal(errorConstants.errorcode[serverResp.errorCode], "", "error");
                                }
                            }
                            else {
                                $rootScope.hideLoader();
                                $rootScope.userlogout();//SweetAlert.swal(errorConstants.errorcode[111], "", "error");
                            }
                        });

                    }
                    else {
                        SweetAlert.swal("Please select to date!", "", "warning");
                    }
                }
                else {
                    SweetAlert.swal("Please select from date!", "", "warning");
                }
            }

            $scope.searchTextChange = function () {
                if ($scope.txtSearchData) {
                    $scope.passbookItems = $filter('filter')($scope.passbookAllItems.passbookItms, $scope.txtSearchData, '==', '$');
                    //switch ($scope.ddlFilterBy) {
                    //    case 'scheme_name':
                    //        //$scope.bankOptions.filter(function (obj) { return obj.name.trim().toUpperCase() === $scope.bankdata.BankName.trim().toUpperCase() })
                    //        debugger;
                    //        $scope.passbookItems = $filter('filter')($scope.passbookAllItems.passbookItms, $scope.txtSearchData, '==', '$');
                    //        debugger;
                    //        break;
                    //    default:
                    //        $scope.passbookItems = $scope.passbookAllItems.passbookItms;
                    //        break;
                    //}
                }
                else {
                    $scope.passbookItems = $scope.passbookAllItems.passbookItms;
                }
            }
        }
        else {
            $state.go('login', { type: 'login' });
        }
    }]);
})(angular.module('fincartApp'));