﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace app.fincart.com
{
    public partial class payment : System.Web.UI.Page
    {
        string app_base_url = "http://localhost:1478";
        string api_base_url = "http://localhost:25178/";
        string app_redirect_url = "#/dashboard/paymentstatus";
        string api_updateCartTransaction_url = "api/v2/payment/updateCartTransaction";


        public void writeLog(string ex)
        {
            TimeZoneInfo mountain = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime utc = DateTime.UtcNow;
            DateTime currdate = TimeZoneInfo.ConvertTimeFromUtc(utc, mountain);

            string filePath = Server.MapPath("~/logs/payment/payment_" + currdate.ToString("yyyy_MM_dd") + ".txt");

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("LOGTRACE ::" + ex + "<br/>" + Environment.NewLine + "Date :" + currdate.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string host = HttpContext.Current.Request.Url.Host;

            if (host.Trim().ToLower() == "dev-app.fincart.com" || host.Trim().ToLower() == "www.dev-app.fincart.com")
            {
                app_base_url = "https://dev-app.fincart.com/";
                api_base_url = "https://dev-api.fincart.com/";
            }
            else if (host.Trim().ToLower() == "qa-app.fincart.com" || host.Trim().ToLower() == "www.qa-app.fincart.com")
            {
                app_base_url = "https://qa-app.fincart.com/";
                api_base_url = "https://qa-api.fincart.com/";
            }
            else if (host.Trim().ToLower() == "app.fincart.com" || host.Trim().ToLower() == "www.app.fincart.com")
            {
                app_base_url = "https://app.fincart.com/";
                api_base_url = "https://api.fincart.com/";
            }

            if (!IsPostBack)
            {
                processpayment();
            }

        }

        void processpayment()
        {
            TimeZoneInfo mountain = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime utc = DateTime.UtcNow;
            DateTime currdate = TimeZoneInfo.ConvertTimeFromUtc(utc, mountain);

            var value = Request.Form["msg"];

            writeLog("LOG FILE OF DATED : " + currdate.ToString());


            if (value != null)
            {
                writeLog("BILLDESK RESPONSE STRING: " + value);

                if (Session["authToken"] != null)
                {

                    try
                    {
                        string[] msg = value.Split('|');

                        if (msg[14] == "0300")
                        {

                            string PGRefID = msg[2];
                            string BankRefN = msg[3];
                            string TxnDate = msg[13];
                            string TxnId = msg[1];

                            updateV2CartTransaction updateCartTransaction = new updateV2CartTransaction();
                            updateCartTransaction.txnId = TxnId;
                            updateCartTransaction.pgRefId = PGRefID;
                            updateCartTransaction.BankRefN = BankRefN;
                            updateCartTransaction.txnDateBillDsk = TxnDate;
                            updateCartTransaction.status = "1";
                            updateCartTransaction.billdeskRespStr = value;

                            HttpClient httpClient = new HttpClient();
                            httpClient.Timeout = TimeSpan.FromMinutes(30);
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToString(Session["authToken"]));
                            HttpResponseMessage response;
                            response = httpClient.PostAsJsonAsync(api_base_url + api_updateCartTransaction_url, updateCartTransaction).Result;
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            if (Convert.ToString(responseData) == "true")
                            {
                                Session["authToken"] = null;
                                Response.Redirect(app_base_url + app_redirect_url);
                                //hdurl.Value = APP_URL_Production + "dashboard/success/paymentstatus";
                            }
                            else
                            {
                                Response.Redirect(app_base_url + app_redirect_url);
                                //hdurl.Value = APP_URL_Production + "dashboard/failed/paymentstatus";
                            }
                        }
                        else if (msg[14] != "0300")
                        {
                            string PGRefID = msg[2];
                            string BankRefN = msg[3];
                            string TxnDate = msg[13];
                            string TxnId = msg[1];

                            updateV2CartTransaction updateCartTransaction = new updateV2CartTransaction();
                            updateCartTransaction.txnId = TxnId;
                            updateCartTransaction.pgRefId = PGRefID;
                            updateCartTransaction.BankRefN = BankRefN;
                            updateCartTransaction.txnDateBillDsk = TxnDate;
                            updateCartTransaction.status = "-1";
                            updateCartTransaction.billdeskRespStr = value;

                            HttpClient httpClient = new HttpClient();
                            httpClient.Timeout = TimeSpan.FromMinutes(30);
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToString(Session["authToken"]));
                            HttpResponseMessage response;
                            response = httpClient.PostAsJsonAsync(api_base_url + api_updateCartTransaction_url, updateCartTransaction).Result;
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            if (Convert.ToString(responseData) == "true")
                            {
                                Session["authToken"] = null;
                                Response.Redirect(app_base_url + app_redirect_url);
                                //hdurl.Value = APP_URL_Production + "dashboard/failed/paymentstatus";
                            }

                        }
                        else
                        {
                            Response.Redirect(app_base_url + app_redirect_url);
                            //hdurl.Value = APP_URL_Production + "dashboard/failed/paymentstatus";
                        }

                    }
                    catch (TaskCanceledException ex)
                    {
                        if (ex.CancellationToken.IsCancellationRequested)
                        {
                            writeLog("Error: " + ex.Message + "Stack Trace : " + ex.StackTrace + "Reason : " + "HttpClient Timeout:" + ex.CancellationToken.IsCancellationRequested);
                        }
                        else { writeLog("Error: " + ex.Message + "Stack Trace : " + ex.StackTrace); }
                        // If false, it's pretty safe to assume it was a timeout.
                    }
                    catch (Exception ex)
                    {
                        writeLog("Error: " + ex.Message + "Stack Trace : " + ex.StackTrace);
                    }


                }
                else
                {
                    writeLog("Error: Session Expired");
                    Response.Redirect(app_base_url + app_redirect_url);
                }

            }
            else
            {
                writeLog("Error: value null returned from billdesk.");
                Response.Redirect(app_base_url + app_redirect_url);
            }

        }
    }

    /// <summary>
    /// Classes for // UPDATE STATUS PURCHASE TRANSACTION MODEL FOR OUR DATABASE-----
    /// </summary>
    /// 
    public class updateV2CartTransaction
    {
        public string txnId { get; set; }
        public string pgRefId { get; set; }
        public string BankRefN { get; set; }
        public string txnDateBillDsk { get; set; }
        public string status { get; set; }
        public string billdeskRespStr { get; set; }


    }
}