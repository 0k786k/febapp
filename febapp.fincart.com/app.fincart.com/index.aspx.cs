﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace app.fincart.com
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Services.WebMethod]
        public static ServerResponse SetAuthToken(string token)
        {
            ServerResponse finalResp = new ServerResponse();
            try
            {
                HttpContext.Current.Session["authToken"] = token;
                finalResp.status = "Success";
                finalResp.msg = "";
                finalResp.errorCode = "";
                finalResp.data = "";
            }
            catch (Exception ex)
            {
                finalResp.status = "Error";
                finalResp.msg = ex.InnerException.ToString();
                finalResp.errorCode = "300";
                finalResp.data = "";
            }

            return finalResp;
        }
    }

    public class ServerResponse
    {
        public string status { get; set; }
        public string errorCode { get; set; }
        public string msg { get; set; }
        public object data { get; set; }
    }
}