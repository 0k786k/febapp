﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="payment.aspx.cs" Inherits="app.fincart.com.payment" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function process(timeinterval) {
            setTimeout(function () {
                var Url = document.getElementById('<%= hdurl.ClientID %>');
                window.location.href = Url.value;
            }, timeinterval);
        }
    </script>
</head>
<body onload="process(5000)">
    <form id="form1" runat="server">
        <div style="width: 30%; margin: 30px auto; text-align: center; font-size: 16px">
            please wait..While Processing Your Payment.<br />
            <img src="/assets/img/finloader.gif" />
        </div>
        <input type="hidden" name="hdurl" id="hdurl" runat="server" />
    </form>
</body>
</html>
