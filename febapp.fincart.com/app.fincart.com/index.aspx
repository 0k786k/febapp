﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="app.fincart.com.index" %>

<!DOCTYPE html>

<html ng-app="fincartApp">
<head runat="server">

    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Financial Planning Advisors in Delhi, CFP | Certified Financial Planner, Best Online Robo Advisors in India, Investment Consultant</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

    <%--Vedio Start--%>
    <!-- web streams API polyfill to support Firefox -->
    <script src="https://unpkg.com/@mattiasbuelens/web-streams-polyfill/dist/polyfill.min.js"></script>

    <!-- ../libs/DBML.js to fix video seeking issues -->
    <script src="https://www.webrtc-experiment.com/EBML.js"></script>

    <!-- for Edge/FF/Chrome/Opera/etc. getUserMedia support -->
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
    <script src="https://www.webrtc-experiment.com/DetectRTC.js"> </script>

    <!-- video element -->
    <link href="https://www.webrtc-experiment.com/getHTMLMediaElement.css" rel="stylesheet">
    <script src="https://www.webrtc-experiment.com/getHTMLMediaElement.js"></script>

    <%--Vedio End--%>

    <!-- Angular Material style sheet -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.10/angular-material.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.6/typed.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/noframework.waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.1.5/masonry.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.11/ngStorage.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.1/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-datepicker/2.1.23/angular-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-idle/1.3.2/angular-idle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/offline-js/0.7.19/offline.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-device-detector/4.0.3/ng-device-detector.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/re-tree/0.0.2/re-tree.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-slider/2.8.0/rzslider.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/12.2.13/ng-file-upload-shim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-mask/1.8.7/mask.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>

    <!-- Angular Library -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>


    <!-- Angular Material Library -->
    <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.10/angular-material.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sweetalert/1.1.2/SweetAlert.min.js"></script>

    <%--Angular Moment--%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-moment/1.3.0/angular-moment.min.js"></script>

    <!-- inject:css -->
    <link href="assets/css/offline-themes/offline-theme-chrome.css" rel="stylesheet" />
    <link href="assets/css/offline-themes/offline-language-english.css" rel="stylesheet" />
    <link href="assets/css/nv.d3.css" rel="stylesheet" />
    <link href="assets/css/slick-theme.min.css" rel="stylesheet" />
    <link href="assets/css/slick.min.css" rel="stylesheet" />
    <link href="assets/css/material-dashboard.css" rel="stylesheet" />
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="assets/css/goal.css" rel="stylesheet" />

    <link href="assets/css/single-page-goals.css" rel="stylesheet" />

    <!-- endinject -->

    <!-- inject:js -->
    <%--Third Party JS--%>
    <script src="assets/js/nvd3/d3.js"></script>
    <script src="assets/js/nvd3/nv.d3.js"></script>
    <script src="assets/js/nvd3/angular-nvd3.js"></script>
    <script src="assets/js/material-dashboard.js"></script>
    <script src="assets/js/angular-animate.js"></script>
    <script src="assets/js/ng-file-upload.js"></script>
    <script src="assets/js/ng-file-upload-all.js"></script>
    <script src="assets/js/RecordRTC.js"></script>

    <%--Environment JS--%>
    <script src="app/environment/environmentModule.app.js"></script>
    <script src="app/environment/environmentProvider.js"></script>
    <script src="app/environment/environment.js"></script>

    <%--Root JS--%>
    <script src="app/finApp.js"></script>
    <script src="app/finroutes.js"></script>

    <%--Shared Config JS--%>
    <script src="app/shared/config/appConfig.js"></script>

    <%--Shared Constant JS--%>
    <script src="app/shared/constants/trackEventsConstants.js"></script>
    <script src="app/shared/constants/trackDeviceConstants.js"></script>
    <script src="app/shared/constants/oauthConstants.js"></script>
    <script src="app/shared/constants/errorConstants.js"></script>
    <script src="app/shared/constants/transactionConstants.js"></script>
    <script src="app/shared/constants/kycCafConstants.js"></script>
    <script src="app/shared/constants/goalConstants.js"></script>
    <script src="app/shared/constants/dashboardConstants.js"></script>

    <%--Shared Controls JS--%>
    <script src="app/shared/controls/RangeSlider/rangeslider.js"></script>
    <script src="app/shared/controls/Enter/enter.js"></script>
    <script src="app/shared/controls/Focus/focus.js"></script>
    <script src="app/shared/controls/Blink/Blink.js"></script>
    <script src="app/shared/controls/Number/number.js"></script>
    <script src="app/shared/controls/ngRepeatDoneEvt/ngRepeatDoneEvt.js"></script>
    <script src="app/shared/controls/ngActivityIndicator/ngActivityIndicator.js"></script>
    <script src="app/shared/controls/EasyDotLoader/easydotloader.js"></script>
    <script src="app/shared/controls/AutoComplete/angucomplete-alt.js"></script>
    <script src="app/shared/controls/Validator/validation.js"></script>
    <script src="app/shared/controls/CompileTemplate/compiletemplate.js"></script>
    <script src="app/shared/controls/Percentage/percentage.js"></script>
    <script src="app/shared/controls/PasswordValidator/zxcvbn.js"></script>
    <script src="app/shared/controls/PasswordValidator/passwordValidator.js"></script>
    <script src="app/shared/controls/MouseWheel/mousewheelevent.js"></script>
    <script src="app/shared/controls/KeyPressEvents/keypressevents.js"></script>
    <script src="app/shared/controls/WayPoint/waypoint.js"></script>
    <script src="app/shared/controls/ImageCropper/imageCrop.js"></script>
    <script src="app/shared/controls/Distinct/distinct.js"></script>
    <script src="app/shared/controls/HtmlToPdf/htmlToPdf.js"></script>
    <script src="app/shared/controls/AutoSubmitter/autoSubmitter.js"></script>
    <script src="app/shared/controls/slickCarousel/slickCarousel.js"></script>
    <script src="app/shared/controls/FileUpload/ngUploadChange.js"></script>

    <%--Shared Model JS--%>
    <script src="app/shared/model/userTokenModel.js"></script>
    <script src="app/shared/model/serverResponseModel.js"></script>
    <script src="app/shared/model/registerReqModel.js"></script>
    <script src="app/shared/model/loggedInDetailsModel.js"></script>
    <script src="app/shared/model/investFullSummaryModel.js"></script>
    <script src="app/shared/model/investFullSummaryListModel.js"></script>
    <script src="app/shared/model/investShortSummaryModel.js"></script>
    <script src="app/shared/model/fundModel.js"></script>
    <script src="app/shared/model/objectiveModel.js"></script>
    <script src="app/shared/model/subobjectiveModel.js"></script>
    <script src="app/shared/model/fundsWithCategoryModel.js"></script>
    <script src="app/shared/model/searchSchemesModel.js"></script>
    <script src="app/shared/model/searchSchemesResultModel.js"></script>
    <script src="app/shared/model/searchSchemesResultListModel.js"></script>
    <script src="app/shared/model/recommendedSchemeModel.js"></script>
    <script src="app/shared/model/memberModel.js"></script>
    <script src="app/shared/model/profileModel.js"></script>
    <script src="app/shared/model/folioModel.js"></script>
    <script src="app/shared/model/mandateModel.js"></script>
    <script src="app/shared/model/bankdetailsModel.js"></script>
    <script src="app/shared/model/allMemberListModel.js"></script>
    <script src="app/shared/model/validDateModel.js"></script>
    <script src="app/shared/model/validDateListModel.js"></script>
    <script src="app/shared/model/addToCartModel.js"></script>
    <script src="app/shared/model/otherTransactionV2Model.js"></script>
    <script src="app/shared/model/userCartDetailsModel.js"></script>
    <script src="app/shared/model/userCartDetailsListModel.js"></script>
    <script src="app/shared/model/billdeskPaymentModeModel.js"></script>
    <script src="app/shared/model/billdeskPaymentModeListModel.js"></script>
    <script src="app/shared/model/cartPurchaseAttemptModel.js"></script>
    <script src="app/shared/model/txnLogDetailsModel.js"></script>
    <script src="app/shared/model/billdskPaymentLogModel.js"></script>
    <script src="app/shared/model/userAllGoalsModel.js"></script>
    <script src="app/shared/model/userAllGoalsListModel.js"></script>
    <script src="app/shared/model/allInvestmentsModel.js"></script>
    <script src="app/shared/model/allInvestmentsListModel.js"></script>
    <script src="app/shared/model/kycModel.js"></script>
    <script src="app/shared/model/fincartCaptchaRequestModel.js"></script>
    <script src="app/shared/model/POIPOAFileUploadModel.js"></script>
    <script src="app/shared/model/fincartPOIResponseModel.js"></script>
    <script src="app/shared/model/fincartDeleteStepModel.js"></script>
    <script src="app/shared/model/fincartPOISaveModel.js"></script>
    <script src="app/shared/model/fincartKYcDataModel.js"></script>
    <script src="app/shared/model/fincartStartVedioResponseModel.js"></script>
    <script src="app/shared/model/getBankInfoResultModel.js"></script>
    <script src="app/shared/model/fincartBankDetailsSaveModel.js"></script>
    <script src="app/shared/model/cafViewProfileModel.js"></script>
    <script src="app/shared/model/cafViewProfileResultModel.js"></script>
    <script src="app/shared/model/finCalcModel.js"></script>
    <script src="app/shared/model/goalresultModel.js"></script>
    <script src="app/shared/model/addGoalModel.js"></script>
    <script src="app/shared/model/sysmSipShortReminderModel.js"></script>
    <script src="app/shared/model/sysmTrxnlistModel.js"></script>
    <script src="app/shared/model/userCartCountModel.js"></script>
    <script src="app/shared/model/birthdayAlertModel.js"></script>
    <script src="app/shared/model/eventAlertModel.js"></script>
    <script src="app/shared/model/dashboardAlertsModel.js"></script>
    <script src="app/shared/model/systematicRemindersModel.js"></script>
    <script src="app/shared/model/systematicRemindersListModel.js"></script>
    <script src="app/shared/model/pincodeDetailsModel.js"></script>
    <script src="app/shared/model/addressChangeRequestModel.js"></script>
    <script src="app/shared/model/bankChangeRequestModel.js"></script>
    <script src="app/shared/model/cafeditRequestsModel.js"></script>
    <script src="app/shared/model/cafeditRequestsResultModel.js"></script>
    <script src="app/shared/model/userCAFNomineeModel.js"></script>
    <script src="app/shared/model/userCAFFatcaTaxModel.js"></script>
    <script src="app/shared/model/userCAFDataModel.js"></script>
    <script src="app/shared/model/investmentAccountsModel.js"></script>
    <script src="app/shared/model/investmentAccountsListModel.js"></script>
    <script src="app/shared/model/investProfileModel.js"></script>
    <script src="app/shared/model/passbookfiltersModel.js"></script>
    <script src="app/shared/model/passbookParamModel.js"></script>
    <script src="app/shared/model/passbookModel.js"></script>
    <script src="app/shared/model/passbookItemsModel.js"></script>
    <script src="app/shared/model/signzyUserProfileStageModel.js"></script>
    <script src="app/shared/model/mandateDetailsModel.js"></script>
    <script src="app/shared/model/mandateDetailsListModel.js"></script>
    <script src="app/shared/model/bestSaverSchemeDetailsModel.js"></script>

    <%--Shared Factory JS--%>
    <script src="app/shared/factory/oauthTokenFactory.js"></script>
    <script src="app/shared/factory/transactionFactory.js"></script>
    <script src="app/shared/factory/userFactory.js"></script>
    <script src="app/shared/factory/usergoalFactory.js"></script>
    <script src="app/shared/factory/userprofileFactory.js"></script>

    <%--Shared Services JS--%>
    <script src="app/shared/services/calculatorService.js"></script>
    <script src="app/shared/services/financeService.js"></script>
    <script src="app/shared/services/oauthService.js"></script>
    <script src="app/shared/services/userService.js"></script>
    <script src="app/shared/services/apiInterceptorService.js"></script>

    <%--Components Controller JS--%>
    <script src="app/components/goal/goalController.js"></script>
    <script src="app/components/login/loginController.js"></script>
    <script src="app/components/dashboard/masterController.js"></script>
    <script src="app/components/dashboard/overview/overviewController.js"></script>
    <script src="app/components/dashboard/searchScheme/searchSchemeController.js"></script>
    <script src="app/components/dashboard/fullSummary/fullSummaryController.js"></script>
    <script src="app/components/dashboard/allocationStatistics/allocationStatisticsController.js"></script>
    <script src="app/components/dashboard/systematicReminder/systematicReminderController.js"></script>
    <script src="app/components/dashboard/passbook/passbookController.js"></script>
    <script src="app/components/dashboard/goalFullView/goalFullViewController.js"></script>
    <script src="app/components/dashboard/kyccaf/kyccafViewController.js"></script>
    <script src="app/components/dashboard/kyccaf/kyccafAddController.js"></script>
    <script src="app/components/dashboard/kyccaf/kyccafEditController.js"></script>
    <script src="app/components/dashboard/recommendedScheme/recommendedSchemeController.js"></script>
    <script src="app/components/dashboard/cart/cartController.js"></script>
    <script src="app/components/dashboard/paymentstatus/paymentstatusController.js"></script>
    <script src="app/components/dashboard/allinvestments/allinvestmentsController.js"></script>
    <script src="app/components/dashboard/goalinvestments/goalinvestmentsController.js"></script>
    <script src="app/components/dashboard/setgoal/newgoalController.js"></script>
    <script src="app/components/dashboard/setgoal/setgoalController.js"></script>
    <script src="app/components/dashboard/financialplanning/financialplanningController.js"></script>
    <script src="app/components/dashboard/primeservices/primeservicesController.js"></script>
    <script src="app/components/dashboard/investmentAccount/investmentAccountController.js"></script>
    <script src="app/components/dashboard/quickSip/quickSipController.js"></script>
    <script src="app/components/dashboard/mandate/mandateController.js"></script>
    <script src="app/components/dashboard/savingAccount/savingAccountController.js"></script>
    <script src="app/components/dashboard/taxSaving/taxSavingController.js"></script>
    <!-- endinject -->

</head>

<body>
    <div ui-view="mainpageview"></div>
</body>


</html>
